.decl VarEqlsConst(var: Var, num:symbol) output

.decl ParamEqlsConst(param: Var, num: symbol)  

.decl VarFromArrLoad(var_load: Var, var_store: Var) output

.decl ArrayIsLoaded(var_load : Var, arr: symbol, index: symbol, alloc: symbol) output

.decl AliasToLoadedArray(alloc: symbol, alias_arr: symbol) output

.decl MatchStoreValue(var_store : Var, alloc: symbol, index: symbol) output

.decl ArrayLoadAlloc(alloc: symbol, arr:symbol) output 

// Direct assign constant
// int a = 1;
// -> a, 1
VarEqlsConst(var, num) :-
ApplicationMethod(meth),
_AssignNumConstant(_, _, num, var, meth).

// VarEqlsConst(var, to_string(num)) :-
// ApplicationMethod(meth),
// _AssignBinop(ins, _, var, meth),
// _AssignOperFrom(ins, 1, var_left),
// VarEqlsConst(var_left, left),
// _AssignOperFrom(ins, 2, var_right),
// VarEqlsConst(var_right, right),
// _OperatorAt(ins, opt),
// (
// 	opt = "+", 
// 	num = to_number(left) + to_number(right);
// 	opt = "-", 
// 	num = to_number(left) - to_number(right);
// 	opt = "*", 
// 	num = to_number(left) * to_number(right);
// 	opt = "/", 
// 	num = to_number(left) / to_number(right)
// ).

// Transfer constant value
// int a = 1;
// b = a;
// -> b, 1
VarEqlsConst(to, num) :-
ApplicationMethod(meth),
_AssignLocal(_, _, from, to, meth),
VarEqlsConst(from, num).

// Assign constant paramters in function to local variable
// id(1, 2)
// public void id(a, b)
ParamEqlsConst(to, num) :-
ApplicationMethod(meth),
CallGraphEdge(_,ins, _, meth),
_FormalParam(index, meth, to),
_ActualParam(index, ins, from),
VarEqlsConst(from, num).

VarEqlsConst(to, num) :-
_AssignLocal(_, _, from, to, _),
ParamEqlsConst(from, num).

// Assign return constant to local variable
// int id(){return 1;}
// int a = id();
VarEqlsConst(to, num) :-
ApplicationMethod(meth),
CallGraphEdge(_, ins, _, meth),
_Return(_, _, from, meth),
VarEqlsConst(from, num),
_AssignReturnValue(ins, to).

// Load constant instance field
VarEqlsConst(to, num) :-
ApplicationMethod(meth),
_LoadInstanceField(_, _, to, base, fld, meth),
_StoreInstanceField(_, _, from, base, fld, _),
VarEqlsConst(from, num).


/****
 * Array element sensitivity
 ****/

// Store array with load instruction
// a = arr[0];
ArrayLoadAlloc(alloc, arr) :-
ApplicationMethod(method),
_LoadArrayIndex(_, _, _, arr , method),
VarPointsTo(_, alloc, _, arr).

// Store loaded array, index and variable load to
// -> a, arr, 0, heap
ArrayIsLoaded(var_load, arr, index, alloc) :-
ApplicationMethod(method),
_LoadArrayIndex(ins_load, _, var_load, arr , method),
_ArrayInsnIndex(ins_load, varIndex_load),
VarEqlsConst(varIndex_load, index),
ArrayLoadAlloc(alloc, arr).

// Store alias to loaded array (self included)
// arr = arr2;
// -> heap, arr2
// -> heap, arr
AliasToLoadedArray(alloc, alias_arr) :-
ArrayIsLoaded(_, loaded_arr, _, alloc),
VarPointsTo(_, alloc, _, alias_arr).

// Store value of loaded array elements
// b = 0;
// arr2[0] = b;
// -> b, heap, 0
MatchStoreValue(var_store, alloc, index) :-
ApplicationMethod(method),
_StoreArrayIndex(ins_store, _, var_store, alias_arr, method),
_ArrayInsnIndex(ins_store, varIndex_store), 
VarEqlsConst(varIndex_store, index),
AliasToLoadedArray(alloc, alias_arr).

// Matach load-to variable and stored value
// -> a, b
VarFromArrLoad(var_load, var_store) :-
ArrayIsLoaded(var_load, loaded_arr, index, alloc),
MatchStoreValue(var_store, alloc, index).

// If array stores constant
// -> a, num
VarEqlsConst(var_load, num) :-
VarFromArrLoad(var_load, var_store),
VarEqlsConst(var_store, num).

// If array stores object
// -> hctx, obj, ctx, a 
VarPointsTo(hctx, value, ctx, var_load) :-
VarFromArrLoad(var_load, var_store),
VarPointsTo(_, value, _, var_store),
isImmutableContext(ctx),
isImmutableHContext(hctx).


