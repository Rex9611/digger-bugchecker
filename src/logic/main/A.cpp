
#include "souffle/CompiledSouffle.h"

extern "C" {
}

namespace souffle {
using namespace ram;
struct t_btree_2__1_0__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__1_0__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_3_1_2__1__9 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,3,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_3_1_2__1__9& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t) const {
context h;
return equalRange_9(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,3,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__2_0_1_3__1__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__2_0_1_3__1__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0__1 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,5,0,1,3,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t) const {
context h;
return equalRange_36(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,5,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,4,0,1,3>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,2,4,1,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t) const {
context h;
return equalRange_20(t, h);
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t) const {
context h;
return equalRange_21(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,4,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,4,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_1_3__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_1_3__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,0,1,3,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_3_0_1_4__4__12 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_3_0_1_4__4__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,3,0,1,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2_1_0__2__3__4__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2_1_0__2__3__4__6& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_3_1__4__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_3_1__4__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_3_0_1__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_3_0_1__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_3_1__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_3_1__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1_0__1__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1_0__1__2& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__2_0_1__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__2_0_1__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2_0_1__2__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2_0_1__2__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_1_3__2_3_0_1__5__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_1_3__2_3_0_1__5__12& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_0_2_3__3_0_1_2__2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_0_2_3__3_0_1_2__2__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,0,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__6& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_3_0_2__10 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,3,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_3_0_2__10& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t) const {
context h;
return equalRange_10(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,3,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__2__6__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__2__6__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_1_2_3_4_5_6 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,1,2,3,4,5,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,1,2,3,4,5,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__3_0_1_2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__3_0_1_2__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_2_5_1_3_4_6__37 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,2,5,1,3,4,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<iterator_0> equalRange_37(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[6] = MIN_RAM_DOMAIN;
high[6] = MAX_RAM_DOMAIN;
return range<iterator_0>(ind_0.lower_bound(&low, h.hints_0), ind_0.upper_bound(&high, h.hints_0));
}
range<iterator_0> equalRange_37(const t_tuple& t) const {
context h; return equalRange_37(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,2,5,1,3,4,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_3_4_0_1__28 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,4,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_3_4_0_1__28& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_28(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_28(const t_tuple& t) const {
context h;
return equalRange_28(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,3,4,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3__7__15 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3__7__15& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t) const {
context h;
return equalRange_15(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};

class Sf_A : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(boolean)_",
	R"_(short)_",
	R"_(int)_",
	R"_(long)_",
	R"_(float)_",
	R"_(double)_",
	R"_(char)_",
	R"_(byte)_",
	R"_(<<null pseudo heap>>)_",
	R"_(Throw NullPointerException)_",
	R"_(throw NPE)_",
	R"_(java.lang.NullPointerException)_",
	R"_(Load Array Index)_",
	R"_(Store Array Index)_",
	R"_(Store Instance Field)_",
	R"_(Load Instance Field)_",
	R"_(Virtual Method Invocation)_",
	R"_(Special Method Invocation)_",
	R"_(Unary Operator)_",
	R"_(Binary Operator)_",
	R"_(Throw Null)_",
	R"_(throw null)_",
	R"_(Enter Monitor (Synchronized))_",
	R"_(null)_",
	R"_(!=)_",
	R"_(==)_",
	R"_(<=)_",
	R"_(>=)_",
	R"_(<)_",
	R"_(>)_",
	R"_(/var_declaration/)_",
	R"_(/map_param/)_",
	R"_(declaration)_",
	R"_(assignNull)_",
	R"_(Alias)_",
	R"_(Transfer)_",
	R"_(Return)_",
	R"_(Parameter)_",
	R"_(0)_",
	R"_(1)_",
	R"_(Boolean If)_",
	R"_(boolean hasNext())_",
	R"_(next())_",
	R"_(JDK Function Summary)_",
	R"_(<java.lang.System: java.io.PrintStream out>)_",
	R"_(<java.lang.System: java.io.PrintStream err>)_",
};// -- Table: _AssignReturnValue
std::unique_ptr<t_btree_2__1_0__2> rel_1_AssignReturnValue = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<0,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_1_AssignReturnValue;
// -- Table: _SpecialMethodInvocation
std::unique_ptr<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17> rel_2_SpecialMethodInvocation = std::make_unique<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17>();
souffle::RelationWrapper<1,t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_2_SpecialMethodInvocation;
// -- Table: _StaticMethodInvocation
std::unique_ptr<t_btree_4__0_3_1_2__1__9> rel_3_StaticMethodInvocation = std::make_unique<t_btree_4__0_3_1_2__1__9>();
souffle::RelationWrapper<2,t_btree_4__0_3_1_2__1__9,Tuple<RamDomain,4>,4,true,false> wrapper_rel_3_StaticMethodInvocation;
// -- Table: _VirtualMethodInvocation
std::unique_ptr<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17> rel_4_VirtualMethodInvocation = std::make_unique<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17>();
souffle::RelationWrapper<3,t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_4_VirtualMethodInvocation;
// -- Table: AssignReturnValue_WithInvoke
std::unique_ptr<t_btree_4__0_1_2_3__2_0_1_3__1__4> rel_5_AssignReturnValue_WithInvoke = std::make_unique<t_btree_4__0_1_2_3__2_0_1_3__1__4>();
souffle::RelationWrapper<4,t_btree_4__0_1_2_3__2_0_1_3__1__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_5_AssignReturnValue_WithInvoke;
// -- Table: IterNextInsn
std::unique_ptr<t_btree_3__0_2_1__5> rel_6_IterNextInsn = std::make_unique<t_btree_3__0_2_1__5>();
souffle::RelationWrapper<5,t_btree_3__0_2_1__5,Tuple<RamDomain,3>,3,false,true> wrapper_rel_6_IterNextInsn;
// -- Table: Primitive
std::unique_ptr<t_btree_1__0__1> rel_7_Primitive = std::make_unique<t_btree_1__0__1>();
// -- Table: _Var_Type
std::unique_ptr<t_btree_2__0_1__3> rel_8_Var_Type = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<6,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_8_Var_Type;
// -- Table: RefTypeVar
std::unique_ptr<t_btree_1__0> rel_9_RefTypeVar = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<7,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_9_RefTypeVar;
// -- Table: _AssignCast
std::unique_ptr<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36> rel_10_AssignCast = std::make_unique<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36>();
souffle::RelationWrapper<8,t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36,Tuple<RamDomain,6>,6,true,false> wrapper_rel_10_AssignCast;
// -- Table: _AssignCastNull
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_11_AssignCastNull = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<9,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_11_AssignCastNull;
// -- Table: _AssignHeapAllocation
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_12_AssignHeapAllocation = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<10,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_12_AssignHeapAllocation;
// -- Table: _AssignLocal
std::unique_ptr<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21> rel_13_AssignLocal = std::make_unique<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21>();
souffle::RelationWrapper<11,t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21,Tuple<RamDomain,5>,5,true,false> wrapper_rel_13_AssignLocal;
// -- Table: DefineVar
std::unique_ptr<t_btree_3__0_1_2__7> rel_14_DefineVar = std::make_unique<t_btree_3__0_1_2__7>();
souffle::RelationWrapper<12,t_btree_3__0_1_2__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_14_DefineVar;
// -- Table: _AssignNull
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_15_AssignNull = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<13,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_15_AssignNull;
// -- Table: _LoadInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_16_LoadInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<14,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_16_LoadInstanceField;
// -- Table: _LoadStaticField
std::unique_ptr<t_btree_5__2_3_0_1_4__4__12> rel_17_LoadStaticField = std::make_unique<t_btree_5__2_3_0_1_4__4__12>();
souffle::RelationWrapper<15,t_btree_5__2_3_0_1_4__4__12,Tuple<RamDomain,5>,5,true,false> wrapper_rel_17_LoadStaticField;
// -- Table: VarDef
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_18_VarDef = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<16,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_18_VarDef;
// -- Table: _ActualParam
std::unique_ptr<t_btree_3__1_0_2__2_1_0__2__3__4__6> rel_19_ActualParam = std::make_unique<t_btree_3__1_0_2__2_1_0__2__3__4__6>();
souffle::RelationWrapper<17,t_btree_3__1_0_2__2_1_0__2__3__4__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_19_ActualParam;
// -- Table: _Return
std::unique_ptr<t_btree_4__2_0_3_1__4__13> rel_20_Return = std::make_unique<t_btree_4__2_0_3_1__4__13>();
souffle::RelationWrapper<18,t_btree_4__2_0_3_1__4__13,Tuple<RamDomain,4>,4,true,false> wrapper_rel_20_Return;
// -- Table: _StoreArrayIndex
std::unique_ptr<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8> rel_21_StoreArrayIndex = std::make_unique<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8>();
souffle::RelationWrapper<19,t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_21_StoreArrayIndex;
// -- Table: _StoreInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_22_StoreInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<20,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_22_StoreInstanceField;
// -- Table: _StoreStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_23_StoreStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<21,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_23_StoreStaticField;
// -- Table: AllUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_24_AllUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<22,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_24_AllUse;
// -- Table: FirstUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_25_FirstUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<23,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_25_FirstUse;
// -- Table: LastUse
std::unique_ptr<t_btree_4__0_2_3_1__13> rel_26_LastUse = std::make_unique<t_btree_4__0_2_3_1__13>();
souffle::RelationWrapper<24,t_btree_4__0_2_3_1__13,Tuple<RamDomain,4>,4,false,true> wrapper_rel_26_LastUse;
// -- Table: ApplicationMethod
std::unique_ptr<t_btree_1__0__1> rel_27_ApplicationMethod = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<25,t_btree_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_27_ApplicationMethod;
// -- Table: AssignMayNull
std::unique_ptr<t_btree_3__0_1_2> rel_28_AssignMayNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<26,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_28_AssignMayNull;
// -- Table: BasicBlockHead
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_29_BasicBlockHead = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<27,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_29_BasicBlockHead;
// -- Table: Instruction_Next
std::unique_ptr<t_btree_2__0_1__1> rel_30_Instruction_Next = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<28,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_30_Instruction_Next;
// -- Table: _IfVar
std::unique_ptr<t_btree_3__2_0_1__4> rel_31_IfVar = std::make_unique<t_btree_3__2_0_1__4>();
souffle::RelationWrapper<29,t_btree_3__2_0_1__4,Tuple<RamDomain,3>,3,true,false> wrapper_rel_31_IfVar;
// -- Table: BoolIf
std::unique_ptr<t_btree_3__1_0_2__2_0_1__2__4> rel_32_BoolIf = std::make_unique<t_btree_3__1_0_2__2_0_1__2__4>();
// -- Table: BoolIfVarInvoke
std::unique_ptr<t_btree_3__1_0_2__2> rel_33_BoolIfVarInvoke = std::make_unique<t_btree_3__1_0_2__2>();
// -- Table: hasNextIf
std::unique_ptr<t_btree_3__0_1_2> rel_34_hasNextIf = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<30,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_34_hasNextIf;
// -- Table: _IfNull
std::unique_ptr<t_btree_3__0_2_1__1__5> rel_35_IfNull = std::make_unique<t_btree_3__0_2_1__1__5>();
souffle::RelationWrapper<31,t_btree_3__0_2_1__1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_35_IfNull;
// -- Table: MayNull_IfInstruction
std::unique_ptr<t_btree_1__0> rel_36_MayNull_IfInstruction = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<32,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_36_MayNull_IfInstruction;
// -- Table: FalseBranch
std::unique_ptr<t_btree_2__0_1__1> rel_37_FalseBranch = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<33,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_37_FalseBranch;
// -- Table: _OperatorAt
std::unique_ptr<t_btree_2__0_1__1> rel_38_OperatorAt = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<34,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_38_OperatorAt;
// -- Table: MayNull_IfInstructionsCond
std::unique_ptr<t_btree_4__0_2_1_3__2_3_0_1__5__12> rel_39_MayNull_IfInstructionsCond = std::make_unique<t_btree_4__0_2_1_3__2_3_0_1__5__12>();
souffle::RelationWrapper<35,t_btree_4__0_2_1_3__2_3_0_1__5__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_39_MayNull_IfInstructionsCond;
// -- Table: BoolFalseBranch
std::unique_ptr<t_btree_2__0_1> rel_40_BoolFalseBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: BoolTrueBranch
std::unique_ptr<t_btree_2__0_1> rel_41_BoolTrueBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: ParamInBoolBranch
std::unique_ptr<t_btree_2__0_1__3> rel_42_ParamInBoolBranch = std::make_unique<t_btree_2__0_1__3>();
// -- Table: CallGraphEdge
std::unique_ptr<t_btree_4__1_0_2_3__3_0_1_2__2__8> rel_43_CallGraphEdge = std::make_unique<t_btree_4__1_0_2_3__3_0_1_2__2__8>();
souffle::RelationWrapper<36,t_btree_4__1_0_2_3__3_0_1_2__2__8,Tuple<RamDomain,4>,4,true,false> wrapper_rel_43_CallGraphEdge;
// -- Table: PhiNodeHead
std::unique_ptr<t_btree_2__0_1> rel_44_PhiNodeHead = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<37,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_44_PhiNodeHead;
// -- Table: PhiNodeHeadVar
std::unique_ptr<t_btree_2__1_0__2> rel_45_PhiNodeHeadVar = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<38,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,false,true> wrapper_rel_45_PhiNodeHeadVar;
// -- Table: CanBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_46_CanBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<39,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_46_CanBeNullBranch;
// -- Table: @delta_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_47_delta_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_48_new_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_49_CannotBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<40,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_49_CannotBeNullBranch;
// -- Table: @delta_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_50_delta_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_51_new_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: Dominates
std::unique_ptr<t_btree_2__0_1__1__3> rel_52_Dominates = std::make_unique<t_btree_2__0_1__1__3>();
souffle::RelationWrapper<41,t_btree_2__0_1__1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_52_Dominates;
// -- Table: NextInsideHasNext
std::unique_ptr<t_btree_2__0_1__3> rel_53_NextInsideHasNext = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<42,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_53_NextInsideHasNext;
// -- Table: _FormalParam
std::unique_ptr<t_btree_3__1_2_0__6> rel_54_FormalParam = std::make_unique<t_btree_3__1_2_0__6>();
souffle::RelationWrapper<43,t_btree_3__1_2_0__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_54_FormalParam;
// -- Table: Instruction_FormalParam
std::unique_ptr<t_btree_4__1_3_0_2__10> rel_55_Instruction_FormalParam = std::make_unique<t_btree_4__1_3_0_2__10>();
souffle::RelationWrapper<44,t_btree_4__1_3_0_2__10,Tuple<RamDomain,4>,4,false,true> wrapper_rel_55_Instruction_FormalParam;
// -- Table: isParam
std::unique_ptr<t_btree_3__1_2_0__2__6__7> rel_56_isParam = std::make_unique<t_btree_3__1_2_0__2__6__7>();
souffle::RelationWrapper<45,t_btree_3__1_2_0__2__6__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_56_isParam;
// -- Table: @delta_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_57_delta_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: @new_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_58_new_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: _ThisVar
std::unique_ptr<t_btree_2__0_1__3> rel_59_ThisVar = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<46,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_59_ThisVar;
// -- Table: _Var_DeclaringMethod
std::unique_ptr<t_btree_2__0_1__1> rel_60_Var_DeclaringMethod = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<47,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_60_Var_DeclaringMethod;
// -- Table: Instruction_VarDeclaringMethod
std::unique_ptr<t_btree_3__0_1_2> rel_61_Instruction_VarDeclaringMethod = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<48,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_61_Instruction_VarDeclaringMethod;
// -- Table: Method_FirstInstruction
std::unique_ptr<t_btree_2__0_1__1> rel_62_Method_FirstInstruction = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<49,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_62_Method_FirstInstruction;
// -- Table: MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_63_MayPredecessorModuloThrow = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<50,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_63_MayPredecessorModuloThrow;
// -- Table: SpecialIfEdge
std::unique_ptr<t_btree_3__0_2_1__5> rel_64_SpecialIfEdge = std::make_unique<t_btree_3__0_2_1__5>();
// -- Table: InstructionLine
std::unique_ptr<t_btree_4__0_1_2_3__3> rel_65_InstructionLine = std::make_unique<t_btree_4__0_1_2_3__3>();
souffle::RelationWrapper<51,t_btree_4__0_1_2_3__3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_65_InstructionLine;
// -- Table: VarPointsTo
std::unique_ptr<t_btree_4__0_1_2_3> rel_66_VarPointsTo = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<52,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_66_VarPointsTo;
// -- Table: VarMayPointToNull
std::unique_ptr<t_btree_1__0> rel_67_VarMayPointToNull = std::make_unique<t_btree_1__0>();
// -- Table: VarCannotBeNull
std::unique_ptr<t_btree_1__0__1> rel_68_VarCannotBeNull = std::make_unique<t_btree_1__0__1>();
// -- Table: VarPointsToNull
std::unique_ptr<t_btree_1__0__1> rel_69_VarPointsToNull = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<53,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_69_VarPointsToNull;
// -- Table: _AssignBinop
std::unique_ptr<t_btree_4__0_1_2_3> rel_70_AssignBinop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<54,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_70_AssignBinop;
// -- Table: _AssignOperFrom
std::unique_ptr<t_btree_3__0_2_1__5> rel_71_AssignOperFrom = std::make_unique<t_btree_3__0_2_1__5>();
souffle::RelationWrapper<55,t_btree_3__0_2_1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_71_AssignOperFrom;
// -- Table: _AssignUnop
std::unique_ptr<t_btree_4__0_1_2_3> rel_72_AssignUnop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<56,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_72_AssignUnop;
// -- Table: _EnterMonitor
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_73_EnterMonitor = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<57,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_73_EnterMonitor;
// -- Table: _LoadArrayIndex
std::unique_ptr<t_btree_5__3_0_1_2_4__8> rel_74_LoadArrayIndex = std::make_unique<t_btree_5__3_0_1_2_4__8>();
souffle::RelationWrapper<58,t_btree_5__3_0_1_2_4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_74_LoadArrayIndex;
// -- Table: _ThrowNull
std::unique_ptr<t_btree_3__0_1_2> rel_75_ThrowNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<59,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,true,false> wrapper_rel_75_ThrowNull;
// -- Table: NullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_76_NullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<60,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_76_NullAt;
// -- Table: ReachableNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_77_ReachableNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<61,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_77_ReachableNullAtLine;
// -- Table: IfInstructions
std::unique_ptr<t_btree_2__0_1> rel_78_IfInstructions = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<62,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_78_IfInstructions;
// -- Table: IfInstructionsCond
std::unique_ptr<t_btree_4__3_0_1_2__8> rel_79_IfInstructionsCond = std::make_unique<t_btree_4__3_0_1_2__8>();
souffle::RelationWrapper<63,t_btree_4__3_0_1_2__8,Tuple<RamDomain,4>,4,false,true> wrapper_rel_79_IfInstructionsCond;
// -- Table: TrueIfInstructions
std::unique_ptr<t_btree_1__0> rel_80_TrueIfInstructions = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<64,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_80_TrueIfInstructions;
// -- Table: DominatesUnreachable
std::unique_ptr<t_btree_2__0_1> rel_81_DominatesUnreachable = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<65,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_81_DominatesUnreachable;
// -- Table: UnreachablePathNPEIns
std::unique_ptr<t_btree_1__0__1> rel_82_UnreachablePathNPEIns = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<66,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_82_UnreachablePathNPEIns;
// -- Table: PathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_2_5_1_3_4_6__37> rel_83_PathSensitiveNullAtLine = std::make_unique<t_btree_7__0_2_5_1_3_4_6__37>();
souffle::RelationWrapper<67,t_btree_7__0_2_5_1_3_4_6__37,Tuple<RamDomain,7>,7,false,true> wrapper_rel_83_PathSensitiveNullAtLine;
// -- Table: MinPathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_84_MinPathSensitiveNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
// -- Table: _AssignNumConstant
std::unique_ptr<t_btree_5__2_3_4_0_1__28> rel_85_AssignNumConstant = std::make_unique<t_btree_5__2_3_4_0_1__28>();
souffle::RelationWrapper<68,t_btree_5__2_3_4_0_1__28,Tuple<RamDomain,5>,5,true,false> wrapper_rel_85_AssignNumConstant;
// -- Table: ReturnFalse
std::unique_ptr<t_btree_2__0_1__3> rel_86_ReturnFalse = std::make_unique<t_btree_2__0_1__3>();
// -- Table: MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3__3__7__15> rel_87_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3__3__7__15>();
souffle::RelationWrapper<69,t_btree_4__0_1_2_3__3__7__15,Tuple<RamDomain,4>,4,false,true> wrapper_rel_87_MayNullPtr;
// -- Table: @delta_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_88_delta_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: @new_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_89_new_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: MethodDerefArg
std::unique_ptr<t_btree_2__0_1__3> rel_90_MethodDerefArg = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<70,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_90_MethodDerefArg;
// -- Table: @delta_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_91_delta_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_92_new_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: JDKFunctionSummary
std::unique_ptr<t_btree_2__0_1> rel_93_JDKFunctionSummary = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<71,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_93_JDKFunctionSummary;
// -- Table: NPEWithMayNull
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_94_NPEWithMayNull = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<72,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_94_NPEWithMayNull;
// -- Table: JumpTarget
std::unique_ptr<t_btree_2__1_0__2> rel_95_JumpTarget = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<73,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_95_JumpTarget;
// -- Table: TrueBranch
std::unique_ptr<t_btree_2__0_1> rel_96_TrueBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<74,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_96_TrueBranch;
// -- Table: ReachableNullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_97_ReachableNullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<75,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_97_ReachableNullAt;
public:
Sf_A() : 
wrapper_rel_1_AssignReturnValue(*rel_1_AssignReturnValue,symTable,"_AssignReturnValue",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"invocation","to"}}),

wrapper_rel_2_SpecialMethodInvocation(*rel_2_SpecialMethodInvocation,symTable,"_SpecialMethodInvocation",std::array<const char *,5>{{"s:symbol","i:Index","s:Method","s:symbol","s:Method"}},std::array<const char *,5>{{"?instruction","i","sig","?base","m"}}),

wrapper_rel_3_StaticMethodInvocation(*rel_3_StaticMethodInvocation,symTable,"_StaticMethodInvocation",std::array<const char *,4>{{"s:Instruction","i:number","s:Method","s:Method"}},std::array<const char *,4>{{"instruction","index","signature","method"}}),

wrapper_rel_4_VirtualMethodInvocation(*rel_4_VirtualMethodInvocation,symTable,"_VirtualMethodInvocation",std::array<const char *,5>{{"s:Instruction","i:Index","s:Method","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","sig","base","m"}}),

wrapper_rel_5_AssignReturnValue_WithInvoke(*rel_5_AssignReturnValue_WithInvoke,symTable,"AssignReturnValue_WithInvoke",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_6_IterNextInsn(*rel_6_IterNextInsn,symTable,"IterNextInsn",std::array<const char *,3>{{"s:Instruction","s:Var","s:Var"}},std::array<const char *,3>{{"insn","returnVar","var"}}),

wrapper_rel_8_Var_Type(*rel_8_Var_Type,symTable,"_Var_Type",std::array<const char *,2>{{"s:Var","s:Type"}},std::array<const char *,2>{{"var","type"}}),

wrapper_rel_9_RefTypeVar(*rel_9_RefTypeVar,symTable,"RefTypeVar",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"var"}}),

wrapper_rel_10_AssignCast(*rel_10_AssignCast,symTable,"_AssignCast",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Type","s:Method"}},std::array<const char *,6>{{"ins","i","from","to","t","m"}}),

wrapper_rel_11_AssignCastNull(*rel_11_AssignCastNull,symTable,"_AssignCastNull",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Type","s:Method"}},std::array<const char *,5>{{"ins","i","to","t","m"}}),

wrapper_rel_12_AssignHeapAllocation(*rel_12_AssignHeapAllocation,symTable,"_AssignHeapAllocation",std::array<const char *,6>{{"s:Instruction","i:number","s:symbol","s:Var","s:Method","i:number"}},std::array<const char *,6>{{"?instruction","?index","?heap","?to","?inmethod","?linenumber"}}),

wrapper_rel_13_AssignLocal(*rel_13_AssignLocal,symTable,"_AssignLocal",std::array<const char *,5>{{"s:Instruction","i:number","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"?instruction","?index","?from","?to","?inmethod"}}),

wrapper_rel_14_DefineVar(*rel_14_DefineVar,symTable,"DefineVar",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_15_AssignNull(*rel_15_AssignNull,symTable,"_AssignNull",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_16_LoadInstanceField(*rel_16_LoadInstanceField,symTable,"_LoadInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","to","base","sig","m"}}),

wrapper_rel_17_LoadStaticField(*rel_17_LoadStaticField,symTable,"_LoadStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","to","sig","m"}}),

wrapper_rel_18_VarDef(*rel_18_VarDef,symTable,"VarDef",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_19_ActualParam(*rel_19_ActualParam,symTable,"_ActualParam",std::array<const char *,3>{{"i:number","s:Instruction","s:Var"}},std::array<const char *,3>{{"index","invocation","var"}}),

wrapper_rel_20_Return(*rel_20_Return,symTable,"_Return",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"instruction","index","var","method"}}),

wrapper_rel_21_StoreArrayIndex(*rel_21_StoreArrayIndex,symTable,"_StoreArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","from","base","m"}}),

wrapper_rel_22_StoreInstanceField(*rel_22_StoreInstanceField,symTable,"_StoreInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","from","base","sig","m"}}),

wrapper_rel_23_StoreStaticField(*rel_23_StoreStaticField,symTable,"_StoreStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","from","sig","m"}}),

wrapper_rel_24_AllUse(*rel_24_AllUse,symTable,"AllUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_25_FirstUse(*rel_25_FirstUse,symTable,"FirstUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_26_LastUse(*rel_26_LastUse,symTable,"LastUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_27_ApplicationMethod(*rel_27_ApplicationMethod,symTable,"ApplicationMethod",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_28_AssignMayNull(*rel_28_AssignMayNull,symTable,"AssignMayNull",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_29_BasicBlockHead(*rel_29_BasicBlockHead,symTable,"BasicBlockHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"insn","headInsn"}}),

wrapper_rel_30_Instruction_Next(*rel_30_Instruction_Next,symTable,"Instruction_Next",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?i","?next"}}),

wrapper_rel_31_IfVar(*rel_31_IfVar,symTable,"_IfVar",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_34_hasNextIf(*rel_34_hasNextIf,symTable,"hasNextIf",std::array<const char *,3>{{"s:Instruction","s:Instruction","s:Var"}},std::array<const char *,3>{{"ifInsn","invokeInsn","var"}}),

wrapper_rel_35_IfNull(*rel_35_IfNull,symTable,"_IfNull",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_36_MayNull_IfInstruction(*rel_36_MayNull_IfInstruction,symTable,"MayNull_IfInstruction",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_37_FalseBranch(*rel_37_FalseBranch,symTable,"FalseBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_38_OperatorAt(*rel_38_OperatorAt,symTable,"_OperatorAt",std::array<const char *,2>{{"s:Instruction","s:symbol"}},std::array<const char *,2>{{"i","operator"}}),

wrapper_rel_39_MayNull_IfInstructionsCond(*rel_39_MayNull_IfInstructionsCond,symTable,"MayNull_IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:Var","s:Var","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_43_CallGraphEdge(*rel_43_CallGraphEdge,symTable,"CallGraphEdge",std::array<const char *,4>{{"s:Context","s:Instruction","s:Context","s:Method"}},std::array<const char *,4>{{"ctx","ins","hctx","sig"}}),

wrapper_rel_44_PhiNodeHead(*rel_44_PhiNodeHead,symTable,"PhiNodeHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?insn","?headInsn"}}),

wrapper_rel_45_PhiNodeHeadVar(*rel_45_PhiNodeHeadVar,symTable,"PhiNodeHeadVar",std::array<const char *,2>{{"s:Var","s:Var"}},std::array<const char *,2>{{"var","headVar"}}),

wrapper_rel_46_CanBeNullBranch(*rel_46_CanBeNullBranch,symTable,"CanBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_49_CannotBeNullBranch(*rel_49_CannotBeNullBranch,symTable,"CannotBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_52_Dominates(*rel_52_Dominates,symTable,"Dominates",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?dominator","?insn"}}),

wrapper_rel_53_NextInsideHasNext(*rel_53_NextInsideHasNext,symTable,"NextInsideHasNext",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_54_FormalParam(*rel_54_FormalParam,symTable,"_FormalParam",std::array<const char *,3>{{"i:number","s:Method","s:Var"}},std::array<const char *,3>{{"index","method","var"}}),

wrapper_rel_55_Instruction_FormalParam(*rel_55_Instruction_FormalParam,symTable,"Instruction_FormalParam",std::array<const char *,4>{{"s:Instruction","s:symbol","s:Var","i:number"}},std::array<const char *,4>{{"insn","method","var","index"}}),

wrapper_rel_56_isParam(*rel_56_isParam,symTable,"isParam",std::array<const char *,3>{{"i:number","s:Var","s:Method"}},std::array<const char *,3>{{"index","var","method"}}),

wrapper_rel_59_ThisVar(*rel_59_ThisVar,symTable,"_ThisVar",std::array<const char *,2>{{"s:Method","s:Var"}},std::array<const char *,2>{{"method","var"}}),

wrapper_rel_60_Var_DeclaringMethod(*rel_60_Var_DeclaringMethod,symTable,"_Var_DeclaringMethod",std::array<const char *,2>{{"s:Var","s:Method"}},std::array<const char *,2>{{"?var","?method"}}),

wrapper_rel_61_Instruction_VarDeclaringMethod(*rel_61_Instruction_VarDeclaringMethod,symTable,"Instruction_VarDeclaringMethod",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"insn","method","var"}}),

wrapper_rel_62_Method_FirstInstruction(*rel_62_Method_FirstInstruction,symTable,"Method_FirstInstruction",std::array<const char *,2>{{"s:Method","s:Instruction"}},std::array<const char *,2>{{"?method","?i"}}),

wrapper_rel_63_MayPredecessorModuloThrow(*rel_63_MayPredecessorModuloThrow,symTable,"MayPredecessorModuloThrow",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?prev","?next"}}),

wrapper_rel_65_InstructionLine(*rel_65_InstructionLine,symTable,"InstructionLine",std::array<const char *,4>{{"s:Method","i:Index","i:LineNumber","s:File"}},std::array<const char *,4>{{"m","i","l","f"}}),

wrapper_rel_66_VarPointsTo(*rel_66_VarPointsTo,symTable,"VarPointsTo",std::array<const char *,4>{{"s:HContext","s:Alloc","s:Context","s:Var"}},std::array<const char *,4>{{"hctx","a","ctx","v"}}),

wrapper_rel_69_VarPointsToNull(*rel_69_VarPointsToNull,symTable,"VarPointsToNull",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"v"}}),

wrapper_rel_70_AssignBinop(*rel_70_AssignBinop,symTable,"_AssignBinop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_71_AssignOperFrom(*rel_71_AssignOperFrom,symTable,"_AssignOperFrom",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"ins","pos","from"}}),

wrapper_rel_72_AssignUnop(*rel_72_AssignUnop,symTable,"_AssignUnop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_73_EnterMonitor(*rel_73_EnterMonitor,symTable,"_EnterMonitor",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_74_LoadArrayIndex(*rel_74_LoadArrayIndex,symTable,"_LoadArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","to","base","m"}}),

wrapper_rel_75_ThrowNull(*rel_75_ThrowNull,symTable,"_ThrowNull",std::array<const char *,3>{{"s:Instruction","i:Index","s:Method"}},std::array<const char *,3>{{"ins","i","m"}}),

wrapper_rel_76_NullAt(*rel_76_NullAt,symTable,"NullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}),

wrapper_rel_77_ReachableNullAtLine(*rel_77_ReachableNullAtLine,symTable,"ReachableNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_78_IfInstructions(*rel_78_IfInstructions,symTable,"IfInstructions",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ins","ifIns"}}),

wrapper_rel_79_IfInstructionsCond(*rel_79_IfInstructionsCond,symTable,"IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:symbol","s:symbol","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_80_TrueIfInstructions(*rel_80_TrueIfInstructions,symTable,"TrueIfInstructions",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_81_DominatesUnreachable(*rel_81_DominatesUnreachable,symTable,"DominatesUnreachable",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","ins"}}),

wrapper_rel_82_UnreachablePathNPEIns(*rel_82_UnreachablePathNPEIns,symTable,"UnreachablePathNPEIns",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_83_PathSensitiveNullAtLine(*rel_83_PathSensitiveNullAtLine,symTable,"PathSensitiveNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_85_AssignNumConstant(*rel_85_AssignNumConstant,symTable,"_AssignNumConstant",std::array<const char *,5>{{"s:Instruction","i:Index","s:symbol","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","constant","var","m"}}),

wrapper_rel_87_MayNullPtr(*rel_87_MayNullPtr,symTable,"MayNullPtr",std::array<const char *,4>{{"s:Instruction","s:Var","s:Method","s:symbol"}},std::array<const char *,4>{{"insn","var","method","reason"}}),

wrapper_rel_90_MethodDerefArg(*rel_90_MethodDerefArg,symTable,"MethodDerefArg",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_93_JDKFunctionSummary(*rel_93_JDKFunctionSummary,symTable,"JDKFunctionSummary",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_94_NPEWithMayNull(*rel_94_NPEWithMayNull,symTable,"NPEWithMayNull",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_95_JumpTarget(*rel_95_JumpTarget,symTable,"JumpTarget",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"to","from"}}),

wrapper_rel_96_TrueBranch(*rel_96_TrueBranch,symTable,"TrueBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_97_ReachableNullAt(*rel_97_ReachableNullAt,symTable,"ReachableNullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}){
addRelation("_AssignReturnValue",&wrapper_rel_1_AssignReturnValue,1,0);
addRelation("_SpecialMethodInvocation",&wrapper_rel_2_SpecialMethodInvocation,1,0);
addRelation("_StaticMethodInvocation",&wrapper_rel_3_StaticMethodInvocation,1,0);
addRelation("_VirtualMethodInvocation",&wrapper_rel_4_VirtualMethodInvocation,1,0);
addRelation("AssignReturnValue_WithInvoke",&wrapper_rel_5_AssignReturnValue_WithInvoke,0,1);
addRelation("IterNextInsn",&wrapper_rel_6_IterNextInsn,0,1);
addRelation("_Var_Type",&wrapper_rel_8_Var_Type,1,0);
addRelation("RefTypeVar",&wrapper_rel_9_RefTypeVar,0,1);
addRelation("_AssignCast",&wrapper_rel_10_AssignCast,1,0);
addRelation("_AssignCastNull",&wrapper_rel_11_AssignCastNull,1,0);
addRelation("_AssignHeapAllocation",&wrapper_rel_12_AssignHeapAllocation,1,0);
addRelation("_AssignLocal",&wrapper_rel_13_AssignLocal,1,0);
addRelation("DefineVar",&wrapper_rel_14_DefineVar,0,1);
addRelation("_AssignNull",&wrapper_rel_15_AssignNull,1,0);
addRelation("_LoadInstanceField",&wrapper_rel_16_LoadInstanceField,1,0);
addRelation("_LoadStaticField",&wrapper_rel_17_LoadStaticField,1,0);
addRelation("VarDef",&wrapper_rel_18_VarDef,0,1);
addRelation("_ActualParam",&wrapper_rel_19_ActualParam,1,0);
addRelation("_Return",&wrapper_rel_20_Return,1,0);
addRelation("_StoreArrayIndex",&wrapper_rel_21_StoreArrayIndex,1,0);
addRelation("_StoreInstanceField",&wrapper_rel_22_StoreInstanceField,1,0);
addRelation("_StoreStaticField",&wrapper_rel_23_StoreStaticField,1,0);
addRelation("AllUse",&wrapper_rel_24_AllUse,0,1);
addRelation("FirstUse",&wrapper_rel_25_FirstUse,0,1);
addRelation("LastUse",&wrapper_rel_26_LastUse,0,1);
addRelation("ApplicationMethod",&wrapper_rel_27_ApplicationMethod,1,0);
addRelation("AssignMayNull",&wrapper_rel_28_AssignMayNull,0,1);
addRelation("BasicBlockHead",&wrapper_rel_29_BasicBlockHead,1,0);
addRelation("Instruction_Next",&wrapper_rel_30_Instruction_Next,1,1);
addRelation("_IfVar",&wrapper_rel_31_IfVar,1,0);
addRelation("hasNextIf",&wrapper_rel_34_hasNextIf,0,1);
addRelation("_IfNull",&wrapper_rel_35_IfNull,1,0);
addRelation("MayNull_IfInstruction",&wrapper_rel_36_MayNull_IfInstruction,0,1);
addRelation("FalseBranch",&wrapper_rel_37_FalseBranch,0,1);
addRelation("_OperatorAt",&wrapper_rel_38_OperatorAt,1,0);
addRelation("MayNull_IfInstructionsCond",&wrapper_rel_39_MayNull_IfInstructionsCond,0,1);
addRelation("CallGraphEdge",&wrapper_rel_43_CallGraphEdge,1,0);
addRelation("PhiNodeHead",&wrapper_rel_44_PhiNodeHead,1,1);
addRelation("PhiNodeHeadVar",&wrapper_rel_45_PhiNodeHeadVar,0,1);
addRelation("CanBeNullBranch",&wrapper_rel_46_CanBeNullBranch,0,1);
addRelation("CannotBeNullBranch",&wrapper_rel_49_CannotBeNullBranch,0,1);
addRelation("Dominates",&wrapper_rel_52_Dominates,1,0);
addRelation("NextInsideHasNext",&wrapper_rel_53_NextInsideHasNext,0,1);
addRelation("_FormalParam",&wrapper_rel_54_FormalParam,1,0);
addRelation("Instruction_FormalParam",&wrapper_rel_55_Instruction_FormalParam,0,1);
addRelation("isParam",&wrapper_rel_56_isParam,0,1);
addRelation("_ThisVar",&wrapper_rel_59_ThisVar,1,0);
addRelation("_Var_DeclaringMethod",&wrapper_rel_60_Var_DeclaringMethod,1,0);
addRelation("Instruction_VarDeclaringMethod",&wrapper_rel_61_Instruction_VarDeclaringMethod,0,1);
addRelation("Method_FirstInstruction",&wrapper_rel_62_Method_FirstInstruction,1,0);
addRelation("MayPredecessorModuloThrow",&wrapper_rel_63_MayPredecessorModuloThrow,1,0);
addRelation("InstructionLine",&wrapper_rel_65_InstructionLine,1,0);
addRelation("VarPointsTo",&wrapper_rel_66_VarPointsTo,1,0);
addRelation("VarPointsToNull",&wrapper_rel_69_VarPointsToNull,0,1);
addRelation("_AssignBinop",&wrapper_rel_70_AssignBinop,1,0);
addRelation("_AssignOperFrom",&wrapper_rel_71_AssignOperFrom,1,0);
addRelation("_AssignUnop",&wrapper_rel_72_AssignUnop,1,0);
addRelation("_EnterMonitor",&wrapper_rel_73_EnterMonitor,1,0);
addRelation("_LoadArrayIndex",&wrapper_rel_74_LoadArrayIndex,1,0);
addRelation("_ThrowNull",&wrapper_rel_75_ThrowNull,1,0);
addRelation("NullAt",&wrapper_rel_76_NullAt,0,1);
addRelation("ReachableNullAtLine",&wrapper_rel_77_ReachableNullAtLine,0,1);
addRelation("IfInstructions",&wrapper_rel_78_IfInstructions,0,1);
addRelation("IfInstructionsCond",&wrapper_rel_79_IfInstructionsCond,0,1);
addRelation("TrueIfInstructions",&wrapper_rel_80_TrueIfInstructions,0,1);
addRelation("DominatesUnreachable",&wrapper_rel_81_DominatesUnreachable,0,1);
addRelation("UnreachablePathNPEIns",&wrapper_rel_82_UnreachablePathNPEIns,0,1);
addRelation("PathSensitiveNullAtLine",&wrapper_rel_83_PathSensitiveNullAtLine,0,1);
addRelation("_AssignNumConstant",&wrapper_rel_85_AssignNumConstant,1,0);
addRelation("MayNullPtr",&wrapper_rel_87_MayNullPtr,0,1);
addRelation("MethodDerefArg",&wrapper_rel_90_MethodDerefArg,0,1);
addRelation("JDKFunctionSummary",&wrapper_rel_93_JDKFunctionSummary,0,1);
addRelation("NPEWithMayNull",&wrapper_rel_94_NPEWithMayNull,0,1);
addRelation("JumpTarget",&wrapper_rel_95_JumpTarget,1,0);
addRelation("TrueBranch",&wrapper_rel_96_TrueBranch,0,1);
addRelation("ReachableNullAt",&wrapper_rel_97_ReachableNullAt,0,1);
}
~Sf_A() {
}
private:
void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1, bool performIO = false) {
SignalHandler::instance()->set();
std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(1);
#endif

// -- query evaluation --
/* BEGIN STRATUM 0 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
[&]() {
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
if (!rel_1_AssignReturnValue->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
if (!rel_1_AssignReturnValue->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
[&]() {
SignalHandler::instance()->setMsg(R"_(IterNextInsn(insn,returnVar,var) :- 
   _AssignReturnValue(insn,returnVar),
   _VirtualMethodInvocation(insn,_,sig,var,_),
   "next()" contains sig.
in file ../may-null/rules.dl [309:1-312:25])_");
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
if( (symTable.resolve(env1[2]).find(symTable.resolve(RamDomain(42))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_6_IterNextInsn->insert(tuple,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 5 */
/* BEGIN STRATUM 6 */
[&]() {
SignalHandler::instance()->setMsg(R"_(Primitive("boolean").
in file ../declarations.dl [5:1-5:22])_");
rel_7_Primitive->insert(RamDomain(0));
SignalHandler::instance()->setMsg(R"_(Primitive("short").
in file ../declarations.dl [6:1-6:20])_");
rel_7_Primitive->insert(RamDomain(1));
SignalHandler::instance()->setMsg(R"_(Primitive("int").
in file ../declarations.dl [7:1-7:18])_");
rel_7_Primitive->insert(RamDomain(2));
SignalHandler::instance()->setMsg(R"_(Primitive("long").
in file ../declarations.dl [8:1-8:19])_");
rel_7_Primitive->insert(RamDomain(3));
SignalHandler::instance()->setMsg(R"_(Primitive("float").
in file ../declarations.dl [9:1-9:20])_");
rel_7_Primitive->insert(RamDomain(4));
SignalHandler::instance()->setMsg(R"_(Primitive("double").
in file ../declarations.dl [10:1-10:21])_");
rel_7_Primitive->insert(RamDomain(5));
SignalHandler::instance()->setMsg(R"_(Primitive("char").
in file ../declarations.dl [11:1-11:19])_");
rel_7_Primitive->insert(RamDomain(6));
SignalHandler::instance()->setMsg(R"_(Primitive("byte").
in file ../declarations.dl [12:1-12:19])_");
rel_7_Primitive->insert(RamDomain(7));
}();
/* END STRATUM 6 */
/* BEGIN STRATUM 7 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 7 */
/* BEGIN STRATUM 8 */
[&]() {
SignalHandler::instance()->setMsg(R"_(RefTypeVar(var) :- 
   _Var_Type(var,type),
   !Primitive(type).
in file ../may-null/rules.dl [100:1-102:18])_");
if (!rel_8_Var_Type->empty()) [&](){
auto part = rel_8_Var_Type->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_7_Primitive_op_ctxt,rel_7_Primitive->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_8_Var_Type_op_ctxt,rel_8_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( !rel_7_Primitive->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_7_Primitive_op_ctxt))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_9_RefTypeVar->insert(tuple,READ_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_Primitive->purge();
}();
/* END STRATUM 8 */
/* BEGIN STRATUM 9 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 9 */
/* BEGIN STRATUM 10 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 10 */
/* BEGIN STRATUM 11 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 11 */
/* BEGIN STRATUM 12 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 12 */
/* BEGIN STRATUM 13 */
[&]() {
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignLocal(insn,_,_,to,method).
in file ../may-null/rules.dl [134:1-140:3])_");
if (!rel_13_AssignLocal->empty()) [&](){
auto part = rel_13_AssignLocal->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignCast(insn,_,_,to,_,method).
in file ../may-null/rules.dl [134:1-140:3])_");
if (!rel_10_AssignCast->empty()) [&](){
auto part = rel_10_AssignCast->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[5])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignHeapAllocation(insn,_,_,to,method,_).
in file ../may-null/rules.dl [134:1-140:3])_");
if (!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_12_AssignHeapAllocation->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   AssignReturnValue_WithInvoke(insn,_,to,method).
in file ../may-null/rules.dl [134:1-140:3])_");
if (!rel_5_AssignReturnValue_WithInvoke->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 13 */
/* BEGIN STRATUM 14 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 14 */
/* BEGIN STRATUM 15 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 15 */
/* BEGIN STRATUM 16 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 16 */
/* BEGIN STRATUM 17 */
[&]() {
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignNull(insn,index,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_15_AssignNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_15_AssignNull->equalRange_4(key,READ_OP_CONTEXT(rel_15_AssignNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,_,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_13_AssignLocal->equalRange_8(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignHeapAllocation(insn,index,_,var,method,_).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_12_AssignHeapAllocation->equalRange_8(key,READ_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_16_LoadInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_17_LoadStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCastNull(insn,index,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_11_AssignCastNull->equalRange_4(key,READ_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,_,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_10_AssignCast->equalRange_8(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   AssignReturnValue_WithInvoke(insn,index,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_4(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_12_AssignHeapAllocation->purge();
}();
/* END STRATUM 17 */
/* BEGIN STRATUM 18 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 18 */
/* BEGIN STRATUM 19 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 19 */
/* BEGIN STRATUM 20 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 20 */
/* BEGIN STRATUM 21 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 21 */
/* BEGIN STRATUM 22 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 22 */
/* BEGIN STRATUM 23 */
[&]() {
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   VarDef(insn,index,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_18_VarDef->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_18_VarDef->equalRange_4(key,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_13_AssignLocal->equalRange_4(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,var,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_10_AssignCast->equalRange_4(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _SpecialMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _VirtualMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreArrayIndex(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_21_StoreArrayIndex->equalRange_4(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_22_StoreInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_23_StoreStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt,rel_23_StoreStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_23_StoreStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _Return(insn,index,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_20_Return->equalRange_4(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   AssignReturnValue_WithInvoke(insn,index,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_23_StoreStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_18_VarDef->purge();
}();
/* END STRATUM 23 */
/* BEGIN STRATUM 24 */
[&]() {
SignalHandler::instance()->setMsg(R"_(FirstUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = min  I0 : AllUse(_, I0,var,method).
in file ../may-null/rules.dl [83:1-85:42])_");
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_25_FirstUse->insert(tuple,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 24 */
/* BEGIN STRATUM 25 */
[&]() {
SignalHandler::instance()->setMsg(R"_(LastUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = max  I1 : AllUse(_, I1,var,method).
in file ../may-null/rules.dl [78:1-80:42])_");
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MIN_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::max(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_26_LastUse->insert(tuple,READ_OP_CONTEXT(rel_26_LastUse_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_24_AllUse->purge();
}();
/* END STRATUM 25 */
/* BEGIN STRATUM 26 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 26 */
/* BEGIN STRATUM 27 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 27 */
/* BEGIN STRATUM 28 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 28 */
/* BEGIN STRATUM 29 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 29 */
/* BEGIN STRATUM 30 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 30 */
/* BEGIN STRATUM 31 */
[&]() {
SignalHandler::instance()->setMsg(R"_(BoolIf(ifIns,pos,var) :- 
   _IfVar(ifIns,pos,var),
   _Var_Type(var,"boolean").
in file ../may-null/rules.dl [196:1-198:27])_");
if (!rel_31_IfVar->empty()&&!rel_8_Var_Type->empty()) [&](){
auto part = rel_31_IfVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
CREATE_OP_CONTEXT(rel_8_Var_Type_op_ctxt,rel_8_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[2],RamDomain(0)}});
auto range = rel_8_Var_Type->equalRange_3(key,READ_OP_CONTEXT(rel_8_Var_Type_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2])}});
rel_32_BoolIf->insert(tuple,READ_OP_CONTEXT(rel_32_BoolIf_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_Var_Type->purge();
}();
/* END STRATUM 31 */
/* BEGIN STRATUM 32 */
[&]() {
SignalHandler::instance()->setMsg(R"_(BoolIfVarInvoke(assignReturn,ifIns,var) :- 
   AssignReturnValue_WithInvoke(assignReturn,_,var,_),
   BoolIf(ifIns,_,var).
in file ../may-null/rules.dl [200:1-202:22])_");
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_32_BoolIf->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[2]}});
auto range = rel_32_BoolIf->equalRange_4(key,READ_OP_CONTEXT(rel_32_BoolIf_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[2])}});
rel_33_BoolIfVarInvoke->insert(tuple,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}();
/* END STRATUM 32 */
/* BEGIN STRATUM 33 */
[&]() {
SignalHandler::instance()->setMsg(R"_(hasNextIf(ifInsn,invokeInsn,var) :- 
   _IfVar(ifInsn,_,returnVar),
   _AssignReturnValue(invokeInsn,returnVar),
   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),
   "boolean hasNext()" contains sig.
in file ../may-null/rules.dl [302:1-306:36])_");
if (!rel_1_AssignReturnValue->empty()&&!rel_31_IfVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_31_IfVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
CREATE_OP_CONTEXT(rel_34_hasNextIf_op_ctxt,rel_34_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[2]}});
auto range = rel_1_AssignReturnValue->equalRange_2(key,READ_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
if( (symTable.resolve(env2[2]).find(symTable.resolve(RamDomain(41))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env2[3])}});
rel_34_hasNextIf->insert(tuple,READ_OP_CONTEXT(rel_34_hasNextIf_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_AssignReturnValue->purge();
}();
/* END STRATUM 33 */
/* BEGIN STRATUM 34 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 34 */
/* BEGIN STRATUM 35 */
[&]() {
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   _IfNull(ifIns,_,_).
in file ../may-null/rules.dl [190:1-194:3])_");
if (!rel_35_IfNull->empty()) [&](){
auto part = rel_35_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_36_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   BoolIf(ifIns,_,_).
in file ../may-null/rules.dl [190:1-194:3])_");
if (!rel_32_BoolIf->empty()) [&](){
auto part = rel_32_BoolIf->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_36_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 35 */
/* BEGIN STRATUM 36 */
[&]() {
SignalHandler::instance()->setMsg(R"_(FalseBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   Instruction_Next(ifIns,insn).
in file ../may-null/rules.dl [262:1-264:31])_");
if (!rel_30_Instruction_Next->empty()&&!rel_36_MayNull_IfInstruction->empty()) [&](){
auto part = rel_36_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_37_FalseBranch->insert(tuple,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 36 */
/* BEGIN STRATUM 37 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 37 */
/* BEGIN STRATUM 38 */
[&]() {
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"null",opt) :- 
   _IfNull(ifIns,_,left),
   _OperatorAt(ifIns,opt).
in file ../may-null/rules.dl [253:1-255:25])_");
if (!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_35_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_39_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"0",opt) :- 
   BoolIf(ifIns,1,left),
   _OperatorAt(ifIns,opt).
in file ../may-null/rules.dl [257:1-259:25])_");
if (!rel_32_BoolIf->empty()&&!rel_38_OperatorAt->empty()) [&](){
const Tuple<RamDomain,3> key({{0,RamDomain(1),0}});
auto range = rel_32_BoolIf->equalRange_2(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(38)),static_cast<RamDomain>(env1[1])}});
rel_39_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_32_BoolIf->purge();
}();
/* END STRATUM 38 */
/* BEGIN STRATUM 39 */
[&]() {
SignalHandler::instance()->setMsg(R"_(BoolFalseBranch(ifIns,insn) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","=="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [213:1-215:26])_");
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_40_BoolFalseBranch->insert(tuple,READ_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(BoolFalseBranch(ifIns,ifIns) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","!=").
in file ../may-null/rules.dl [217:1-218:51])_");
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[0])}});
rel_40_BoolFalseBranch->insert(tuple,READ_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}();
/* END STRATUM 39 */
/* BEGIN STRATUM 40 */
[&]() {
SignalHandler::instance()->setMsg(R"_(BoolTrueBranch(ifIns,insn) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","!="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [220:1-222:26])_");
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_41_BoolTrueBranch->insert(tuple,READ_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(BoolTrueBranch(ifIns,ifIns) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","==").
in file ../may-null/rules.dl [224:1-225:51])_");
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[0])}});
rel_41_BoolTrueBranch->insert(tuple,READ_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}();
/* END STRATUM 40 */
/* BEGIN STRATUM 41 */
[&]() {
SignalHandler::instance()->setMsg(R"_(ParamInBoolBranch(insn,var) :- 
   BoolFalseBranch(ifIns,insn),
   BoolIfVarInvoke(assignReturn,ifIns,_),
   _ActualParam(_,assignReturn,var).
in file ../may-null/rules.dl [204:1-210:36])_");
if (!rel_40_BoolFalseBranch->empty()&&!rel_33_BoolIfVarInvoke->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_40_BoolFalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],0}});
auto range = rel_33_BoolIfVarInvoke->equalRange_2(key,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{0,env1[0],0}});
auto range = rel_19_ActualParam->equalRange_2(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[2])}});
rel_42_ParamInBoolBranch->insert(tuple,READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(ParamInBoolBranch(insn,var) :- 
   BoolTrueBranch(ifIns,insn),
   BoolIfVarInvoke(assignReturn,ifIns,_),
   _ActualParam(_,assignReturn,var).
in file ../may-null/rules.dl [204:1-210:36])_");
if (!rel_33_BoolIfVarInvoke->empty()&&!rel_41_BoolTrueBranch->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_41_BoolTrueBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],0}});
auto range = rel_33_BoolIfVarInvoke->equalRange_2(key,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{0,env1[0],0}});
auto range = rel_19_ActualParam->equalRange_2(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[2])}});
rel_42_ParamInBoolBranch->insert(tuple,READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_41_BoolTrueBranch->purge();
}();
/* END STRATUM 41 */
/* BEGIN STRATUM 42 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 42 */
/* BEGIN STRATUM 43 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 43 */
/* BEGIN STRATUM 44 */
[&]() {
SignalHandler::instance()->setMsg(R"_(PhiNodeHeadVar(var,headVar) :- 
   PhiNodeHead(insn,headInsn),
   _AssignLocal(insn,_,var,_,_),
   _AssignLocal(headInsn,_,headVar,_,_).
in file ../may-null/rules.dl [272:1-275:42])_");
if (!rel_44_PhiNodeHead->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_44_PhiNodeHead->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_44_PhiNodeHead_op_ctxt,rel_44_PhiNodeHead->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env0[1],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[2]),static_cast<RamDomain>(env2[2])}});
rel_45_PhiNodeHeadVar->insert(tuple,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_44_PhiNodeHead->purge();
}();
/* END STRATUM 44 */
/* BEGIN STRATUM 45 */
[&]() {
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","==").
in file ../may-null/rules.dl [289:1-290:54])_");
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_46_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [292:1-294:26])_");
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_46_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();rel_47_delta_CanBeNullBranch->insertAll(*rel_46_CanBeNullBranch);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   CanBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [296:1-298:30])_");
if (!rel_47_delta_CanBeNullBranch->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_47_delta_CanBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_delta_CanBeNullBranch_op_ctxt,rel_47_delta_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_48_new_CanBeNullBranch_op_ctxt,rel_48_new_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( !rel_46_CanBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_48_new_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_48_new_CanBeNullBranch_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if(rel_48_new_CanBeNullBranch->empty()) break;
rel_46_CanBeNullBranch->insertAll(*rel_48_new_CanBeNullBranch);
std::swap(rel_47_delta_CanBeNullBranch, rel_48_new_CanBeNullBranch);
rel_48_new_CanBeNullBranch->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_47_delta_CanBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_48_new_CanBeNullBranch->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_46_CanBeNullBranch->purge();
}();
/* END STRATUM 45 */
/* BEGIN STRATUM 46 */
[&]() {
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!=").
in file ../may-null/rules.dl [278:1-279:54])_");
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_49_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","=="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [281:1-283:26])_");
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_49_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();rel_50_delta_CannotBeNullBranch->insertAll(*rel_49_CannotBeNullBranch);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   CannotBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [285:1-287:30])_");
if (!rel_50_delta_CannotBeNullBranch->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_50_delta_CannotBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_50_delta_CannotBeNullBranch_op_ctxt,rel_50_delta_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_51_new_CannotBeNullBranch_op_ctxt,rel_51_new_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( !rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_51_new_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_51_new_CannotBeNullBranch_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if(rel_51_new_CannotBeNullBranch->empty()) break;
rel_49_CannotBeNullBranch->insertAll(*rel_51_new_CannotBeNullBranch);
std::swap(rel_50_delta_CannotBeNullBranch, rel_51_new_CannotBeNullBranch);
rel_51_new_CannotBeNullBranch->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_50_delta_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_51_new_CannotBeNullBranch->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 46 */
/* BEGIN STRATUM 47 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 47 */
/* BEGIN STRATUM 48 */
[&]() {
SignalHandler::instance()->setMsg(R"_(NextInsideHasNext(nextInsn,var) :- 
   hasNextIf(ifInsn,_,invokeVar),
   Instruction_Next(ifInsn,falseBlockInsn),
   Dominates(falseBlockInsn,nextInsnHead),
   BasicBlockHead(nextInsn,nextInsnHead),
   IterNextInsn(nextInsn,var,invokeVar).
in file ../may-null/rules.dl [318:1-323:40])_");
if (!rel_29_BasicBlockHead->empty()&&!rel_52_Dominates->empty()&&!rel_30_Instruction_Next->empty()&&!rel_6_IterNextInsn->empty()&&!rel_34_hasNextIf->empty()) [&](){
auto part = rel_34_hasNextIf->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_52_Dominates_op_ctxt,rel_52_Dominates->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_34_hasNextIf_op_ctxt,rel_34_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{env1[1],0}});
auto range = rel_52_Dominates->equalRange_1(key,READ_OP_CONTEXT(rel_52_Dominates_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{0,env2[1]}});
auto range = rel_29_BasicBlockHead->equalRange_2(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env3 : range) {
const Tuple<RamDomain,3> key({{env3[0],0,env0[2]}});
auto range = rel_6_IterNextInsn->equalRange_5(key,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
for(const auto& env4 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env4[1])}});
rel_53_NextInsideHasNext->insert(tuple,READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt));
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_34_hasNextIf->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_6_IterNextInsn->purge();
}();
/* END STRATUM 48 */
/* BEGIN STRATUM 49 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 49 */
/* BEGIN STRATUM 50 */
[&]() {
SignalHandler::instance()->setMsg(R"_(Instruction_FormalParam(cat(param,"/map_param/"),method,param,index) :- 
   _FormalParam(index,method,param).
in file ../may-null/rules.dl [114:1-116:34])_");
if (!rel_54_FormalParam->empty()) [&](){
auto part = rel_54_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[2]) + symTable.resolve(RamDomain(31)))),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[0])}});
rel_55_Instruction_FormalParam->insert(tuple,READ_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 50 */
/* BEGIN STRATUM 51 */
[&]() {
SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   _FormalParam(index,method,var).
in file ../may-null/rules.dl [341:1-342:35])_");
if (!rel_54_FormalParam->empty()) [&](){
auto part = rel_54_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1])}});
rel_56_isParam->insert(tuple,READ_OP_CONTEXT(rel_56_isParam_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();rel_57_delta_isParam->insertAll(*rel_56_isParam);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(isParam(index,to,method) :- 
   isParam(index,from,method),
   _AssignLocal(_,_,from,to,method).
in file ../may-null/rules.dl [344:1-349:3])_");
if (!rel_57_delta_isParam->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_57_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_57_delta_isParam_op_ctxt,rel_57_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_58_new_isParam_op_ctxt,rel_58_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_20(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( !rel_56_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_56_isParam_op_ctxt))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_58_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_58_new_isParam_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(isParam(index,to,method) :- 
   isParam(index,from,method),
   _AssignCast(_,_,from,to,_,method).
in file ../may-null/rules.dl [344:1-349:3])_");
if (!rel_57_delta_isParam->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_57_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_57_delta_isParam_op_ctxt,rel_57_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_58_new_isParam_op_ctxt,rel_58_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( !rel_56_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_56_isParam_op_ctxt))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_58_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_58_new_isParam_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   isParam(index,headVar,method),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [351:1-353:30])_");
if (!rel_57_delta_isParam->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_57_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_57_delta_isParam_op_ctxt,rel_57_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_58_new_isParam_op_ctxt,rel_58_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( !rel_56_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[0],env0[2]}}),READ_OP_CONTEXT(rel_56_isParam_op_ctxt))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[2])}});
rel_58_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_58_new_isParam_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if(rel_58_new_isParam->empty()) break;
rel_56_isParam->insertAll(*rel_58_new_isParam);
std::swap(rel_57_delta_isParam, rel_58_new_isParam);
rel_58_new_isParam->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_57_delta_isParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_58_new_isParam->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_56_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_45_PhiNodeHeadVar->purge();
}();
/* END STRATUM 51 */
/* BEGIN STRATUM 52 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_59_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 52 */
/* BEGIN STRATUM 53 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_60_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 53 */
/* BEGIN STRATUM 54 */
[&]() {
SignalHandler::instance()->setMsg(R"_(Instruction_VarDeclaringMethod(cat(var,"/var_declaration/"),method,var) :- 
   RefTypeVar(var),
   _Var_DeclaringMethod(var,method),
   !_FormalParam(_,method,var),
   !_ThisVar(method,var).
in file ../may-null/rules.dl [106:1-111:38])_");
if (!rel_9_RefTypeVar->empty()&&!rel_60_Var_DeclaringMethod->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_61_Instruction_VarDeclaringMethod_op_ctxt,rel_61_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_59_ThisVar_op_ctxt,rel_59_ThisVar->createContext());
CREATE_OP_CONTEXT(rel_60_Var_DeclaringMethod_op_ctxt,rel_60_Var_DeclaringMethod->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_60_Var_DeclaringMethod->equalRange_1(key,READ_OP_CONTEXT(rel_60_Var_DeclaringMethod_op_ctxt));
for(const auto& env1 : range) {
if( ((rel_54_FormalParam->equalRange_6(Tuple<RamDomain,3>({{0,env1[1],env0[0]}}),READ_OP_CONTEXT(rel_54_FormalParam_op_ctxt)).empty()) && (!rel_59_ThisVar->contains(Tuple<RamDomain,2>({{env1[1],env0[0]}}),READ_OP_CONTEXT(rel_59_ThisVar_op_ctxt))))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[0]) + symTable.resolve(RamDomain(30)))),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0])}});
rel_61_Instruction_VarDeclaringMethod->insert(tuple,READ_OP_CONTEXT(rel_61_Instruction_VarDeclaringMethod_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_61_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_60_Var_DeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_54_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_59_ThisVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_9_RefTypeVar->purge();
}();
/* END STRATUM 54 */
/* BEGIN STRATUM 55 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 55 */
/* BEGIN STRATUM 56 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_VarDeclaringMethod(insn,method,var),
   FirstUse(next,_,var,method).
in file ../may-null/rules.dl [88:1-90:32])_");
if (!rel_25_FirstUse->empty()&&!rel_61_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_61_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
CREATE_OP_CONTEXT(rel_61_Instruction_VarDeclaringMethod_op_ctxt,rel_61_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt,rel_63_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[2],env0[1]}});
auto range = rel_25_FirstUse->equalRange_12(key,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_63_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_FormalParam(insn,method,_,_),
   Method_FirstInstruction(method,next).
in file ../may-null/rules.dl [119:1-121:39])_");
if (!rel_55_Instruction_FormalParam->empty()&&!rel_62_Method_FirstInstruction->empty()) [&](){
auto part = rel_55_Instruction_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt,rel_63_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_62_Method_FirstInstruction_op_ctxt,rel_62_Method_FirstInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_62_Method_FirstInstruction->equalRange_1(key,READ_OP_CONTEXT(rel_62_Method_FirstInstruction_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_63_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_62_Method_FirstInstruction->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_25_FirstUse->purge();
}();
/* END STRATUM 56 */
/* BEGIN STRATUM 57 */
[&]() {
SignalHandler::instance()->setMsg(R"_(SpecialIfEdge(beforeIf,next,var) :- 
   FalseBranch(ifIns,next),
   MayPredecessorModuloThrow(beforeIf,ifIns),
   MayNull_IfInstructionsCond(ifIns,var,"null",_).
in file ../may-null/rules.dl [94:1-97:51])_");
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()&&!rel_63_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_37_FalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt,rel_63_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_64_SpecialIfEdge_op_ctxt,rel_64_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_63_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env0[0],0,RamDomain(23),0}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_5(key,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[1])}});
rel_64_SpecialIfEdge->insert(tuple,READ_OP_CONTEXT(rel_64_SpecialIfEdge_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_39_MayNull_IfInstructionsCond->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_37_FalseBranch->purge();
}();
/* END STRATUM 57 */
/* BEGIN STRATUM 58 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 58 */
/* BEGIN STRATUM 59 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 59 */
/* BEGIN STRATUM 60 */
[&]() {
SignalHandler::instance()->setMsg(R"_(VarMayPointToNull(var) :- 
   VarPointsTo(_,alloc,_,var),
   "<<null pseudo heap>>" contains alloc.
in file ../rules.dl [1:1-3:40])_");
if (!rel_66_VarPointsTo->empty()) [&](){
auto part = rel_66_VarPointsTo->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_67_VarMayPointToNull_op_ctxt,rel_67_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_66_VarPointsTo_op_ctxt,rel_66_VarPointsTo->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(8))) != std::string::npos)) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[3])}});
rel_67_VarMayPointToNull->insert(tuple,READ_OP_CONTEXT(rel_67_VarMayPointToNull_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_66_VarPointsTo->purge();
}();
/* END STRATUM 60 */
/* BEGIN STRATUM 61 */
[&]() {
SignalHandler::instance()->setMsg(R"_(VarCannotBeNull(var) :- 
   VarMayPointToNull(var),
   _LoadStaticField(_,_,var,out,_),
   out = "<java.lang.System: java.io.PrintStream out>".
in file ../may_rules.dl [9:1-12:53])_");
if (!rel_67_VarMayPointToNull->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_67_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt,rel_68_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_67_VarMayPointToNull_op_ctxt,rel_67_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],RamDomain(44),0}});
auto range = rel_17_LoadStaticField->equalRange_12(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_68_VarCannotBeNull->insert(tuple,READ_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarCannotBeNull(var) :- 
   VarMayPointToNull(var),
   _LoadStaticField(_,_,var,out,_),
   out = "<java.lang.System: java.io.PrintStream err>".
in file ../may_rules.dl [14:1-17:53])_");
if (!rel_67_VarMayPointToNull->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_67_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt,rel_68_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_67_VarMayPointToNull_op_ctxt,rel_67_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],RamDomain(45),0}});
auto range = rel_17_LoadStaticField->equalRange_12(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_68_VarCannotBeNull->insert(tuple,READ_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_17_LoadStaticField->purge();
}();
/* END STRATUM 61 */
/* BEGIN STRATUM 62 */
[&]() {
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   _AssignCastNull(_,_,var,_,_).
in file ../rules.dl [6:1-6:54])_");
if (!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_11_AssignCastNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[2])}});
rel_69_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   VarMayPointToNull(var),
   !VarCannotBeNull(var).
in file ../may_rules.dl [4:1-6:23])_");
if (!rel_67_VarMayPointToNull->empty()) [&](){
auto part = rel_67_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt,rel_68_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_67_VarMayPointToNull_op_ctxt,rel_67_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( !rel_68_VarCannotBeNull->contains(Tuple<RamDomain,1>({{env0[0]}}),READ_OP_CONTEXT(rel_68_VarCannotBeNull_op_ctxt))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_69_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_69_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_11_AssignCastNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_67_VarMayPointToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_68_VarCannotBeNull->purge();
}();
/* END STRATUM 62 */
/* BEGIN STRATUM 63 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_70_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 63 */
/* BEGIN STRATUM 64 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_71_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 64 */
/* BEGIN STRATUM 65 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 65 */
/* BEGIN STRATUM 66 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_73_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 66 */
/* BEGIN STRATUM 67 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_74_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 67 */
/* BEGIN STRATUM 68 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_75_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 68 */
/* BEGIN STRATUM 69 */
[&]() {
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw NullPointerException","throw NPE",a) :- 
   CallGraphEdge(_,a,_,b),
   _SpecialMethodInvocation(a,index,b,_,meth),
   "java.lang.NullPointerException" contains a.
in file ../rules.dl [9:1-12:48])_");
if (!rel_43_CallGraphEdge->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_43_CallGraphEdge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(11))) != std::string::npos)) {
const Tuple<RamDomain,5> key({{env0[1],0,env0[3],0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_5(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(9)),static_cast<RamDomain>(RamDomain(10)),static_cast<RamDomain>(env0[1])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Array Index",var,insn) :- 
   VarPointsToNull(var),
   _LoadArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [17:1-19:44])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_74_LoadArrayIndex->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_74_LoadArrayIndex_op_ctxt,rel_74_LoadArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_74_LoadArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_74_LoadArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(12)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Array Index",var,insn) :- 
   VarPointsToNull(var),
   _StoreArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [24:1-26:45])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_21_StoreArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(13)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _StoreInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [31:1-33:51])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_22_StoreInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(14)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _LoadInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [38:1-40:50])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_16_LoadInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(15)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Virtual Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _VirtualMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [45:1-47:53])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(16)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Special Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _SpecialMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [52:1-54:53])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(17)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Unary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignUnop(insn,index,_,meth),
   _AssignOperFrom(insn,_,var).
in file ../rules.dl [59:1-62:31])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_71_AssignOperFrom->empty()&&!rel_72_AssignUnop->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_71_AssignOperFrom_op_ctxt,rel_71_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_72_AssignUnop_op_ctxt,rel_72_AssignUnop->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_72_AssignUnop) {
const Tuple<RamDomain,3> key({{env1[0],0,env0[0]}});
auto range = rel_71_AssignOperFrom->equalRange_5(key,READ_OP_CONTEXT(rel_71_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(18)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Binary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignBinop(insn,index,_,meth),
   _AssignOperFrom(insn,_,var).
in file ../rules.dl [67:1-70:31])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_70_AssignBinop->empty()&&!rel_71_AssignOperFrom->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_70_AssignBinop_op_ctxt,rel_70_AssignBinop->createContext());
CREATE_OP_CONTEXT(rel_71_AssignOperFrom_op_ctxt,rel_71_AssignOperFrom->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_70_AssignBinop) {
const Tuple<RamDomain,3> key({{env1[0],0,env0[0]}});
auto range = rel_71_AssignOperFrom->equalRange_5(key,READ_OP_CONTEXT(rel_71_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(19)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw Null","throw null",insn) :- 
   _ThrowNull(insn,index,meth).
in file ../rules.dl [73:1-74:31])_");
if (!rel_75_ThrowNull->empty()) [&](){
auto part = rel_75_ThrowNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_ThrowNull_op_ctxt,rel_75_ThrowNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(20)),static_cast<RamDomain>(RamDomain(21)),static_cast<RamDomain>(env0[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Enter Monitor (Synchronized)",var,insn) :- 
   VarPointsToNull(var),
   _EnterMonitor(insn,index,var,meth).
in file ../rules.dl [79:1-81:39])_");
if (!rel_69_VarPointsToNull->empty()&&!rel_73_EnterMonitor->empty()) [&](){
auto part = rel_69_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_73_EnterMonitor_op_ctxt,rel_73_EnterMonitor->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_73_EnterMonitor->equalRange_4(key,READ_OP_CONTEXT(rel_73_EnterMonitor_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(22)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_76_NullAt->insert(tuple,READ_OP_CONTEXT(rel_76_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_74_LoadArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_21_StoreArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_16_LoadInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_22_StoreInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_75_ThrowNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_72_AssignUnop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_70_AssignBinop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_71_AssignOperFrom->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_73_EnterMonitor->purge();
}();
/* END STRATUM 69 */
/* BEGIN STRATUM 70 */
[&]() {
SignalHandler::instance()->setMsg(R"_(ReachableNullAtLine(meth,index,file,line,type,var,insn) :- 
   NullAt(meth,index,type,var,insn),
   ApplicationMethod(meth),
   InstructionLine(meth,index,line,file).
in file ../rules.dl [85:1-88:42])_");
if (!rel_27_ApplicationMethod->empty()&&!rel_65_InstructionLine->empty()&&!rel_76_NullAt->empty()) [&](){
auto part = rel_76_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_65_InstructionLine_op_ctxt,rel_65_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt,rel_77_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env0[0],env0[1],0,0}});
auto range = rel_65_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_65_InstructionLine_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_77_ReachableNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./ReachableNullAtLine.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_77_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 70 */
/* BEGIN STRATUM 71 */
[&]() {
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   BasicBlockHead(ins,headIns),
   MayPredecessorModuloThrow(ifIns,headIns).
in file ../path_sensitivity/rules.dl [8:1-11:43])_");
if (!rel_29_BasicBlockHead->empty()&&!rel_63_MayPredecessorModuloThrow->empty()&&!rel_77_ReachableNullAtLine->empty()) [&](){
auto part = rel_77_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_78_IfInstructions_op_ctxt,rel_78_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt,rel_63_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt,rel_77_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{0,env1[1]}});
auto range = rel_63_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env2[0])}});
rel_78_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_78_IfInstructions_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,var,ins),
   _IfVar(ifIns,_,var).
in file ../path_sensitivity/rules.dl [14:1-16:23])_");
if (!rel_77_ReachableNullAtLine->empty()&&!rel_31_IfVar->empty()) [&](){
auto part = rel_77_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_78_IfInstructions_op_ctxt,rel_78_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt,rel_77_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[5]}});
auto range = rel_31_IfVar->equalRange_4(key,READ_OP_CONTEXT(rel_31_IfVar_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env1[0])}});
rel_78_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_78_IfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_31_IfVar->purge();
}();
/* END STRATUM 71 */
/* BEGIN STRATUM 72 */
[&]() {
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   varLeft = "null".
in file ../path_sensitivity/rules.dl [31:1-35:18])_");
if (!rel_78_IfInstructions->empty()&&!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_78_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_78_IfInstructions_op_ctxt,rel_78_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,RamDomain(23)}});
auto range = rel_35_IfNull->equalRange_5(key,READ_OP_CONTEXT(rel_35_IfNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_79_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   VarPointsToNull(varLeft).
in file ../path_sensitivity/rules.dl [37:1-41:26])_");
if (!rel_78_IfInstructions->empty()&&!rel_69_VarPointsToNull->empty()&&!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_78_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_78_IfInstructions_op_ctxt,rel_78_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,0}});
auto range = rel_35_IfNull->equalRange_1(key,READ_OP_CONTEXT(rel_35_IfNull_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_69_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_79_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_38_OperatorAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_35_IfNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_78_IfInstructions->purge();
}();
/* END STRATUM 72 */
/* BEGIN STRATUM 73 */
[&]() {
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"!="),
   ord(left) != ord(right).
in file ../path_sensitivity/rules.dl [43:1-45:25])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(24)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) != (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"=="),
   ord(left) = ord(right).
in file ../path_sensitivity/rules.dl [47:1-49:24])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(25)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) == (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<="),
   ord(left) <= ord(right).
in file ../path_sensitivity/rules.dl [51:1-53:25])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(26)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) <= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">="),
   ord(left) >= ord(right).
in file ../path_sensitivity/rules.dl [55:1-57:25])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(27)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) >= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<"),
   ord(left) < ord(right).
in file ../path_sensitivity/rules.dl [59:1-61:24])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(28)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) < (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">"),
   ord(left) > ord(right).
in file ../path_sensitivity/rules.dl [63:1-65:24])_");
if (!rel_79_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(29)}});
auto range = rel_79_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_79_IfInstructionsCond_op_ctxt,rel_79_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) > (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_80_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_80_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_79_IfInstructionsCond->purge();
}();
/* END STRATUM 73 */
/* BEGIN STRATUM 74 */
[&]() {
SignalHandler::instance()->setMsg(R"_(DominatesUnreachable(ifIns,ins) :- 
   TrueIfInstructions(ifIns),
   Instruction_Next(ifIns,ins).
in file ../path_sensitivity/rules.dl [67:1-69:30])_");
if (!rel_30_Instruction_Next->empty()&&!rel_80_TrueIfInstructions->empty()) [&](){
auto part = rel_80_TrueIfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_DominatesUnreachable_op_ctxt,rel_81_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_80_TrueIfInstructions_op_ctxt,rel_80_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_81_DominatesUnreachable->insert(tuple,READ_OP_CONTEXT(rel_81_DominatesUnreachable_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_81_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_30_Instruction_Next->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_80_TrueIfInstructions->purge();
}();
/* END STRATUM 74 */
/* BEGIN STRATUM 75 */
[&]() {
SignalHandler::instance()->setMsg(R"_(UnreachablePathNPEIns(ins) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   DominatesUnreachable(_,parent),
   BasicBlockHead(ins,headIns),
   Dominates(parent,headIns).
in file ../path_sensitivity/rules.dl [75:1-79:28])_");
if (!rel_29_BasicBlockHead->empty()&&!rel_52_Dominates->empty()&&!rel_81_DominatesUnreachable->empty()&&!rel_77_ReachableNullAtLine->empty()) [&](){
auto part = rel_77_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_52_Dominates_op_ctxt,rel_52_Dominates->createContext());
CREATE_OP_CONTEXT(rel_81_DominatesUnreachable_op_ctxt,rel_81_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt,rel_77_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_82_UnreachablePathNPEIns_op_ctxt,rel_82_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_81_DominatesUnreachable) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{env1[1],env2[1]}});
auto range = rel_52_Dominates->equalRange_3(key,READ_OP_CONTEXT(rel_52_Dominates_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[6])}});
rel_82_UnreachablePathNPEIns->insert(tuple,READ_OP_CONTEXT(rel_82_UnreachablePathNPEIns_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_82_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_29_BasicBlockHead->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_52_Dominates->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_81_DominatesUnreachable->purge();
}();
/* END STRATUM 75 */
/* BEGIN STRATUM 76 */
[&]() {
SignalHandler::instance()->setMsg(R"_(PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- 
   ReachableNullAtLine(meth,index,file,line,type,var,ins),
   !UnreachablePathNPEIns(ins).
in file ../path_sensitivity/rules.dl [81:1-83:29])_");
if (!rel_77_ReachableNullAtLine->empty()) [&](){
auto part = rel_77_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_83_PathSensitiveNullAtLine_op_ctxt,rel_83_PathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_77_ReachableNullAtLine_op_ctxt,rel_77_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_82_UnreachablePathNPEIns_op_ctxt,rel_82_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( !rel_82_UnreachablePathNPEIns->contains(Tuple<RamDomain,1>({{env0[6]}}),READ_OP_CONTEXT(rel_82_UnreachablePathNPEIns_op_ctxt))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_83_PathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_83_PathSensitiveNullAtLine_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./PathSensitiveNullAtLine.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_77_ReachableNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_82_UnreachablePathNPEIns->purge();
}();
/* END STRATUM 76 */
/* BEGIN STRATUM 77 */
[&]() {
SignalHandler::instance()->setMsg(R"_(MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- 
   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).
in file ../may-null/rules.dl [329:1-331:70])_");
if (!rel_83_PathSensitiveNullAtLine->empty()) [&](){
auto part = rel_83_PathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_84_MinPathSensitiveNullAtLine_op_ctxt,rel_84_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_83_PathSensitiveNullAtLine_op_ctxt,rel_83_PathSensitiveNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,7> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,7> key({{env0[0],0,env0[2],0,0,env0[5],0}});
auto range = rel_83_PathSensitiveNullAtLine->equalRange_37(key,READ_OP_CONTEXT(rel_83_PathSensitiveNullAtLine_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_84_MinPathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_84_MinPathSensitiveNullAtLine_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_83_PathSensitiveNullAtLine->purge();
}();
/* END STRATUM 77 */
/* BEGIN STRATUM 78 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNumConstant.facts"},{"name","_AssignNumConstant"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_85_AssignNumConstant);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 78 */
/* BEGIN STRATUM 79 */
[&]() {
SignalHandler::instance()->setMsg(R"_(ReturnFalse(retInsn,invokedMethod) :- 
   _Return(retInsn,_,returnVar,invokedMethod),
   _AssignNumConstant(_,_,"0",returnVar,invokedMethod).
in file ../may-null/rules.dl [227:1-229:57])_");
if (!rel_85_AssignNumConstant->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_20_Return->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_86_ReturnFalse_op_ctxt,rel_86_ReturnFalse->createContext());
CREATE_OP_CONTEXT(rel_85_AssignNumConstant_op_ctxt,rel_85_AssignNumConstant->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,RamDomain(38),env0[2],env0[3]}});
auto range = rel_85_AssignNumConstant->equalRange_28(key,READ_OP_CONTEXT(rel_85_AssignNumConstant_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3])}});
rel_86_ReturnFalse->insert(tuple,READ_OP_CONTEXT(rel_86_ReturnFalse_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_85_AssignNumConstant->purge();
}();
/* END STRATUM 79 */
/* BEGIN STRATUM 80 */
[&]() {
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"declaration") :- 
   Instruction_VarDeclaringMethod(insn,method,var).
in file ../may-null/rules.dl [125:1-126:51])_");
if (!rel_61_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_61_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_61_Instruction_VarDeclaringMethod_op_ctxt,rel_61_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(32))}});
rel_87_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"assignNull") :- 
   _AssignNull(insn,_,var,method).
in file ../may-null/rules.dl [129:1-130:35])_");
if (!rel_15_AssignNull->empty()) [&](){
auto part = rel_15_AssignNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(RamDomain(33))}});
rel_87_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();rel_88_delta_MayNullPtr->insertAll(*rel_87_MayNullPtr);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignLocal(next,_,from,to,method).
in file ../may-null/rules.dl [143:1-148:3])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_21(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( !rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignCast(_,_,from,to,_,method).
in file ../may-null/rules.dl [143:1-148:3])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( !rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   MayPredecessorModuloThrow(insn,next),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method),
   !ParamInBoolBranch(insn,var).
in file ../may-null/rules.dl [154:1-163:31])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_63_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt,rel_63_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( ((!rel_42_ParamInBoolBranch->contains(Tuple<RamDomain,2>({{env0[0],env0[1]}}),READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt))) && (rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty()))) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_63_MayPredecessorModuloThrow->equalRange_1(key,READ_OP_CONTEXT(rel_63_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
if( ((((!rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt))) && (!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt))))) && (!rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   SpecialIfEdge(insn,next,var),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method),
   !ParamInBoolBranch(insn,var).
in file ../may-null/rules.dl [154:1-163:31])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_64_SpecialIfEdge->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_64_SpecialIfEdge_op_ctxt,rel_64_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( ((!rel_42_ParamInBoolBranch->contains(Tuple<RamDomain,2>({{env0[0],env0[1]}}),READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt))) && (rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty()))) {
const Tuple<RamDomain,3> key({{env0[0],0,env0[1]}});
auto range = rel_64_SpecialIfEdge->equalRange_5(key,READ_OP_CONTEXT(rel_64_SpecialIfEdge_op_ctxt));
for(const auto& env1 : range) {
if( ((((!rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt))) && (!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt))))) && (!rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Return") :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod,_),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method),
   !NextInsideHasNext(insn,to).
in file ../may-null/rules.dl [167:1-172:30])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( ((!rel_53_NextInsideHasNext->contains(Tuple<RamDomain,2>({{env2[1],env3[2]}}),READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt))) && (!rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env2[1],env3[2],env3[3],RamDomain(36)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3]),static_cast<RamDomain>(RamDomain(36))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Parameter") :- 
   MayNullPtr(from_insn,from,_,_),
   _ActualParam(index,from_insn,from),
   CallGraphEdge(_,from_insn,_,method),
   Instruction_FormalParam(insn,method,to,index).
in file ../may-null/rules.dl [175:1-179:50])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_55_Instruction_FormalParam->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],env0[1]}});
auto range = rel_19_ActualParam->equalRange_6(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{0,env0[0],0,0}});
auto range = rel_43_CallGraphEdge->equalRange_2(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{0,env2[3],0,env1[0]}});
auto range = rel_55_Instruction_FormalParam->equalRange_10(key,READ_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt));
for(const auto& env3 : range) {
if( !rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env3[0],env3[2],env2[3],RamDomain(37)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(RamDomain(37))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"Boolean If") :- 
   MayNullPtr(returnInsn,paramVar,callee,_),
   ReturnFalse(returnInsn,callee),
   isParam(index,paramVar,callee),
   BoolFalseBranch(ifIns,insn),
   BoolIfVarInvoke(from_insn,ifIns,_),
   _ActualParam(index,from_insn,var),
   AssignReturnValue_WithInvoke(from_insn,_,_,method).
in file ../may-null/rules.dl [235:1-242:55])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_40_BoolFalseBranch->empty()&&!rel_33_BoolIfVarInvoke->empty()&&!rel_86_ReturnFalse->empty()&&!rel_19_ActualParam->empty()&&!rel_56_isParam->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_86_ReturnFalse_op_ctxt,rel_86_ReturnFalse->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],env0[2]}});
auto range = rel_86_ReturnFalse->equalRange_3(key,READ_OP_CONTEXT(rel_86_ReturnFalse_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,3> key({{0,env0[1],env0[2]}});
auto range = rel_56_isParam->equalRange_6(key,READ_OP_CONTEXT(rel_56_isParam_op_ctxt));
for(const auto& env2 : range) {
for(const auto& env3 : *rel_40_BoolFalseBranch) {
const Tuple<RamDomain,3> key({{0,env3[0],0}});
auto range = rel_33_BoolIfVarInvoke->equalRange_2(key,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,3> key({{env2[0],env4[0],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env5 : range) {
const Tuple<RamDomain,4> key({{env4[0],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env6 : range) {
if( !rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env3[1],env5[2],env6[3],RamDomain(40)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env3[1]),static_cast<RamDomain>(env5[2]),static_cast<RamDomain>(env6[3]),static_cast<RamDomain>(RamDomain(40))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Return") :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod,_),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method),
   !NextInsideHasNext(insn,to).
in file ../may-null/rules.dl [245:1-250:30])_");
if (!rel_88_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_88_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_delta_MayNullPtr_op_ctxt,rel_88_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt,rel_89_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( ((!rel_53_NextInsideHasNext->contains(Tuple<RamDomain,2>({{env2[1],env3[2]}}),READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt))) && (!rel_87_MayNullPtr->contains(Tuple<RamDomain,4>({{env2[1],env3[2],env3[3],RamDomain(36)}}),READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3]),static_cast<RamDomain>(RamDomain(36))}});
rel_89_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_89_new_MayNullPtr_op_ctxt));
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if(rel_89_new_MayNullPtr->empty()) break;
rel_87_MayNullPtr->insertAll(*rel_89_new_MayNullPtr);
std::swap(rel_88_delta_MayNullPtr, rel_89_new_MayNullPtr);
rel_89_new_MayNullPtr->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_88_delta_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_89_new_MayNullPtr->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_63_MayPredecessorModuloThrow->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_10_AssignCast->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_15_AssignNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_13_AssignLocal->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_20_Return->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_61_Instruction_VarDeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_55_Instruction_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_14_DefineVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_26_LastUse->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_5_AssignReturnValue_WithInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_64_SpecialIfEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_49_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_53_NextInsideHasNext->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_33_BoolIfVarInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_42_ParamInBoolBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_40_BoolFalseBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_86_ReturnFalse->purge();
}();
/* END STRATUM 80 */
/* BEGIN STRATUM 81 */
[&]() {
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   NullAt(method,_,_,var,insn),
   MayNullPtr(insn,var,method,_),
   isParam(index,var,method).
in file ../may-null/rules.dl [355:1-358:29])_");
if (!rel_87_MayNullPtr->empty()&&!rel_76_NullAt->empty()&&!rel_56_isParam->empty()) [&](){
auto part = rel_76_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_90_MethodDerefArg_op_ctxt,rel_90_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_76_NullAt_op_ctxt,rel_76_NullAt->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[4],env0[3],env0[0],0}});
auto range = rel_87_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,3> key({{0,env0[3],env0[0]}});
auto range = rel_56_isParam->equalRange_6(key,READ_OP_CONTEXT(rel_56_isParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env2[0]),static_cast<RamDomain>(env0[0])}});
rel_90_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_90_MethodDerefArg_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();rel_91_delta_MethodDerefArg->insertAll(*rel_90_MethodDerefArg);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   MethodDerefArg(invokedIndex,invokedMethod),
   CallGraphEdge(_,from_insn,_,invokedMethod),
   _ActualParam(invokedIndex,from_insn,from),
   isParam(index,from,method).
in file ../may-null/rules.dl [360:1-364:30])_");
if (!rel_91_delta_MethodDerefArg->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_19_ActualParam->empty()&&!rel_56_isParam->empty()) [&](){
auto part = rel_91_delta_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_91_delta_MethodDerefArg_op_ctxt,rel_91_delta_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_92_new_MethodDerefArg_op_ctxt,rel_92_new_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_90_MethodDerefArg_op_ctxt,rel_90_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_56_isParam_op_ctxt,rel_56_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,3> key({{0,env2[2],0}});
auto range = rel_56_isParam->equalRange_2(key,READ_OP_CONTEXT(rel_56_isParam_op_ctxt));
for(const auto& env3 : range) {
if( !rel_90_MethodDerefArg->contains(Tuple<RamDomain,2>({{env3[0],env3[2]}}),READ_OP_CONTEXT(rel_90_MethodDerefArg_op_ctxt))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2])}});
rel_92_new_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_92_new_MethodDerefArg_op_ctxt));
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if(rel_92_new_MethodDerefArg->empty()) break;
rel_90_MethodDerefArg->insertAll(*rel_92_new_MethodDerefArg);
std::swap(rel_91_delta_MethodDerefArg, rel_92_new_MethodDerefArg);
rel_92_new_MethodDerefArg->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_91_delta_MethodDerefArg->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_92_new_MethodDerefArg->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_76_NullAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_56_isParam->purge();
}();
/* END STRATUM 81 */
/* BEGIN STRATUM 82 */
[&]() {
SignalHandler::instance()->setMsg(R"_(JDKFunctionSummary(index,method) :- 
   MethodDerefArg(index,method),
   !ApplicationMethod(method).
in file ../may-null/rules.dl [366:1-368:28])_");
if (!rel_90_MethodDerefArg->empty()) [&](){
auto part = rel_90_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_93_JDKFunctionSummary_op_ctxt,rel_93_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_90_MethodDerefArg_op_ctxt,rel_90_MethodDerefArg->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( !rel_27_ApplicationMethod->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_93_JDKFunctionSummary->insert(tuple,READ_OP_CONTEXT(rel_93_JDKFunctionSummary_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_93_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_90_MethodDerefArg->purge();
}();
/* END STRATUM 82 */
/* BEGIN STRATUM 83 */
[&]() {
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(meth,index,file,line,type,var,insn) :- 
   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   MayNullPtr(insn,var,meth,_).
in file ../may-null/rules.dl [334:1-336:32])_");
if (!rel_87_MayNullPtr->empty()&&!rel_84_MinPathSensitiveNullAtLine->empty()) [&](){
auto part = rel_84_MinPathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_84_MinPathSensitiveNullAtLine_op_ctxt,rel_84_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt,rel_94_NPEWithMayNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[6],env0[5],env0[0],0}});
auto range = rel_87_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_94_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _StaticMethodInvocation(from_insn,index,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [371:1-384:31])_");
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_65_InstructionLine->empty()&&!rel_93_JDKFunctionSummary->empty()&&!rel_87_MayNullPtr->empty()&&!rel_69_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_93_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_65_InstructionLine_op_ctxt,rel_65_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_93_JDKFunctionSummary_op_ctxt,rel_93_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt,rel_94_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_69_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_87_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],0,0,env4[2]}});
auto range = rel_3_StaticMethodInvocation->equalRange_9(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_65_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_65_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(43)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_94_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt));
}
}
}
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _VirtualMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [371:1-384:31])_");
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_65_InstructionLine->empty()&&!rel_93_JDKFunctionSummary->empty()&&!rel_87_MayNullPtr->empty()&&!rel_69_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_93_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_65_InstructionLine_op_ctxt,rel_65_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_93_JDKFunctionSummary_op_ctxt,rel_93_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt,rel_94_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_69_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_87_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_4_VirtualMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_65_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_65_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(43)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_94_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt));
}
}
}
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _SpecialMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [371:1-384:31])_");
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_65_InstructionLine->empty()&&!rel_93_JDKFunctionSummary->empty()&&!rel_87_MayNullPtr->empty()&&!rel_69_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_93_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_65_InstructionLine_op_ctxt,rel_65_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_93_JDKFunctionSummary_op_ctxt,rel_93_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt,rel_87_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt,rel_94_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt,rel_69_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_69_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_69_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_87_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_87_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_2_SpecialMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_65_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_65_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(43)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_94_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_94_NPEWithMayNull_op_ctxt));
}
}
}
}
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_65_InstructionLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_43_CallGraphEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_27_ApplicationMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_SpecialMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_VirtualMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_StaticMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_19_ActualParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_69_VarPointsToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_87_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_84_MinPathSensitiveNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_93_JDKFunctionSummary->purge();
}();
/* END STRATUM 83 */
/* BEGIN STRATUM 84 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_95_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 84 */
/* BEGIN STRATUM 85 */
[&]() {
SignalHandler::instance()->setMsg(R"_(TrueBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   JumpTarget(insn,ifIns).
in file ../may-null/rules.dl [267:1-269:25])_");
if (!rel_95_JumpTarget->empty()&&!rel_36_MayNull_IfInstruction->empty()) [&](){
auto part = rel_36_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_95_JumpTarget_op_ctxt,rel_95_JumpTarget->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_96_TrueBranch_op_ctxt,rel_96_TrueBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_95_JumpTarget->equalRange_2(key,READ_OP_CONTEXT(rel_95_JumpTarget_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_96_TrueBranch->insert(tuple,READ_OP_CONTEXT(rel_96_TrueBranch_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_96_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_95_JumpTarget->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_36_MayNull_IfInstruction->purge();
}();
/* END STRATUM 85 */
/* BEGIN STRATUM 86 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}();
/* END STRATUM 86 */

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_AssignReturnValue:\n";
rel_1_AssignReturnValue->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_SpecialMethodInvocation:\n";
rel_2_SpecialMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_StaticMethodInvocation:\n";
rel_3_StaticMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_VirtualMethodInvocation:\n";
rel_4_VirtualMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_AssignReturnValue_WithInvoke:\n";
rel_5_AssignReturnValue_WithInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_IterNextInsn:\n";
rel_6_IterNextInsn->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_Primitive:\n";
rel_7_Primitive->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_Var_Type:\n";
rel_8_Var_Type->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_RefTypeVar:\n";
rel_9_RefTypeVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_AssignCast:\n";
rel_10_AssignCast->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_11_AssignCastNull:\n";
rel_11_AssignCastNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_12_AssignHeapAllocation:\n";
rel_12_AssignHeapAllocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_13_AssignLocal:\n";
rel_13_AssignLocal->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_14_DefineVar:\n";
rel_14_DefineVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_15_AssignNull:\n";
rel_15_AssignNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_16_LoadInstanceField:\n";
rel_16_LoadInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_17_LoadStaticField:\n";
rel_17_LoadStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_18_VarDef:\n";
rel_18_VarDef->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_19_ActualParam:\n";
rel_19_ActualParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_20_Return:\n";
rel_20_Return->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_21_StoreArrayIndex:\n";
rel_21_StoreArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_22_StoreInstanceField:\n";
rel_22_StoreInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_23_StoreStaticField:\n";
rel_23_StoreStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_24_AllUse:\n";
rel_24_AllUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_25_FirstUse:\n";
rel_25_FirstUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_26_LastUse:\n";
rel_26_LastUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_27_ApplicationMethod:\n";
rel_27_ApplicationMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_28_AssignMayNull:\n";
rel_28_AssignMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_29_BasicBlockHead:\n";
rel_29_BasicBlockHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_30_Instruction_Next:\n";
rel_30_Instruction_Next->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_31_IfVar:\n";
rel_31_IfVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_32_BoolIf:\n";
rel_32_BoolIf->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_33_BoolIfVarInvoke:\n";
rel_33_BoolIfVarInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_34_hasNextIf:\n";
rel_34_hasNextIf->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_35_IfNull:\n";
rel_35_IfNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_36_MayNull_IfInstruction:\n";
rel_36_MayNull_IfInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_37_FalseBranch:\n";
rel_37_FalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_38_OperatorAt:\n";
rel_38_OperatorAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_39_MayNull_IfInstructionsCond:\n";
rel_39_MayNull_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_40_BoolFalseBranch:\n";
rel_40_BoolFalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_41_BoolTrueBranch:\n";
rel_41_BoolTrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_42_ParamInBoolBranch:\n";
rel_42_ParamInBoolBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_43_CallGraphEdge:\n";
rel_43_CallGraphEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_44_PhiNodeHead:\n";
rel_44_PhiNodeHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_45_PhiNodeHeadVar:\n";
rel_45_PhiNodeHeadVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_46_CanBeNullBranch:\n";
rel_46_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_47_delta_CanBeNullBranch:\n";
rel_47_delta_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_48_new_CanBeNullBranch:\n";
rel_48_new_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_49_CannotBeNullBranch:\n";
rel_49_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_50_delta_CannotBeNullBranch:\n";
rel_50_delta_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_51_new_CannotBeNullBranch:\n";
rel_51_new_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_52_Dominates:\n";
rel_52_Dominates->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_53_NextInsideHasNext:\n";
rel_53_NextInsideHasNext->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_54_FormalParam:\n";
rel_54_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_55_Instruction_FormalParam:\n";
rel_55_Instruction_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_56_isParam:\n";
rel_56_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_57_delta_isParam:\n";
rel_57_delta_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_58_new_isParam:\n";
rel_58_new_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_59_ThisVar:\n";
rel_59_ThisVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_60_Var_DeclaringMethod:\n";
rel_60_Var_DeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_61_Instruction_VarDeclaringMethod:\n";
rel_61_Instruction_VarDeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_62_Method_FirstInstruction:\n";
rel_62_Method_FirstInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_63_MayPredecessorModuloThrow:\n";
rel_63_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_64_SpecialIfEdge:\n";
rel_64_SpecialIfEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_65_InstructionLine:\n";
rel_65_InstructionLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_66_VarPointsTo:\n";
rel_66_VarPointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_67_VarMayPointToNull:\n";
rel_67_VarMayPointToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_68_VarCannotBeNull:\n";
rel_68_VarCannotBeNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_69_VarPointsToNull:\n";
rel_69_VarPointsToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_70_AssignBinop:\n";
rel_70_AssignBinop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_71_AssignOperFrom:\n";
rel_71_AssignOperFrom->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_72_AssignUnop:\n";
rel_72_AssignUnop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_73_EnterMonitor:\n";
rel_73_EnterMonitor->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_74_LoadArrayIndex:\n";
rel_74_LoadArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_75_ThrowNull:\n";
rel_75_ThrowNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_76_NullAt:\n";
rel_76_NullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_77_ReachableNullAtLine:\n";
rel_77_ReachableNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_78_IfInstructions:\n";
rel_78_IfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_79_IfInstructionsCond:\n";
rel_79_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_80_TrueIfInstructions:\n";
rel_80_TrueIfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_81_DominatesUnreachable:\n";
rel_81_DominatesUnreachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_82_UnreachablePathNPEIns:\n";
rel_82_UnreachablePathNPEIns->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_83_PathSensitiveNullAtLine:\n";
rel_83_PathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_84_MinPathSensitiveNullAtLine:\n";
rel_84_MinPathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_85_AssignNumConstant:\n";
rel_85_AssignNumConstant->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_86_ReturnFalse:\n";
rel_86_ReturnFalse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_87_MayNullPtr:\n";
rel_87_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_88_delta_MayNullPtr:\n";
rel_88_delta_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_89_new_MayNullPtr:\n";
rel_89_new_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_90_MethodDerefArg:\n";
rel_90_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_91_delta_MethodDerefArg:\n";
rel_91_delta_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_92_new_MethodDerefArg:\n";
rel_92_new_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_93_JDKFunctionSummary:\n";
rel_93_JDKFunctionSummary->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_94_NPEWithMayNull:\n";
rel_94_NPEWithMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_95_JumpTarget:\n";
rel_95_JumpTarget->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_96_TrueBranch:\n";
rel_96_TrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_97_ReachableNullAt:\n";
rel_97_ReachableNullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction(".", ".", stratumIndex, false); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction(inputDirectory, outputDirectory, stratumIndex, true);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_56_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_61_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_69_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./ReachableNullAtLine.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_77_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_80_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_81_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_82_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./PathSensitiveNullAtLine.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_93_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_96_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_59_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_60_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_70_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_71_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_73_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_74_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_75_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNumConstant.facts"},{"name","_AssignNumConstant"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_85_AssignNumConstant);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_95_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_AssignReturnValue");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_SpecialMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_StaticMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_VirtualMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_Var_Type");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_10_AssignCast");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_11_AssignCastNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_12_AssignHeapAllocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_13_AssignLocal");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_15_AssignNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_16_LoadInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_17_LoadStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_19_ActualParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_20_Return");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_21_StoreArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_22_StoreInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_23_StoreStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_27_ApplicationMethod");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_29_BasicBlockHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_IfVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_35_IfNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_38_OperatorAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_43_CallGraphEdge");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_44_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_52_Dominates");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_54_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_59_ThisVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_59_ThisVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_60_Var_DeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_60_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_62_Method_FirstInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_62_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_63_MayPredecessorModuloThrow");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_63_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_65_InstructionLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_65_InstructionLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_66_VarPointsTo");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_66_VarPointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_70_AssignBinop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_70_AssignBinop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_71_AssignOperFrom");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_71_AssignOperFrom);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_72_AssignUnop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_72_AssignUnop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_73_EnterMonitor");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_EnterMonitor);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_74_LoadArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_74_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_75_ThrowNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_75_ThrowNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_85_AssignNumConstant");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_85_AssignNumConstant);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_95_JumpTarget");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_95_JumpTarget);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_5_AssignReturnValue_WithInvoke");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_6_IterNextInsn");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_9_RefTypeVar");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_14_DefineVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_18_VarDef");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_24_AllUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_25_FirstUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_26_LastUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_28_AssignMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_34_hasNextIf");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_36_MayNull_IfInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_FalseBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_39_MayNull_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_44_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_45_PhiNodeHeadVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_46_CanBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_49_CannotBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_53_NextInsideHasNext");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_55_Instruction_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_56_isParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_56_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_61_Instruction_VarDeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_61_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_69_VarPointsToNull");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_69_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_76_NullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_77_ReachableNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_77_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_78_IfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_79_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_80_TrueIfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_80_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_81_DominatesUnreachable");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_81_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_82_UnreachablePathNPEIns");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_82_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_83_PathSensitiveNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_87_MayNullPtr");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_90_MethodDerefArg");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_93_JDKFunctionSummary");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_93_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_94_NPEWithMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_96_TrueBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_96_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_97_ReachableNullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_A(){return new Sf_A;}
SymbolTable *getST_A(SouffleProgram *p){return &reinterpret_cast<Sf_A*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_A: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_A();
};
public:
factory_Sf_A() : ProgramFactory("A"){}
};
static factory_Sf_A __factory_Sf_A_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(main_no_uninit_may.dl)",
R"(.)",
R"(.)",
false,
R"()",
1,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf_A obj;
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
