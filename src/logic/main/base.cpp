
#include "souffle/CompiledSouffle.h"

extern "C" {
}

namespace souffle {
using namespace ram;
struct t_btree_2__0_1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_2_1_3_4__3_0_1_2_4__1__5__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_2_1_3_4__3_0_1_2_4__1__5__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__1 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4__3_0_1_2_4__1__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4__3_0_1_2_4__1__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__2_0_1_3__1__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__2_0_1_3__1__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0__1 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,0,1,3,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__3_0_1_2_4__0_2_4_1_3__1__4__8__21 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,2,4,1,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__3_0_1_2_4__0_2_4_1_3__1__4__8__21& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t) const {
context h;
return equalRange_21(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,4,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_1_3__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_1_3__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__2_1_0__4__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,1,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__2_1_0__4__6& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_3_1__4__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_3_1__4__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_3_0_1__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_3_0_1__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_3_1__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_3_1__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_0_2_3__3_0_1_2__2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_0_2_3__3_0_1_2__2__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,0,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__1_0__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__1_0__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__6& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_3_0_2__10 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,3,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_3_0_2__10& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t) const {
context h;
return equalRange_10(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,3,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__1_0__0_1__1__2__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__1_0__0_1__1__2__3& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_1.find(t, h.hints_1);
auto fin = ind_1.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_1::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_1_2_3_4_5_6 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,1,2,3,4,5,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,1,2,3,4,5,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__2_0_1__3__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__2_0_1__3__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__3_0_1_2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__3_0_1_2__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};

class Sf_base : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
std::string profiling_fname;
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(boolean)_",
	R"_(short)_",
	R"_(int)_",
	R"_(long)_",
	R"_(float)_",
	R"_(double)_",
	R"_(char)_",
	R"_(byte)_",
	R"_(<<null pseudo heap>>)_",
	R"_(Throw NullPointerException)_",
	R"_(throw NPE)_",
	R"_(java.lang.NullPointerException)_",
	R"_(Load Array Index)_",
	R"_(Store Array Index)_",
	R"_(Store Instance Field)_",
	R"_(Load Instance Field)_",
	R"_(Virtual Method Invocation)_",
	R"_(Special Method Invocation)_",
	R"_(Unary Operator)_",
	R"_(Binary Operator)_",
	R"_(Throw Null)_",
	R"_(throw null)_",
	R"_(Enter Monitor (Synchronized))_",
	R"_(null)_",
	R"_(!=)_",
	R"_(==)_",
	R"_(<=)_",
	R"_(>=)_",
	R"_(<)_",
	R"_(>)_",
	R"_(/var_declaration/)_",
	R"_(/map_param/)_",
};private:
  size_t freqs[913]{};
  size_t reads[70]{};
// -- Table: _AssignReturnValue
std::unique_ptr<t_btree_2__0_1> rel_1_AssignReturnValue = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<0,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_1_AssignReturnValue;
// -- Table: _SpecialMethodInvocation
std::unique_ptr<t_btree_5__0_2_1_3_4__3_0_1_2_4__1__5__8> rel_2_SpecialMethodInvocation = std::make_unique<t_btree_5__0_2_1_3_4__3_0_1_2_4__1__5__8>();
souffle::RelationWrapper<1,t_btree_5__0_2_1_3_4__3_0_1_2_4__1__5__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_2_SpecialMethodInvocation;
// -- Table: _StaticMethodInvocation
std::unique_ptr<t_btree_4__0_1_2_3__1> rel_3_StaticMethodInvocation = std::make_unique<t_btree_4__0_1_2_3__1>();
souffle::RelationWrapper<2,t_btree_4__0_1_2_3__1,Tuple<RamDomain,4>,4,true,false> wrapper_rel_3_StaticMethodInvocation;
// -- Table: _VirtualMethodInvocation
std::unique_ptr<t_btree_5__0_1_2_3_4__3_0_1_2_4__1__8> rel_4_VirtualMethodInvocation = std::make_unique<t_btree_5__0_1_2_3_4__3_0_1_2_4__1__8>();
souffle::RelationWrapper<3,t_btree_5__0_1_2_3_4__3_0_1_2_4__1__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_4_VirtualMethodInvocation;
// -- Table: AssignReturnValue_WithInvoke
std::unique_ptr<t_btree_4__0_1_2_3__2_0_1_3__1__4> rel_5_AssignReturnValue_WithInvoke = std::make_unique<t_btree_4__0_1_2_3__2_0_1_3__1__4>();
souffle::RelationWrapper<4,t_btree_4__0_1_2_3__2_0_1_3__1__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_5_AssignReturnValue_WithInvoke;
// -- Table: Primitive
std::unique_ptr<t_btree_1__0__1> rel_6_Primitive = std::make_unique<t_btree_1__0__1>();
// -- Table: _Var_Type
std::unique_ptr<t_btree_2__0_1> rel_7_Var_Type = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<5,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_7_Var_Type;
// -- Table: RefTypeVar
std::unique_ptr<t_btree_1__0> rel_8_RefTypeVar = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<6,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_8_RefTypeVar;
// -- Table: _AssignCast
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_9_AssignCast = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<7,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_9_AssignCast;
// -- Table: _AssignCastNull
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_10_AssignCastNull = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<8,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_10_AssignCastNull;
// -- Table: VarPointsToNull
std::unique_ptr<t_btree_1__0__1> rel_11_VarPointsToNull = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<9,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_11_VarPointsToNull;
// -- Table: _AssignHeapAllocation
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_12_AssignHeapAllocation = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<10,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_12_AssignHeapAllocation;
// -- Table: _AssignLocal
std::unique_ptr<t_btree_5__2_0_1_3_4__3_0_1_2_4__0_2_4_1_3__1__4__8__21> rel_13_AssignLocal = std::make_unique<t_btree_5__2_0_1_3_4__3_0_1_2_4__0_2_4_1_3__1__4__8__21>();
souffle::RelationWrapper<11,t_btree_5__2_0_1_3_4__3_0_1_2_4__0_2_4_1_3__1__4__8__21,Tuple<RamDomain,5>,5,true,false> wrapper_rel_13_AssignLocal;
// -- Table: DefineVar
std::unique_ptr<t_btree_3__0_1_2__7> rel_14_DefineVar = std::make_unique<t_btree_3__0_1_2__7>();
souffle::RelationWrapper<12,t_btree_3__0_1_2__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_14_DefineVar;
// -- Table: _AssignNull
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_15_AssignNull = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<13,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_15_AssignNull;
// -- Table: _LoadInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_16_LoadInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<14,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_16_LoadInstanceField;
// -- Table: _LoadStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_17_LoadStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<15,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_17_LoadStaticField;
// -- Table: VarDef
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_18_VarDef = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<16,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_18_VarDef;
// -- Table: _ActualParam
std::unique_ptr<t_btree_3__2_1_0__4__6> rel_19_ActualParam = std::make_unique<t_btree_3__2_1_0__4__6>();
souffle::RelationWrapper<17,t_btree_3__2_1_0__4__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_19_ActualParam;
// -- Table: _Return
std::unique_ptr<t_btree_4__2_0_3_1__4__13> rel_20_Return = std::make_unique<t_btree_4__2_0_3_1__4__13>();
souffle::RelationWrapper<18,t_btree_4__2_0_3_1__4__13,Tuple<RamDomain,4>,4,true,false> wrapper_rel_20_Return;
// -- Table: _StoreArrayIndex
std::unique_ptr<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8> rel_21_StoreArrayIndex = std::make_unique<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8>();
souffle::RelationWrapper<19,t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_21_StoreArrayIndex;
// -- Table: _StoreInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_22_StoreInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<20,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_22_StoreInstanceField;
// -- Table: _StoreStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_23_StoreStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<21,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_23_StoreStaticField;
// -- Table: AllUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_24_AllUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<22,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_24_AllUse;
// -- Table: FirstUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_25_FirstUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<23,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_25_FirstUse;
// -- Table: LastUse
std::unique_ptr<t_btree_4__0_2_3_1__13> rel_26_LastUse = std::make_unique<t_btree_4__0_2_3_1__13>();
souffle::RelationWrapper<24,t_btree_4__0_2_3_1__13,Tuple<RamDomain,4>,4,false,true> wrapper_rel_26_LastUse;
// -- Table: ApplicationMethod
std::unique_ptr<t_btree_1__0__1> rel_27_ApplicationMethod = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<25,t_btree_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_27_ApplicationMethod;
// -- Table: AssignMayNull
std::unique_ptr<t_btree_3__0_1_2> rel_28_AssignMayNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<26,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_28_AssignMayNull;
// -- Table: BasicBlockHead
std::unique_ptr<t_btree_2__0_1__1> rel_29_BasicBlockHead = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<27,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_29_BasicBlockHead;
// -- Table: CallGraphEdge
std::unique_ptr<t_btree_4__1_0_2_3__3_0_1_2__2__8> rel_30_CallGraphEdge = std::make_unique<t_btree_4__1_0_2_3__3_0_1_2__2__8>();
souffle::RelationWrapper<28,t_btree_4__1_0_2_3__3_0_1_2__2__8,Tuple<RamDomain,4>,4,true,false> wrapper_rel_30_CallGraphEdge;
// -- Table: Instruction_Next
std::unique_ptr<t_btree_2__0_1__1> rel_31_Instruction_Next = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<29,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_31_Instruction_Next;
// -- Table: _IfNull
std::unique_ptr<t_btree_3__0_2_1__1__5> rel_32_IfNull = std::make_unique<t_btree_3__0_2_1__1__5>();
souffle::RelationWrapper<30,t_btree_3__0_2_1__1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_32_IfNull;
// -- Table: MayNull_IfInstruction
std::unique_ptr<t_btree_1__0> rel_33_MayNull_IfInstruction = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<31,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_33_MayNull_IfInstruction;
// -- Table: FalseBranch
std::unique_ptr<t_btree_2__0_1__1> rel_34_FalseBranch = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<32,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_34_FalseBranch;
// -- Table: _OperatorAt
std::unique_ptr<t_btree_2__0_1__1> rel_35_OperatorAt = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<33,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_35_OperatorAt;
// -- Table: MayNull_IfInstructionsCond
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_36_MayNull_IfInstructionsCond = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<34,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_36_MayNull_IfInstructionsCond;
// -- Table: PhiNodeHead
std::unique_ptr<t_btree_2__0_1> rel_37_PhiNodeHead = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<35,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_37_PhiNodeHead;
// -- Table: PhiNodeHeadVar
std::unique_ptr<t_btree_2__1_0__2> rel_38_PhiNodeHeadVar = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<36,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,false,true> wrapper_rel_38_PhiNodeHeadVar;
// -- Table: CanBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_39_CanBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<37,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_39_CanBeNullBranch;
// -- Table: @delta_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_40_delta_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_41_new_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_42_CannotBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<38,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_42_CannotBeNullBranch;
// -- Table: @delta_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_43_delta_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_44_new_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: Dominates
std::unique_ptr<t_btree_2__0_1__3> rel_45_Dominates = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<39,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_45_Dominates;
// -- Table: _FormalParam
std::unique_ptr<t_btree_3__1_2_0__6> rel_46_FormalParam = std::make_unique<t_btree_3__1_2_0__6>();
souffle::RelationWrapper<40,t_btree_3__1_2_0__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_46_FormalParam;
// -- Table: Instruction_FormalParam
std::unique_ptr<t_btree_4__1_3_0_2__10> rel_47_Instruction_FormalParam = std::make_unique<t_btree_4__1_3_0_2__10>();
souffle::RelationWrapper<41,t_btree_4__1_3_0_2__10,Tuple<RamDomain,4>,4,false,true> wrapper_rel_47_Instruction_FormalParam;
// -- Table: _Var_DeclaringMethod
std::unique_ptr<t_btree_2__0_1__1> rel_48_Var_DeclaringMethod = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<42,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_48_Var_DeclaringMethod;
// -- Table: Instruction_VarDeclaringMethod
std::unique_ptr<t_btree_3__0_1_2> rel_49_Instruction_VarDeclaringMethod = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<43,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_49_Instruction_VarDeclaringMethod;
// -- Table: Method_FirstInstruction
std::unique_ptr<t_btree_2__0_1__1> rel_50_Method_FirstInstruction = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<44,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_50_Method_FirstInstruction;
// -- Table: MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__1_0__0_1__1__2__3> rel_51_MayPredecessorModuloThrow = std::make_unique<t_btree_2__1_0__0_1__1__2__3>();
souffle::RelationWrapper<45,t_btree_2__1_0__0_1__1__2__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_51_MayPredecessorModuloThrow;
// -- Table: @delta_MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__1_0__2> rel_52_delta_MayPredecessorModuloThrow = std::make_unique<t_btree_2__1_0__2>();
// -- Table: @new_MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__1_0__2> rel_53_new_MayPredecessorModuloThrow = std::make_unique<t_btree_2__1_0__2>();
// -- Table: MayNullPtr
std::unique_ptr<t_btree_3__0_1_2__7> rel_54_MayNullPtr = std::make_unique<t_btree_3__0_1_2__7>();
souffle::RelationWrapper<46,t_btree_3__0_1_2__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_54_MayNullPtr;
// -- Table: @delta_MayNullPtr
std::unique_ptr<t_btree_3__0_1_2> rel_55_delta_MayNullPtr = std::make_unique<t_btree_3__0_1_2>();
// -- Table: @new_MayNullPtr
std::unique_ptr<t_btree_3__0_1_2> rel_56_new_MayNullPtr = std::make_unique<t_btree_3__0_1_2>();
// -- Table: InstructionLine
std::unique_ptr<t_btree_4__0_1_2_3__3> rel_57_InstructionLine = std::make_unique<t_btree_4__0_1_2_3__3>();
souffle::RelationWrapper<47,t_btree_4__0_1_2_3__3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_57_InstructionLine;
// -- Table: _AssignBinop
std::unique_ptr<t_btree_4__0_1_2_3> rel_58_AssignBinop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<48,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_58_AssignBinop;
// -- Table: _AssignOperFrom
std::unique_ptr<t_btree_2__0_1__3> rel_59_AssignOperFrom = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<49,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_59_AssignOperFrom;
// -- Table: _AssignUnop
std::unique_ptr<t_btree_4__0_1_2_3> rel_60_AssignUnop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<50,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_60_AssignUnop;
// -- Table: _EnterMonitor
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_61_EnterMonitor = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<51,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_61_EnterMonitor;
// -- Table: _LoadArrayIndex
std::unique_ptr<t_btree_5__3_0_1_2_4__8> rel_62_LoadArrayIndex = std::make_unique<t_btree_5__3_0_1_2_4__8>();
souffle::RelationWrapper<52,t_btree_5__3_0_1_2_4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_62_LoadArrayIndex;
// -- Table: _ThrowNull
std::unique_ptr<t_btree_3__0_1_2> rel_63_ThrowNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<53,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,true,false> wrapper_rel_63_ThrowNull;
// -- Table: NullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_64_NullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
// -- Table: Reachable
std::unique_ptr<t_btree_1__0__1> rel_65_Reachable = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<54,t_btree_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_65_Reachable;
// -- Table: ReachableNullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_66_ReachableNullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
// -- Table: ReachableNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_67_ReachableNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
// -- Table: _IfVar
std::unique_ptr<t_btree_3__0_1_2__2_0_1__3__4> rel_68_IfVar = std::make_unique<t_btree_3__0_1_2__2_0_1__3__4>();
souffle::RelationWrapper<55,t_btree_3__0_1_2__2_0_1__3__4,Tuple<RamDomain,3>,3,true,false> wrapper_rel_68_IfVar;
// -- Table: IfInstructions
std::unique_ptr<t_btree_2__0_1> rel_69_IfInstructions = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<56,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_69_IfInstructions;
// -- Table: VarEqlsConst
std::unique_ptr<t_btree_2__0_1__1> rel_70_VarEqlsConst = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<57,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_70_VarEqlsConst;
// -- Table: IfInstructionsCond
std::unique_ptr<t_btree_4__3_0_1_2__8> rel_71_IfInstructionsCond = std::make_unique<t_btree_4__3_0_1_2__8>();
souffle::RelationWrapper<58,t_btree_4__3_0_1_2__8,Tuple<RamDomain,4>,4,false,true> wrapper_rel_71_IfInstructionsCond;
// -- Table: TrueIfInstructions
std::unique_ptr<t_btree_1__0> rel_72_TrueIfInstructions = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<59,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_72_TrueIfInstructions;
// -- Table: DominatesUnreachable
std::unique_ptr<t_btree_2__0_1> rel_73_DominatesUnreachable = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<60,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_73_DominatesUnreachable;
// -- Table: UnreachablePathNPEIns
std::unique_ptr<t_btree_1__0__1> rel_74_UnreachablePathNPEIns = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<61,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_74_UnreachablePathNPEIns;
// -- Table: PathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_75_PathSensitiveNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
// -- Table: NPEWithMayNull
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_76_NPEWithMayNull = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<62,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_76_NPEWithMayNull;
// -- Table: JumpTarget
std::unique_ptr<t_btree_2__1_0__2> rel_77_JumpTarget = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<63,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_77_JumpTarget;
// -- Table: TrueBranch
std::unique_ptr<t_btree_2__0_1> rel_78_TrueBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<64,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_78_TrueBranch;
public:
Sf_base(std::string pf="profile.log") : profiling_fname(pf),

wrapper_rel_1_AssignReturnValue(*rel_1_AssignReturnValue,symTable,"_AssignReturnValue",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"invocation","to"}}),

wrapper_rel_2_SpecialMethodInvocation(*rel_2_SpecialMethodInvocation,symTable,"_SpecialMethodInvocation",std::array<const char *,5>{{"s:symbol","i:Index","s:Method","s:symbol","s:Method"}},std::array<const char *,5>{{"?instruction","i","sig","?base","m"}}),

wrapper_rel_3_StaticMethodInvocation(*rel_3_StaticMethodInvocation,symTable,"_StaticMethodInvocation",std::array<const char *,4>{{"s:Instruction","i:number","s:Method","s:Method"}},std::array<const char *,4>{{"instruction","index","signature","method"}}),

wrapper_rel_4_VirtualMethodInvocation(*rel_4_VirtualMethodInvocation,symTable,"_VirtualMethodInvocation",std::array<const char *,5>{{"s:Instruction","i:Index","s:Method","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","sig","base","m"}}),

wrapper_rel_5_AssignReturnValue_WithInvoke(*rel_5_AssignReturnValue_WithInvoke,symTable,"AssignReturnValue_WithInvoke",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_7_Var_Type(*rel_7_Var_Type,symTable,"_Var_Type",std::array<const char *,2>{{"s:Var","s:Type"}},std::array<const char *,2>{{"var","type"}}),

wrapper_rel_8_RefTypeVar(*rel_8_RefTypeVar,symTable,"RefTypeVar",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"var"}}),

wrapper_rel_9_AssignCast(*rel_9_AssignCast,symTable,"_AssignCast",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Type","s:Method"}},std::array<const char *,6>{{"ins","i","from","to","t","m"}}),

wrapper_rel_10_AssignCastNull(*rel_10_AssignCastNull,symTable,"_AssignCastNull",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Type","s:Method"}},std::array<const char *,5>{{"ins","i","to","t","m"}}),

wrapper_rel_11_VarPointsToNull(*rel_11_VarPointsToNull,symTable,"VarPointsToNull",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"v"}}),

wrapper_rel_12_AssignHeapAllocation(*rel_12_AssignHeapAllocation,symTable,"_AssignHeapAllocation",std::array<const char *,6>{{"s:Instruction","i:number","s:symbol","s:Var","s:Method","i:number"}},std::array<const char *,6>{{"?instruction","?index","?heap","?to","?inmethod","?linenumber"}}),

wrapper_rel_13_AssignLocal(*rel_13_AssignLocal,symTable,"_AssignLocal",std::array<const char *,5>{{"s:Instruction","i:number","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"?instruction","?index","?from","?to","?inmethod"}}),

wrapper_rel_14_DefineVar(*rel_14_DefineVar,symTable,"DefineVar",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_15_AssignNull(*rel_15_AssignNull,symTable,"_AssignNull",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_16_LoadInstanceField(*rel_16_LoadInstanceField,symTable,"_LoadInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","to","base","sig","m"}}),

wrapper_rel_17_LoadStaticField(*rel_17_LoadStaticField,symTable,"_LoadStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","to","sig","m"}}),

wrapper_rel_18_VarDef(*rel_18_VarDef,symTable,"VarDef",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_19_ActualParam(*rel_19_ActualParam,symTable,"_ActualParam",std::array<const char *,3>{{"i:number","s:Instruction","s:Var"}},std::array<const char *,3>{{"index","invocation","var"}}),

wrapper_rel_20_Return(*rel_20_Return,symTable,"_Return",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"instruction","index","var","method"}}),

wrapper_rel_21_StoreArrayIndex(*rel_21_StoreArrayIndex,symTable,"_StoreArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","from","base","m"}}),

wrapper_rel_22_StoreInstanceField(*rel_22_StoreInstanceField,symTable,"_StoreInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","from","base","sig","m"}}),

wrapper_rel_23_StoreStaticField(*rel_23_StoreStaticField,symTable,"_StoreStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","from","sig","m"}}),

wrapper_rel_24_AllUse(*rel_24_AllUse,symTable,"AllUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_25_FirstUse(*rel_25_FirstUse,symTable,"FirstUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_26_LastUse(*rel_26_LastUse,symTable,"LastUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_27_ApplicationMethod(*rel_27_ApplicationMethod,symTable,"ApplicationMethod",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_28_AssignMayNull(*rel_28_AssignMayNull,symTable,"AssignMayNull",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_29_BasicBlockHead(*rel_29_BasicBlockHead,symTable,"BasicBlockHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"insn","headInsn"}}),

wrapper_rel_30_CallGraphEdge(*rel_30_CallGraphEdge,symTable,"CallGraphEdge",std::array<const char *,4>{{"s:Context","s:Instruction","s:Context","s:Method"}},std::array<const char *,4>{{"ctx","ins","hctx","sig"}}),

wrapper_rel_31_Instruction_Next(*rel_31_Instruction_Next,symTable,"Instruction_Next",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?i","?next"}}),

wrapper_rel_32_IfNull(*rel_32_IfNull,symTable,"_IfNull",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_33_MayNull_IfInstruction(*rel_33_MayNull_IfInstruction,symTable,"MayNull_IfInstruction",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_34_FalseBranch(*rel_34_FalseBranch,symTable,"FalseBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_35_OperatorAt(*rel_35_OperatorAt,symTable,"_OperatorAt",std::array<const char *,2>{{"s:Instruction","s:symbol"}},std::array<const char *,2>{{"i","operator"}}),

wrapper_rel_36_MayNull_IfInstructionsCond(*rel_36_MayNull_IfInstructionsCond,symTable,"MayNull_IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:Var","s:Var","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_37_PhiNodeHead(*rel_37_PhiNodeHead,symTable,"PhiNodeHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?insn","?headInsn"}}),

wrapper_rel_38_PhiNodeHeadVar(*rel_38_PhiNodeHeadVar,symTable,"PhiNodeHeadVar",std::array<const char *,2>{{"s:Var","s:Var"}},std::array<const char *,2>{{"var","headVar"}}),

wrapper_rel_39_CanBeNullBranch(*rel_39_CanBeNullBranch,symTable,"CanBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_42_CannotBeNullBranch(*rel_42_CannotBeNullBranch,symTable,"CannotBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_45_Dominates(*rel_45_Dominates,symTable,"Dominates",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?dominator","?insn"}}),

wrapper_rel_46_FormalParam(*rel_46_FormalParam,symTable,"_FormalParam",std::array<const char *,3>{{"i:number","s:Method","s:Var"}},std::array<const char *,3>{{"index","method","var"}}),

wrapper_rel_47_Instruction_FormalParam(*rel_47_Instruction_FormalParam,symTable,"Instruction_FormalParam",std::array<const char *,4>{{"s:Instruction","s:symbol","s:Var","i:number"}},std::array<const char *,4>{{"insn","method","var","index"}}),

wrapper_rel_48_Var_DeclaringMethod(*rel_48_Var_DeclaringMethod,symTable,"_Var_DeclaringMethod",std::array<const char *,2>{{"s:Var","s:Method"}},std::array<const char *,2>{{"?var","?method"}}),

wrapper_rel_49_Instruction_VarDeclaringMethod(*rel_49_Instruction_VarDeclaringMethod,symTable,"Instruction_VarDeclaringMethod",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"insn","method","var"}}),

wrapper_rel_50_Method_FirstInstruction(*rel_50_Method_FirstInstruction,symTable,"Method_FirstInstruction",std::array<const char *,2>{{"s:Method","s:Instruction"}},std::array<const char *,2>{{"?method","?i"}}),

wrapper_rel_51_MayPredecessorModuloThrow(*rel_51_MayPredecessorModuloThrow,symTable,"MayPredecessorModuloThrow",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?prev","?next"}}),

wrapper_rel_54_MayNullPtr(*rel_54_MayNullPtr,symTable,"MayNullPtr",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_57_InstructionLine(*rel_57_InstructionLine,symTable,"InstructionLine",std::array<const char *,4>{{"s:Method","i:Index","i:LineNumber","s:File"}},std::array<const char *,4>{{"m","i","l","f"}}),

wrapper_rel_58_AssignBinop(*rel_58_AssignBinop,symTable,"_AssignBinop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_59_AssignOperFrom(*rel_59_AssignOperFrom,symTable,"_AssignOperFrom",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"ins","from"}}),

wrapper_rel_60_AssignUnop(*rel_60_AssignUnop,symTable,"_AssignUnop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_61_EnterMonitor(*rel_61_EnterMonitor,symTable,"_EnterMonitor",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_62_LoadArrayIndex(*rel_62_LoadArrayIndex,symTable,"_LoadArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","to","base","m"}}),

wrapper_rel_63_ThrowNull(*rel_63_ThrowNull,symTable,"_ThrowNull",std::array<const char *,3>{{"s:Instruction","i:Index","s:Method"}},std::array<const char *,3>{{"ins","i","m"}}),

wrapper_rel_65_Reachable(*rel_65_Reachable,symTable,"Reachable",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_68_IfVar(*rel_68_IfVar,symTable,"_IfVar",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_69_IfInstructions(*rel_69_IfInstructions,symTable,"IfInstructions",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ins","ifIns"}}),

wrapper_rel_70_VarEqlsConst(*rel_70_VarEqlsConst,symTable,"VarEqlsConst",std::array<const char *,2>{{"s:Var","s:symbol"}},std::array<const char *,2>{{"var","num"}}),

wrapper_rel_71_IfInstructionsCond(*rel_71_IfInstructionsCond,symTable,"IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:symbol","s:symbol","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_72_TrueIfInstructions(*rel_72_TrueIfInstructions,symTable,"TrueIfInstructions",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_73_DominatesUnreachable(*rel_73_DominatesUnreachable,symTable,"DominatesUnreachable",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","ins"}}),

wrapper_rel_74_UnreachablePathNPEIns(*rel_74_UnreachablePathNPEIns,symTable,"UnreachablePathNPEIns",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_76_NPEWithMayNull(*rel_76_NPEWithMayNull,symTable,"NPEWithMayNull",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_77_JumpTarget(*rel_77_JumpTarget,symTable,"JumpTarget",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"to","from"}}),

wrapper_rel_78_TrueBranch(*rel_78_TrueBranch,symTable,"TrueBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}){
ProfileEventSingleton::instance().setOutputFile(profiling_fname);
addRelation("_AssignReturnValue",&wrapper_rel_1_AssignReturnValue,1,0);
addRelation("_SpecialMethodInvocation",&wrapper_rel_2_SpecialMethodInvocation,1,0);
addRelation("_StaticMethodInvocation",&wrapper_rel_3_StaticMethodInvocation,1,0);
addRelation("_VirtualMethodInvocation",&wrapper_rel_4_VirtualMethodInvocation,1,0);
addRelation("AssignReturnValue_WithInvoke",&wrapper_rel_5_AssignReturnValue_WithInvoke,0,1);
addRelation("_Var_Type",&wrapper_rel_7_Var_Type,1,0);
addRelation("RefTypeVar",&wrapper_rel_8_RefTypeVar,0,1);
addRelation("_AssignCast",&wrapper_rel_9_AssignCast,1,0);
addRelation("_AssignCastNull",&wrapper_rel_10_AssignCastNull,1,0);
addRelation("VarPointsToNull",&wrapper_rel_11_VarPointsToNull,0,1);
addRelation("_AssignHeapAllocation",&wrapper_rel_12_AssignHeapAllocation,1,0);
addRelation("_AssignLocal",&wrapper_rel_13_AssignLocal,1,0);
addRelation("DefineVar",&wrapper_rel_14_DefineVar,0,1);
addRelation("_AssignNull",&wrapper_rel_15_AssignNull,1,0);
addRelation("_LoadInstanceField",&wrapper_rel_16_LoadInstanceField,1,0);
addRelation("_LoadStaticField",&wrapper_rel_17_LoadStaticField,1,0);
addRelation("VarDef",&wrapper_rel_18_VarDef,0,1);
addRelation("_ActualParam",&wrapper_rel_19_ActualParam,1,0);
addRelation("_Return",&wrapper_rel_20_Return,1,0);
addRelation("_StoreArrayIndex",&wrapper_rel_21_StoreArrayIndex,1,0);
addRelation("_StoreInstanceField",&wrapper_rel_22_StoreInstanceField,1,0);
addRelation("_StoreStaticField",&wrapper_rel_23_StoreStaticField,1,0);
addRelation("AllUse",&wrapper_rel_24_AllUse,0,1);
addRelation("FirstUse",&wrapper_rel_25_FirstUse,0,1);
addRelation("LastUse",&wrapper_rel_26_LastUse,0,1);
addRelation("ApplicationMethod",&wrapper_rel_27_ApplicationMethod,1,0);
addRelation("AssignMayNull",&wrapper_rel_28_AssignMayNull,0,1);
addRelation("BasicBlockHead",&wrapper_rel_29_BasicBlockHead,1,0);
addRelation("CallGraphEdge",&wrapper_rel_30_CallGraphEdge,1,0);
addRelation("Instruction_Next",&wrapper_rel_31_Instruction_Next,1,1);
addRelation("_IfNull",&wrapper_rel_32_IfNull,1,0);
addRelation("MayNull_IfInstruction",&wrapper_rel_33_MayNull_IfInstruction,0,1);
addRelation("FalseBranch",&wrapper_rel_34_FalseBranch,0,1);
addRelation("_OperatorAt",&wrapper_rel_35_OperatorAt,1,0);
addRelation("MayNull_IfInstructionsCond",&wrapper_rel_36_MayNull_IfInstructionsCond,0,1);
addRelation("PhiNodeHead",&wrapper_rel_37_PhiNodeHead,1,1);
addRelation("PhiNodeHeadVar",&wrapper_rel_38_PhiNodeHeadVar,0,1);
addRelation("CanBeNullBranch",&wrapper_rel_39_CanBeNullBranch,0,1);
addRelation("CannotBeNullBranch",&wrapper_rel_42_CannotBeNullBranch,0,1);
addRelation("Dominates",&wrapper_rel_45_Dominates,1,0);
addRelation("_FormalParam",&wrapper_rel_46_FormalParam,1,0);
addRelation("Instruction_FormalParam",&wrapper_rel_47_Instruction_FormalParam,0,1);
addRelation("_Var_DeclaringMethod",&wrapper_rel_48_Var_DeclaringMethod,1,0);
addRelation("Instruction_VarDeclaringMethod",&wrapper_rel_49_Instruction_VarDeclaringMethod,0,1);
addRelation("Method_FirstInstruction",&wrapper_rel_50_Method_FirstInstruction,1,0);
addRelation("MayPredecessorModuloThrow",&wrapper_rel_51_MayPredecessorModuloThrow,1,0);
addRelation("MayNullPtr",&wrapper_rel_54_MayNullPtr,0,1);
addRelation("InstructionLine",&wrapper_rel_57_InstructionLine,1,0);
addRelation("_AssignBinop",&wrapper_rel_58_AssignBinop,1,0);
addRelation("_AssignOperFrom",&wrapper_rel_59_AssignOperFrom,1,0);
addRelation("_AssignUnop",&wrapper_rel_60_AssignUnop,1,0);
addRelation("_EnterMonitor",&wrapper_rel_61_EnterMonitor,1,0);
addRelation("_LoadArrayIndex",&wrapper_rel_62_LoadArrayIndex,1,0);
addRelation("_ThrowNull",&wrapper_rel_63_ThrowNull,1,0);
addRelation("Reachable",&wrapper_rel_65_Reachable,1,0);
addRelation("_IfVar",&wrapper_rel_68_IfVar,1,0);
addRelation("IfInstructions",&wrapper_rel_69_IfInstructions,0,1);
addRelation("VarEqlsConst",&wrapper_rel_70_VarEqlsConst,1,0);
addRelation("IfInstructionsCond",&wrapper_rel_71_IfInstructionsCond,0,1);
addRelation("TrueIfInstructions",&wrapper_rel_72_TrueIfInstructions,0,1);
addRelation("DominatesUnreachable",&wrapper_rel_73_DominatesUnreachable,0,1);
addRelation("UnreachablePathNPEIns",&wrapper_rel_74_UnreachablePathNPEIns,0,1);
addRelation("NPEWithMayNull",&wrapper_rel_76_NPEWithMayNull,0,1);
addRelation("JumpTarget",&wrapper_rel_77_JumpTarget,1,0);
addRelation("TrueBranch",&wrapper_rel_78_TrueBranch,0,1);
}
~Sf_base() {
}
private:
void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1, bool performIO = false) {
SignalHandler::instance()->set();
std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(4);
#endif

// -- query evaluation --
ProfileEventSingleton::instance().startTimer();
ProfileEventSingleton::instance().makeTimeEvent("@time;starttime");
{
Logger logger("@runtime;", 0);
ProfileEventSingleton::instance().makeConfigRecord("relationCount", std::to_string(70));[](){
ProfileEventSingleton::instance().makeStratumRecord(0, "relation", "_AssignReturnValue", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(1, "relation", "_SpecialMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(2, "relation", "_StaticMethodInvocation", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(3, "relation", "_VirtualMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(4, "relation", "AssignReturnValue_WithInvoke", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(5, "relation", "Primitive", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(6, "relation", "_Var_Type", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(7, "relation", "RefTypeVar", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(8, "relation", "_AssignCast", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(9, "relation", "_AssignCastNull", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(10, "relation", "VarPointsToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(11, "relation", "_AssignHeapAllocation", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(12, "relation", "_AssignLocal", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(13, "relation", "DefineVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(14, "relation", "_AssignNull", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(15, "relation", "_LoadInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(16, "relation", "_LoadStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(17, "relation", "VarDef", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(18, "relation", "_ActualParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(19, "relation", "_Return", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(20, "relation", "_StoreArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(21, "relation", "_StoreInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(22, "relation", "_StoreStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(23, "relation", "AllUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(24, "relation", "FirstUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(25, "relation", "LastUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(26, "relation", "ApplicationMethod", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(27, "relation", "AssignMayNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(28, "relation", "BasicBlockHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(29, "relation", "CallGraphEdge", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(30, "relation", "Instruction_Next", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(31, "relation", "_IfNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(32, "relation", "MayNull_IfInstruction", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(33, "relation", "FalseBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(34, "relation", "_OperatorAt", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(35, "relation", "MayNull_IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(36, "relation", "PhiNodeHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(37, "relation", "PhiNodeHeadVar", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(38, "relation", "CanBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(39, "relation", "CannotBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(40, "relation", "Dominates", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(41, "relation", "_FormalParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(42, "relation", "Instruction_FormalParam", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(43, "relation", "_Var_DeclaringMethod", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(44, "relation", "Instruction_VarDeclaringMethod", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(45, "relation", "Method_FirstInstruction", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(46, "relation", "MayPredecessorModuloThrow", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(47, "relation", "MayNullPtr", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(48, "relation", "InstructionLine", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(49, "relation", "_AssignBinop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(50, "relation", "_AssignOperFrom", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(51, "relation", "_AssignUnop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(52, "relation", "_EnterMonitor", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(53, "relation", "_LoadArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(54, "relation", "_ThrowNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(55, "relation", "NullAt", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(56, "relation", "Reachable", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(57, "relation", "ReachableNullAt", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(58, "relation", "ReachableNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(59, "relation", "_IfVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(60, "relation", "IfInstructions", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(61, "relation", "VarEqlsConst", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(62, "relation", "IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(63, "relation", "TrueIfInstructions", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(64, "relation", "DominatesUnreachable", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(65, "relation", "UnreachablePathNPEIns", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(66, "relation", "PathSensitiveNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(67, "relation", "NPEWithMayNull", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(68, "relation", "JumpTarget", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(69, "relation", "TrueBranch", "arity", "2");
}();
/* BEGIN STRATUM 0 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignReturnValue;../declarations.dl [137:7-137:59];loadtime;)_",iter, [&](){return rel_1_AssignReturnValue->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignReturnValue;../declarations.dl [137:7-137:59];)",rel_1_AssignReturnValue->size(),iter);}();
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];loadtime;)_",iter, [&](){return rel_2_SpecialMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];)",rel_2_SpecialMethodInvocation->size(),iter);}();
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StaticMethodInvocation;../declarations.dl [140:7-140:105];loadtime;)_",iter, [&](){return rel_3_StaticMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StaticMethodInvocation;../declarations.dl [140:7-140:105];)",rel_3_StaticMethodInvocation->size(),iter);}();
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];loadtime;)_",iter, [&](){return rel_4_VirtualMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];)",rel_4_VirtualMethodInvocation->size(),iter);}();
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AssignReturnValue_WithInvoke;../may-null/rules.dl [14:7-14:102];)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file ../may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[0]++;
}
freqs[1]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[2]++;
}
freqs[3]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[4]++;
}
freqs[5]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AssignReturnValue_WithInvoke;../may-null/rules.dl [14:7-14:102];savetime;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_AssignReturnValue->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_StaticMethodInvocation->purge();
}();
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Primitive;../declarations.dl [4:7-4:28];)_",iter, [&](){return rel_6_Primitive->size();});
SignalHandler::instance()->setMsg(R"_(Primitive("boolean").
in file ../declarations.dl [5:1-5:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [5:1-5:22];Primitive(\"boolean\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(0));
}
SignalHandler::instance()->setMsg(R"_(Primitive("short").
in file ../declarations.dl [6:1-6:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [6:1-6:20];Primitive(\"short\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(1));
}
SignalHandler::instance()->setMsg(R"_(Primitive("int").
in file ../declarations.dl [7:1-7:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [7:1-7:18];Primitive(\"int\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(2));
}
SignalHandler::instance()->setMsg(R"_(Primitive("long").
in file ../declarations.dl [8:1-8:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [8:1-8:19];Primitive(\"long\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(3));
}
SignalHandler::instance()->setMsg(R"_(Primitive("float").
in file ../declarations.dl [9:1-9:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [9:1-9:20];Primitive(\"float\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(4));
}
SignalHandler::instance()->setMsg(R"_(Primitive("double").
in file ../declarations.dl [10:1-10:21])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [10:1-10:21];Primitive(\"double\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(5));
}
SignalHandler::instance()->setMsg(R"_(Primitive("char").
in file ../declarations.dl [11:1-11:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [11:1-11:19];Primitive(\"char\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(6));
}
SignalHandler::instance()->setMsg(R"_(Primitive("byte").
in file ../declarations.dl [12:1-12:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [12:1-12:19];Primitive(\"byte\").;)_",iter, [&](){return rel_6_Primitive->size();});
rel_6_Primitive->insert(RamDomain(7));
}
}
}();
/* END STRATUM 5 */
/* BEGIN STRATUM 6 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_Type;../declarations.dl [107:7-107:38];loadtime;)_",iter, [&](){return rel_7_Var_Type->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_Type;../declarations.dl [107:7-107:38];)",rel_7_Var_Type->size(),iter);}();
/* END STRATUM 6 */
/* BEGIN STRATUM 7 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;RefTypeVar;../may-null/rules.dl [2:7-2:34];)_",iter, [&](){return rel_8_RefTypeVar->size();});
SignalHandler::instance()->setMsg(R"_(RefTypeVar(var) :- 
   _Var_Type(var,type),
   !Primitive(type).
in file ../may-null/rules.dl [70:1-72:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;RefTypeVar;../may-null/rules.dl [70:1-72:18];RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;)_",iter, [&](){return rel_8_RefTypeVar->size();});
if (!rel_7_Var_Type->empty()) [&](){
auto part = rel_7_Var_Type->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_6_Primitive_op_ctxt,rel_6_Primitive->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_7_Var_Type_op_ctxt,rel_7_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[0]++,!rel_6_Primitive->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_6_Primitive_op_ctxt)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_8_RefTypeVar->insert(tuple,READ_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt));
}
freqs[6]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;RefTypeVar;../may-null/rules.dl [2:7-2:34];savetime;)_",iter, [&](){return rel_8_RefTypeVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_8_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_6_Primitive->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_Var_Type->purge();
}();
/* END STRATUM 7 */
/* BEGIN STRATUM 8 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCast;../declarations.dl [86:7-86:85];loadtime;)_",iter, [&](){return rel_9_AssignCast->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_9_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCast;../declarations.dl [86:7-86:85];)",rel_9_AssignCast->size(),iter);}();
/* END STRATUM 8 */
/* BEGIN STRATUM 9 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCastNull;../declarations.dl [90:7-90:79];loadtime;)_",iter, [&](){return rel_10_AssignCastNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCastNull;../declarations.dl [90:7-90:79];)",rel_10_AssignCastNull->size(),iter);}();
/* END STRATUM 9 */
/* BEGIN STRATUM 10 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarPointsToNull;../declarations.dl [155:7-155:37];)_",iter, [&](){return rel_11_VarPointsToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   _AssignCastNull(_,_,var,_,_).
in file ../rules.dl [5:1-5:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarPointsToNull;../rules.dl [5:1-5:54];VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;)_",iter, [&](){return rel_11_VarPointsToNull->size();});
if (!rel_10_AssignCastNull->empty()) [&](){
auto part = rel_10_AssignCastNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCastNull_op_ctxt,rel_10_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[2])}});
rel_11_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt));
freqs[7]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarPointsToNull;../declarations.dl [155:7-155:37];savetime;)_",iter, [&](){return rel_11_VarPointsToNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_11_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 10 */
/* BEGIN STRATUM 11 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignHeapAllocation;../declarations.dl [131:7-131:131];loadtime;)_",iter, [&](){return rel_12_AssignHeapAllocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignHeapAllocation;../declarations.dl [131:7-131:131];)",rel_12_AssignHeapAllocation->size(),iter);}();
/* END STRATUM 11 */
/* BEGIN STRATUM 12 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignLocal;../declarations.dl [134:7-134:98];loadtime;)_",iter, [&](){return rel_13_AssignLocal->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignLocal;../declarations.dl [134:7-134:98];)",rel_13_AssignLocal->size(),iter);}();
/* END STRATUM 12 */
/* BEGIN STRATUM 13 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DefineVar;../may-null/rules.dl [5:7-5:68];)_",iter, [&](){return rel_14_DefineVar->size();});
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,var,method) :- 
   _AssignHeapAllocation(insn,_,_,var,method,_).
in file ../may-null/rules.dl [95:1-96:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [95:1-96:51];DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_12_AssignHeapAllocation->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[8]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignLocal(insn,_,_,to,method).
in file ../may-null/rules.dl [102:1-103:41])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [102:1-103:41];DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_13_AssignLocal->empty()) [&](){
auto part = rel_13_AssignLocal->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[9]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   AssignReturnValue_WithInvoke(insn,_,to,method).
in file ../may-null/rules.dl [105:1-106:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [105:1-106:51];DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[10]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DefineVar;../may-null/rules.dl [5:7-5:68];savetime;)_",iter, [&](){return rel_14_DefineVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 13 */
/* BEGIN STRATUM 14 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignNull;../declarations.dl [93:7-93:66];loadtime;)_",iter, [&](){return rel_15_AssignNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignNull;../declarations.dl [93:7-93:66];)",rel_15_AssignNull->size(),iter);}();
/* END STRATUM 14 */
/* BEGIN STRATUM 15 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadInstanceField;../declarations.dl [66:7-66:97];loadtime;)_",iter, [&](){return rel_16_LoadInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadInstanceField;../declarations.dl [66:7-66:97];)",rel_16_LoadInstanceField->size(),iter);}();
/* END STRATUM 15 */
/* BEGIN STRATUM 16 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadStaticField;../declarations.dl [72:7-72:84];loadtime;)_",iter, [&](){return rel_17_LoadStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadStaticField;../declarations.dl [72:7-72:84];)",rel_17_LoadStaticField->size(),iter);}();
/* END STRATUM 16 */
/* BEGIN STRATUM 17 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarDef;../may-null/rules.dl [13:7-13:80];)_",iter, [&](){return rel_18_VarDef->size();});
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignNull(insn,index,var,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_15_AssignNull->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_15_AssignNull->equalRange_4(key,READ_OP_CONTEXT(rel_15_AssignNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[11]++;
}
freqs[12]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,_,var,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_13_AssignLocal->equalRange_8(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[13]++;
}
freqs[14]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignHeapAllocation(insn,index,_,var,method,_).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_12_AssignHeapAllocation->equalRange_8(key,READ_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[15]++;
}
freqs[16]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_16_LoadInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[17]++;
}
freqs[18]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_17_LoadStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[19]++;
}
freqs[20]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCastNull(insn,index,var,_,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_10_AssignCastNull->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCastNull_op_ctxt,rel_10_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_10_AssignCastNull->equalRange_4(key,READ_OP_CONTEXT(rel_10_AssignCastNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[21]++;
}
freqs[22]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,_,var,_,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_9_AssignCast->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_9_AssignCast_op_ctxt,rel_9_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_9_AssignCast->equalRange_8(key,READ_OP_CONTEXT(rel_9_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[23]++;
}
freqs[24]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   AssignReturnValue_WithInvoke(insn,index,var,method).
in file ../may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_8_RefTypeVar->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_4(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[25]++;
}
freqs[26]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarDef;../may-null/rules.dl [13:7-13:80];savetime;)_",iter, [&](){return rel_18_VarDef->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_17_LoadStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_10_AssignCastNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_12_AssignHeapAllocation->purge();
}();
/* END STRATUM 17 */
/* BEGIN STRATUM 18 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ActualParam;../declarations.dl [149:7-149:69];loadtime;)_",iter, [&](){return rel_19_ActualParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ActualParam;../declarations.dl [149:7-149:69];)",rel_19_ActualParam->size(),iter);}();
/* END STRATUM 18 */
/* BEGIN STRATUM 19 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Return;../declarations.dl [143:7-143:80];loadtime;)_",iter, [&](){return rel_20_Return->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Return;../declarations.dl [143:7-143:80];)",rel_20_Return->size(),iter);}();
/* END STRATUM 19 */
/* BEGIN STRATUM 20 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreArrayIndex;../declarations.dl [62:7-62:84];loadtime;)_",iter, [&](){return rel_21_StoreArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreArrayIndex;../declarations.dl [62:7-62:84];)",rel_21_StoreArrayIndex->size(),iter);}();
/* END STRATUM 20 */
/* BEGIN STRATUM 21 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreInstanceField;../declarations.dl [68:7-68:100];loadtime;)_",iter, [&](){return rel_22_StoreInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreInstanceField;../declarations.dl [68:7-68:100];)",rel_22_StoreInstanceField->size(),iter);}();
/* END STRATUM 21 */
/* BEGIN STRATUM 22 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreStaticField;../declarations.dl [74:7-74:87];loadtime;)_",iter, [&](){return rel_23_StoreStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreStaticField;../declarations.dl [74:7-74:87];)",rel_23_StoreStaticField->size(),iter);}();
/* END STRATUM 22 */
/* BEGIN STRATUM 23 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AllUse;../may-null/rules.dl [8:7-8:80];)_",iter, [&](){return rel_24_AllUse->size();});
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   VarDef(insn,index,var,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_18_VarDef->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_18_VarDef->equalRange_4(key,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[27]++;
}
freqs[28]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,var,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_13_AssignLocal->equalRange_4(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[29]++;
}
freqs[30]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,var,_,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_9_AssignCast->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_9_AssignCast_op_ctxt,rel_9_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_9_AssignCast->equalRange_4(key,READ_OP_CONTEXT(rel_9_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[31]++;
}
freqs[32]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _SpecialMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[33]++;
}
freqs[34]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _VirtualMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[35]++;
}
freqs[36]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreArrayIndex(insn,index,var,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_21_StoreArrayIndex->equalRange_4(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[37]++;
}
freqs[38]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_22_StoreInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[39]++;
}
freqs[40]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_23_StoreStaticField->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt,rel_23_StoreStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_23_StoreStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[41]++;
}
freqs[42]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _Return(insn,index,var,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_20_Return->equalRange_4(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[43]++;
}
freqs[44]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   AssignReturnValue_WithInvoke(insn,index,_,method).
in file ../may-null/rules.dl [37:1-50:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [37:1-50:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_8_RefTypeVar->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[45]++;
}
freqs[46]++;
}
freqs[47]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AllUse;../may-null/rules.dl [8:7-8:80];savetime;)_",iter, [&](){return rel_24_AllUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_23_StoreStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_9_AssignCast->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_18_VarDef->purge();
}();
/* END STRATUM 23 */
/* BEGIN STRATUM 24 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FirstUse;../may-null/rules.dl [10:7-10:82];)_",iter, [&](){return rel_25_FirstUse->size();});
SignalHandler::instance()->setMsg(R"_(FirstUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = min  I0 : AllUse(_, I0,var,method).
in file ../may-null/rules.dl [57:1-59:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FirstUse;../may-null/rules.dl [57:1-59:42];FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;)_",iter, [&](){return rel_25_FirstUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_25_FirstUse->insert(tuple,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
}
}
}
freqs[48]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FirstUse;../may-null/rules.dl [10:7-10:82];savetime;)_",iter, [&](){return rel_25_FirstUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 24 */
/* BEGIN STRATUM 25 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;LastUse;../may-null/rules.dl [9:7-9:81];)_",iter, [&](){return rel_26_LastUse->size();});
SignalHandler::instance()->setMsg(R"_(LastUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = max  I1 : AllUse(_, I1,var,method).
in file ../may-null/rules.dl [52:1-54:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;LastUse;../may-null/rules.dl [52:1-54:42];LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;)_",iter, [&](){return rel_26_LastUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MIN_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::max(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_26_LastUse->insert(tuple,READ_OP_CONTEXT(rel_26_LastUse_op_ctxt));
}
}
}
freqs[49]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;LastUse;../may-null/rules.dl [9:7-9:81];savetime;)_",iter, [&](){return rel_26_LastUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_24_AllUse->purge();
}();
/* END STRATUM 25 */
/* BEGIN STRATUM 26 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;ApplicationMethod;../declarations.dl [31:7-31:35];loadtime;)_",iter, [&](){return rel_27_ApplicationMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;ApplicationMethod;../declarations.dl [31:7-31:35];)",rel_27_ApplicationMethod->size(),iter);}();
/* END STRATUM 26 */
/* BEGIN STRATUM 27 */
[&]() {
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;AssignMayNull;../may-null/rules.dl [6:7-6:72];)",rel_28_AssignMayNull->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;AssignMayNull;../may-null/rules.dl [6:7-6:72];savetime;)_",iter, [&](){return rel_28_AssignMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 27 */
/* BEGIN STRATUM 28 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;BasicBlockHead;../declarations.dl [37:7-37:61];loadtime;)_",iter, [&](){return rel_29_BasicBlockHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;BasicBlockHead;../declarations.dl [37:7-37:61];)",rel_29_BasicBlockHead->size(),iter);}();
/* END STRATUM 28 */
/* BEGIN STRATUM 29 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;CallGraphEdge;../declarations.dl [24:7-24:80];loadtime;)_",iter, [&](){return rel_30_CallGraphEdge->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;CallGraphEdge;../declarations.dl [24:7-24:80];)",rel_30_CallGraphEdge->size(),iter);}();
/* END STRATUM 29 */
/* BEGIN STRATUM 30 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Instruction_Next;../declarations.dl [46:7-46:65];loadtime;)_",iter, [&](){return rel_31_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Instruction_Next;../declarations.dl [46:7-46:65];)",rel_31_Instruction_Next->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;Instruction_Next;../declarations.dl [46:7-46:65];savetime;)_",iter, [&](){return rel_31_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 30 */
/* BEGIN STRATUM 31 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfNull;../declarations.dl [128:7-128:52];loadtime;)_",iter, [&](){return rel_32_IfNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfNull;../declarations.dl [128:7-128:52];)",rel_32_IfNull->size(),iter);}();
/* END STRATUM 31 */
/* BEGIN STRATUM 32 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstruction;../may-null/rules.dl [160:7-160:55];)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   _IfNull(ifIns,_,_).
in file ../may-null/rules.dl [165:1-166:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstruction;../may-null/rules.dl [165:1-166:22];MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
if (!rel_32_IfNull->empty()) [&](){
auto part = rel_32_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_33_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt));
freqs[50]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstruction;../may-null/rules.dl [160:7-160:55];savetime;)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 32 */
/* BEGIN STRATUM 33 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FalseBranch;../may-null/rules.dl [163:7-163:64];)_",iter, [&](){return rel_34_FalseBranch->size();});
SignalHandler::instance()->setMsg(R"_(FalseBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   Instruction_Next(ifIns,insn).
in file ../may-null/rules.dl [172:1-174:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FalseBranch;../may-null/rules.dl [172:1-174:31];FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;)_",iter, [&](){return rel_34_FalseBranch->size();});
if (!rel_31_Instruction_Next->empty()&&!rel_33_MayNull_IfInstruction->empty()) [&](){
auto part = rel_33_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt,rel_31_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_31_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_34_FalseBranch->insert(tuple,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
freqs[51]++;
}
freqs[52]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FalseBranch;../may-null/rules.dl [163:7-163:64];savetime;)_",iter, [&](){return rel_34_FalseBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 33 */
/* BEGIN STRATUM 34 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_OperatorAt;../declarations.dl [122:7-122:52];loadtime;)_",iter, [&](){return rel_35_OperatorAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_OperatorAt;../declarations.dl [122:7-122:52];)",rel_35_OperatorAt->size(),iter);}();
/* END STRATUM 34 */
/* BEGIN STRATUM 35 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstructionsCond;../may-null/rules.dl [161:7-161:96];)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"null",opt) :- 
   _IfNull(ifIns,_,left),
   _OperatorAt(ifIns,opt).
in file ../may-null/rules.dl [168:1-170:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstructionsCond;../may-null/rules.dl [168:1-170:25];MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
if (!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_32_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_36_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt));
freqs[53]++;
}
freqs[54]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstructionsCond;../may-null/rules.dl [161:7-161:96];savetime;)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 35 */
/* BEGIN STRATUM 36 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;PhiNodeHead;../declarations.dl [43:7-43:67];loadtime;)_",iter, [&](){return rel_37_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;PhiNodeHead;../declarations.dl [43:7-43:67];)",rel_37_PhiNodeHead->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHead;../declarations.dl [43:7-43:67];savetime;)_",iter, [&](){return rel_37_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 36 */
/* BEGIN STRATUM 37 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PhiNodeHeadVar;../may-null/rules.dl [180:7-180:52];)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
SignalHandler::instance()->setMsg(R"_(PhiNodeHeadVar(var,headVar) :- 
   PhiNodeHead(insn,headInsn),
   _AssignLocal(insn,_,var,_,_),
   _AssignLocal(headInsn,_,headVar,_,_).
in file ../may-null/rules.dl [182:1-185:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PhiNodeHeadVar;../may-null/rules.dl [182:1-185:42];PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
if (!rel_37_PhiNodeHead->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_37_PhiNodeHead->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_PhiNodeHead_op_ctxt,rel_37_PhiNodeHead->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env0[1],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[2]),static_cast<RamDomain>(env2[2])}});
rel_38_PhiNodeHeadVar->insert(tuple,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
freqs[55]++;
}
freqs[56]++;
}
freqs[57]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHeadVar;../may-null/rules.dl [180:7-180:52];savetime;)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_37_PhiNodeHead->purge();
}();
/* END STRATUM 37 */
/* BEGIN STRATUM 38 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CanBeNullBranch;../may-null/rules.dl [188:7-188:58];)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","==").
in file ../may-null/rules.dl [201:1-202:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;../may-null/rules.dl [201:1-202:54];CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_39_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt));
freqs[58]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [204:1-206:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;../may-null/rules.dl [204:1-206:26];CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (!rel_34_FalseBranch->empty()&&!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_34_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_39_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt));
freqs[59]++;
}
freqs[60]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_40_delta_CanBeNullBranch->insertAll(*rel_39_CanBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CanBeNullBranch;../may-null/rules.dl [188:7-188:58];)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   CanBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [208:1-210:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CanBeNullBranch;0;../may-null/rules.dl [208:1-210:30];CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
if (!rel_40_delta_CanBeNullBranch->empty()&&!rel_38_PhiNodeHeadVar->empty()) [&](){
auto part = rel_40_delta_CanBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_delta_CanBeNullBranch_op_ctxt,rel_40_delta_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_41_new_CanBeNullBranch_op_ctxt,rel_41_new_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_38_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[1]++,!rel_39_CanBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_41_new_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_41_new_CanBeNullBranch_op_ctxt));
}
freqs[61]++;
}
freqs[62]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_41_new_CanBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CanBeNullBranch;../may-null/rules.dl [188:7-188:58];)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
rel_39_CanBeNullBranch->insertAll(*rel_41_new_CanBeNullBranch);
std::swap(rel_40_delta_CanBeNullBranch, rel_41_new_CanBeNullBranch);
rel_41_new_CanBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_40_delta_CanBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_41_new_CanBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CanBeNullBranch;../may-null/rules.dl [188:7-188:58];savetime;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_39_CanBeNullBranch->purge();
}();
/* END STRATUM 38 */
/* BEGIN STRATUM 39 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CannotBeNullBranch;../may-null/rules.dl [187:7-187:61];)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!=").
in file ../may-null/rules.dl [190:1-191:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;../may-null/rules.dl [190:1-191:54];CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_42_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt));
freqs[63]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","=="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [193:1-195:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;../may-null/rules.dl [193:1-195:26];CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (!rel_34_FalseBranch->empty()&&!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_34_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_42_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt));
freqs[64]++;
}
freqs[65]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_43_delta_CannotBeNullBranch->insertAll(*rel_42_CannotBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CannotBeNullBranch;../may-null/rules.dl [187:7-187:61];)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   CannotBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [197:1-199:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CannotBeNullBranch;0;../may-null/rules.dl [197:1-199:30];CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
if (!rel_43_delta_CannotBeNullBranch->empty()&&!rel_38_PhiNodeHeadVar->empty()) [&](){
auto part = rel_43_delta_CannotBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_43_delta_CannotBeNullBranch_op_ctxt,rel_43_delta_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_44_new_CannotBeNullBranch_op_ctxt,rel_44_new_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_38_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[2]++,!rel_42_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_44_new_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_44_new_CannotBeNullBranch_op_ctxt));
}
freqs[66]++;
}
freqs[67]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_44_new_CannotBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CannotBeNullBranch;../may-null/rules.dl [187:7-187:61];)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
rel_42_CannotBeNullBranch->insertAll(*rel_44_new_CannotBeNullBranch);
std::swap(rel_43_delta_CannotBeNullBranch, rel_44_new_CannotBeNullBranch);
rel_44_new_CannotBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_43_delta_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_44_new_CannotBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CannotBeNullBranch;../may-null/rules.dl [187:7-187:61];savetime;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_36_MayNull_IfInstructionsCond->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_38_PhiNodeHeadVar->purge();
}();
/* END STRATUM 39 */
/* BEGIN STRATUM 40 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Dominates;../declarations.dl [40:7-40:59];loadtime;)_",iter, [&](){return rel_45_Dominates->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Dominates;../declarations.dl [40:7-40:59];)",rel_45_Dominates->size(),iter);}();
/* END STRATUM 40 */
/* BEGIN STRATUM 41 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_FormalParam;../declarations.dl [146:7-146:60];loadtime;)_",iter, [&](){return rel_46_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_FormalParam;../declarations.dl [146:7-146:60];)",rel_46_FormalParam->size(),iter);}();
/* END STRATUM 41 */
/* BEGIN STRATUM 42 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_FormalParam;../may-null/rules.dl [4:7-4:97];)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_FormalParam(cat(param,"/map_param/"),method,param,index) :- 
   _FormalParam(index,method,param).
in file ../may-null/rules.dl [80:1-82:34])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_FormalParam;../may-null/rules.dl [80:1-82:34];Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
if (!rel_46_FormalParam->empty()) [&](){
auto part = rel_46_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_46_FormalParam_op_ctxt,rel_46_FormalParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[2]) + symTable.resolve(RamDomain(31)))),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[0])}});
rel_47_Instruction_FormalParam->insert(tuple,READ_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt));
freqs[68]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_FormalParam;../may-null/rules.dl [4:7-4:97];savetime;)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 42 */
/* BEGIN STRATUM 43 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];loadtime;)_",iter, [&](){return rel_48_Var_DeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_48_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];)",rel_48_Var_DeclaringMethod->size(),iter);}();
/* END STRATUM 43 */
/* BEGIN STRATUM 44 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_VarDeclaringMethod;../may-null/rules.dl [3:7-3:89];)_",iter, [&](){return rel_49_Instruction_VarDeclaringMethod->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_VarDeclaringMethod(cat(var,"/var_declaration/"),method,var) :- 
   RefTypeVar(var),
   _Var_DeclaringMethod(var,method),
   !_FormalParam(_,method,var).
in file ../may-null/rules.dl [74:1-78:38])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_VarDeclaringMethod;../may-null/rules.dl [74:1-78:38];Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var).;)_",iter, [&](){return rel_49_Instruction_VarDeclaringMethod->size();});
if (!rel_8_RefTypeVar->empty()&&!rel_48_Var_DeclaringMethod->empty()) [&](){
auto part = rel_8_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_Instruction_VarDeclaringMethod_op_ctxt,rel_49_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_8_RefTypeVar_op_ctxt,rel_8_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_46_FormalParam_op_ctxt,rel_46_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_48_Var_DeclaringMethod_op_ctxt,rel_48_Var_DeclaringMethod->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_48_Var_DeclaringMethod->equalRange_1(key,READ_OP_CONTEXT(rel_48_Var_DeclaringMethod_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,rel_46_FormalParam->equalRange_6(Tuple<RamDomain,3>({{0,env1[1],env0[0]}}),READ_OP_CONTEXT(rel_46_FormalParam_op_ctxt)).empty())) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[0]) + symTable.resolve(RamDomain(30)))),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0])}});
rel_49_Instruction_VarDeclaringMethod->insert(tuple,READ_OP_CONTEXT(rel_49_Instruction_VarDeclaringMethod_op_ctxt));
}
freqs[69]++;
}
freqs[70]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_VarDeclaringMethod;../may-null/rules.dl [3:7-3:89];savetime;)_",iter, [&](){return rel_49_Instruction_VarDeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_48_Var_DeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_46_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_RefTypeVar->purge();
}();
/* END STRATUM 44 */
/* BEGIN STRATUM 45 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Method_FirstInstruction;../declarations.dl [49:7-49:63];loadtime;)_",iter, [&](){return rel_50_Method_FirstInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_50_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Method_FirstInstruction;../declarations.dl [49:7-49:63];)",rel_50_Method_FirstInstruction->size(),iter);}();
/* END STRATUM 45 */
/* BEGIN STRATUM 46 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];loadtime;)_",iter, [&](){return rel_51_MayPredecessorModuloThrow->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_51_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
{
	Logger logger(R"_(@t-nonrecursive-relation;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];)_",iter, [&](){return rel_51_MayPredecessorModuloThrow->size();});
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_VarDeclaringMethod(insn,method,var),
   FirstUse(next,_,var,method).
in file ../may-null/rules.dl [61:1-63:32])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;../may-null/rules.dl [61:1-63:32];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;)_",iter, [&](){return rel_51_MayPredecessorModuloThrow->size();});
if (!rel_25_FirstUse->empty()&&!rel_49_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_49_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
CREATE_OP_CONTEXT(rel_49_Instruction_VarDeclaringMethod_op_ctxt,rel_49_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt,rel_51_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[2],env0[1]}});
auto range = rel_25_FirstUse->equalRange_12(key,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_51_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt));
freqs[71]++;
}
freqs[72]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_FormalParam(insn,method,_,_),
   Method_FirstInstruction(method,next).
in file ../may-null/rules.dl [84:1-86:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;../may-null/rules.dl [84:1-86:39];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;)_",iter, [&](){return rel_51_MayPredecessorModuloThrow->size();});
if (!rel_47_Instruction_FormalParam->empty()&&!rel_50_Method_FirstInstruction->empty()) [&](){
auto part = rel_47_Instruction_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt,rel_51_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_50_Method_FirstInstruction_op_ctxt,rel_50_Method_FirstInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_50_Method_FirstInstruction->equalRange_1(key,READ_OP_CONTEXT(rel_50_Method_FirstInstruction_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_51_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt));
freqs[73]++;
}
freqs[74]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_52_delta_MayPredecessorModuloThrow->insertAll(*rel_51_MayPredecessorModuloThrow);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];)_",iter, [&](){return rel_53_new_MayPredecessorModuloThrow->size();});
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(beforeIf,next) :- 
   FalseBranch(ifIns,next),
   MayPredecessorModuloThrow(beforeIf,ifIns).
in file ../may-null/rules.dl [66:1-68:44])_");
{
	Logger logger(R"_(@t-recursive-rule;MayPredecessorModuloThrow;0;../may-null/rules.dl [66:1-68:44];MayPredecessorModuloThrow(beforeIf,next) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns).;)_",iter, [&](){return rel_53_new_MayPredecessorModuloThrow->size();});
if (!rel_52_delta_MayPredecessorModuloThrow->empty()&&!rel_34_FalseBranch->empty()) [&](){
auto part = rel_34_FalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_52_delta_MayPredecessorModuloThrow_op_ctxt,rel_52_delta_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_53_new_MayPredecessorModuloThrow_op_ctxt,rel_53_new_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt,rel_51_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_52_delta_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_52_delta_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
if( (reads[4]++,!rel_51_MayPredecessorModuloThrow->contains(Tuple<RamDomain,2>({{env1[0],env0[1]}}),READ_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[1])}});
rel_53_new_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_53_new_MayPredecessorModuloThrow_op_ctxt));
}
freqs[75]++;
}
freqs[76]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_53_new_MayPredecessorModuloThrow->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];)_",iter, [&](){return rel_53_new_MayPredecessorModuloThrow->size();});
rel_51_MayPredecessorModuloThrow->insertAll(*rel_53_new_MayPredecessorModuloThrow);
std::swap(rel_52_delta_MayPredecessorModuloThrow, rel_53_new_MayPredecessorModuloThrow);
rel_53_new_MayPredecessorModuloThrow->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_52_delta_MayPredecessorModuloThrow->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_53_new_MayPredecessorModuloThrow->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_50_Method_FirstInstruction->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_25_FirstUse->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_34_FalseBranch->purge();
}();
/* END STRATUM 46 */
/* BEGIN STRATUM 47 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:69];)_",iter, [&](){return rel_54_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method) :- 
   Instruction_VarDeclaringMethod(insn,method,var).
in file ../may-null/rules.dl [89:1-90:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;../may-null/rules.dl [89:1-90:51];MayNullPtr(insn,var,method) :- \n   Instruction_VarDeclaringMethod(insn,method,var).;)_",iter, [&](){return rel_54_MayNullPtr->size();});
if (!rel_49_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_49_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_Instruction_VarDeclaringMethod_op_ctxt,rel_49_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1])}});
rel_54_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt));
freqs[77]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method) :- 
   _AssignNull(insn,_,var,method).
in file ../may-null/rules.dl [92:1-93:35])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;../may-null/rules.dl [92:1-93:35];MayNullPtr(insn,var,method) :- \n   _AssignNull(insn,_,var,method).;)_",iter, [&](){return rel_54_MayNullPtr->size();});
if (!rel_15_AssignNull->empty()) [&](){
auto part = rel_15_AssignNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_54_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt));
freqs[78]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_55_delta_MayNullPtr->insertAll(*rel_54_MayNullPtr);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:69];)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method) :- 
   MayNullPtr(next,from,method),
   _AssignLocal(next,_,from,var,method).
in file ../may-null/rules.dl [119:1-121:42])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [119:1-121:42];MayNullPtr(next,var,method) :- \n   MayNullPtr(next,from,method),\n   _AssignLocal(next,_,from,var,method).;)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
if (!rel_55_delta_MayNullPtr->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_55_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_delta_MayNullPtr_op_ctxt,rel_55_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt,rel_56_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_21(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( (reads[5]++,!rel_54_MayNullPtr->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_56_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt));
}
freqs[79]++;
}
freqs[80]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method) :- 
   MayNullPtr(insn,var,method),
   MayPredecessorModuloThrow(insn,next),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method).
in file ../may-null/rules.dl [130:1-135:31])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [130:1-135:31];MayNullPtr(next,var,method) :- \n   MayNullPtr(insn,var,method),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
if (!rel_55_delta_MayNullPtr->empty()&&!rel_51_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_55_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_delta_MayNullPtr_op_ctxt,rel_55_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt,rel_56_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt,rel_51_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[6]++,rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty())) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_51_MayPredecessorModuloThrow->equalRange_1(key,READ_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
if( (((((reads[2]++,!rel_42_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt)))) && ((reads[7]++,!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt)))))) && ((reads[5]++,!rel_54_MayNullPtr->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2])}});
rel_56_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt));
}
freqs[81]++;
}
}
freqs[82]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method) :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method).
in file ../may-null/rules.dl [137:1-141:51])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [137:1-141:51];MayNullPtr(insn,to,method) :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method).;)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
if (!rel_55_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_55_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_delta_MayNullPtr_op_ctxt,rel_55_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt,rel_56_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( (reads[5]++,!rel_54_MayNullPtr->contains(Tuple<RamDomain,3>({{env2[1],env3[2],env3[3]}}),READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3])}});
rel_56_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt));
}
freqs[83]++;
}
freqs[84]++;
}
freqs[85]++;
}
freqs[86]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method) :- 
   MayNullPtr(from_insn,from,_),
   _ActualParam(index,from_insn,from),
   CallGraphEdge(_,from_insn,_,method),
   Instruction_FormalParam(insn,method,to,index).
in file ../may-null/rules.dl [143:1-147:50])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [143:1-147:50];MayNullPtr(insn,to,method) :- \n   MayNullPtr(from_insn,from,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
if (!rel_55_delta_MayNullPtr->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_47_Instruction_FormalParam->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_55_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_delta_MayNullPtr_op_ctxt,rel_55_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt,rel_56_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],env0[1]}});
auto range = rel_19_ActualParam->equalRange_6(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{0,env0[0],0,0}});
auto range = rel_30_CallGraphEdge->equalRange_2(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{0,env2[3],0,env1[0]}});
auto range = rel_47_Instruction_FormalParam->equalRange_10(key,READ_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt));
for(const auto& env3 : range) {
if( (reads[5]++,!rel_54_MayNullPtr->contains(Tuple<RamDomain,3>({{env3[0],env3[2],env2[3]}}),READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env2[3])}});
rel_56_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_56_new_MayNullPtr_op_ctxt));
}
freqs[87]++;
}
freqs[88]++;
}
freqs[89]++;
}
freqs[90]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_56_new_MayNullPtr->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:69];)_",iter, [&](){return rel_56_new_MayNullPtr->size();});
rel_54_MayNullPtr->insertAll(*rel_56_new_MayNullPtr);
std::swap(rel_55_delta_MayNullPtr, rel_56_new_MayNullPtr);
rel_56_new_MayNullPtr->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_55_delta_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_56_new_MayNullPtr->purge();
{
	Logger logger(R"_(@t-relation-savetime;MayNullPtr;../may-null/rules.dl [1:7-1:69];savetime;)_",iter, [&](){return rel_54_MayNullPtr->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_15_AssignNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_13_AssignLocal->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_20_Return->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_19_ActualParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_49_Instruction_VarDeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_47_Instruction_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_14_DefineVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_26_LastUse->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_5_AssignReturnValue_WithInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_42_CannotBeNullBranch->purge();
}();
/* END STRATUM 47 */
/* BEGIN STRATUM 48 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;InstructionLine;../declarations.dl [16:7-16:67];loadtime;)_",iter, [&](){return rel_57_InstructionLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;InstructionLine;../declarations.dl [16:7-16:67];)",rel_57_InstructionLine->size(),iter);}();
/* END STRATUM 48 */
/* BEGIN STRATUM 49 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignBinop;../declarations.dl [100:7-100:67];loadtime;)_",iter, [&](){return rel_58_AssignBinop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_58_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignBinop;../declarations.dl [100:7-100:67];)",rel_58_AssignBinop->size(),iter);}();
/* END STRATUM 49 */
/* BEGIN STRATUM 50 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignOperFrom;../declarations.dl [103:7-103:51];loadtime;)_",iter, [&](){return rel_59_AssignOperFrom->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_59_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignOperFrom;../declarations.dl [103:7-103:51];)",rel_59_AssignOperFrom->size(),iter);}();
/* END STRATUM 50 */
/* BEGIN STRATUM 51 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignUnop;../declarations.dl [98:7-98:66];loadtime;)_",iter, [&](){return rel_60_AssignUnop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_60_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignUnop;../declarations.dl [98:7-98:66];)",rel_60_AssignUnop->size(),iter);}();
/* END STRATUM 51 */
/* BEGIN STRATUM 52 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_EnterMonitor;../declarations.dl [111:7-111:68];loadtime;)_",iter, [&](){return rel_61_EnterMonitor->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_61_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_EnterMonitor;../declarations.dl [111:7-111:68];)",rel_61_EnterMonitor->size(),iter);}();
/* END STRATUM 52 */
/* BEGIN STRATUM 53 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadArrayIndex;../declarations.dl [60:7-60:81];loadtime;)_",iter, [&](){return rel_62_LoadArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadArrayIndex;../declarations.dl [60:7-60:81];)",rel_62_LoadArrayIndex->size(),iter);}();
/* END STRATUM 53 */
/* BEGIN STRATUM 54 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ThrowNull;../declarations.dl [83:7-83:56];loadtime;)_",iter, [&](){return rel_63_ThrowNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ThrowNull;../declarations.dl [83:7-83:56];)",rel_63_ThrowNull->size(),iter);}();
/* END STRATUM 54 */
/* BEGIN STRATUM 55 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NullAt;../declarations.dl [158:7-158:76];)_",iter, [&](){return rel_64_NullAt->size();});
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw NullPointerException","throw NPE",a) :- 
   CallGraphEdge(_,a,_,b),
   _SpecialMethodInvocation(a,index,b,_,meth),
   "java.lang.NullPointerException" contains a.
in file ../rules.dl [8:1-11:48])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [8:1-11:48];NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_30_CallGraphEdge->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_30_CallGraphEdge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(11))) != std::string::npos)) {
const Tuple<RamDomain,5> key({{env0[1],0,env0[3],0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_5(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(9)),static_cast<RamDomain>(RamDomain(10)),static_cast<RamDomain>(env0[1])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[91]++;
}
}
freqs[92]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Array Index",var,insn) :- 
   VarPointsToNull(var),
   _LoadArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [16:1-18:44])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [16:1-18:44];NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_62_LoadArrayIndex->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_62_LoadArrayIndex_op_ctxt,rel_62_LoadArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_62_LoadArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_62_LoadArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(12)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[93]++;
}
freqs[94]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Array Index",var,insn) :- 
   VarPointsToNull(var),
   _StoreArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [28:1-30:45])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [28:1-30:45];NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_21_StoreArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(13)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[95]++;
}
freqs[96]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _StoreInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [40:1-42:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [40:1-42:51];NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_22_StoreInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(14)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[97]++;
}
freqs[98]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _LoadInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [53:1-55:50])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [53:1-55:50];NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_16_LoadInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(15)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[99]++;
}
freqs[100]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Virtual Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _VirtualMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [66:1-68:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [66:1-68:53];NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(16)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[101]++;
}
freqs[102]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Special Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _SpecialMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [78:1-80:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [78:1-80:53];NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(17)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[103]++;
}
freqs[104]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Unary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignUnop(insn,index,_,meth),
   _AssignOperFrom(insn,var).
in file ../rules.dl [90:1-93:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [90:1-93:28];NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_59_AssignOperFrom->empty()&&!rel_60_AssignUnop->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_59_AssignOperFrom_op_ctxt,rel_59_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_60_AssignUnop_op_ctxt,rel_60_AssignUnop->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_60_AssignUnop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_59_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_59_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(18)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[105]++;
}
freqs[106]++;
}
freqs[107]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Binary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignBinop(insn,index,_,meth),
   _AssignOperFrom(insn,var).
in file ../rules.dl [106:1-109:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [106:1-109:28];NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_58_AssignBinop->empty()&&!rel_59_AssignOperFrom->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_58_AssignBinop_op_ctxt,rel_58_AssignBinop->createContext());
CREATE_OP_CONTEXT(rel_59_AssignOperFrom_op_ctxt,rel_59_AssignOperFrom->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_58_AssignBinop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_59_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_59_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(19)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[108]++;
}
freqs[109]++;
}
freqs[110]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw Null","throw null",insn) :- 
   _ThrowNull(insn,index,meth).
in file ../rules.dl [121:1-122:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [121:1-122:31];NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_63_ThrowNull->empty()) [&](){
auto part = rel_63_ThrowNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_63_ThrowNull_op_ctxt,rel_63_ThrowNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(20)),static_cast<RamDomain>(RamDomain(21)),static_cast<RamDomain>(env0[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[111]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Enter Monitor (Synchronized)",var,insn) :- 
   VarPointsToNull(var),
   _EnterMonitor(insn,index,var,meth).
in file ../rules.dl [127:1-129:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [127:1-129:39];NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;)_",iter, [&](){return rel_64_NullAt->size();});
if (!rel_11_VarPointsToNull->empty()&&!rel_61_EnterMonitor->empty()) [&](){
auto part = rel_11_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_61_EnterMonitor_op_ctxt,rel_61_EnterMonitor->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_61_EnterMonitor->equalRange_4(key,READ_OP_CONTEXT(rel_61_EnterMonitor_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(22)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_64_NullAt->insert(tuple,READ_OP_CONTEXT(rel_64_NullAt_op_ctxt));
freqs[112]++;
}
freqs[113]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_30_CallGraphEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_SpecialMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_62_LoadArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_21_StoreArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_16_LoadInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_22_StoreInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_VirtualMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_63_ThrowNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_60_AssignUnop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_58_AssignBinop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_59_AssignOperFrom->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_61_EnterMonitor->purge();
}();
/* END STRATUM 55 */
/* BEGIN STRATUM 56 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Reachable;../declarations.dl [28:7-28:27];loadtime;)_",iter, [&](){return rel_65_Reachable->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/Reachable.csv"},{"name","Reachable"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_65_Reachable);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Reachable;../declarations.dl [28:7-28:27];)",rel_65_Reachable->size(),iter);}();
/* END STRATUM 56 */
/* BEGIN STRATUM 57 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReachableNullAt;../declarations.dl [161:7-161:85];)_",iter, [&](){return rel_66_ReachableNullAt->size();});
SignalHandler::instance()->setMsg(R"_(ReachableNullAt(meth,index,type,var,insn) :- 
   NullAt(meth,index,type,var,insn),
   Reachable(meth).
in file ../rules.dl [140:1-140:104])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReachableNullAt;../rules.dl [140:1-140:104];ReachableNullAt(meth,index,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   Reachable(meth).;)_",iter, [&](){return rel_66_ReachableNullAt->size();});
if (!rel_64_NullAt->empty()&&!rel_65_Reachable->empty()) [&](){
auto part = rel_64_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_NullAt_op_ctxt,rel_64_NullAt->createContext());
CREATE_OP_CONTEXT(rel_65_Reachable_op_ctxt,rel_65_Reachable->createContext());
CREATE_OP_CONTEXT(rel_66_ReachableNullAt_op_ctxt,rel_66_ReachableNullAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_65_Reachable->equalRange_1(key,READ_OP_CONTEXT(rel_65_Reachable_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_66_ReachableNullAt->insert(tuple,READ_OP_CONTEXT(rel_66_ReachableNullAt_op_ctxt));
freqs[114]++;
}
freqs[115]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_65_Reachable->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_64_NullAt->purge();
}();
/* END STRATUM 57 */
/* BEGIN STRATUM 58 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReachableNullAtLine;../declarations.dl [164:7-164:113];)_",iter, [&](){return rel_67_ReachableNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(ReachableNullAtLine(meth,index,file,line,type,var,insn) :- 
   ReachableNullAt(meth,index,type,var,insn),
   ApplicationMethod(meth),
   InstructionLine(meth,index,line,file).
in file ../rules.dl [144:1-147:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReachableNullAtLine;../rules.dl [144:1-147:42];ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;)_",iter, [&](){return rel_67_ReachableNullAtLine->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_57_InstructionLine->empty()&&!rel_66_ReachableNullAt->empty()) [&](){
auto part = rel_66_ReachableNullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_57_InstructionLine_op_ctxt,rel_57_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_66_ReachableNullAt_op_ctxt,rel_66_ReachableNullAt->createContext());
CREATE_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt,rel_67_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env0[0],env0[1],0,0}});
auto range = rel_57_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_57_InstructionLine_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_67_ReachableNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt));
freqs[116]++;
}
freqs[117]++;
}
freqs[118]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_57_InstructionLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_27_ApplicationMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_66_ReachableNullAt->purge();
}();
/* END STRATUM 58 */
/* BEGIN STRATUM 59 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfVar;../declarations.dl [125:7-125:51];loadtime;)_",iter, [&](){return rel_68_IfVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_68_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfVar;../declarations.dl [125:7-125:51];)",rel_68_IfVar->size(),iter);}();
/* END STRATUM 59 */
/* BEGIN STRATUM 60 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructions;../path_sensitivity/rules.dl [1:7-1:66];)_",iter, [&](){return rel_69_IfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   BasicBlockHead(ins,headIns),
   MayPredecessorModuloThrow(ifIns,headIns).
in file ../path_sensitivity/rules.dl [8:1-11:43])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;../path_sensitivity/rules.dl [8:1-11:43];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;)_",iter, [&](){return rel_69_IfInstructions->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_51_MayPredecessorModuloThrow->empty()&&!rel_67_ReachableNullAtLine->empty()) [&](){
auto part = rel_67_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_69_IfInstructions_op_ctxt,rel_69_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt,rel_51_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt,rel_67_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{0,env1[1]}});
auto range = rel_51_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_51_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env2[0])}});
rel_69_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_69_IfInstructions_op_ctxt));
freqs[119]++;
}
freqs[120]++;
}
freqs[121]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,var,ins),
   _IfVar(ifIns,_,var).
in file ../path_sensitivity/rules.dl [14:1-16:23])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;../path_sensitivity/rules.dl [14:1-16:23];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;)_",iter, [&](){return rel_69_IfInstructions->size();});
if (!rel_67_ReachableNullAtLine->empty()&&!rel_68_IfVar->empty()) [&](){
auto part = rel_67_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_IfInstructions_op_ctxt,rel_69_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt,rel_67_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_68_IfVar_op_ctxt,rel_68_IfVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[5]}});
auto range = rel_68_IfVar->equalRange_4(key,READ_OP_CONTEXT(rel_68_IfVar_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env1[0])}});
rel_69_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_69_IfInstructions_op_ctxt));
freqs[122]++;
}
freqs[123]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructions;../path_sensitivity/rules.dl [1:7-1:66];savetime;)_",iter, [&](){return rel_69_IfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_51_MayPredecessorModuloThrow->purge();
}();
/* END STRATUM 60 */
/* BEGIN STRATUM 61 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;VarEqlsConst;../declarations.dl [119:7-119:42];loadtime;)_",iter, [&](){return rel_70_VarEqlsConst->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/mainAnalysis.VarEqlsConst.csv"},{"name","VarEqlsConst"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_70_VarEqlsConst);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;VarEqlsConst;../declarations.dl [119:7-119:42];)",rel_70_VarEqlsConst->size(),iter);}();
/* END STRATUM 61 */
/* BEGIN STRATUM 62 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructionsCond;../path_sensitivity/rules.dl [2:7-2:94];)_",iter, [&](){return rel_71_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   varLeft = "null".
in file ../path_sensitivity/rules.dl [34:1-38:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;../path_sensitivity/rules.dl [34:1-38:18];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;)_",iter, [&](){return rel_71_IfInstructionsCond->size();});
if (!rel_69_IfInstructions->empty()&&!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_69_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_IfInstructions_op_ctxt,rel_69_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,RamDomain(23)}});
auto range = rel_32_IfNull->equalRange_5(key,READ_OP_CONTEXT(rel_32_IfNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_71_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt));
freqs[124]++;
}
freqs[125]++;
}
freqs[126]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   VarPointsToNull(varLeft).
in file ../path_sensitivity/rules.dl [40:1-44:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;../path_sensitivity/rules.dl [40:1-44:26];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;)_",iter, [&](){return rel_71_IfInstructionsCond->size();});
if (!rel_69_IfInstructions->empty()&&!rel_11_VarPointsToNull->empty()&&!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_69_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_IfInstructions_op_ctxt,rel_69_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt,rel_11_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,0}});
auto range = rel_32_IfNull->equalRange_1(key,READ_OP_CONTEXT(rel_32_IfNull_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_11_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_11_VarPointsToNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_71_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt));
freqs[127]++;
}
freqs[128]++;
}
freqs[129]++;
}
freqs[130]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,num_left,num_right,opt) :- 
   IfInstructions(_,ifIns),
   _IfVar(ifIns,1,left),
   VarEqlsConst(left,num_left),
   _IfVar(ifIns,2,right),
   VarEqlsConst(right,num_right),
   _OperatorAt(ifIns,opt).
in file ../path_sensitivity/rules.dl [46:1-52:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;../path_sensitivity/rules.dl [46:1-52:25];IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;)_",iter, [&](){return rel_71_IfInstructionsCond->size();});
if (!rel_69_IfInstructions->empty()&&!rel_70_VarEqlsConst->empty()&&!rel_68_IfVar->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_69_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_IfInstructions_op_ctxt,rel_69_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_70_VarEqlsConst_op_ctxt,rel_70_VarEqlsConst->createContext());
CREATE_OP_CONTEXT(rel_68_IfVar_op_ctxt,rel_68_IfVar->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{env0[1],RamDomain(1),0}});
auto range = rel_68_IfVar->equalRange_3(key,READ_OP_CONTEXT(rel_68_IfVar_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{env1[2],0}});
auto range = rel_70_VarEqlsConst->equalRange_1(key,READ_OP_CONTEXT(rel_70_VarEqlsConst_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,3> key({{env0[1],RamDomain(2),0}});
auto range = rel_68_IfVar->equalRange_3(key,READ_OP_CONTEXT(rel_68_IfVar_op_ctxt));
for(const auto& env3 : range) {
const Tuple<RamDomain,2> key({{env3[2],0}});
auto range = rel_70_VarEqlsConst->equalRange_1(key,READ_OP_CONTEXT(rel_70_VarEqlsConst_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env5 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env4[1]),static_cast<RamDomain>(env5[1])}});
rel_71_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt));
freqs[131]++;
}
freqs[132]++;
}
freqs[133]++;
}
freqs[134]++;
}
freqs[135]++;
}
freqs[136]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructionsCond;../path_sensitivity/rules.dl [2:7-2:94];savetime;)_",iter, [&](){return rel_71_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_71_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_70_VarEqlsConst->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_35_OperatorAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_68_IfVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_32_IfNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_11_VarPointsToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_69_IfInstructions->purge();
}();
/* END STRATUM 62 */
/* BEGIN STRATUM 63 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueIfInstructions;../path_sensitivity/rules.dl [3:7-3:52];)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"!="),
   ord(left) != ord(right).
in file ../path_sensitivity/rules.dl [54:1-56:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [54:1-56:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(24)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) != (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[137]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"=="),
   ord(left) = ord(right).
in file ../path_sensitivity/rules.dl [58:1-60:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [58:1-60:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(25)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) == (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[138]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<="),
   ord(left) <= ord(right).
in file ../path_sensitivity/rules.dl [62:1-64:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [62:1-64:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(26)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) <= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[139]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">="),
   ord(left) >= ord(right).
in file ../path_sensitivity/rules.dl [66:1-68:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [66:1-68:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(27)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) >= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[140]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<"),
   ord(left) < ord(right).
in file ../path_sensitivity/rules.dl [70:1-72:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [70:1-72:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(28)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) < (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[141]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">"),
   ord(left) > ord(right).
in file ../path_sensitivity/rules.dl [74:1-76:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [74:1-76:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (!rel_71_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(29)}});
auto range = rel_71_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_IfInstructionsCond_op_ctxt,rel_71_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) > (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_72_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt));
}
freqs[142]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueIfInstructions;../path_sensitivity/rules.dl [3:7-3:52];savetime;)_",iter, [&](){return rel_72_TrueIfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_72_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_71_IfInstructionsCond->purge();
}();
/* END STRATUM 63 */
/* BEGIN STRATUM 64 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DominatesUnreachable;../path_sensitivity/rules.dl [5:7-5:72];)_",iter, [&](){return rel_73_DominatesUnreachable->size();});
SignalHandler::instance()->setMsg(R"_(DominatesUnreachable(ifIns,ins) :- 
   TrueIfInstructions(ifIns),
   Instruction_Next(ifIns,ins).
in file ../path_sensitivity/rules.dl [78:1-80:30])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DominatesUnreachable;../path_sensitivity/rules.dl [78:1-80:30];DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;)_",iter, [&](){return rel_73_DominatesUnreachable->size();});
if (!rel_31_Instruction_Next->empty()&&!rel_72_TrueIfInstructions->empty()) [&](){
auto part = rel_72_TrueIfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_73_DominatesUnreachable_op_ctxt,rel_73_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt,rel_31_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_72_TrueIfInstructions_op_ctxt,rel_72_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_31_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_73_DominatesUnreachable->insert(tuple,READ_OP_CONTEXT(rel_73_DominatesUnreachable_op_ctxt));
freqs[143]++;
}
freqs[144]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DominatesUnreachable;../path_sensitivity/rules.dl [5:7-5:72];savetime;)_",iter, [&](){return rel_73_DominatesUnreachable->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_31_Instruction_Next->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_72_TrueIfInstructions->purge();
}();
/* END STRATUM 64 */
/* BEGIN STRATUM 65 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;UnreachablePathNPEIns;../path_sensitivity/rules.dl [4:7-4:55];)_",iter, [&](){return rel_74_UnreachablePathNPEIns->size();});
SignalHandler::instance()->setMsg(R"_(UnreachablePathNPEIns(ins) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   DominatesUnreachable(_,parent),
   BasicBlockHead(ins,headIns),
   Dominates(parent,headIns).
in file ../path_sensitivity/rules.dl [86:1-90:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;UnreachablePathNPEIns;../path_sensitivity/rules.dl [86:1-90:28];UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;)_",iter, [&](){return rel_74_UnreachablePathNPEIns->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_45_Dominates->empty()&&!rel_73_DominatesUnreachable->empty()&&!rel_67_ReachableNullAtLine->empty()) [&](){
auto part = rel_67_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_45_Dominates_op_ctxt,rel_45_Dominates->createContext());
CREATE_OP_CONTEXT(rel_73_DominatesUnreachable_op_ctxt,rel_73_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt,rel_67_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_74_UnreachablePathNPEIns_op_ctxt,rel_74_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_73_DominatesUnreachable) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{env1[1],env2[1]}});
auto range = rel_45_Dominates->equalRange_3(key,READ_OP_CONTEXT(rel_45_Dominates_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[6])}});
rel_74_UnreachablePathNPEIns->insert(tuple,READ_OP_CONTEXT(rel_74_UnreachablePathNPEIns_op_ctxt));
freqs[145]++;
}
freqs[146]++;
}
freqs[147]++;
}
freqs[148]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;UnreachablePathNPEIns;../path_sensitivity/rules.dl [4:7-4:55];savetime;)_",iter, [&](){return rel_74_UnreachablePathNPEIns->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_74_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_29_BasicBlockHead->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_45_Dominates->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_73_DominatesUnreachable->purge();
}();
/* END STRATUM 65 */
/* BEGIN STRATUM 66 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PathSensitiveNullAtLine;../declarations.dl [167:7-167:117];)_",iter, [&](){return rel_75_PathSensitiveNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- 
   ReachableNullAtLine(meth,index,file,line,type,var,ins),
   !UnreachablePathNPEIns(ins).
in file ../path_sensitivity/rules.dl [92:1-94:29])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PathSensitiveNullAtLine;../path_sensitivity/rules.dl [92:1-94:29];PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;)_",iter, [&](){return rel_75_PathSensitiveNullAtLine->size();});
if (!rel_67_ReachableNullAtLine->empty()) [&](){
auto part = rel_67_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_75_PathSensitiveNullAtLine_op_ctxt,rel_75_PathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_67_ReachableNullAtLine_op_ctxt,rel_67_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_74_UnreachablePathNPEIns_op_ctxt,rel_74_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[8]++,!rel_74_UnreachablePathNPEIns->contains(Tuple<RamDomain,1>({{env0[6]}}),READ_OP_CONTEXT(rel_74_UnreachablePathNPEIns_op_ctxt)))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_75_PathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_75_PathSensitiveNullAtLine_op_ctxt));
}
freqs[149]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_67_ReachableNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_74_UnreachablePathNPEIns->purge();
}();
/* END STRATUM 66 */
/* BEGIN STRATUM 67 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NPEWithMayNull;../may-null/rules.dl [212:7-212:115];)_",iter, [&](){return rel_76_NPEWithMayNull->size();});
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(meth,index,file,line,type,var,insn) :- 
   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   MayNullPtr(insn,var,meth).
in file ../may-null/rules.dl [215:1-218:29])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;../may-null/rules.dl [215:1-218:29];NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth).;)_",iter, [&](){return rel_76_NPEWithMayNull->size();});
if (!rel_54_MayNullPtr->empty()&&!rel_75_PathSensitiveNullAtLine->empty()) [&](){
auto part = rel_75_PathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt,rel_54_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_76_NPEWithMayNull_op_ctxt,rel_76_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_75_PathSensitiveNullAtLine_op_ctxt,rel_75_PathSensitiveNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{env0[6],env0[5],env0[0]}});
auto range = rel_54_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_54_MayNullPtr_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_76_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_76_NPEWithMayNull_op_ctxt));
freqs[150]++;
}
freqs[151]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NPEWithMayNull;../may-null/rules.dl [212:7-212:115];savetime;)_",iter, [&](){return rel_76_NPEWithMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_75_PathSensitiveNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_54_MayNullPtr->purge();
}();
/* END STRATUM 67 */
/* BEGIN STRATUM 68 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;JumpTarget;../declarations.dl [116:7-116:53];loadtime;)_",iter, [&](){return rel_77_JumpTarget->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_77_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;JumpTarget;../declarations.dl [116:7-116:53];)",rel_77_JumpTarget->size(),iter);}();
/* END STRATUM 68 */
/* BEGIN STRATUM 69 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueBranch;../may-null/rules.dl [162:7-162:63];)_",iter, [&](){return rel_78_TrueBranch->size();});
SignalHandler::instance()->setMsg(R"_(TrueBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   JumpTarget(insn,ifIns).
in file ../may-null/rules.dl [176:1-178:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueBranch;../may-null/rules.dl [176:1-178:25];TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;)_",iter, [&](){return rel_78_TrueBranch->size();});
if (!rel_77_JumpTarget->empty()&&!rel_33_MayNull_IfInstruction->empty()) [&](){
auto part = rel_33_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_77_JumpTarget_op_ctxt,rel_77_JumpTarget->createContext());
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_78_TrueBranch_op_ctxt,rel_78_TrueBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_77_JumpTarget->equalRange_2(key,READ_OP_CONTEXT(rel_77_JumpTarget_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_78_TrueBranch->insert(tuple,READ_OP_CONTEXT(rel_78_TrueBranch_op_ctxt));
freqs[152]++;
}
freqs[153]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueBranch;../may-null/rules.dl [162:7-162:63];savetime;)_",iter, [&](){return rel_78_TrueBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_77_JumpTarget->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_33_MayNull_IfInstruction->purge();
}();
/* END STRATUM 69 */
}
ProfileEventSingleton::instance().stopTimer();
dumpFreqs();

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_AssignReturnValue:\n";
rel_1_AssignReturnValue->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_SpecialMethodInvocation:\n";
rel_2_SpecialMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_StaticMethodInvocation:\n";
rel_3_StaticMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_VirtualMethodInvocation:\n";
rel_4_VirtualMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_AssignReturnValue_WithInvoke:\n";
rel_5_AssignReturnValue_WithInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_Primitive:\n";
rel_6_Primitive->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_Var_Type:\n";
rel_7_Var_Type->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_RefTypeVar:\n";
rel_8_RefTypeVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_AssignCast:\n";
rel_9_AssignCast->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_AssignCastNull:\n";
rel_10_AssignCastNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_11_VarPointsToNull:\n";
rel_11_VarPointsToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_12_AssignHeapAllocation:\n";
rel_12_AssignHeapAllocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_13_AssignLocal:\n";
rel_13_AssignLocal->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_14_DefineVar:\n";
rel_14_DefineVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_15_AssignNull:\n";
rel_15_AssignNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_16_LoadInstanceField:\n";
rel_16_LoadInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_17_LoadStaticField:\n";
rel_17_LoadStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_18_VarDef:\n";
rel_18_VarDef->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_19_ActualParam:\n";
rel_19_ActualParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_20_Return:\n";
rel_20_Return->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_21_StoreArrayIndex:\n";
rel_21_StoreArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_22_StoreInstanceField:\n";
rel_22_StoreInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_23_StoreStaticField:\n";
rel_23_StoreStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_24_AllUse:\n";
rel_24_AllUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_25_FirstUse:\n";
rel_25_FirstUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_26_LastUse:\n";
rel_26_LastUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_27_ApplicationMethod:\n";
rel_27_ApplicationMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_28_AssignMayNull:\n";
rel_28_AssignMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_29_BasicBlockHead:\n";
rel_29_BasicBlockHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_30_CallGraphEdge:\n";
rel_30_CallGraphEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_31_Instruction_Next:\n";
rel_31_Instruction_Next->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_32_IfNull:\n";
rel_32_IfNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_33_MayNull_IfInstruction:\n";
rel_33_MayNull_IfInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_34_FalseBranch:\n";
rel_34_FalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_35_OperatorAt:\n";
rel_35_OperatorAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_36_MayNull_IfInstructionsCond:\n";
rel_36_MayNull_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_37_PhiNodeHead:\n";
rel_37_PhiNodeHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_38_PhiNodeHeadVar:\n";
rel_38_PhiNodeHeadVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_39_CanBeNullBranch:\n";
rel_39_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_40_delta_CanBeNullBranch:\n";
rel_40_delta_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_41_new_CanBeNullBranch:\n";
rel_41_new_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_42_CannotBeNullBranch:\n";
rel_42_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_43_delta_CannotBeNullBranch:\n";
rel_43_delta_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_44_new_CannotBeNullBranch:\n";
rel_44_new_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_45_Dominates:\n";
rel_45_Dominates->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_46_FormalParam:\n";
rel_46_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_47_Instruction_FormalParam:\n";
rel_47_Instruction_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_48_Var_DeclaringMethod:\n";
rel_48_Var_DeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_49_Instruction_VarDeclaringMethod:\n";
rel_49_Instruction_VarDeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_50_Method_FirstInstruction:\n";
rel_50_Method_FirstInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_51_MayPredecessorModuloThrow:\n";
rel_51_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_52_delta_MayPredecessorModuloThrow:\n";
rel_52_delta_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_53_new_MayPredecessorModuloThrow:\n";
rel_53_new_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_54_MayNullPtr:\n";
rel_54_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_55_delta_MayNullPtr:\n";
rel_55_delta_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_56_new_MayNullPtr:\n";
rel_56_new_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_57_InstructionLine:\n";
rel_57_InstructionLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_58_AssignBinop:\n";
rel_58_AssignBinop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_59_AssignOperFrom:\n";
rel_59_AssignOperFrom->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_60_AssignUnop:\n";
rel_60_AssignUnop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_61_EnterMonitor:\n";
rel_61_EnterMonitor->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_62_LoadArrayIndex:\n";
rel_62_LoadArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_63_ThrowNull:\n";
rel_63_ThrowNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_64_NullAt:\n";
rel_64_NullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_65_Reachable:\n";
rel_65_Reachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_66_ReachableNullAt:\n";
rel_66_ReachableNullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_67_ReachableNullAtLine:\n";
rel_67_ReachableNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_68_IfVar:\n";
rel_68_IfVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_69_IfInstructions:\n";
rel_69_IfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_70_VarEqlsConst:\n";
rel_70_VarEqlsConst->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_71_IfInstructionsCond:\n";
rel_71_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_72_TrueIfInstructions:\n";
rel_72_TrueIfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_73_DominatesUnreachable:\n";
rel_73_DominatesUnreachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_74_UnreachablePathNPEIns:\n";
rel_74_UnreachablePathNPEIns->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_75_PathSensitiveNullAtLine:\n";
rel_75_PathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_76_NPEWithMayNull:\n";
rel_76_NPEWithMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_77_JumpTarget:\n";
rel_77_JumpTarget->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_78_TrueBranch:\n";
rel_78_TrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction(".", ".", stratumIndex, false); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction(inputDirectory, outputDirectory, stratumIndex, true);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_8_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_11_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_71_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_72_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_74_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
private:
void dumpFreqs() {
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;0;)_", freqs[28],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;VarDef(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;1;)_", freqs[27],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;AssignReturnValue_WithInvoke(insn,index,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;2;)_", freqs[45],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;0;)_", freqs[47],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;1;)_", freqs[46],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;0;)_", freqs[32],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;_AssignCast(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;1;)_", freqs[31],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;0;)_", freqs[30],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;_AssignLocal(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;1;)_", freqs[29],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;0;)_", freqs[44],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;_Return(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;1;)_", freqs[43],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;0;)_", freqs[34],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;_SpecialMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;1;)_", freqs[33],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;0;)_", freqs[38],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;_StoreArrayIndex(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;1;)_", freqs[37],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;0;)_", freqs[40],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;_StoreInstanceField(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;1;)_", freqs[39],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;0;)_", freqs[42],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;_StoreStaticField(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;1;)_", freqs[41],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;0;)_", freqs[36],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;_VirtualMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;1;)_", freqs[35],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;0;)_", freqs[5],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_SpecialMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;1;)_", freqs[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;0;)_", freqs[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_StaticMethodInvocation(insn,index,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;1;)_", freqs[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;0;)_", freqs[3],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_VirtualMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;1;)_", freqs[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;@delta_CanBeNullBranch(insn,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[62],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[61],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;0;)_", freqs[58],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[59],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[60],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;@delta_CannotBeNullBranch(insn,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[67],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[66],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;0;)_", freqs[63],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[64],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[65],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;AssignReturnValue_WithInvoke(insn,_,to,method);DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;0;)_", freqs[10],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;_AssignLocal(insn,_,_,to,method);DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;0;)_", freqs[9],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;_AssignHeapAllocation(insn,_,_,var,method,_);DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;0;)_", freqs[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;Instruction_Next(ifIns,ins);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;1;)_", freqs[143],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;TrueIfInstructions(ifIns);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;0;)_", freqs[144],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;Instruction_Next(ifIns,insn);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;1;)_", freqs[51],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;MayNull_IfInstruction(ifIns);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;0;)_", freqs[52],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FirstUse;0;FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;AllUse(insn,last,var,method);FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;0;)_", freqs[48],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;BasicBlockHead(ins,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;1;)_", freqs[120],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;MayPredecessorModuloThrow(ifIns,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;2;)_", freqs[119],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;0;)_", freqs[121],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;ReachableNullAtLine(_,_,_,_,_,var,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;0;)_", freqs[123],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;_IfVar(ifIns,_,var);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;1;)_", freqs[122],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;0;)_", freqs[130],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;VarPointsToNull(varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;3;)_", freqs[127],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;2;)_", freqs[128],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;1;)_", freqs[129],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;0;)_", freqs[126],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;2;)_", freqs[124],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;1;)_", freqs[125],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;0;)_", freqs[136],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;VarEqlsConst(left,num_left);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;2;)_", freqs[134],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;VarEqlsConst(right,num_right);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;4;)_", freqs[132],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;_IfVar(ifIns,1,left);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;1;)_", freqs[135],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;_IfVar(ifIns,2,right);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;3;)_", freqs[133],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,num_left,num_right,opt) :- \n   IfInstructions(_,ifIns),\n   _IfVar(ifIns,1,left),\n   VarEqlsConst(left,num_left),\n   _IfVar(ifIns,2,right),\n   VarEqlsConst(right,num_right),\n   _OperatorAt(ifIns,opt).;5;)_", freqs[131],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_FormalParam;0;Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;_FormalParam(index,method,param);Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;0;)_", freqs[68],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var).;RefTypeVar(var);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var).;0;)_", freqs[70],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var).;_Var_DeclaringMethod(var,method);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var).;1;)_", freqs[69],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;LastUse;0;LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;AllUse(insn,last,var,method);LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;0;)_", freqs[49],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var2,from_insn, _unnamed_var3,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method).;@delta_MayNullPtr(from_insn,from, _unnamed_var1);MayNullPtr(insn,to,method) :- \n   MayNullPtr(from_insn,from,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;0;)_", freqs[90],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var2,from_insn, _unnamed_var3,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method).;CallGraphEdge( _unnamed_var2,from_insn, _unnamed_var3,method);MayNullPtr(insn,to,method) :- \n   MayNullPtr(from_insn,from,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;2;)_", freqs[88],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var2,from_insn, _unnamed_var3,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method).;Instruction_FormalParam(insn,method,to,index);MayNullPtr(insn,to,method) :- \n   MayNullPtr(from_insn,from,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;3;)_", freqs[87],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var2,from_insn, _unnamed_var3,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method).;_ActualParam(index,from_insn,from);MayNullPtr(insn,to,method) :- \n   MayNullPtr(from_insn,from,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;1;)_", freqs[89],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn, _unnamed_var1,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var2,insn, _unnamed_var3,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var4,to,method),\n   !MayNullPtr(insn,to,method).;@delta_MayNullPtr(returnInsn,returnVar,invokedMethod);MayNullPtr(insn,to,method) :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method).;0;)_", freqs[86],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn, _unnamed_var1,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var2,insn, _unnamed_var3,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var4,to,method),\n   !MayNullPtr(insn,to,method).;AssignReturnValue_WithInvoke(insn, _unnamed_var4,to,method);MayNullPtr(insn,to,method) :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method).;3;)_", freqs[83],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn, _unnamed_var1,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var2,insn, _unnamed_var3,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var4,to,method),\n   !MayNullPtr(insn,to,method).;CallGraphEdge( _unnamed_var2,insn, _unnamed_var3,invokedMethod);MayNullPtr(insn,to,method) :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method).;2;)_", freqs[84],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method) :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn, _unnamed_var1,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var2,insn, _unnamed_var3,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var4,to,method),\n   !MayNullPtr(insn,to,method).;_Return(returnInsn, _unnamed_var1,returnVar,invokedMethod);MayNullPtr(insn,to,method) :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method).;1;)_", freqs[85],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method) :- \n   @delta_MayNullPtr(insn,var,method),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method).;@delta_MayNullPtr(insn,var,method);MayNullPtr(next,var,method) :- \n   MayNullPtr(insn,var,method),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;0;)_", freqs[82],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method) :- \n   @delta_MayNullPtr(insn,var,method),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method).;MayPredecessorModuloThrow(insn,next);MayNullPtr(next,var,method) :- \n   MayNullPtr(insn,var,method),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;1;)_", freqs[81],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method) :- \n   @delta_MayNullPtr(next,from,method),\n   _AssignLocal(next, _unnamed_var1,from,var,method),\n   !MayNullPtr(next,var,method).;@delta_MayNullPtr(next,from,method);MayNullPtr(next,var,method) :- \n   MayNullPtr(next,from,method),\n   _AssignLocal(next,_,from,var,method).;0;)_", freqs[80],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method) :- \n   @delta_MayNullPtr(next,from,method),\n   _AssignLocal(next, _unnamed_var1,from,var,method),\n   !MayNullPtr(next,var,method).;_AssignLocal(next, _unnamed_var1,from,var,method);MayNullPtr(next,var,method) :- \n   MayNullPtr(next,from,method),\n   _AssignLocal(next,_,from,var,method).;1;)_", freqs[79],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method) :- \n   Instruction_VarDeclaringMethod(insn,method,var).;Instruction_VarDeclaringMethod(insn,method,var);MayNullPtr(insn,var,method) :- \n   Instruction_VarDeclaringMethod(insn,method,var).;0;)_", freqs[77],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method) :- \n   _AssignNull(insn,_,var,method).;_AssignNull(insn,_,var,method);MayNullPtr(insn,var,method) :- \n   _AssignNull(insn,_,var,method).;0;)_", freqs[78],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstruction;0;MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;_IfNull(ifIns,_,_);MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;0;)_", freqs[50],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_IfNull(ifIns,_,left);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;0;)_", freqs[54],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_OperatorAt(ifIns,opt);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;1;)_", freqs[53],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;@new_MayPredecessorModuloThrow(beforeIf,next) :- \n   FalseBranch(ifIns,next),\n   @delta_MayPredecessorModuloThrow(beforeIf,ifIns),\n   !MayPredecessorModuloThrow(beforeIf,next).;@delta_MayPredecessorModuloThrow(beforeIf,ifIns);MayPredecessorModuloThrow(beforeIf,next) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns).;1;)_", freqs[75],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;@new_MayPredecessorModuloThrow(beforeIf,next) :- \n   FalseBranch(ifIns,next),\n   @delta_MayPredecessorModuloThrow(beforeIf,ifIns),\n   !MayPredecessorModuloThrow(beforeIf,next).;FalseBranch(ifIns,next);MayPredecessorModuloThrow(beforeIf,next) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns).;0;)_", freqs[76],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Instruction_FormalParam(insn,method,_,_);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;0;)_", freqs[74],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Method_FirstInstruction(method,next);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;1;)_", freqs[73],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;FirstUse(next,_,var,method);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;1;)_", freqs[71],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;Instruction_VarDeclaringMethod(insn,method,var);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;0;)_", freqs[72],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth).;MayNullPtr(insn,var,meth);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth).;1;)_", freqs[150],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth).;PathSensitiveNullAtLine(meth,index,file,line,type,var,insn);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth).;0;)_", freqs[151],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;VarPointsToNull(var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;0;)_", freqs[110],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignBinop(insn,index,_,meth);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;1;)_", freqs[109],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignOperFrom(insn,var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;2;)_", freqs[108],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;0;)_", freqs[113],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;_EnterMonitor(insn,index,var,meth);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;1;)_", freqs[112],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;0;)_", freqs[94],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;_LoadArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;1;)_", freqs[93],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[100],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;_LoadInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[99],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[104],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;_SpecialMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[103],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;0;)_", freqs[96],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;_StoreArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;1;)_", freqs[95],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[98],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;_StoreInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[97],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;CallGraphEdge(_,a,_,b);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;0;)_", freqs[92],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;_SpecialMethodInvocation(a,index,b,_,meth);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;1;)_", freqs[91],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;_ThrowNull(insn,index,meth);NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;0;)_", freqs[111],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;VarPointsToNull(var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;0;)_", freqs[107],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignOperFrom(insn,var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;2;)_", freqs[105],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignUnop(insn,index,_,meth);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;1;)_", freqs[106],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[102],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;_VirtualMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[101],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PathSensitiveNullAtLine;0;PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;ReachableNullAtLine(meth,index,file,line,type,var,ins);PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;0;)_", freqs[149],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;PhiNodeHead(insn,headInsn);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;0;)_", freqs[57],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(headInsn,_,headVar,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;2;)_", freqs[55],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(insn,_,var,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;1;)_", freqs[56],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAt;0;ReachableNullAt(meth,index,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   Reachable(meth).;NullAt(meth,index,type,var,insn);ReachableNullAt(meth,index,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   Reachable(meth).;0;)_", freqs[115],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAt;0;ReachableNullAt(meth,index,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   Reachable(meth).;Reachable(meth);ReachableNullAt(meth,index,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   Reachable(meth).;1;)_", freqs[114],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;ApplicationMethod(meth);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;1;)_", freqs[117],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;InstructionLine(meth,index,line,file);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;2;)_", freqs[116],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;ReachableNullAt(meth,index,type,var,insn);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   ReachableNullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;0;)_", freqs[118],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;RefTypeVar;0;RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;_Var_Type(var,type);RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;0;)_", freqs[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;JumpTarget(insn,ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;1;)_", freqs[152],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;MayNull_IfInstruction(ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;0;)_", freqs[153],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;IfInstructionsCond(ifIns,left,right,\"!=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;0;)_", freqs[137],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;IfInstructionsCond(ifIns,left,right,\"<=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;0;)_", freqs[139],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;IfInstructionsCond(ifIns,left,right,\"<\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;0;)_", freqs[141],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;IfInstructionsCond(ifIns,left,right,\"==\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;0;)_", freqs[138],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;IfInstructionsCond(ifIns,left,right,\">=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;0;)_", freqs[140],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;IfInstructionsCond(ifIns,left,right,\">\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;0;)_", freqs[142],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;BasicBlockHead(ins,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;2;)_", freqs[146],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;Dominates(parent,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;3;)_", freqs[145],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;DominatesUnreachable(_,parent);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;1;)_", freqs[147],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;0;)_", freqs[148],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;AssignReturnValue_WithInvoke(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;1;)_", freqs[25],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;0;)_", freqs[26],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;0;)_", freqs[24],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;_AssignCast(insn,index,_,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;1;)_", freqs[23],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;0;)_", freqs[22],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;_AssignCastNull(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;1;)_", freqs[21],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;0;)_", freqs[16],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;_AssignHeapAllocation(insn,index,_,var,method,_);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;1;)_", freqs[15],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;0;)_", freqs[14],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;_AssignLocal(insn,index,_,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;1;)_", freqs[13],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;0;)_", freqs[12],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;_AssignNull(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;1;)_", freqs[11],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;0;)_", freqs[18],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;_LoadInstanceField(insn,index,var,_,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;1;)_", freqs[17],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;0;)_", freqs[20],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;_LoadStaticField(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;1;)_", freqs[19],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarPointsToNull;0;VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;_AssignCastNull(_,_,var,_,_);VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;0;)_", freqs[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CanBeNullBranch)_", reads[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CannotBeNullBranch)_", reads[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;DefineVar)_", reads[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;LastUse)_", reads[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MayNullPtr)_", reads[5],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MayPredecessorModuloThrow)_", reads[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;Primitive)_", reads[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;UnreachablePathNPEIns)_", reads[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;_FormalParam)_", reads[3],0);
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_9_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_48_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_50_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_51_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_58_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_59_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_60_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_61_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/Reachable.csv"},{"name","Reachable"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_65_Reachable);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_68_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/mainAnalysis.VarEqlsConst.csv"},{"name","VarEqlsConst"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_70_VarEqlsConst);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_77_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_AssignReturnValue");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_SpecialMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_StaticMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_VirtualMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_7_Var_Type");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_7_Var_Type);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_9_AssignCast");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_9_AssignCast);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_10_AssignCastNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_10_AssignCastNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_12_AssignHeapAllocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_13_AssignLocal");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_15_AssignNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_16_LoadInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_17_LoadStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_19_ActualParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_20_Return");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_21_StoreArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_22_StoreInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_23_StoreStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_27_ApplicationMethod");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_29_BasicBlockHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_CallGraphEdge");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_32_IfNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_35_OperatorAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_45_Dominates");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_46_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_48_Var_DeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_48_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_50_Method_FirstInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_50_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_51_MayPredecessorModuloThrow");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_51_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_57_InstructionLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_58_AssignBinop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_58_AssignBinop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_59_AssignOperFrom");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_59_AssignOperFrom);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_60_AssignUnop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_60_AssignUnop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_61_EnterMonitor");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_61_EnterMonitor);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_62_LoadArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_62_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_63_ThrowNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_63_ThrowNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_65_Reachable");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_65_Reachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_68_IfVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_IfVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_70_VarEqlsConst");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_70_VarEqlsConst);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_77_JumpTarget");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_77_JumpTarget);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_5_AssignReturnValue_WithInvoke");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_RefTypeVar");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_8_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_11_VarPointsToNull");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_11_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_14_DefineVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_18_VarDef");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_24_AllUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_25_FirstUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_26_LastUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_28_AssignMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_33_MayNull_IfInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_34_FalseBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_36_MayNull_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_38_PhiNodeHeadVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_39_CanBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_42_CannotBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_47_Instruction_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_49_Instruction_VarDeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_54_MayNullPtr");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_69_IfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_71_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_71_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_72_TrueIfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_72_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_73_DominatesUnreachable");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_74_UnreachablePathNPEIns");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_74_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_76_NPEWithMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_78_TrueBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_base(){return new Sf_base;}
SymbolTable *getST_base(SouffleProgram *p){return &reinterpret_cast<Sf_base*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_base: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_base();
};
public:
factory_Sf_base() : ProgramFactory("base"){}
};
static factory_Sf_base __factory_Sf_base_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(base.dl)",
R"(.)",
R"(.)",
true,
R"(profile.txt)",
4,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf_base obj(opt.getProfileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("", opt.getSourceFileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("fact-dir", opt.getInputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("jobs", std::to_string(opt.getNumJobs()));
souffle::ProfileEventSingleton::instance().makeConfigRecord("output-dir", opt.getOutputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("version", "1.4.0-737-gc0b3cde6");
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
