
#include "souffle/CompiledSouffle.h"

extern "C" {
}

namespace souffle {
using namespace ram;
struct t_btree_2__1_0__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__1_0__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_3_1_2__1__9 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,3,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_3_1_2__1__9& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t) const {
context h;
return equalRange_9(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,3,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__2_0_1_3__1__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__2_0_1_3__1__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0__1 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,5,0,1,3,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t) const {
context h;
return equalRange_36(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,5,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,4,0,1,3>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,2,4,1,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t) const {
context h;
return equalRange_20(t, h);
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t) const {
context h;
return equalRange_21(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,4,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,4,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_1_3__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_1_3__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,0,1,3,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_3_0_1_4__4__12 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_3_0_1_4__4__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,3,0,1,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2_1_0__2__3__4__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2_1_0__2__3__4__6& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_3_1__4__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_3_1__4__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_3_0_1__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_3_0_1__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_3_1__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_3_1__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1_0__1__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1_0__1__2& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__2_0_1__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__2_0_1__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2_0_1__2__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2_0_1__2__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_0_2__2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_0_2__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_1_3__2_3_0_1__5__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_1_3__2_3_0_1__5__12& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_0_2_3__3_0_1_2__2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_0_2_3__3_0_1_2__2__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,0,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__6__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__6__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_3_0_2__10 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,3,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_3_0_2__10& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t) const {
context h;
return equalRange_10(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,3,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__2__6__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__2__6__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3__7__15 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3__7__15& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t) const {
context h;
return equalRange_15(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_1_2_3_4_5_6 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,1,2,3,4,5,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,1,2,3,4,5,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__3_0_1_2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__3_0_1_2__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_2_5_1_3_4_6__37 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,2,5,1,3,4,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<iterator_0> equalRange_37(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[6] = MIN_RAM_DOMAIN;
high[6] = MAX_RAM_DOMAIN;
return range<iterator_0>(ind_0.lower_bound(&low, h.hints_0), ind_0.upper_bound(&high, h.hints_0));
}
range<iterator_0> equalRange_37(const t_tuple& t) const {
context h; return equalRange_37(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,2,5,1,3,4,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_3_4_0_1__28 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,4,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_3_4_0_1__28& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_28(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_28(const t_tuple& t) const {
context h;
return equalRange_28(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,3,4,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};

class Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
std::string profiling_fname;
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(boolean)_",
	R"_(short)_",
	R"_(int)_",
	R"_(long)_",
	R"_(float)_",
	R"_(double)_",
	R"_(char)_",
	R"_(byte)_",
	R"_(<<null pseudo heap>>)_",
	R"_(Throw NullPointerException)_",
	R"_(throw NPE)_",
	R"_(java.lang.NullPointerException)_",
	R"_(Load Array Index)_",
	R"_(Store Array Index)_",
	R"_(Store Instance Field)_",
	R"_(Load Instance Field)_",
	R"_(Virtual Method Invocation)_",
	R"_(Special Method Invocation)_",
	R"_(Unary Operator)_",
	R"_(Binary Operator)_",
	R"_(Throw Null)_",
	R"_(throw null)_",
	R"_(Enter Monitor (Synchronized))_",
	R"_(null)_",
	R"_(!=)_",
	R"_(==)_",
	R"_(<=)_",
	R"_(>=)_",
	R"_(<)_",
	R"_(>)_",
	R"_(/var_declaration/)_",
	R"_(/map_param/)_",
	R"_(declaration)_",
	R"_(assignNull)_",
	R"_(Alias)_",
	R"_(Transfer)_",
	R"_(Return)_",
	R"_(Parameter)_",
	R"_(0)_",
	R"_(1)_",
	R"_(boolean hasNext())_",
	R"_(next())_",
	R"_(JDK Function Summary)_",
	R"_(<java.lang.System: java.io.PrintStream out>)_",
	R"_(<java.lang.System: java.io.PrintStream err>)_",
};private:
  size_t freqs[1230]{};
  size_t reads[89]{};
// -- Table: _AssignReturnValue
std::unique_ptr<t_btree_2__1_0__2> rel_1_AssignReturnValue = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<0,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_1_AssignReturnValue;
// -- Table: _SpecialMethodInvocation
std::unique_ptr<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17> rel_2_SpecialMethodInvocation = std::make_unique<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17>();
souffle::RelationWrapper<1,t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_2_SpecialMethodInvocation;
// -- Table: _StaticMethodInvocation
std::unique_ptr<t_btree_4__0_3_1_2__1__9> rel_3_StaticMethodInvocation = std::make_unique<t_btree_4__0_3_1_2__1__9>();
souffle::RelationWrapper<2,t_btree_4__0_3_1_2__1__9,Tuple<RamDomain,4>,4,true,false> wrapper_rel_3_StaticMethodInvocation;
// -- Table: _VirtualMethodInvocation
std::unique_ptr<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17> rel_4_VirtualMethodInvocation = std::make_unique<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17>();
souffle::RelationWrapper<3,t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_4_VirtualMethodInvocation;
// -- Table: AssignReturnValue_WithInvoke
std::unique_ptr<t_btree_4__0_1_2_3__2_0_1_3__1__4> rel_5_AssignReturnValue_WithInvoke = std::make_unique<t_btree_4__0_1_2_3__2_0_1_3__1__4>();
souffle::RelationWrapper<4,t_btree_4__0_1_2_3__2_0_1_3__1__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_5_AssignReturnValue_WithInvoke;
// -- Table: IterNextInsn
std::unique_ptr<t_btree_3__0_2_1__5> rel_6_IterNextInsn = std::make_unique<t_btree_3__0_2_1__5>();
souffle::RelationWrapper<5,t_btree_3__0_2_1__5,Tuple<RamDomain,3>,3,false,true> wrapper_rel_6_IterNextInsn;
// -- Table: Primitive
std::unique_ptr<t_btree_1__0__1> rel_7_Primitive = std::make_unique<t_btree_1__0__1>();
// -- Table: _Var_Type
std::unique_ptr<t_btree_2__0_1__3> rel_8_Var_Type = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<6,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_8_Var_Type;
// -- Table: RefTypeVar
std::unique_ptr<t_btree_1__0> rel_9_RefTypeVar = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<7,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_9_RefTypeVar;
// -- Table: _AssignCast
std::unique_ptr<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36> rel_10_AssignCast = std::make_unique<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36>();
souffle::RelationWrapper<8,t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36,Tuple<RamDomain,6>,6,true,false> wrapper_rel_10_AssignCast;
// -- Table: _AssignCastNull
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_11_AssignCastNull = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<9,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_11_AssignCastNull;
// -- Table: _AssignHeapAllocation
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_12_AssignHeapAllocation = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<10,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_12_AssignHeapAllocation;
// -- Table: _AssignLocal
std::unique_ptr<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21> rel_13_AssignLocal = std::make_unique<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21>();
souffle::RelationWrapper<11,t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21,Tuple<RamDomain,5>,5,true,false> wrapper_rel_13_AssignLocal;
// -- Table: DefineVar
std::unique_ptr<t_btree_3__0_1_2__7> rel_14_DefineVar = std::make_unique<t_btree_3__0_1_2__7>();
souffle::RelationWrapper<12,t_btree_3__0_1_2__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_14_DefineVar;
// -- Table: _AssignNull
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_15_AssignNull = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<13,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_15_AssignNull;
// -- Table: _LoadInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_16_LoadInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<14,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_16_LoadInstanceField;
// -- Table: _LoadStaticField
std::unique_ptr<t_btree_5__2_3_0_1_4__4__12> rel_17_LoadStaticField = std::make_unique<t_btree_5__2_3_0_1_4__4__12>();
souffle::RelationWrapper<15,t_btree_5__2_3_0_1_4__4__12,Tuple<RamDomain,5>,5,true,false> wrapper_rel_17_LoadStaticField;
// -- Table: VarDef
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_18_VarDef = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<16,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_18_VarDef;
// -- Table: _ActualParam
std::unique_ptr<t_btree_3__1_0_2__2_1_0__2__3__4__6> rel_19_ActualParam = std::make_unique<t_btree_3__1_0_2__2_1_0__2__3__4__6>();
souffle::RelationWrapper<17,t_btree_3__1_0_2__2_1_0__2__3__4__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_19_ActualParam;
// -- Table: _Return
std::unique_ptr<t_btree_4__2_0_3_1__4__13> rel_20_Return = std::make_unique<t_btree_4__2_0_3_1__4__13>();
souffle::RelationWrapper<18,t_btree_4__2_0_3_1__4__13,Tuple<RamDomain,4>,4,true,false> wrapper_rel_20_Return;
// -- Table: _StoreArrayIndex
std::unique_ptr<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8> rel_21_StoreArrayIndex = std::make_unique<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8>();
souffle::RelationWrapper<19,t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_21_StoreArrayIndex;
// -- Table: _StoreInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_22_StoreInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<20,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_22_StoreInstanceField;
// -- Table: _StoreStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_23_StoreStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<21,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_23_StoreStaticField;
// -- Table: AllUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_24_AllUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<22,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_24_AllUse;
// -- Table: FirstUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_25_FirstUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<23,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_25_FirstUse;
// -- Table: LastUse
std::unique_ptr<t_btree_4__0_2_3_1__13> rel_26_LastUse = std::make_unique<t_btree_4__0_2_3_1__13>();
souffle::RelationWrapper<24,t_btree_4__0_2_3_1__13,Tuple<RamDomain,4>,4,false,true> wrapper_rel_26_LastUse;
// -- Table: ApplicationMethod
std::unique_ptr<t_btree_1__0__1> rel_27_ApplicationMethod = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<25,t_btree_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_27_ApplicationMethod;
// -- Table: AssignMayNull
std::unique_ptr<t_btree_3__0_1_2> rel_28_AssignMayNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<26,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_28_AssignMayNull;
// -- Table: BasicBlockHead
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_29_BasicBlockHead = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<27,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_29_BasicBlockHead;
// -- Table: Instruction_Next
std::unique_ptr<t_btree_2__0_1__1> rel_30_Instruction_Next = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<28,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_30_Instruction_Next;
// -- Table: _IfVar
std::unique_ptr<t_btree_3__2_0_1__4> rel_31_IfVar = std::make_unique<t_btree_3__2_0_1__4>();
souffle::RelationWrapper<29,t_btree_3__2_0_1__4,Tuple<RamDomain,3>,3,true,false> wrapper_rel_31_IfVar;
// -- Table: BoolIf
std::unique_ptr<t_btree_3__1_0_2__2_0_1__2__4> rel_32_BoolIf = std::make_unique<t_btree_3__1_0_2__2_0_1__2__4>();
souffle::RelationWrapper<30,t_btree_3__1_0_2__2_0_1__2__4,Tuple<RamDomain,3>,3,false,true> wrapper_rel_32_BoolIf;
// -- Table: BoolIfVarInvoke
std::unique_ptr<t_btree_3__1_0_2__2> rel_33_BoolIfVarInvoke = std::make_unique<t_btree_3__1_0_2__2>();
souffle::RelationWrapper<31,t_btree_3__1_0_2__2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_33_BoolIfVarInvoke;
// -- Table: hasNextIf
std::unique_ptr<t_btree_3__0_1_2> rel_34_hasNextIf = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<32,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_34_hasNextIf;
// -- Table: _IfNull
std::unique_ptr<t_btree_3__0_2_1__1__5> rel_35_IfNull = std::make_unique<t_btree_3__0_2_1__1__5>();
souffle::RelationWrapper<33,t_btree_3__0_2_1__1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_35_IfNull;
// -- Table: MayNull_IfInstruction
std::unique_ptr<t_btree_1__0> rel_36_MayNull_IfInstruction = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<34,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_36_MayNull_IfInstruction;
// -- Table: FalseBranch
std::unique_ptr<t_btree_2__0_1__1> rel_37_FalseBranch = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<35,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_37_FalseBranch;
// -- Table: _OperatorAt
std::unique_ptr<t_btree_2__0_1__1> rel_38_OperatorAt = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<36,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_38_OperatorAt;
// -- Table: MayNull_IfInstructionsCond
std::unique_ptr<t_btree_4__0_2_1_3__2_3_0_1__5__12> rel_39_MayNull_IfInstructionsCond = std::make_unique<t_btree_4__0_2_1_3__2_3_0_1__5__12>();
souffle::RelationWrapper<37,t_btree_4__0_2_1_3__2_3_0_1__5__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_39_MayNull_IfInstructionsCond;
// -- Table: BoolFalseBranch
std::unique_ptr<t_btree_2__0_1> rel_40_BoolFalseBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<38,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_40_BoolFalseBranch;
// -- Table: BoolTrueBranch
std::unique_ptr<t_btree_2__0_1> rel_41_BoolTrueBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<39,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_41_BoolTrueBranch;
// -- Table: ParamInBoolBranch
std::unique_ptr<t_btree_2__0_1__3> rel_42_ParamInBoolBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<40,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_42_ParamInBoolBranch;
// -- Table: CallGraphEdge
std::unique_ptr<t_btree_4__1_0_2_3__3_0_1_2__2__8> rel_43_CallGraphEdge = std::make_unique<t_btree_4__1_0_2_3__3_0_1_2__2__8>();
souffle::RelationWrapper<41,t_btree_4__1_0_2_3__3_0_1_2__2__8,Tuple<RamDomain,4>,4,true,false> wrapper_rel_43_CallGraphEdge;
// -- Table: PhiNodeHead
std::unique_ptr<t_btree_2__0_1> rel_44_PhiNodeHead = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<42,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_44_PhiNodeHead;
// -- Table: PhiNodeHeadVar
std::unique_ptr<t_btree_2__1_0__2> rel_45_PhiNodeHeadVar = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<43,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,false,true> wrapper_rel_45_PhiNodeHeadVar;
// -- Table: CanBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_46_CanBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<44,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_46_CanBeNullBranch;
// -- Table: @delta_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_47_delta_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_48_new_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_49_CannotBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<45,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_49_CannotBeNullBranch;
// -- Table: @delta_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_50_delta_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_51_new_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: Dominates
std::unique_ptr<t_btree_2__0_1__1__3> rel_52_Dominates = std::make_unique<t_btree_2__0_1__1__3>();
souffle::RelationWrapper<46,t_btree_2__0_1__1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_52_Dominates;
// -- Table: NextInsideHasNext
std::unique_ptr<t_btree_2__0_1__3> rel_53_NextInsideHasNext = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<47,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_53_NextInsideHasNext;
// -- Table: _FormalParam
std::unique_ptr<t_btree_3__1_2_0__6__7> rel_54_FormalParam = std::make_unique<t_btree_3__1_2_0__6__7>();
souffle::RelationWrapper<48,t_btree_3__1_2_0__6__7,Tuple<RamDomain,3>,3,true,false> wrapper_rel_54_FormalParam;
// -- Table: Instruction_FormalParam
std::unique_ptr<t_btree_4__1_3_0_2__10> rel_55_Instruction_FormalParam = std::make_unique<t_btree_4__1_3_0_2__10>();
souffle::RelationWrapper<49,t_btree_4__1_3_0_2__10,Tuple<RamDomain,4>,4,false,true> wrapper_rel_55_Instruction_FormalParam;
// -- Table: UnfilteredIsParam
std::unique_ptr<t_btree_3__0_1_2__7> rel_56_UnfilteredIsParam = std::make_unique<t_btree_3__0_1_2__7>();
// -- Table: @delta_UnfilteredIsParam
std::unique_ptr<t_btree_3__0_1_2> rel_57_delta_UnfilteredIsParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: @new_UnfilteredIsParam
std::unique_ptr<t_btree_3__0_1_2> rel_58_new_UnfilteredIsParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: isParam
std::unique_ptr<t_btree_3__1_2_0__2__6__7> rel_59_isParam = std::make_unique<t_btree_3__1_2_0__2__6__7>();
souffle::RelationWrapper<50,t_btree_3__1_2_0__2__6__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_59_isParam;
// -- Table: @delta_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_60_delta_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: @new_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_61_new_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: _ThisVar
std::unique_ptr<t_btree_2__0_1__3> rel_62_ThisVar = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<51,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_62_ThisVar;
// -- Table: _Var_DeclaringMethod
std::unique_ptr<t_btree_2__0_1__1> rel_63_Var_DeclaringMethod = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<52,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_63_Var_DeclaringMethod;
// -- Table: Instruction_VarDeclaringMethod
std::unique_ptr<t_btree_3__0_1_2> rel_64_Instruction_VarDeclaringMethod = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<53,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_64_Instruction_VarDeclaringMethod;
// -- Table: Method_FirstInstruction
std::unique_ptr<t_btree_2__0_1__1> rel_65_Method_FirstInstruction = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<54,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_65_Method_FirstInstruction;
// -- Table: MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_66_MayPredecessorModuloThrow = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<55,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_66_MayPredecessorModuloThrow;
// -- Table: SpecialIfEdge
std::unique_ptr<t_btree_3__0_2_1__5> rel_67_SpecialIfEdge = std::make_unique<t_btree_3__0_2_1__5>();
// -- Table: MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3__3__7__15> rel_68_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3__3__7__15>();
souffle::RelationWrapper<56,t_btree_4__0_1_2_3__3__7__15,Tuple<RamDomain,4>,4,false,true> wrapper_rel_68_MayNullPtr;
// -- Table: @delta_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_69_delta_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: @new_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_70_new_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: InstructionLine
std::unique_ptr<t_btree_4__0_1_2_3__3> rel_71_InstructionLine = std::make_unique<t_btree_4__0_1_2_3__3>();
souffle::RelationWrapper<57,t_btree_4__0_1_2_3__3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_71_InstructionLine;
// -- Table: VarPointsTo
std::unique_ptr<t_btree_4__0_1_2_3> rel_72_VarPointsTo = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<58,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_72_VarPointsTo;
// -- Table: VarMayPointToNull
std::unique_ptr<t_btree_1__0> rel_73_VarMayPointToNull = std::make_unique<t_btree_1__0>();
// -- Table: VarCannotBeNull
std::unique_ptr<t_btree_1__0__1> rel_74_VarCannotBeNull = std::make_unique<t_btree_1__0__1>();
// -- Table: VarPointsToNull
std::unique_ptr<t_btree_1__0__1> rel_75_VarPointsToNull = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<59,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_75_VarPointsToNull;
// -- Table: _AssignBinop
std::unique_ptr<t_btree_4__0_1_2_3> rel_76_AssignBinop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<60,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_76_AssignBinop;
// -- Table: _AssignOperFrom
std::unique_ptr<t_btree_3__0_2_1__5> rel_77_AssignOperFrom = std::make_unique<t_btree_3__0_2_1__5>();
souffle::RelationWrapper<61,t_btree_3__0_2_1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_77_AssignOperFrom;
// -- Table: _AssignUnop
std::unique_ptr<t_btree_4__0_1_2_3> rel_78_AssignUnop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<62,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_78_AssignUnop;
// -- Table: _EnterMonitor
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_79_EnterMonitor = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<63,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_79_EnterMonitor;
// -- Table: _LoadArrayIndex
std::unique_ptr<t_btree_5__3_0_1_2_4__8> rel_80_LoadArrayIndex = std::make_unique<t_btree_5__3_0_1_2_4__8>();
souffle::RelationWrapper<64,t_btree_5__3_0_1_2_4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_80_LoadArrayIndex;
// -- Table: _ThrowNull
std::unique_ptr<t_btree_3__0_1_2> rel_81_ThrowNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<65,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,true,false> wrapper_rel_81_ThrowNull;
// -- Table: NullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_82_NullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<66,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_82_NullAt;
// -- Table: ReachableNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_83_ReachableNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<67,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_83_ReachableNullAtLine;
// -- Table: IfInstructions
std::unique_ptr<t_btree_2__0_1> rel_84_IfInstructions = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<68,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_84_IfInstructions;
// -- Table: IfInstructionsCond
std::unique_ptr<t_btree_4__3_0_1_2__8> rel_85_IfInstructionsCond = std::make_unique<t_btree_4__3_0_1_2__8>();
souffle::RelationWrapper<69,t_btree_4__3_0_1_2__8,Tuple<RamDomain,4>,4,false,true> wrapper_rel_85_IfInstructionsCond;
// -- Table: TrueIfInstructions
std::unique_ptr<t_btree_1__0> rel_86_TrueIfInstructions = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<70,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_86_TrueIfInstructions;
// -- Table: DominatesUnreachable
std::unique_ptr<t_btree_2__0_1> rel_87_DominatesUnreachable = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<71,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_87_DominatesUnreachable;
// -- Table: UnreachablePathNPEIns
std::unique_ptr<t_btree_1__0__1> rel_88_UnreachablePathNPEIns = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<72,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_88_UnreachablePathNPEIns;
// -- Table: PathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_2_5_1_3_4_6__37> rel_89_PathSensitiveNullAtLine = std::make_unique<t_btree_7__0_2_5_1_3_4_6__37>();
souffle::RelationWrapper<73,t_btree_7__0_2_5_1_3_4_6__37,Tuple<RamDomain,7>,7,false,true> wrapper_rel_89_PathSensitiveNullAtLine;
// -- Table: MinPathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_90_MinPathSensitiveNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
// -- Table: MethodDerefArg
std::unique_ptr<t_btree_2__0_1__3> rel_91_MethodDerefArg = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<74,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_91_MethodDerefArg;
// -- Table: @delta_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_92_delta_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_93_new_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: JDKFunctionSummary
std::unique_ptr<t_btree_2__0_1> rel_94_JDKFunctionSummary = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<75,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_94_JDKFunctionSummary;
// -- Table: NPEWithMayNull
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_95_NPEWithMayNull = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<76,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_95_NPEWithMayNull;
// -- Table: JumpTarget
std::unique_ptr<t_btree_2__1_0__2> rel_96_JumpTarget = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<77,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_96_JumpTarget;
// -- Table: TrueBranch
std::unique_ptr<t_btree_2__0_1> rel_97_TrueBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<78,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_97_TrueBranch;
// -- Table: ReachableNullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_98_ReachableNullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<79,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_98_ReachableNullAt;
// -- Table: _AssignNumConstant
std::unique_ptr<t_btree_5__2_3_4_0_1__28> rel_99_AssignNumConstant = std::make_unique<t_btree_5__2_3_4_0_1__28>();
souffle::RelationWrapper<80,t_btree_5__2_3_4_0_1__28,Tuple<RamDomain,5>,5,true,false> wrapper_rel_99_AssignNumConstant;
// -- Table: ReturnFalse
std::unique_ptr<t_btree_2__0_1> rel_100_ReturnFalse = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<81,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_100_ReturnFalse;
// -- Table: ReturnTrue
std::unique_ptr<t_btree_2__0_1> rel_101_ReturnTrue = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<82,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_101_ReturnTrue;
public:
Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may(std::string pf="profile.log") : profiling_fname(pf),

wrapper_rel_1_AssignReturnValue(*rel_1_AssignReturnValue,symTable,"_AssignReturnValue",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"invocation","to"}}),

wrapper_rel_2_SpecialMethodInvocation(*rel_2_SpecialMethodInvocation,symTable,"_SpecialMethodInvocation",std::array<const char *,5>{{"s:symbol","i:Index","s:Method","s:symbol","s:Method"}},std::array<const char *,5>{{"?instruction","i","sig","?base","m"}}),

wrapper_rel_3_StaticMethodInvocation(*rel_3_StaticMethodInvocation,symTable,"_StaticMethodInvocation",std::array<const char *,4>{{"s:Instruction","i:number","s:Method","s:Method"}},std::array<const char *,4>{{"instruction","index","signature","method"}}),

wrapper_rel_4_VirtualMethodInvocation(*rel_4_VirtualMethodInvocation,symTable,"_VirtualMethodInvocation",std::array<const char *,5>{{"s:Instruction","i:Index","s:Method","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","sig","base","m"}}),

wrapper_rel_5_AssignReturnValue_WithInvoke(*rel_5_AssignReturnValue_WithInvoke,symTable,"AssignReturnValue_WithInvoke",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_6_IterNextInsn(*rel_6_IterNextInsn,symTable,"IterNextInsn",std::array<const char *,3>{{"s:Instruction","s:Var","s:Var"}},std::array<const char *,3>{{"insn","returnVar","var"}}),

wrapper_rel_8_Var_Type(*rel_8_Var_Type,symTable,"_Var_Type",std::array<const char *,2>{{"s:Var","s:Type"}},std::array<const char *,2>{{"var","type"}}),

wrapper_rel_9_RefTypeVar(*rel_9_RefTypeVar,symTable,"RefTypeVar",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"var"}}),

wrapper_rel_10_AssignCast(*rel_10_AssignCast,symTable,"_AssignCast",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Type","s:Method"}},std::array<const char *,6>{{"ins","i","from","to","t","m"}}),

wrapper_rel_11_AssignCastNull(*rel_11_AssignCastNull,symTable,"_AssignCastNull",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Type","s:Method"}},std::array<const char *,5>{{"ins","i","to","t","m"}}),

wrapper_rel_12_AssignHeapAllocation(*rel_12_AssignHeapAllocation,symTable,"_AssignHeapAllocation",std::array<const char *,6>{{"s:Instruction","i:number","s:symbol","s:Var","s:Method","i:number"}},std::array<const char *,6>{{"?instruction","?index","?heap","?to","?inmethod","?linenumber"}}),

wrapper_rel_13_AssignLocal(*rel_13_AssignLocal,symTable,"_AssignLocal",std::array<const char *,5>{{"s:Instruction","i:number","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"?instruction","?index","?from","?to","?inmethod"}}),

wrapper_rel_14_DefineVar(*rel_14_DefineVar,symTable,"DefineVar",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_15_AssignNull(*rel_15_AssignNull,symTable,"_AssignNull",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_16_LoadInstanceField(*rel_16_LoadInstanceField,symTable,"_LoadInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","to","base","sig","m"}}),

wrapper_rel_17_LoadStaticField(*rel_17_LoadStaticField,symTable,"_LoadStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","to","sig","m"}}),

wrapper_rel_18_VarDef(*rel_18_VarDef,symTable,"VarDef",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_19_ActualParam(*rel_19_ActualParam,symTable,"_ActualParam",std::array<const char *,3>{{"i:number","s:Instruction","s:Var"}},std::array<const char *,3>{{"index","invocation","var"}}),

wrapper_rel_20_Return(*rel_20_Return,symTable,"_Return",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"instruction","index","var","method"}}),

wrapper_rel_21_StoreArrayIndex(*rel_21_StoreArrayIndex,symTable,"_StoreArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","from","base","m"}}),

wrapper_rel_22_StoreInstanceField(*rel_22_StoreInstanceField,symTable,"_StoreInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","from","base","sig","m"}}),

wrapper_rel_23_StoreStaticField(*rel_23_StoreStaticField,symTable,"_StoreStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","from","sig","m"}}),

wrapper_rel_24_AllUse(*rel_24_AllUse,symTable,"AllUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_25_FirstUse(*rel_25_FirstUse,symTable,"FirstUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_26_LastUse(*rel_26_LastUse,symTable,"LastUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_27_ApplicationMethod(*rel_27_ApplicationMethod,symTable,"ApplicationMethod",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_28_AssignMayNull(*rel_28_AssignMayNull,symTable,"AssignMayNull",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_29_BasicBlockHead(*rel_29_BasicBlockHead,symTable,"BasicBlockHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"insn","headInsn"}}),

wrapper_rel_30_Instruction_Next(*rel_30_Instruction_Next,symTable,"Instruction_Next",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?i","?next"}}),

wrapper_rel_31_IfVar(*rel_31_IfVar,symTable,"_IfVar",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_32_BoolIf(*rel_32_BoolIf,symTable,"BoolIf",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"ifIns","pos","var"}}),

wrapper_rel_33_BoolIfVarInvoke(*rel_33_BoolIfVarInvoke,symTable,"BoolIfVarInvoke",std::array<const char *,3>{{"s:Instruction","s:Instruction","s:Var"}},std::array<const char *,3>{{"assignReturn","ifIns","var"}}),

wrapper_rel_34_hasNextIf(*rel_34_hasNextIf,symTable,"hasNextIf",std::array<const char *,3>{{"s:Instruction","s:Instruction","s:Var"}},std::array<const char *,3>{{"ifInsn","invokeInsn","var"}}),

wrapper_rel_35_IfNull(*rel_35_IfNull,symTable,"_IfNull",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_36_MayNull_IfInstruction(*rel_36_MayNull_IfInstruction,symTable,"MayNull_IfInstruction",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_37_FalseBranch(*rel_37_FalseBranch,symTable,"FalseBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_38_OperatorAt(*rel_38_OperatorAt,symTable,"_OperatorAt",std::array<const char *,2>{{"s:Instruction","s:symbol"}},std::array<const char *,2>{{"i","operator"}}),

wrapper_rel_39_MayNull_IfInstructionsCond(*rel_39_MayNull_IfInstructionsCond,symTable,"MayNull_IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:Var","s:Var","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_40_BoolFalseBranch(*rel_40_BoolFalseBranch,symTable,"BoolFalseBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_41_BoolTrueBranch(*rel_41_BoolTrueBranch,symTable,"BoolTrueBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_42_ParamInBoolBranch(*rel_42_ParamInBoolBranch,symTable,"ParamInBoolBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_43_CallGraphEdge(*rel_43_CallGraphEdge,symTable,"CallGraphEdge",std::array<const char *,4>{{"s:Context","s:Instruction","s:Context","s:Method"}},std::array<const char *,4>{{"ctx","ins","hctx","sig"}}),

wrapper_rel_44_PhiNodeHead(*rel_44_PhiNodeHead,symTable,"PhiNodeHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?insn","?headInsn"}}),

wrapper_rel_45_PhiNodeHeadVar(*rel_45_PhiNodeHeadVar,symTable,"PhiNodeHeadVar",std::array<const char *,2>{{"s:Var","s:Var"}},std::array<const char *,2>{{"var","headVar"}}),

wrapper_rel_46_CanBeNullBranch(*rel_46_CanBeNullBranch,symTable,"CanBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_49_CannotBeNullBranch(*rel_49_CannotBeNullBranch,symTable,"CannotBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_52_Dominates(*rel_52_Dominates,symTable,"Dominates",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?dominator","?insn"}}),

wrapper_rel_53_NextInsideHasNext(*rel_53_NextInsideHasNext,symTable,"NextInsideHasNext",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_54_FormalParam(*rel_54_FormalParam,symTable,"_FormalParam",std::array<const char *,3>{{"i:number","s:Method","s:Var"}},std::array<const char *,3>{{"index","method","var"}}),

wrapper_rel_55_Instruction_FormalParam(*rel_55_Instruction_FormalParam,symTable,"Instruction_FormalParam",std::array<const char *,4>{{"s:Instruction","s:symbol","s:Var","i:number"}},std::array<const char *,4>{{"insn","method","var","index"}}),

wrapper_rel_59_isParam(*rel_59_isParam,symTable,"isParam",std::array<const char *,3>{{"i:number","s:Var","s:Method"}},std::array<const char *,3>{{"index","var","method"}}),

wrapper_rel_62_ThisVar(*rel_62_ThisVar,symTable,"_ThisVar",std::array<const char *,2>{{"s:Method","s:Var"}},std::array<const char *,2>{{"method","var"}}),

wrapper_rel_63_Var_DeclaringMethod(*rel_63_Var_DeclaringMethod,symTable,"_Var_DeclaringMethod",std::array<const char *,2>{{"s:Var","s:Method"}},std::array<const char *,2>{{"?var","?method"}}),

wrapper_rel_64_Instruction_VarDeclaringMethod(*rel_64_Instruction_VarDeclaringMethod,symTable,"Instruction_VarDeclaringMethod",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"insn","method","var"}}),

wrapper_rel_65_Method_FirstInstruction(*rel_65_Method_FirstInstruction,symTable,"Method_FirstInstruction",std::array<const char *,2>{{"s:Method","s:Instruction"}},std::array<const char *,2>{{"?method","?i"}}),

wrapper_rel_66_MayPredecessorModuloThrow(*rel_66_MayPredecessorModuloThrow,symTable,"MayPredecessorModuloThrow",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?prev","?next"}}),

wrapper_rel_68_MayNullPtr(*rel_68_MayNullPtr,symTable,"MayNullPtr",std::array<const char *,4>{{"s:Instruction","s:Var","s:Method","s:symbol"}},std::array<const char *,4>{{"insn","var","method","reason"}}),

wrapper_rel_71_InstructionLine(*rel_71_InstructionLine,symTable,"InstructionLine",std::array<const char *,4>{{"s:Method","i:Index","i:LineNumber","s:File"}},std::array<const char *,4>{{"m","i","l","f"}}),

wrapper_rel_72_VarPointsTo(*rel_72_VarPointsTo,symTable,"VarPointsTo",std::array<const char *,4>{{"s:HContext","s:Alloc","s:Context","s:Var"}},std::array<const char *,4>{{"hctx","a","ctx","v"}}),

wrapper_rel_75_VarPointsToNull(*rel_75_VarPointsToNull,symTable,"VarPointsToNull",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"v"}}),

wrapper_rel_76_AssignBinop(*rel_76_AssignBinop,symTable,"_AssignBinop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_77_AssignOperFrom(*rel_77_AssignOperFrom,symTable,"_AssignOperFrom",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"ins","pos","from"}}),

wrapper_rel_78_AssignUnop(*rel_78_AssignUnop,symTable,"_AssignUnop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_79_EnterMonitor(*rel_79_EnterMonitor,symTable,"_EnterMonitor",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_80_LoadArrayIndex(*rel_80_LoadArrayIndex,symTable,"_LoadArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","to","base","m"}}),

wrapper_rel_81_ThrowNull(*rel_81_ThrowNull,symTable,"_ThrowNull",std::array<const char *,3>{{"s:Instruction","i:Index","s:Method"}},std::array<const char *,3>{{"ins","i","m"}}),

wrapper_rel_82_NullAt(*rel_82_NullAt,symTable,"NullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}),

wrapper_rel_83_ReachableNullAtLine(*rel_83_ReachableNullAtLine,symTable,"ReachableNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_84_IfInstructions(*rel_84_IfInstructions,symTable,"IfInstructions",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ins","ifIns"}}),

wrapper_rel_85_IfInstructionsCond(*rel_85_IfInstructionsCond,symTable,"IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:symbol","s:symbol","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_86_TrueIfInstructions(*rel_86_TrueIfInstructions,symTable,"TrueIfInstructions",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_87_DominatesUnreachable(*rel_87_DominatesUnreachable,symTable,"DominatesUnreachable",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","ins"}}),

wrapper_rel_88_UnreachablePathNPEIns(*rel_88_UnreachablePathNPEIns,symTable,"UnreachablePathNPEIns",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_89_PathSensitiveNullAtLine(*rel_89_PathSensitiveNullAtLine,symTable,"PathSensitiveNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_91_MethodDerefArg(*rel_91_MethodDerefArg,symTable,"MethodDerefArg",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_94_JDKFunctionSummary(*rel_94_JDKFunctionSummary,symTable,"JDKFunctionSummary",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_95_NPEWithMayNull(*rel_95_NPEWithMayNull,symTable,"NPEWithMayNull",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_96_JumpTarget(*rel_96_JumpTarget,symTable,"JumpTarget",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"to","from"}}),

wrapper_rel_97_TrueBranch(*rel_97_TrueBranch,symTable,"TrueBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_98_ReachableNullAt(*rel_98_ReachableNullAt,symTable,"ReachableNullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}),

wrapper_rel_99_AssignNumConstant(*rel_99_AssignNumConstant,symTable,"_AssignNumConstant",std::array<const char *,5>{{"s:Instruction","i:Index","s:symbol","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","constant","var","m"}}),

wrapper_rel_100_ReturnFalse(*rel_100_ReturnFalse,symTable,"ReturnFalse",std::array<const char *,2>{{"s:Instruction","s:Method"}},std::array<const char *,2>{{"returnInsn","callee"}}),

wrapper_rel_101_ReturnTrue(*rel_101_ReturnTrue,symTable,"ReturnTrue",std::array<const char *,2>{{"s:Instruction","s:Method"}},std::array<const char *,2>{{"returnInsn","callee"}}){
ProfileEventSingleton::instance().setOutputFile(profiling_fname);
addRelation("_AssignReturnValue",&wrapper_rel_1_AssignReturnValue,1,0);
addRelation("_SpecialMethodInvocation",&wrapper_rel_2_SpecialMethodInvocation,1,0);
addRelation("_StaticMethodInvocation",&wrapper_rel_3_StaticMethodInvocation,1,0);
addRelation("_VirtualMethodInvocation",&wrapper_rel_4_VirtualMethodInvocation,1,0);
addRelation("AssignReturnValue_WithInvoke",&wrapper_rel_5_AssignReturnValue_WithInvoke,0,1);
addRelation("IterNextInsn",&wrapper_rel_6_IterNextInsn,0,1);
addRelation("_Var_Type",&wrapper_rel_8_Var_Type,1,0);
addRelation("RefTypeVar",&wrapper_rel_9_RefTypeVar,0,1);
addRelation("_AssignCast",&wrapper_rel_10_AssignCast,1,0);
addRelation("_AssignCastNull",&wrapper_rel_11_AssignCastNull,1,0);
addRelation("_AssignHeapAllocation",&wrapper_rel_12_AssignHeapAllocation,1,0);
addRelation("_AssignLocal",&wrapper_rel_13_AssignLocal,1,0);
addRelation("DefineVar",&wrapper_rel_14_DefineVar,0,1);
addRelation("_AssignNull",&wrapper_rel_15_AssignNull,1,0);
addRelation("_LoadInstanceField",&wrapper_rel_16_LoadInstanceField,1,0);
addRelation("_LoadStaticField",&wrapper_rel_17_LoadStaticField,1,0);
addRelation("VarDef",&wrapper_rel_18_VarDef,0,1);
addRelation("_ActualParam",&wrapper_rel_19_ActualParam,1,0);
addRelation("_Return",&wrapper_rel_20_Return,1,0);
addRelation("_StoreArrayIndex",&wrapper_rel_21_StoreArrayIndex,1,0);
addRelation("_StoreInstanceField",&wrapper_rel_22_StoreInstanceField,1,0);
addRelation("_StoreStaticField",&wrapper_rel_23_StoreStaticField,1,0);
addRelation("AllUse",&wrapper_rel_24_AllUse,0,1);
addRelation("FirstUse",&wrapper_rel_25_FirstUse,0,1);
addRelation("LastUse",&wrapper_rel_26_LastUse,0,1);
addRelation("ApplicationMethod",&wrapper_rel_27_ApplicationMethod,1,0);
addRelation("AssignMayNull",&wrapper_rel_28_AssignMayNull,0,1);
addRelation("BasicBlockHead",&wrapper_rel_29_BasicBlockHead,1,0);
addRelation("Instruction_Next",&wrapper_rel_30_Instruction_Next,1,1);
addRelation("_IfVar",&wrapper_rel_31_IfVar,1,0);
addRelation("BoolIf",&wrapper_rel_32_BoolIf,0,1);
addRelation("BoolIfVarInvoke",&wrapper_rel_33_BoolIfVarInvoke,0,1);
addRelation("hasNextIf",&wrapper_rel_34_hasNextIf,0,1);
addRelation("_IfNull",&wrapper_rel_35_IfNull,1,0);
addRelation("MayNull_IfInstruction",&wrapper_rel_36_MayNull_IfInstruction,0,1);
addRelation("FalseBranch",&wrapper_rel_37_FalseBranch,0,1);
addRelation("_OperatorAt",&wrapper_rel_38_OperatorAt,1,0);
addRelation("MayNull_IfInstructionsCond",&wrapper_rel_39_MayNull_IfInstructionsCond,0,1);
addRelation("BoolFalseBranch",&wrapper_rel_40_BoolFalseBranch,0,1);
addRelation("BoolTrueBranch",&wrapper_rel_41_BoolTrueBranch,0,1);
addRelation("ParamInBoolBranch",&wrapper_rel_42_ParamInBoolBranch,0,1);
addRelation("CallGraphEdge",&wrapper_rel_43_CallGraphEdge,1,0);
addRelation("PhiNodeHead",&wrapper_rel_44_PhiNodeHead,1,1);
addRelation("PhiNodeHeadVar",&wrapper_rel_45_PhiNodeHeadVar,0,1);
addRelation("CanBeNullBranch",&wrapper_rel_46_CanBeNullBranch,0,1);
addRelation("CannotBeNullBranch",&wrapper_rel_49_CannotBeNullBranch,0,1);
addRelation("Dominates",&wrapper_rel_52_Dominates,1,0);
addRelation("NextInsideHasNext",&wrapper_rel_53_NextInsideHasNext,0,1);
addRelation("_FormalParam",&wrapper_rel_54_FormalParam,1,0);
addRelation("Instruction_FormalParam",&wrapper_rel_55_Instruction_FormalParam,0,1);
addRelation("isParam",&wrapper_rel_59_isParam,0,1);
addRelation("_ThisVar",&wrapper_rel_62_ThisVar,1,0);
addRelation("_Var_DeclaringMethod",&wrapper_rel_63_Var_DeclaringMethod,1,0);
addRelation("Instruction_VarDeclaringMethod",&wrapper_rel_64_Instruction_VarDeclaringMethod,0,1);
addRelation("Method_FirstInstruction",&wrapper_rel_65_Method_FirstInstruction,1,0);
addRelation("MayPredecessorModuloThrow",&wrapper_rel_66_MayPredecessorModuloThrow,1,0);
addRelation("MayNullPtr",&wrapper_rel_68_MayNullPtr,0,1);
addRelation("InstructionLine",&wrapper_rel_71_InstructionLine,1,0);
addRelation("VarPointsTo",&wrapper_rel_72_VarPointsTo,1,0);
addRelation("VarPointsToNull",&wrapper_rel_75_VarPointsToNull,0,1);
addRelation("_AssignBinop",&wrapper_rel_76_AssignBinop,1,0);
addRelation("_AssignOperFrom",&wrapper_rel_77_AssignOperFrom,1,0);
addRelation("_AssignUnop",&wrapper_rel_78_AssignUnop,1,0);
addRelation("_EnterMonitor",&wrapper_rel_79_EnterMonitor,1,0);
addRelation("_LoadArrayIndex",&wrapper_rel_80_LoadArrayIndex,1,0);
addRelation("_ThrowNull",&wrapper_rel_81_ThrowNull,1,0);
addRelation("NullAt",&wrapper_rel_82_NullAt,0,1);
addRelation("ReachableNullAtLine",&wrapper_rel_83_ReachableNullAtLine,0,1);
addRelation("IfInstructions",&wrapper_rel_84_IfInstructions,0,1);
addRelation("IfInstructionsCond",&wrapper_rel_85_IfInstructionsCond,0,1);
addRelation("TrueIfInstructions",&wrapper_rel_86_TrueIfInstructions,0,1);
addRelation("DominatesUnreachable",&wrapper_rel_87_DominatesUnreachable,0,1);
addRelation("UnreachablePathNPEIns",&wrapper_rel_88_UnreachablePathNPEIns,0,1);
addRelation("PathSensitiveNullAtLine",&wrapper_rel_89_PathSensitiveNullAtLine,0,1);
addRelation("MethodDerefArg",&wrapper_rel_91_MethodDerefArg,0,1);
addRelation("JDKFunctionSummary",&wrapper_rel_94_JDKFunctionSummary,0,1);
addRelation("NPEWithMayNull",&wrapper_rel_95_NPEWithMayNull,0,1);
addRelation("JumpTarget",&wrapper_rel_96_JumpTarget,1,0);
addRelation("TrueBranch",&wrapper_rel_97_TrueBranch,0,1);
addRelation("ReachableNullAt",&wrapper_rel_98_ReachableNullAt,0,1);
addRelation("_AssignNumConstant",&wrapper_rel_99_AssignNumConstant,1,0);
addRelation("ReturnFalse",&wrapper_rel_100_ReturnFalse,0,1);
addRelation("ReturnTrue",&wrapper_rel_101_ReturnTrue,0,1);
}
~Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may() {
}
private:
void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1, bool performIO = false) {
SignalHandler::instance()->set();
std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(6);
#endif

// -- query evaluation --
ProfileEventSingleton::instance().startTimer();
ProfileEventSingleton::instance().makeTimeEvent("@time;starttime");
{
Logger logger("@runtime;", 0);
ProfileEventSingleton::instance().makeConfigRecord("relationCount", std::to_string(89));[](){
ProfileEventSingleton::instance().makeStratumRecord(0, "relation", "_AssignReturnValue", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(1, "relation", "_SpecialMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(2, "relation", "_StaticMethodInvocation", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(3, "relation", "_VirtualMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(4, "relation", "AssignReturnValue_WithInvoke", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(5, "relation", "IterNextInsn", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(6, "relation", "Primitive", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(7, "relation", "_Var_Type", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(8, "relation", "RefTypeVar", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(9, "relation", "_AssignCast", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(10, "relation", "_AssignCastNull", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(11, "relation", "_AssignHeapAllocation", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(12, "relation", "_AssignLocal", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(13, "relation", "DefineVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(14, "relation", "_AssignNull", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(15, "relation", "_LoadInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(16, "relation", "_LoadStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(17, "relation", "VarDef", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(18, "relation", "_ActualParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(19, "relation", "_Return", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(20, "relation", "_StoreArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(21, "relation", "_StoreInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(22, "relation", "_StoreStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(23, "relation", "AllUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(24, "relation", "FirstUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(25, "relation", "LastUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(26, "relation", "ApplicationMethod", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(27, "relation", "AssignMayNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(28, "relation", "BasicBlockHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(29, "relation", "Instruction_Next", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(30, "relation", "_IfVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(31, "relation", "BoolIf", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(32, "relation", "BoolIfVarInvoke", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(33, "relation", "hasNextIf", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(34, "relation", "_IfNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(35, "relation", "MayNull_IfInstruction", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(36, "relation", "FalseBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(37, "relation", "_OperatorAt", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(38, "relation", "MayNull_IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(39, "relation", "BoolFalseBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(40, "relation", "BoolTrueBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(41, "relation", "ParamInBoolBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(42, "relation", "CallGraphEdge", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(43, "relation", "PhiNodeHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(44, "relation", "PhiNodeHeadVar", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(45, "relation", "CanBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(46, "relation", "CannotBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(47, "relation", "Dominates", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(48, "relation", "NextInsideHasNext", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(49, "relation", "_FormalParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(50, "relation", "Instruction_FormalParam", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(51, "relation", "UnfilteredIsParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(52, "relation", "isParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(53, "relation", "_ThisVar", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(54, "relation", "_Var_DeclaringMethod", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(55, "relation", "Instruction_VarDeclaringMethod", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(56, "relation", "Method_FirstInstruction", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(57, "relation", "MayPredecessorModuloThrow", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(58, "relation", "SpecialIfEdge", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(59, "relation", "MayNullPtr", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(60, "relation", "InstructionLine", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(61, "relation", "VarPointsTo", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(62, "relation", "VarMayPointToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(63, "relation", "VarCannotBeNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(64, "relation", "VarPointsToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(65, "relation", "_AssignBinop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(66, "relation", "_AssignOperFrom", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(67, "relation", "_AssignUnop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(68, "relation", "_EnterMonitor", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(69, "relation", "_LoadArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(70, "relation", "_ThrowNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(71, "relation", "NullAt", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(72, "relation", "ReachableNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(73, "relation", "IfInstructions", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(74, "relation", "IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(75, "relation", "TrueIfInstructions", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(76, "relation", "DominatesUnreachable", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(77, "relation", "UnreachablePathNPEIns", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(78, "relation", "PathSensitiveNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(79, "relation", "MinPathSensitiveNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(80, "relation", "MethodDerefArg", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(81, "relation", "JDKFunctionSummary", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(82, "relation", "NPEWithMayNull", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(83, "relation", "JumpTarget", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(84, "relation", "TrueBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(85, "relation", "ReachableNullAt", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(86, "relation", "_AssignNumConstant", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(87, "relation", "ReturnFalse", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(88, "relation", "ReturnTrue", "arity", "2");
}();
/* BEGIN STRATUM 0 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignReturnValue;../declarations.dl [140:7-140:59];loadtime;)_",iter, [&](){return rel_1_AssignReturnValue->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignReturnValue;../declarations.dl [140:7-140:59];)",rel_1_AssignReturnValue->size(),iter);}();
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];loadtime;)_",iter, [&](){return rel_2_SpecialMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];)",rel_2_SpecialMethodInvocation->size(),iter);}();
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StaticMethodInvocation;../declarations.dl [143:7-143:105];loadtime;)_",iter, [&](){return rel_3_StaticMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StaticMethodInvocation;../declarations.dl [143:7-143:105];)",rel_3_StaticMethodInvocation->size(),iter);}();
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];loadtime;)_",iter, [&](){return rel_4_VirtualMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];)",rel_4_VirtualMethodInvocation->size(),iter);}();
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AssignReturnValue_WithInvoke;../may-null/rules.dl [11:7-11:102];)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [36:1-42:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[0]++;
}
freqs[1]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [36:1-42:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[2]++;
}
freqs[3]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [36:1-42:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;../may-null/rules.dl [36:1-42:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[4]++;
}
freqs[5]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AssignReturnValue_WithInvoke;../may-null/rules.dl [11:7-11:102];savetime;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IterNextInsn;../may-null/rules.dl [27:7-27:71];)_",iter, [&](){return rel_6_IterNextInsn->size();});
SignalHandler::instance()->setMsg(R"_(IterNextInsn(insn,returnVar,var) :- 
   _AssignReturnValue(insn,returnVar),
   _VirtualMethodInvocation(insn,_,sig,var,_),
   "next()" contains sig.
in file ../may-null/rules.dl [319:1-322:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IterNextInsn;../may-null/rules.dl [319:1-322:25];IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;)_",iter, [&](){return rel_6_IterNextInsn->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
if( (symTable.resolve(env1[2]).find(symTable.resolve(RamDomain(41))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_6_IterNextInsn->insert(tuple,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
}
freqs[6]++;
}
freqs[7]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IterNextInsn;../may-null/rules.dl [27:7-27:71];savetime;)_",iter, [&](){return rel_6_IterNextInsn->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 5 */
/* BEGIN STRATUM 6 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Primitive;../declarations.dl [4:7-4:28];)_",iter, [&](){return rel_7_Primitive->size();});
SignalHandler::instance()->setMsg(R"_(Primitive("boolean").
in file ../declarations.dl [5:1-5:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [5:1-5:22];Primitive(\"boolean\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(0));
}
SignalHandler::instance()->setMsg(R"_(Primitive("short").
in file ../declarations.dl [6:1-6:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [6:1-6:20];Primitive(\"short\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(1));
}
SignalHandler::instance()->setMsg(R"_(Primitive("int").
in file ../declarations.dl [7:1-7:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [7:1-7:18];Primitive(\"int\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(2));
}
SignalHandler::instance()->setMsg(R"_(Primitive("long").
in file ../declarations.dl [8:1-8:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [8:1-8:19];Primitive(\"long\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(3));
}
SignalHandler::instance()->setMsg(R"_(Primitive("float").
in file ../declarations.dl [9:1-9:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [9:1-9:20];Primitive(\"float\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(4));
}
SignalHandler::instance()->setMsg(R"_(Primitive("double").
in file ../declarations.dl [10:1-10:21])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [10:1-10:21];Primitive(\"double\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(5));
}
SignalHandler::instance()->setMsg(R"_(Primitive("char").
in file ../declarations.dl [11:1-11:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [11:1-11:19];Primitive(\"char\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(6));
}
SignalHandler::instance()->setMsg(R"_(Primitive("byte").
in file ../declarations.dl [12:1-12:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [12:1-12:19];Primitive(\"byte\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(7));
}
}
}();
/* END STRATUM 6 */
/* BEGIN STRATUM 7 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_Type;../declarations.dl [110:7-110:38];loadtime;)_",iter, [&](){return rel_8_Var_Type->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_Type;../declarations.dl [110:7-110:38];)",rel_8_Var_Type->size(),iter);}();
/* END STRATUM 7 */
/* BEGIN STRATUM 8 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;RefTypeVar;../may-null/rules.dl [2:7-2:34];)_",iter, [&](){return rel_9_RefTypeVar->size();});
SignalHandler::instance()->setMsg(R"_(RefTypeVar(var) :- 
   _Var_Type(var,type),
   !Primitive(type).
in file ../may-null/rules.dl [101:1-103:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;RefTypeVar;../may-null/rules.dl [101:1-103:18];RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;)_",iter, [&](){return rel_9_RefTypeVar->size();});
if (!rel_8_Var_Type->empty()) [&](){
auto part = rel_8_Var_Type->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_7_Primitive_op_ctxt,rel_7_Primitive->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_8_Var_Type_op_ctxt,rel_8_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[0]++,!rel_7_Primitive->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_7_Primitive_op_ctxt)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_9_RefTypeVar->insert(tuple,READ_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt));
}
freqs[8]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;RefTypeVar;../may-null/rules.dl [2:7-2:34];savetime;)_",iter, [&](){return rel_9_RefTypeVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_Primitive->purge();
}();
/* END STRATUM 8 */
/* BEGIN STRATUM 9 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCast;../declarations.dl [86:7-86:85];loadtime;)_",iter, [&](){return rel_10_AssignCast->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCast;../declarations.dl [86:7-86:85];)",rel_10_AssignCast->size(),iter);}();
/* END STRATUM 9 */
/* BEGIN STRATUM 10 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCastNull;../declarations.dl [90:7-90:79];loadtime;)_",iter, [&](){return rel_11_AssignCastNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCastNull;../declarations.dl [90:7-90:79];)",rel_11_AssignCastNull->size(),iter);}();
/* END STRATUM 10 */
/* BEGIN STRATUM 11 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignHeapAllocation;../declarations.dl [134:7-134:131];loadtime;)_",iter, [&](){return rel_12_AssignHeapAllocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignHeapAllocation;../declarations.dl [134:7-134:131];)",rel_12_AssignHeapAllocation->size(),iter);}();
/* END STRATUM 11 */
/* BEGIN STRATUM 12 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignLocal;../declarations.dl [137:7-137:98];loadtime;)_",iter, [&](){return rel_13_AssignLocal->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignLocal;../declarations.dl [137:7-137:98];)",rel_13_AssignLocal->size(),iter);}();
/* END STRATUM 12 */
/* BEGIN STRATUM 13 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DefineVar;../may-null/rules.dl [5:7-5:68];)_",iter, [&](){return rel_14_DefineVar->size();});
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignLocal(insn,_,_,to,method).
in file ../may-null/rules.dl [135:1-141:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [135:1-141:3];DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_13_AssignLocal->empty()) [&](){
auto part = rel_13_AssignLocal->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[9]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignCast(insn,_,_,to,_,method).
in file ../may-null/rules.dl [135:1-141:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [135:1-141:3];DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_10_AssignCast->empty()) [&](){
auto part = rel_10_AssignCast->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[5])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[10]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignHeapAllocation(insn,_,_,to,method,_).
in file ../may-null/rules.dl [135:1-141:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [135:1-141:3];DefineVar(insn,to,method) :- \n   _AssignHeapAllocation(insn,_,_,to,method,_).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_12_AssignHeapAllocation->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[11]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   AssignReturnValue_WithInvoke(insn,_,to,method).
in file ../may-null/rules.dl [135:1-141:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;../may-null/rules.dl [135:1-141:3];DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[12]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DefineVar;../may-null/rules.dl [5:7-5:68];savetime;)_",iter, [&](){return rel_14_DefineVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 13 */
/* BEGIN STRATUM 14 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignNull;../declarations.dl [93:7-93:66];loadtime;)_",iter, [&](){return rel_15_AssignNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignNull;../declarations.dl [93:7-93:66];)",rel_15_AssignNull->size(),iter);}();
/* END STRATUM 14 */
/* BEGIN STRATUM 15 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadInstanceField;../declarations.dl [66:7-66:97];loadtime;)_",iter, [&](){return rel_16_LoadInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadInstanceField;../declarations.dl [66:7-66:97];)",rel_16_LoadInstanceField->size(),iter);}();
/* END STRATUM 15 */
/* BEGIN STRATUM 16 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadStaticField;../declarations.dl [72:7-72:84];loadtime;)_",iter, [&](){return rel_17_LoadStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadStaticField;../declarations.dl [72:7-72:84];)",rel_17_LoadStaticField->size(),iter);}();
/* END STRATUM 16 */
/* BEGIN STRATUM 17 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarDef;../may-null/rules.dl [10:7-10:80];)_",iter, [&](){return rel_18_VarDef->size();});
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignNull(insn,index,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_15_AssignNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_15_AssignNull->equalRange_4(key,READ_OP_CONTEXT(rel_15_AssignNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[13]++;
}
freqs[14]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,_,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_13_AssignLocal->equalRange_8(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[15]++;
}
freqs[16]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignHeapAllocation(insn,index,_,var,method,_).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_12_AssignHeapAllocation->equalRange_8(key,READ_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[17]++;
}
freqs[18]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_16_LoadInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[19]++;
}
freqs[20]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_17_LoadStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[21]++;
}
freqs[22]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCastNull(insn,index,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_11_AssignCastNull->equalRange_4(key,READ_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[23]++;
}
freqs[24]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,_,var,_,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_10_AssignCast->equalRange_8(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[25]++;
}
freqs[26]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   AssignReturnValue_WithInvoke(insn,index,var,method).
in file ../may-null/rules.dl [45:1-56:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;../may-null/rules.dl [45:1-56:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_4(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[27]++;
}
freqs[28]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarDef;../may-null/rules.dl [10:7-10:80];savetime;)_",iter, [&](){return rel_18_VarDef->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_12_AssignHeapAllocation->purge();
}();
/* END STRATUM 17 */
/* BEGIN STRATUM 18 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ActualParam;../declarations.dl [152:7-152:69];loadtime;)_",iter, [&](){return rel_19_ActualParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ActualParam;../declarations.dl [152:7-152:69];)",rel_19_ActualParam->size(),iter);}();
/* END STRATUM 18 */
/* BEGIN STRATUM 19 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Return;../declarations.dl [146:7-146:80];loadtime;)_",iter, [&](){return rel_20_Return->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Return;../declarations.dl [146:7-146:80];)",rel_20_Return->size(),iter);}();
/* END STRATUM 19 */
/* BEGIN STRATUM 20 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreArrayIndex;../declarations.dl [62:7-62:84];loadtime;)_",iter, [&](){return rel_21_StoreArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreArrayIndex;../declarations.dl [62:7-62:84];)",rel_21_StoreArrayIndex->size(),iter);}();
/* END STRATUM 20 */
/* BEGIN STRATUM 21 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreInstanceField;../declarations.dl [68:7-68:100];loadtime;)_",iter, [&](){return rel_22_StoreInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreInstanceField;../declarations.dl [68:7-68:100];)",rel_22_StoreInstanceField->size(),iter);}();
/* END STRATUM 21 */
/* BEGIN STRATUM 22 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreStaticField;../declarations.dl [74:7-74:87];loadtime;)_",iter, [&](){return rel_23_StoreStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreStaticField;../declarations.dl [74:7-74:87];)",rel_23_StoreStaticField->size(),iter);}();
/* END STRATUM 22 */
/* BEGIN STRATUM 23 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AllUse;../may-null/rules.dl [7:7-7:80];)_",iter, [&](){return rel_24_AllUse->size();});
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   VarDef(insn,index,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_18_VarDef->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_18_VarDef->equalRange_4(key,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[29]++;
}
freqs[30]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_13_AssignLocal->equalRange_4(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[31]++;
}
freqs[32]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,var,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_10_AssignCast->equalRange_4(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[33]++;
}
freqs[34]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _SpecialMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[35]++;
}
freqs[36]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _VirtualMethodInvocation(insn,index,_,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[37]++;
}
freqs[38]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreArrayIndex(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_21_StoreArrayIndex->equalRange_4(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[39]++;
}
freqs[40]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreInstanceField(insn,index,var,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_22_StoreInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[41]++;
}
freqs[42]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreStaticField(insn,index,var,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_23_StoreStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt,rel_23_StoreStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_23_StoreStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[43]++;
}
freqs[44]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _Return(insn,index,var,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_20_Return->equalRange_4(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[45]++;
}
freqs[46]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   AssignReturnValue_WithInvoke(insn,index,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[47]++;
}
freqs[48]++;
}
freqs[49]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[50]++;
}
freqs[51]++;
}
freqs[52]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[53]++;
}
freqs[54]++;
}
freqs[55]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file ../may-null/rules.dl [59:1-75:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;../may-null/rules.dl [59:1-75:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[56]++;
}
freqs[57]++;
}
freqs[58]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AllUse;../may-null/rules.dl [7:7-7:80];savetime;)_",iter, [&](){return rel_24_AllUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_23_StoreStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_18_VarDef->purge();
}();
/* END STRATUM 23 */
/* BEGIN STRATUM 24 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FirstUse;../may-null/rules.dl [9:7-9:82];)_",iter, [&](){return rel_25_FirstUse->size();});
SignalHandler::instance()->setMsg(R"_(FirstUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = min  I0 : AllUse(_, I0,var,method).
in file ../may-null/rules.dl [84:1-86:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FirstUse;../may-null/rules.dl [84:1-86:42];FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;)_",iter, [&](){return rel_25_FirstUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_25_FirstUse->insert(tuple,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
}
}
}
freqs[59]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FirstUse;../may-null/rules.dl [9:7-9:82];savetime;)_",iter, [&](){return rel_25_FirstUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 24 */
/* BEGIN STRATUM 25 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;LastUse;../may-null/rules.dl [8:7-8:81];)_",iter, [&](){return rel_26_LastUse->size();});
SignalHandler::instance()->setMsg(R"_(LastUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = max  I1 : AllUse(_, I1,var,method).
in file ../may-null/rules.dl [78:1-81:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;LastUse;../may-null/rules.dl [78:1-81:42];LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;)_",iter, [&](){return rel_26_LastUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MIN_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::max(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_26_LastUse->insert(tuple,READ_OP_CONTEXT(rel_26_LastUse_op_ctxt));
}
}
}
freqs[60]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;LastUse;../may-null/rules.dl [8:7-8:81];savetime;)_",iter, [&](){return rel_26_LastUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_24_AllUse->purge();
}();
/* END STRATUM 25 */
/* BEGIN STRATUM 26 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;ApplicationMethod;../declarations.dl [31:7-31:35];loadtime;)_",iter, [&](){return rel_27_ApplicationMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;ApplicationMethod;../declarations.dl [31:7-31:35];)",rel_27_ApplicationMethod->size(),iter);}();
/* END STRATUM 26 */
/* BEGIN STRATUM 27 */
[&]() {
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;AssignMayNull;../may-null/rules.dl [6:7-6:72];)",rel_28_AssignMayNull->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;AssignMayNull;../may-null/rules.dl [6:7-6:72];savetime;)_",iter, [&](){return rel_28_AssignMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 27 */
/* BEGIN STRATUM 28 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;BasicBlockHead;../declarations.dl [37:7-37:61];loadtime;)_",iter, [&](){return rel_29_BasicBlockHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;BasicBlockHead;../declarations.dl [37:7-37:61];)",rel_29_BasicBlockHead->size(),iter);}();
/* END STRATUM 28 */
/* BEGIN STRATUM 29 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Instruction_Next;../declarations.dl [46:7-46:65];loadtime;)_",iter, [&](){return rel_30_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Instruction_Next;../declarations.dl [46:7-46:65];)",rel_30_Instruction_Next->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;Instruction_Next;../declarations.dl [46:7-46:65];savetime;)_",iter, [&](){return rel_30_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 29 */
/* BEGIN STRATUM 30 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfVar;../declarations.dl [128:7-128:51];loadtime;)_",iter, [&](){return rel_31_IfVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfVar;../declarations.dl [128:7-128:51];)",rel_31_IfVar->size(),iter);}();
/* END STRATUM 30 */
/* BEGIN STRATUM 31 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;BoolIf;../may-null/rules.dl [182:7-182:63];)_",iter, [&](){return rel_32_BoolIf->size();});
SignalHandler::instance()->setMsg(R"_(BoolIf(ifIns,pos,var) :- 
   _IfVar(ifIns,pos,var),
   _Var_Type(var,"boolean").
in file ../may-null/rules.dl [197:1-199:27])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolIf;../may-null/rules.dl [197:1-199:27];BoolIf(ifIns,pos,var) :- \n   _IfVar(ifIns,pos,var),\n   _Var_Type(var,\"boolean\").;)_",iter, [&](){return rel_32_BoolIf->size();});
if (!rel_31_IfVar->empty()&&!rel_8_Var_Type->empty()) [&](){
auto part = rel_31_IfVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
CREATE_OP_CONTEXT(rel_8_Var_Type_op_ctxt,rel_8_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[2],RamDomain(0)}});
auto range = rel_8_Var_Type->equalRange_3(key,READ_OP_CONTEXT(rel_8_Var_Type_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2])}});
rel_32_BoolIf->insert(tuple,READ_OP_CONTEXT(rel_32_BoolIf_op_ctxt));
freqs[61]++;
}
freqs[62]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;BoolIf;../may-null/rules.dl [182:7-182:63];savetime;)_",iter, [&](){return rel_32_BoolIf->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tpos\tvar"},{"filename","./BoolIf.csv"},{"name","BoolIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_32_BoolIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_Var_Type->purge();
}();
/* END STRATUM 31 */
/* BEGIN STRATUM 32 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;BoolIfVarInvoke;../may-null/rules.dl [183:7-183:86];)_",iter, [&](){return rel_33_BoolIfVarInvoke->size();});
SignalHandler::instance()->setMsg(R"_(BoolIfVarInvoke(assignReturn,ifIns,var) :- 
   AssignReturnValue_WithInvoke(assignReturn,_,var,_),
   BoolIf(ifIns,_,var).
in file ../may-null/rules.dl [201:1-203:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolIfVarInvoke;../may-null/rules.dl [201:1-203:22];BoolIfVarInvoke(assignReturn,ifIns,var) :- \n   AssignReturnValue_WithInvoke(assignReturn,_,var,_),\n   BoolIf(ifIns,_,var).;)_",iter, [&](){return rel_33_BoolIfVarInvoke->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_32_BoolIf->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[2]}});
auto range = rel_32_BoolIf->equalRange_4(key,READ_OP_CONTEXT(rel_32_BoolIf_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[2])}});
rel_33_BoolIfVarInvoke->insert(tuple,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
freqs[63]++;
}
freqs[64]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;BoolIfVarInvoke;../may-null/rules.dl [183:7-183:86];savetime;)_",iter, [&](){return rel_33_BoolIfVarInvoke->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","assignReturn\tifIns\tvar"},{"filename","./BoolIfVarInvoke.csv"},{"name","BoolIfVarInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_33_BoolIfVarInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 32 */
/* BEGIN STRATUM 33 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;hasNextIf;../may-null/rules.dl [26:7-26:79];)_",iter, [&](){return rel_34_hasNextIf->size();});
SignalHandler::instance()->setMsg(R"_(hasNextIf(ifInsn,invokeInsn,var) :- 
   _IfVar(ifInsn,_,returnVar),
   _AssignReturnValue(invokeInsn,returnVar),
   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),
   "boolean hasNext()" contains sig.
in file ../may-null/rules.dl [312:1-316:36])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;hasNextIf;../may-null/rules.dl [312:1-316:36];hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;)_",iter, [&](){return rel_34_hasNextIf->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_31_IfVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_31_IfVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
CREATE_OP_CONTEXT(rel_34_hasNextIf_op_ctxt,rel_34_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[2]}});
auto range = rel_1_AssignReturnValue->equalRange_2(key,READ_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
if( (symTable.resolve(env2[2]).find(symTable.resolve(RamDomain(40))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env2[3])}});
rel_34_hasNextIf->insert(tuple,READ_OP_CONTEXT(rel_34_hasNextIf_op_ctxt));
}
freqs[65]++;
}
freqs[66]++;
}
freqs[67]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;hasNextIf;../may-null/rules.dl [26:7-26:79];savetime;)_",iter, [&](){return rel_34_hasNextIf->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_AssignReturnValue->purge();
}();
/* END STRATUM 33 */
/* BEGIN STRATUM 34 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfNull;../declarations.dl [131:7-131:52];loadtime;)_",iter, [&](){return rel_35_IfNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfNull;../declarations.dl [131:7-131:52];)",rel_35_IfNull->size(),iter);}();
/* END STRATUM 34 */
/* BEGIN STRATUM 35 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstruction;../may-null/rules.dl [14:7-14:55];)_",iter, [&](){return rel_36_MayNull_IfInstruction->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   _IfNull(ifIns,_,_).
in file ../may-null/rules.dl [191:1-195:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstruction;../may-null/rules.dl [191:1-195:3];MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;)_",iter, [&](){return rel_36_MayNull_IfInstruction->size();});
if (!rel_35_IfNull->empty()) [&](){
auto part = rel_35_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_36_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt));
freqs[68]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   BoolIf(ifIns,_,_).
in file ../may-null/rules.dl [191:1-195:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstruction;../may-null/rules.dl [191:1-195:3];MayNull_IfInstruction(ifIns) :- \n   BoolIf(ifIns,_,_).;)_",iter, [&](){return rel_36_MayNull_IfInstruction->size();});
if (!rel_32_BoolIf->empty()) [&](){
auto part = rel_32_BoolIf->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_36_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt));
freqs[69]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstruction;../may-null/rules.dl [14:7-14:55];savetime;)_",iter, [&](){return rel_36_MayNull_IfInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 35 */
/* BEGIN STRATUM 36 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FalseBranch;../may-null/rules.dl [17:7-17:64];)_",iter, [&](){return rel_37_FalseBranch->size();});
SignalHandler::instance()->setMsg(R"_(FalseBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   Instruction_Next(ifIns,insn).
in file ../may-null/rules.dl [272:1-274:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FalseBranch;../may-null/rules.dl [272:1-274:31];FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;)_",iter, [&](){return rel_37_FalseBranch->size();});
if (!rel_30_Instruction_Next->empty()&&!rel_36_MayNull_IfInstruction->empty()) [&](){
auto part = rel_36_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_37_FalseBranch->insert(tuple,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
freqs[70]++;
}
freqs[71]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FalseBranch;../may-null/rules.dl [17:7-17:64];savetime;)_",iter, [&](){return rel_37_FalseBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 36 */
/* BEGIN STRATUM 37 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_OperatorAt;../declarations.dl [125:7-125:52];loadtime;)_",iter, [&](){return rel_38_OperatorAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_OperatorAt;../declarations.dl [125:7-125:52];)",rel_38_OperatorAt->size(),iter);}();
/* END STRATUM 37 */
/* BEGIN STRATUM 38 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstructionsCond;../may-null/rules.dl [15:7-15:96];)_",iter, [&](){return rel_39_MayNull_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"null",opt) :- 
   _IfNull(ifIns,_,left),
   _OperatorAt(ifIns,opt).
in file ../may-null/rules.dl [263:1-265:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstructionsCond;../may-null/rules.dl [263:1-265:25];MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;)_",iter, [&](){return rel_39_MayNull_IfInstructionsCond->size();});
if (!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_35_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_39_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
freqs[72]++;
}
freqs[73]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"0",opt) :- 
   BoolIf(ifIns,1,left),
   _OperatorAt(ifIns,opt).
in file ../may-null/rules.dl [267:1-269:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstructionsCond;../may-null/rules.dl [267:1-269:25];MayNull_IfInstructionsCond(ifIns,left,\"0\",opt) :- \n   BoolIf(ifIns,1,left),\n   _OperatorAt(ifIns,opt).;)_",iter, [&](){return rel_39_MayNull_IfInstructionsCond->size();});
if (!rel_32_BoolIf->empty()&&!rel_38_OperatorAt->empty()) [&](){
const Tuple<RamDomain,3> key({{0,RamDomain(1),0}});
auto range = rel_32_BoolIf->equalRange_2(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_32_BoolIf_op_ctxt,rel_32_BoolIf->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(38)),static_cast<RamDomain>(env1[1])}});
rel_39_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
freqs[74]++;
}
freqs[75]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstructionsCond;../may-null/rules.dl [15:7-15:96];savetime;)_",iter, [&](){return rel_39_MayNull_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_32_BoolIf->purge();
}();
/* END STRATUM 38 */
/* BEGIN STRATUM 39 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;BoolFalseBranch;../may-null/rules.dl [185:7-185:68];)_",iter, [&](){return rel_40_BoolFalseBranch->size();});
SignalHandler::instance()->setMsg(R"_(BoolFalseBranch(ifIns,insn) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","!="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [214:1-216:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolFalseBranch;../may-null/rules.dl [214:1-216:26];BoolFalseBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_40_BoolFalseBranch->size();});
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_40_BoolFalseBranch->insert(tuple,READ_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt));
freqs[76]++;
}
freqs[77]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(BoolFalseBranch(ifIns,ifIns) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","==").
in file ../may-null/rules.dl [218:1-219:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolFalseBranch;../may-null/rules.dl [218:1-219:51];BoolFalseBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\").;)_",iter, [&](){return rel_40_BoolFalseBranch->size();});
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[0])}});
rel_40_BoolFalseBranch->insert(tuple,READ_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt));
freqs[78]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;BoolFalseBranch;../may-null/rules.dl [185:7-185:68];savetime;)_",iter, [&](){return rel_40_BoolFalseBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./BoolFalseBranch.csv"},{"name","BoolFalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_40_BoolFalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 39 */
/* BEGIN STRATUM 40 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;BoolTrueBranch;../may-null/rules.dl [186:7-186:67];)_",iter, [&](){return rel_41_BoolTrueBranch->size();});
SignalHandler::instance()->setMsg(R"_(BoolTrueBranch(ifIns,insn) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","=="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [221:1-223:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolTrueBranch;../may-null/rules.dl [221:1-223:26];BoolTrueBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_41_BoolTrueBranch->size();});
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_41_BoolTrueBranch->insert(tuple,READ_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt));
freqs[79]++;
}
freqs[80]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(BoolTrueBranch(ifIns,ifIns) :- 
   MayNull_IfInstructionsCond(ifIns,_,"0","!=").
in file ../may-null/rules.dl [225:1-226:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;BoolTrueBranch;../may-null/rules.dl [225:1-226:51];BoolTrueBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\").;)_",iter, [&](){return rel_41_BoolTrueBranch->size();});
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(38),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[0])}});
rel_41_BoolTrueBranch->insert(tuple,READ_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt));
freqs[81]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;BoolTrueBranch;../may-null/rules.dl [186:7-186:67];savetime;)_",iter, [&](){return rel_41_BoolTrueBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./BoolTrueBranch.csv"},{"name","BoolTrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_41_BoolTrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 40 */
/* BEGIN STRATUM 41 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ParamInBoolBranch;../may-null/rules.dl [184:7-184:60];)_",iter, [&](){return rel_42_ParamInBoolBranch->size();});
SignalHandler::instance()->setMsg(R"_(ParamInBoolBranch(insn,var) :- 
   BoolFalseBranch(ifIns,insn),
   BoolIfVarInvoke(assignReturn,ifIns,_),
   _ActualParam(_,assignReturn,var).
in file ../may-null/rules.dl [205:1-211:36])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ParamInBoolBranch;../may-null/rules.dl [205:1-211:36];ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;)_",iter, [&](){return rel_42_ParamInBoolBranch->size();});
if (!rel_40_BoolFalseBranch->empty()&&!rel_33_BoolIfVarInvoke->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_40_BoolFalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_BoolFalseBranch_op_ctxt,rel_40_BoolFalseBranch->createContext());
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],0}});
auto range = rel_33_BoolIfVarInvoke->equalRange_2(key,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{0,env1[0],0}});
auto range = rel_19_ActualParam->equalRange_2(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[2])}});
rel_42_ParamInBoolBranch->insert(tuple,READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt));
freqs[82]++;
}
freqs[83]++;
}
freqs[84]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(ParamInBoolBranch(insn,var) :- 
   BoolTrueBranch(ifIns,insn),
   BoolIfVarInvoke(assignReturn,ifIns,_),
   _ActualParam(_,assignReturn,var).
in file ../may-null/rules.dl [205:1-211:36])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ParamInBoolBranch;../may-null/rules.dl [205:1-211:36];ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;)_",iter, [&](){return rel_42_ParamInBoolBranch->size();});
if (!rel_33_BoolIfVarInvoke->empty()&&!rel_41_BoolTrueBranch->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_41_BoolTrueBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt,rel_33_BoolIfVarInvoke->createContext());
CREATE_OP_CONTEXT(rel_41_BoolTrueBranch_op_ctxt,rel_41_BoolTrueBranch->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],0}});
auto range = rel_33_BoolIfVarInvoke->equalRange_2(key,READ_OP_CONTEXT(rel_33_BoolIfVarInvoke_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{0,env1[0],0}});
auto range = rel_19_ActualParam->equalRange_2(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[2])}});
rel_42_ParamInBoolBranch->insert(tuple,READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt));
freqs[85]++;
}
freqs[86]++;
}
freqs[87]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;ParamInBoolBranch;../may-null/rules.dl [184:7-184:60];savetime;)_",iter, [&](){return rel_42_ParamInBoolBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./ParamInBoolBranch.csv"},{"name","ParamInBoolBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_ParamInBoolBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_33_BoolIfVarInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_40_BoolFalseBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_41_BoolTrueBranch->purge();
}();
/* END STRATUM 41 */
/* BEGIN STRATUM 42 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;CallGraphEdge;../declarations.dl [24:7-24:80];loadtime;)_",iter, [&](){return rel_43_CallGraphEdge->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;CallGraphEdge;../declarations.dl [24:7-24:80];)",rel_43_CallGraphEdge->size(),iter);}();
/* END STRATUM 42 */
/* BEGIN STRATUM 43 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;PhiNodeHead;../declarations.dl [43:7-43:67];loadtime;)_",iter, [&](){return rel_44_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;PhiNodeHead;../declarations.dl [43:7-43:67];)",rel_44_PhiNodeHead->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHead;../declarations.dl [43:7-43:67];savetime;)_",iter, [&](){return rel_44_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 43 */
/* BEGIN STRATUM 44 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PhiNodeHeadVar;../may-null/rules.dl [18:7-18:52];)_",iter, [&](){return rel_45_PhiNodeHeadVar->size();});
SignalHandler::instance()->setMsg(R"_(PhiNodeHeadVar(var,headVar) :- 
   PhiNodeHead(insn,headInsn),
   _AssignLocal(insn,_,var,_,_),
   _AssignLocal(headInsn,_,headVar,_,_).
in file ../may-null/rules.dl [282:1-285:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PhiNodeHeadVar;../may-null/rules.dl [282:1-285:42];PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;)_",iter, [&](){return rel_45_PhiNodeHeadVar->size();});
if (!rel_44_PhiNodeHead->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_44_PhiNodeHead->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_44_PhiNodeHead_op_ctxt,rel_44_PhiNodeHead->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env0[1],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[2]),static_cast<RamDomain>(env2[2])}});
rel_45_PhiNodeHeadVar->insert(tuple,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
freqs[88]++;
}
freqs[89]++;
}
freqs[90]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHeadVar;../may-null/rules.dl [18:7-18:52];savetime;)_",iter, [&](){return rel_45_PhiNodeHeadVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_44_PhiNodeHead->purge();
}();
/* END STRATUM 44 */
/* BEGIN STRATUM 45 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CanBeNullBranch;../may-null/rules.dl [20:7-20:58];)_",iter, [&](){return rel_46_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","==").
in file ../may-null/rules.dl [299:1-300:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;../may-null/rules.dl [299:1-300:54];CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;)_",iter, [&](){return rel_46_CanBeNullBranch->size();});
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_46_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt));
freqs[91]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [302:1-304:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;../may-null/rules.dl [302:1-304:26];CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_46_CanBeNullBranch->size();});
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_46_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt));
freqs[92]++;
}
freqs[93]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_47_delta_CanBeNullBranch->insertAll(*rel_46_CanBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CanBeNullBranch;../may-null/rules.dl [20:7-20:58];)_",iter, [&](){return rel_48_new_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   CanBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [306:1-308:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CanBeNullBranch;0;../may-null/rules.dl [306:1-308:30];CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_48_new_CanBeNullBranch->size();});
if (!rel_47_delta_CanBeNullBranch->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_47_delta_CanBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_delta_CanBeNullBranch_op_ctxt,rel_47_delta_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_48_new_CanBeNullBranch_op_ctxt,rel_48_new_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt,rel_46_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[1]++,!rel_46_CanBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_46_CanBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_48_new_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_48_new_CanBeNullBranch_op_ctxt));
}
freqs[94]++;
}
freqs[95]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_48_new_CanBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CanBeNullBranch;../may-null/rules.dl [20:7-20:58];)_",iter, [&](){return rel_48_new_CanBeNullBranch->size();});
rel_46_CanBeNullBranch->insertAll(*rel_48_new_CanBeNullBranch);
std::swap(rel_47_delta_CanBeNullBranch, rel_48_new_CanBeNullBranch);
rel_48_new_CanBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_47_delta_CanBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_48_new_CanBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CanBeNullBranch;../may-null/rules.dl [20:7-20:58];savetime;)_",iter, [&](){return rel_46_CanBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_46_CanBeNullBranch->purge();
}();
/* END STRATUM 45 */
/* BEGIN STRATUM 46 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CannotBeNullBranch;../may-null/rules.dl [19:7-19:61];)_",iter, [&](){return rel_49_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!=").
in file ../may-null/rules.dl [288:1-289:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;../may-null/rules.dl [288:1-289:54];CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;)_",iter, [&](){return rel_49_CannotBeNullBranch->size();});
if (!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_49_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt));
freqs[96]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","=="),
   FalseBranch(ifIns,insn).
in file ../may-null/rules.dl [291:1-293:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;../may-null/rules.dl [291:1-293:26];CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_49_CannotBeNullBranch->size();});
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_37_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_37_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_49_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt));
freqs[97]++;
}
freqs[98]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_50_delta_CannotBeNullBranch->insertAll(*rel_49_CannotBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CannotBeNullBranch;../may-null/rules.dl [19:7-19:61];)_",iter, [&](){return rel_51_new_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   CannotBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [295:1-297:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CannotBeNullBranch;0;../may-null/rules.dl [295:1-297:30];CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_51_new_CannotBeNullBranch->size();});
if (!rel_50_delta_CannotBeNullBranch->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_50_delta_CannotBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_50_delta_CannotBeNullBranch_op_ctxt,rel_50_delta_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_51_new_CannotBeNullBranch_op_ctxt,rel_51_new_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[2]++,!rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_51_new_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_51_new_CannotBeNullBranch_op_ctxt));
}
freqs[99]++;
}
freqs[100]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_51_new_CannotBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CannotBeNullBranch;../may-null/rules.dl [19:7-19:61];)_",iter, [&](){return rel_51_new_CannotBeNullBranch->size();});
rel_49_CannotBeNullBranch->insertAll(*rel_51_new_CannotBeNullBranch);
std::swap(rel_50_delta_CannotBeNullBranch, rel_51_new_CannotBeNullBranch);
rel_51_new_CannotBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_50_delta_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_51_new_CannotBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CannotBeNullBranch;../may-null/rules.dl [19:7-19:61];savetime;)_",iter, [&](){return rel_49_CannotBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 46 */
/* BEGIN STRATUM 47 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Dominates;../declarations.dl [40:7-40:59];loadtime;)_",iter, [&](){return rel_52_Dominates->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Dominates;../declarations.dl [40:7-40:59];)",rel_52_Dominates->size(),iter);}();
/* END STRATUM 47 */
/* BEGIN STRATUM 48 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NextInsideHasNext;../may-null/rules.dl [28:7-28:60];)_",iter, [&](){return rel_53_NextInsideHasNext->size();});
SignalHandler::instance()->setMsg(R"_(NextInsideHasNext(nextInsn,var) :- 
   hasNextIf(ifInsn,_,invokeVar),
   Instruction_Next(ifInsn,falseBlockInsn),
   Dominates(falseBlockInsn,nextInsnHead),
   BasicBlockHead(nextInsn,nextInsnHead),
   IterNextInsn(nextInsn,var,invokeVar).
in file ../may-null/rules.dl [328:1-333:40])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NextInsideHasNext;../may-null/rules.dl [328:1-333:40];NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;)_",iter, [&](){return rel_53_NextInsideHasNext->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_52_Dominates->empty()&&!rel_30_Instruction_Next->empty()&&!rel_6_IterNextInsn->empty()&&!rel_34_hasNextIf->empty()) [&](){
auto part = rel_34_hasNextIf->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_52_Dominates_op_ctxt,rel_52_Dominates->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_34_hasNextIf_op_ctxt,rel_34_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{env1[1],0}});
auto range = rel_52_Dominates->equalRange_1(key,READ_OP_CONTEXT(rel_52_Dominates_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{0,env2[1]}});
auto range = rel_29_BasicBlockHead->equalRange_2(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env3 : range) {
const Tuple<RamDomain,3> key({{env3[0],0,env0[2]}});
auto range = rel_6_IterNextInsn->equalRange_5(key,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
for(const auto& env4 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env4[1])}});
rel_53_NextInsideHasNext->insert(tuple,READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt));
freqs[101]++;
}
freqs[102]++;
}
freqs[103]++;
}
freqs[104]++;
}
freqs[105]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NextInsideHasNext;../may-null/rules.dl [28:7-28:60];savetime;)_",iter, [&](){return rel_53_NextInsideHasNext->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_34_hasNextIf->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_6_IterNextInsn->purge();
}();
/* END STRATUM 48 */
/* BEGIN STRATUM 49 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_FormalParam;../declarations.dl [149:7-149:60];loadtime;)_",iter, [&](){return rel_54_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_FormalParam;../declarations.dl [149:7-149:60];)",rel_54_FormalParam->size(),iter);}();
/* END STRATUM 49 */
/* BEGIN STRATUM 50 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_FormalParam;../may-null/rules.dl [4:7-4:97];)_",iter, [&](){return rel_55_Instruction_FormalParam->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_FormalParam(cat(param,"/map_param/"),method,param,index) :- 
   _FormalParam(index,method,param).
in file ../may-null/rules.dl [115:1-117:34])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_FormalParam;../may-null/rules.dl [115:1-117:34];Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;)_",iter, [&](){return rel_55_Instruction_FormalParam->size();});
if (!rel_54_FormalParam->empty()) [&](){
auto part = rel_54_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[2]) + symTable.resolve(RamDomain(31)))),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[0])}});
rel_55_Instruction_FormalParam->insert(tuple,READ_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt));
freqs[106]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_FormalParam;../may-null/rules.dl [4:7-4:97];savetime;)_",iter, [&](){return rel_55_Instruction_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 50 */
/* BEGIN STRATUM 51 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;UnfilteredIsParam;../may-null/rules.dl [351:7-351:65];)_",iter, [&](){return rel_56_UnfilteredIsParam->size();});
SignalHandler::instance()->setMsg(R"_(UnfilteredIsParam(index,var,method) :- 
   _FormalParam(index,method,var).
in file ../may-null/rules.dl [353:1-354:35])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;UnfilteredIsParam;../may-null/rules.dl [353:1-354:35];UnfilteredIsParam(index,var,method) :- \n   _FormalParam(index,method,var).;)_",iter, [&](){return rel_56_UnfilteredIsParam->size();});
if (!rel_54_FormalParam->empty()) [&](){
auto part = rel_54_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt,rel_56_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1])}});
rel_56_UnfilteredIsParam->insert(tuple,READ_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt));
freqs[107]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_57_delta_UnfilteredIsParam->insertAll(*rel_56_UnfilteredIsParam);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;UnfilteredIsParam;../may-null/rules.dl [351:7-351:65];)_",iter, [&](){return rel_58_new_UnfilteredIsParam->size();});
SignalHandler::instance()->setMsg(R"_(UnfilteredIsParam(index,to,method) :- 
   UnfilteredIsParam(index,from,method),
   _AssignLocal(_,_,from,to,method).
in file ../may-null/rules.dl [356:1-361:3])_");
{
	Logger logger(R"_(@t-recursive-rule;UnfilteredIsParam;0;../may-null/rules.dl [356:1-361:3];UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;)_",iter, [&](){return rel_58_new_UnfilteredIsParam->size();});
if (!rel_57_delta_UnfilteredIsParam->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_57_delta_UnfilteredIsParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_57_delta_UnfilteredIsParam_op_ctxt,rel_57_delta_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_58_new_UnfilteredIsParam_op_ctxt,rel_58_new_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt,rel_56_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_20(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,!rel_56_UnfilteredIsParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_58_new_UnfilteredIsParam->insert(tuple,READ_OP_CONTEXT(rel_58_new_UnfilteredIsParam_op_ctxt));
}
freqs[108]++;
}
freqs[109]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(UnfilteredIsParam(index,to,method) :- 
   UnfilteredIsParam(index,from,method),
   _AssignCast(_,_,from,to,_,method).
in file ../may-null/rules.dl [356:1-361:3])_");
{
	Logger logger(R"_(@t-recursive-rule;UnfilteredIsParam;0;../may-null/rules.dl [356:1-361:3];UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;)_",iter, [&](){return rel_58_new_UnfilteredIsParam->size();});
if (!rel_57_delta_UnfilteredIsParam->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_57_delta_UnfilteredIsParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_57_delta_UnfilteredIsParam_op_ctxt,rel_57_delta_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_58_new_UnfilteredIsParam_op_ctxt,rel_58_new_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt,rel_56_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,!rel_56_UnfilteredIsParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_58_new_UnfilteredIsParam->insert(tuple,READ_OP_CONTEXT(rel_58_new_UnfilteredIsParam_op_ctxt));
}
freqs[110]++;
}
freqs[111]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_58_new_UnfilteredIsParam->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;UnfilteredIsParam;../may-null/rules.dl [351:7-351:65];)_",iter, [&](){return rel_58_new_UnfilteredIsParam->size();});
rel_56_UnfilteredIsParam->insertAll(*rel_58_new_UnfilteredIsParam);
std::swap(rel_57_delta_UnfilteredIsParam, rel_58_new_UnfilteredIsParam);
rel_58_new_UnfilteredIsParam->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_57_delta_UnfilteredIsParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_58_new_UnfilteredIsParam->purge();
}();
/* END STRATUM 51 */
/* BEGIN STRATUM 52 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;isParam;../may-null/rules.dl [30:7-30:62];)_",iter, [&](){return rel_59_isParam->size();});
SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   UnfilteredIsParam(index,var,method),
   !_FormalParam(index,method,var).
in file ../may-null/rules.dl [363:1-365:36])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;isParam;../may-null/rules.dl [363:1-365:36];isParam(index,var,method) :- \n   UnfilteredIsParam(index,var,method),\n   !_FormalParam(index,method,var).;)_",iter, [&](){return rel_59_isParam->size();});
if (!rel_56_UnfilteredIsParam->empty()) [&](){
auto part = rel_56_UnfilteredIsParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_56_UnfilteredIsParam_op_ctxt,rel_56_UnfilteredIsParam->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_59_isParam_op_ctxt,rel_59_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[4]++,!rel_54_FormalParam->contains(Tuple<RamDomain,3>({{env0[0],env0[2],env0[1]}}),READ_OP_CONTEXT(rel_54_FormalParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2])}});
rel_59_isParam->insert(tuple,READ_OP_CONTEXT(rel_59_isParam_op_ctxt));
}
freqs[112]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_60_delta_isParam->insertAll(*rel_59_isParam);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;isParam;../may-null/rules.dl [30:7-30:62];)_",iter, [&](){return rel_61_new_isParam->size();});
SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   isParam(index,headVar,method),
   PhiNodeHeadVar(var,headVar).
in file ../may-null/rules.dl [368:1-370:30])_");
{
	Logger logger(R"_(@t-recursive-rule;isParam;0;../may-null/rules.dl [368:1-370:30];isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_61_new_isParam->size();});
if (!rel_60_delta_isParam->empty()&&!rel_45_PhiNodeHeadVar->empty()) [&](){
auto part = rel_60_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_60_delta_isParam_op_ctxt,rel_60_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_61_new_isParam_op_ctxt,rel_61_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt,rel_45_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_59_isParam_op_ctxt,rel_59_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_45_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_45_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[5]++,!rel_59_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[0],env0[2]}}),READ_OP_CONTEXT(rel_59_isParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[2])}});
rel_61_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_61_new_isParam_op_ctxt));
}
freqs[113]++;
}
freqs[114]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_61_new_isParam->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;isParam;../may-null/rules.dl [30:7-30:62];)_",iter, [&](){return rel_61_new_isParam->size();});
rel_59_isParam->insertAll(*rel_61_new_isParam);
std::swap(rel_60_delta_isParam, rel_61_new_isParam);
rel_61_new_isParam->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_60_delta_isParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_61_new_isParam->purge();
{
	Logger logger(R"_(@t-relation-savetime;isParam;../may-null/rules.dl [30:7-30:62];savetime;)_",iter, [&](){return rel_59_isParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_59_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_45_PhiNodeHeadVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_56_UnfilteredIsParam->purge();
}();
/* END STRATUM 52 */
/* BEGIN STRATUM 53 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ThisVar;../declarations.dl [155:7-155:41];loadtime;)_",iter, [&](){return rel_62_ThisVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ThisVar;../declarations.dl [155:7-155:41];)",rel_62_ThisVar->size(),iter);}();
/* END STRATUM 53 */
/* BEGIN STRATUM 54 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];loadtime;)_",iter, [&](){return rel_63_Var_DeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];)",rel_63_Var_DeclaringMethod->size(),iter);}();
/* END STRATUM 54 */
/* BEGIN STRATUM 55 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_VarDeclaringMethod;../may-null/rules.dl [3:7-3:89];)_",iter, [&](){return rel_64_Instruction_VarDeclaringMethod->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_VarDeclaringMethod(cat(var,"/var_declaration/"),method,var) :- 
   RefTypeVar(var),
   _Var_DeclaringMethod(var,method),
   !_FormalParam(_,method,var),
   !_ThisVar(method,var).
in file ../may-null/rules.dl [107:1-112:38])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_VarDeclaringMethod;../may-null/rules.dl [107:1-112:38];Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;)_",iter, [&](){return rel_64_Instruction_VarDeclaringMethod->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_63_Var_DeclaringMethod->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_Instruction_VarDeclaringMethod_op_ctxt,rel_64_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_54_FormalParam_op_ctxt,rel_54_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_62_ThisVar_op_ctxt,rel_62_ThisVar->createContext());
CREATE_OP_CONTEXT(rel_63_Var_DeclaringMethod_op_ctxt,rel_63_Var_DeclaringMethod->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_63_Var_DeclaringMethod->equalRange_1(key,READ_OP_CONTEXT(rel_63_Var_DeclaringMethod_op_ctxt));
for(const auto& env1 : range) {
if( (((reads[4]++,rel_54_FormalParam->equalRange_6(Tuple<RamDomain,3>({{0,env1[1],env0[0]}}),READ_OP_CONTEXT(rel_54_FormalParam_op_ctxt)).empty())) && ((reads[6]++,!rel_62_ThisVar->contains(Tuple<RamDomain,2>({{env1[1],env0[0]}}),READ_OP_CONTEXT(rel_62_ThisVar_op_ctxt)))))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[0]) + symTable.resolve(RamDomain(30)))),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0])}});
rel_64_Instruction_VarDeclaringMethod->insert(tuple,READ_OP_CONTEXT(rel_64_Instruction_VarDeclaringMethod_op_ctxt));
}
freqs[115]++;
}
freqs[116]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_VarDeclaringMethod;../may-null/rules.dl [3:7-3:89];savetime;)_",iter, [&](){return rel_64_Instruction_VarDeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_64_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_63_Var_DeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_54_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_62_ThisVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_9_RefTypeVar->purge();
}();
/* END STRATUM 55 */
/* BEGIN STRATUM 56 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Method_FirstInstruction;../declarations.dl [49:7-49:63];loadtime;)_",iter, [&](){return rel_65_Method_FirstInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Method_FirstInstruction;../declarations.dl [49:7-49:63];)",rel_65_Method_FirstInstruction->size(),iter);}();
/* END STRATUM 56 */
/* BEGIN STRATUM 57 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];loadtime;)_",iter, [&](){return rel_66_MayPredecessorModuloThrow->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
{
	Logger logger(R"_(@t-nonrecursive-relation;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];)_",iter, [&](){return rel_66_MayPredecessorModuloThrow->size();});
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_VarDeclaringMethod(insn,method,var),
   FirstUse(next,_,var,method).
in file ../may-null/rules.dl [89:1-91:32])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;../may-null/rules.dl [89:1-91:32];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;)_",iter, [&](){return rel_66_MayPredecessorModuloThrow->size();});
if (!rel_25_FirstUse->empty()&&!rel_64_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_64_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
CREATE_OP_CONTEXT(rel_64_Instruction_VarDeclaringMethod_op_ctxt,rel_64_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt,rel_66_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[2],env0[1]}});
auto range = rel_25_FirstUse->equalRange_12(key,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_66_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt));
freqs[117]++;
}
freqs[118]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_FormalParam(insn,method,_,_),
   Method_FirstInstruction(method,next).
in file ../may-null/rules.dl [120:1-122:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;../may-null/rules.dl [120:1-122:39];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;)_",iter, [&](){return rel_66_MayPredecessorModuloThrow->size();});
if (!rel_55_Instruction_FormalParam->empty()&&!rel_65_Method_FirstInstruction->empty()) [&](){
auto part = rel_55_Instruction_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt,rel_66_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_65_Method_FirstInstruction_op_ctxt,rel_65_Method_FirstInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_65_Method_FirstInstruction->equalRange_1(key,READ_OP_CONTEXT(rel_65_Method_FirstInstruction_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_66_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt));
freqs[119]++;
}
freqs[120]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_65_Method_FirstInstruction->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_25_FirstUse->purge();
}();
/* END STRATUM 57 */
/* BEGIN STRATUM 58 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;SpecialIfEdge;../may-null/rules.dl [13:7-13:72];)_",iter, [&](){return rel_67_SpecialIfEdge->size();});
SignalHandler::instance()->setMsg(R"_(SpecialIfEdge(beforeIf,next,var) :- 
   FalseBranch(ifIns,next),
   MayPredecessorModuloThrow(beforeIf,ifIns),
   MayNull_IfInstructionsCond(ifIns,var,"null",_).
in file ../may-null/rules.dl [95:1-98:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;SpecialIfEdge;../may-null/rules.dl [95:1-98:51];SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;)_",iter, [&](){return rel_67_SpecialIfEdge->size();});
if (!rel_37_FalseBranch->empty()&&!rel_39_MayNull_IfInstructionsCond->empty()&&!rel_66_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_37_FalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_FalseBranch_op_ctxt,rel_37_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt,rel_39_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt,rel_66_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_67_SpecialIfEdge_op_ctxt,rel_67_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_66_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env0[0],0,RamDomain(23),0}});
auto range = rel_39_MayNull_IfInstructionsCond->equalRange_5(key,READ_OP_CONTEXT(rel_39_MayNull_IfInstructionsCond_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[1])}});
rel_67_SpecialIfEdge->insert(tuple,READ_OP_CONTEXT(rel_67_SpecialIfEdge_op_ctxt));
freqs[121]++;
}
freqs[122]++;
}
freqs[123]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_39_MayNull_IfInstructionsCond->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_37_FalseBranch->purge();
}();
/* END STRATUM 58 */
/* BEGIN STRATUM 59 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_68_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"declaration") :- 
   Instruction_VarDeclaringMethod(insn,method,var).
in file ../may-null/rules.dl [126:1-127:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;../may-null/rules.dl [126:1-127:51];MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;)_",iter, [&](){return rel_68_MayNullPtr->size();});
if (!rel_64_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_64_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_64_Instruction_VarDeclaringMethod_op_ctxt,rel_64_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(32))}});
rel_68_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
freqs[124]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"assignNull") :- 
   _AssignNull(insn,_,var,method).
in file ../may-null/rules.dl [130:1-131:35])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;../may-null/rules.dl [130:1-131:35];MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;)_",iter, [&](){return rel_68_MayNullPtr->size();});
if (!rel_15_AssignNull->empty()) [&](){
auto part = rel_15_AssignNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(RamDomain(33))}});
rel_68_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
freqs[125]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_69_delta_MayNullPtr->insertAll(*rel_68_MayNullPtr);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignLocal(next,_,from,to,method).
in file ../may-null/rules.dl [144:1-149:3])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [144:1-149:3];MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_21(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( (reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[126]++;
}
freqs[127]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignCast(_,_,from,to,_,method).
in file ../may-null/rules.dl [144:1-149:3])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [144:1-149:3];MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( (reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[128]++;
}
freqs[129]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   MayPredecessorModuloThrow(insn,next),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method),
   !ParamInBoolBranch(next,var).
in file ../may-null/rules.dl [155:1-164:31])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [155:1-164:31];MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_66_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt,rel_66_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[8]++,rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty())) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_66_MayPredecessorModuloThrow->equalRange_1(key,READ_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
if( (((((((reads[2]++,!rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt)))) && ((reads[9]++,!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt)))))) && ((reads[10]++,!rel_42_ParamInBoolBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt)))))) && ((reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[130]++;
}
}
freqs[131]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   SpecialIfEdge(insn,next,var),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method),
   !ParamInBoolBranch(next,var).
in file ../may-null/rules.dl [155:1-164:31])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [155:1-164:31];MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_67_SpecialIfEdge->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt,rel_49_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt,rel_42_ParamInBoolBranch->createContext());
CREATE_OP_CONTEXT(rel_67_SpecialIfEdge_op_ctxt,rel_67_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[8]++,rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty())) {
const Tuple<RamDomain,3> key({{env0[0],0,env0[1]}});
auto range = rel_67_SpecialIfEdge->equalRange_5(key,READ_OP_CONTEXT(rel_67_SpecialIfEdge_op_ctxt));
for(const auto& env1 : range) {
if( (((((((reads[2]++,!rel_49_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_49_CannotBeNullBranch_op_ctxt)))) && ((reads[9]++,!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt)))))) && ((reads[10]++,!rel_42_ParamInBoolBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_42_ParamInBoolBranch_op_ctxt)))))) && ((reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[132]++;
}
}
freqs[133]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Return") :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod,_),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method),
   !NextInsideHasNext(insn,to).
in file ../may-null/rules.dl [168:1-173:30])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [168:1-173:30];MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( (((reads[11]++,!rel_53_NextInsideHasNext->contains(Tuple<RamDomain,2>({{env2[1],env3[2]}}),READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt)))) && ((reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env2[1],env3[2],env3[3],RamDomain(36)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3]),static_cast<RamDomain>(RamDomain(36))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[134]++;
}
freqs[135]++;
}
freqs[136]++;
}
freqs[137]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Parameter") :- 
   MayNullPtr(from_insn,from,_,_),
   _ActualParam(index,from_insn,from),
   CallGraphEdge(_,from_insn,_,method),
   Instruction_FormalParam(insn,method,to,index).
in file ../may-null/rules.dl [176:1-180:50])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [176:1-180:50];MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_55_Instruction_FormalParam->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt,rel_55_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],env0[1]}});
auto range = rel_19_ActualParam->equalRange_6(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{0,env0[0],0,0}});
auto range = rel_43_CallGraphEdge->equalRange_2(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{0,env2[3],0,env1[0]}});
auto range = rel_55_Instruction_FormalParam->equalRange_10(key,READ_OP_CONTEXT(rel_55_Instruction_FormalParam_op_ctxt));
for(const auto& env3 : range) {
if( (reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env3[0],env3[2],env2[3],RamDomain(37)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(RamDomain(37))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[138]++;
}
freqs[139]++;
}
freqs[140]++;
}
freqs[141]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Return") :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod,_),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method),
   !NextInsideHasNext(insn,to).
in file ../may-null/rules.dl [255:1-260:30])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;../may-null/rules.dl [255:1-260:30];MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
if (!rel_69_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_69_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_69_delta_MayNullPtr_op_ctxt,rel_69_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt,rel_70_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt,rel_53_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( (((reads[11]++,!rel_53_NextInsideHasNext->contains(Tuple<RamDomain,2>({{env2[1],env3[2]}}),READ_OP_CONTEXT(rel_53_NextInsideHasNext_op_ctxt)))) && ((reads[7]++,!rel_68_MayNullPtr->contains(Tuple<RamDomain,4>({{env2[1],env3[2],env3[3],RamDomain(36)}}),READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3]),static_cast<RamDomain>(RamDomain(36))}});
rel_70_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_70_new_MayNullPtr_op_ctxt));
}
freqs[134]++;
}
freqs[135]++;
}
freqs[136]++;
}
freqs[137]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_70_new_MayNullPtr->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MayNullPtr;../may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_70_new_MayNullPtr->size();});
rel_68_MayNullPtr->insertAll(*rel_70_new_MayNullPtr);
std::swap(rel_69_delta_MayNullPtr, rel_70_new_MayNullPtr);
rel_70_new_MayNullPtr->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_69_delta_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_70_new_MayNullPtr->purge();
{
	Logger logger(R"_(@t-relation-savetime;MayNullPtr;../may-null/rules.dl [1:7-1:85];savetime;)_",iter, [&](){return rel_68_MayNullPtr->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_10_AssignCast->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_15_AssignNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_13_AssignLocal->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_64_Instruction_VarDeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_55_Instruction_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_14_DefineVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_26_LastUse->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_5_AssignReturnValue_WithInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_67_SpecialIfEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_49_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_53_NextInsideHasNext->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_42_ParamInBoolBranch->purge();
}();
/* END STRATUM 59 */
/* BEGIN STRATUM 60 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;InstructionLine;../declarations.dl [16:7-16:67];loadtime;)_",iter, [&](){return rel_71_InstructionLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_71_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;InstructionLine;../declarations.dl [16:7-16:67];)",rel_71_InstructionLine->size(),iter);}();
/* END STRATUM 60 */
/* BEGIN STRATUM 61 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;VarPointsTo;../declarations.dl [20:7-20:66];loadtime;)_",iter, [&](){return rel_72_VarPointsTo->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;VarPointsTo;../declarations.dl [20:7-20:66];)",rel_72_VarPointsTo->size(),iter);}();
/* END STRATUM 61 */
/* BEGIN STRATUM 62 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarMayPointToNull;../declarations.dl [176:7-176:32];)_",iter, [&](){return rel_73_VarMayPointToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarMayPointToNull(var) :- 
   VarPointsTo(_,alloc,_,var),
   "<<null pseudo heap>>" contains alloc.
in file ../rules.dl [1:1-3:40])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarMayPointToNull;../rules.dl [1:1-3:40];VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   \"<<null pseudo heap>>\" contains alloc.;)_",iter, [&](){return rel_73_VarMayPointToNull->size();});
if (!rel_72_VarPointsTo->empty()) [&](){
auto part = rel_72_VarPointsTo->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_73_VarMayPointToNull_op_ctxt,rel_73_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_72_VarPointsTo_op_ctxt,rel_72_VarPointsTo->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(8))) != std::string::npos)) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[3])}});
rel_73_VarMayPointToNull->insert(tuple,READ_OP_CONTEXT(rel_73_VarMayPointToNull_op_ctxt));
}
freqs[142]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_72_VarPointsTo->purge();
}();
/* END STRATUM 62 */
/* BEGIN STRATUM 63 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarCannotBeNull;../may_rules.dl [1:7-1:32];)_",iter, [&](){return rel_74_VarCannotBeNull->size();});
SignalHandler::instance()->setMsg(R"_(VarCannotBeNull(var) :- 
   VarMayPointToNull(var),
   _LoadStaticField(_,_,var,out,_),
   out = "<java.lang.System: java.io.PrintStream out>".
in file ../may_rules.dl [9:1-12:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarCannotBeNull;../may_rules.dl [9:1-12:53];VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream out>\".;)_",iter, [&](){return rel_74_VarCannotBeNull->size();});
if (!rel_73_VarMayPointToNull->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_73_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt,rel_74_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_73_VarMayPointToNull_op_ctxt,rel_73_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],RamDomain(43),0}});
auto range = rel_17_LoadStaticField->equalRange_12(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_74_VarCannotBeNull->insert(tuple,READ_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt));
freqs[143]++;
}
freqs[144]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarCannotBeNull(var) :- 
   VarMayPointToNull(var),
   _LoadStaticField(_,_,var,out,_),
   out = "<java.lang.System: java.io.PrintStream err>".
in file ../may_rules.dl [14:1-17:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarCannotBeNull;../may_rules.dl [14:1-17:53];VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream err>\".;)_",iter, [&](){return rel_74_VarCannotBeNull->size();});
if (!rel_73_VarMayPointToNull->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_73_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt,rel_74_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_73_VarMayPointToNull_op_ctxt,rel_73_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],RamDomain(44),0}});
auto range = rel_17_LoadStaticField->equalRange_12(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_74_VarCannotBeNull->insert(tuple,READ_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt));
freqs[145]++;
}
freqs[146]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_17_LoadStaticField->purge();
}();
/* END STRATUM 63 */
/* BEGIN STRATUM 64 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarPointsToNull;../declarations.dl [161:7-161:37];)_",iter, [&](){return rel_75_VarPointsToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   _AssignCastNull(_,_,var,_,_).
in file ../rules.dl [6:1-6:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarPointsToNull;../rules.dl [6:1-6:54];VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;)_",iter, [&](){return rel_75_VarPointsToNull->size();});
if (!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_11_AssignCastNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[2])}});
rel_75_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
freqs[147]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   VarMayPointToNull(var),
   !VarCannotBeNull(var).
in file ../may_rules.dl [4:1-6:23])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarPointsToNull;../may_rules.dl [4:1-6:23];VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarCannotBeNull(var).;)_",iter, [&](){return rel_75_VarPointsToNull->size();});
if (!rel_73_VarMayPointToNull->empty()) [&](){
auto part = rel_73_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt,rel_74_VarCannotBeNull->createContext());
CREATE_OP_CONTEXT(rel_73_VarMayPointToNull_op_ctxt,rel_73_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[12]++,!rel_74_VarCannotBeNull->contains(Tuple<RamDomain,1>({{env0[0]}}),READ_OP_CONTEXT(rel_74_VarCannotBeNull_op_ctxt)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
}
freqs[148]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarPointsToNull;../declarations.dl [161:7-161:37];savetime;)_",iter, [&](){return rel_75_VarPointsToNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_11_AssignCastNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_73_VarMayPointToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_74_VarCannotBeNull->purge();
}();
/* END STRATUM 64 */
/* BEGIN STRATUM 65 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignBinop;../declarations.dl [103:7-103:67];loadtime;)_",iter, [&](){return rel_76_AssignBinop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_76_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignBinop;../declarations.dl [103:7-103:67];)",rel_76_AssignBinop->size(),iter);}();
/* END STRATUM 65 */
/* BEGIN STRATUM 66 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignOperFrom;../declarations.dl [106:7-106:64];loadtime;)_",iter, [&](){return rel_77_AssignOperFrom->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_77_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignOperFrom;../declarations.dl [106:7-106:64];)",rel_77_AssignOperFrom->size(),iter);}();
/* END STRATUM 66 */
/* BEGIN STRATUM 67 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignUnop;../declarations.dl [101:7-101:66];loadtime;)_",iter, [&](){return rel_78_AssignUnop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_78_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignUnop;../declarations.dl [101:7-101:66];)",rel_78_AssignUnop->size(),iter);}();
/* END STRATUM 67 */
/* BEGIN STRATUM 68 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_EnterMonitor;../declarations.dl [114:7-114:68];loadtime;)_",iter, [&](){return rel_79_EnterMonitor->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_79_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_EnterMonitor;../declarations.dl [114:7-114:68];)",rel_79_EnterMonitor->size(),iter);}();
/* END STRATUM 68 */
/* BEGIN STRATUM 69 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadArrayIndex;../declarations.dl [60:7-60:81];loadtime;)_",iter, [&](){return rel_80_LoadArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_80_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadArrayIndex;../declarations.dl [60:7-60:81];)",rel_80_LoadArrayIndex->size(),iter);}();
/* END STRATUM 69 */
/* BEGIN STRATUM 70 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ThrowNull;../declarations.dl [83:7-83:56];loadtime;)_",iter, [&](){return rel_81_ThrowNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_81_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ThrowNull;../declarations.dl [83:7-83:56];)",rel_81_ThrowNull->size(),iter);}();
/* END STRATUM 70 */
/* BEGIN STRATUM 71 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NullAt;../declarations.dl [164:7-164:83];)_",iter, [&](){return rel_82_NullAt->size();});
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw NullPointerException","throw NPE",a) :- 
   CallGraphEdge(_,a,_,b),
   _SpecialMethodInvocation(a,index,b,_,meth),
   "java.lang.NullPointerException" contains a.
in file ../rules.dl [9:1-12:48])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [9:1-12:48];NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_43_CallGraphEdge->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_43_CallGraphEdge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(11))) != std::string::npos)) {
const Tuple<RamDomain,5> key({{env0[1],0,env0[3],0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_5(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(9)),static_cast<RamDomain>(RamDomain(10)),static_cast<RamDomain>(env0[1])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[149]++;
}
}
freqs[150]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Array Index",var,insn) :- 
   VarPointsToNull(var),
   _LoadArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [17:1-19:44])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [17:1-19:44];NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_80_LoadArrayIndex->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_80_LoadArrayIndex_op_ctxt,rel_80_LoadArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_80_LoadArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_80_LoadArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(12)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[151]++;
}
freqs[152]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Array Index",var,insn) :- 
   VarPointsToNull(var),
   _StoreArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [24:1-26:45])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [24:1-26:45];NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_21_StoreArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(13)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[153]++;
}
freqs[154]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _StoreInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [31:1-33:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [31:1-33:51];NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_22_StoreInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(14)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[155]++;
}
freqs[156]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _LoadInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [38:1-40:50])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [38:1-40:50];NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_16_LoadInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(15)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[157]++;
}
freqs[158]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Virtual Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _VirtualMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [45:1-47:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [45:1-47:53];NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(16)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[159]++;
}
freqs[160]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Special Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _SpecialMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [52:1-54:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [52:1-54:53];NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(17)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[161]++;
}
freqs[162]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Unary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignUnop(insn,index,_,meth),
   _AssignOperFrom(insn,_,var).
in file ../rules.dl [59:1-62:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [59:1-62:31];NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_77_AssignOperFrom->empty()&&!rel_78_AssignUnop->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_77_AssignOperFrom_op_ctxt,rel_77_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_78_AssignUnop_op_ctxt,rel_78_AssignUnop->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_78_AssignUnop) {
const Tuple<RamDomain,3> key({{env1[0],0,env0[0]}});
auto range = rel_77_AssignOperFrom->equalRange_5(key,READ_OP_CONTEXT(rel_77_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(18)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[163]++;
}
freqs[164]++;
}
freqs[165]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Binary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignBinop(insn,index,_,meth),
   _AssignOperFrom(insn,_,var).
in file ../rules.dl [67:1-70:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [67:1-70:31];NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_76_AssignBinop->empty()&&!rel_77_AssignOperFrom->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_76_AssignBinop_op_ctxt,rel_76_AssignBinop->createContext());
CREATE_OP_CONTEXT(rel_77_AssignOperFrom_op_ctxt,rel_77_AssignOperFrom->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_76_AssignBinop) {
const Tuple<RamDomain,3> key({{env1[0],0,env0[0]}});
auto range = rel_77_AssignOperFrom->equalRange_5(key,READ_OP_CONTEXT(rel_77_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(19)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[166]++;
}
freqs[167]++;
}
freqs[168]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw Null","throw null",insn) :- 
   _ThrowNull(insn,index,meth).
in file ../rules.dl [73:1-74:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [73:1-74:31];NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_81_ThrowNull->empty()) [&](){
auto part = rel_81_ThrowNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_81_ThrowNull_op_ctxt,rel_81_ThrowNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(20)),static_cast<RamDomain>(RamDomain(21)),static_cast<RamDomain>(env0[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[169]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Enter Monitor (Synchronized)",var,insn) :- 
   VarPointsToNull(var),
   _EnterMonitor(insn,index,var,meth).
in file ../rules.dl [79:1-81:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [79:1-81:39];NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;)_",iter, [&](){return rel_82_NullAt->size();});
if (!rel_75_VarPointsToNull->empty()&&!rel_79_EnterMonitor->empty()) [&](){
auto part = rel_75_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_79_EnterMonitor_op_ctxt,rel_79_EnterMonitor->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_79_EnterMonitor->equalRange_4(key,READ_OP_CONTEXT(rel_79_EnterMonitor_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(22)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_82_NullAt->insert(tuple,READ_OP_CONTEXT(rel_82_NullAt_op_ctxt));
freqs[170]++;
}
freqs[171]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NullAt;../declarations.dl [164:7-164:83];savetime;)_",iter, [&](){return rel_82_NullAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_82_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_80_LoadArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_21_StoreArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_16_LoadInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_22_StoreInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_81_ThrowNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_78_AssignUnop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_76_AssignBinop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_77_AssignOperFrom->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_79_EnterMonitor->purge();
}();
/* END STRATUM 71 */
/* BEGIN STRATUM 72 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReachableNullAtLine;../declarations.dl [170:7-170:120];)_",iter, [&](){return rel_83_ReachableNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(ReachableNullAtLine(meth,index,file,line,type,var,insn) :- 
   NullAt(meth,index,type,var,insn),
   ApplicationMethod(meth),
   InstructionLine(meth,index,line,file).
in file ../rules.dl [85:1-88:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReachableNullAtLine;../rules.dl [85:1-88:42];ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;)_",iter, [&](){return rel_83_ReachableNullAtLine->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_71_InstructionLine->empty()&&!rel_82_NullAt->empty()) [&](){
auto part = rel_82_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_71_InstructionLine_op_ctxt,rel_71_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt,rel_83_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env0[0],env0[1],0,0}});
auto range = rel_71_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_71_InstructionLine_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_83_ReachableNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt));
freqs[172]++;
}
freqs[173]++;
}
freqs[174]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;ReachableNullAtLine;../declarations.dl [170:7-170:120];savetime;)_",iter, [&](){return rel_83_ReachableNullAtLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 72 */
/* BEGIN STRATUM 73 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructions;../path_sensitivity/rules.dl [1:7-1:66];)_",iter, [&](){return rel_84_IfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   BasicBlockHead(ins,headIns),
   MayPredecessorModuloThrow(ifIns,headIns).
in file ../path_sensitivity/rules.dl [8:1-11:43])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;../path_sensitivity/rules.dl [8:1-11:43];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;)_",iter, [&](){return rel_84_IfInstructions->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_66_MayPredecessorModuloThrow->empty()&&!rel_83_ReachableNullAtLine->empty()) [&](){
auto part = rel_83_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_84_IfInstructions_op_ctxt,rel_84_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt,rel_66_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt,rel_83_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{0,env1[1]}});
auto range = rel_66_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_66_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env2[0])}});
rel_84_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_84_IfInstructions_op_ctxt));
freqs[175]++;
}
freqs[176]++;
}
freqs[177]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,var,ins),
   _IfVar(ifIns,_,var).
in file ../path_sensitivity/rules.dl [14:1-16:23])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;../path_sensitivity/rules.dl [14:1-16:23];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;)_",iter, [&](){return rel_84_IfInstructions->size();});
if (!rel_83_ReachableNullAtLine->empty()&&!rel_31_IfVar->empty()) [&](){
auto part = rel_83_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_84_IfInstructions_op_ctxt,rel_84_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt,rel_83_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_31_IfVar_op_ctxt,rel_31_IfVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[5]}});
auto range = rel_31_IfVar->equalRange_4(key,READ_OP_CONTEXT(rel_31_IfVar_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env1[0])}});
rel_84_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_84_IfInstructions_op_ctxt));
freqs[178]++;
}
freqs[179]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructions;../path_sensitivity/rules.dl [1:7-1:66];savetime;)_",iter, [&](){return rel_84_IfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_84_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_66_MayPredecessorModuloThrow->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_31_IfVar->purge();
}();
/* END STRATUM 73 */
/* BEGIN STRATUM 74 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructionsCond;../path_sensitivity/rules.dl [2:7-2:94];)_",iter, [&](){return rel_85_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   varLeft = "null".
in file ../path_sensitivity/rules.dl [31:1-35:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;../path_sensitivity/rules.dl [31:1-35:18];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;)_",iter, [&](){return rel_85_IfInstructionsCond->size();});
if (!rel_84_IfInstructions->empty()&&!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_84_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_84_IfInstructions_op_ctxt,rel_84_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,RamDomain(23)}});
auto range = rel_35_IfNull->equalRange_5(key,READ_OP_CONTEXT(rel_35_IfNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_85_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt));
freqs[180]++;
}
freqs[181]++;
}
freqs[182]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   VarPointsToNull(varLeft).
in file ../path_sensitivity/rules.dl [37:1-41:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;../path_sensitivity/rules.dl [37:1-41:26];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;)_",iter, [&](){return rel_85_IfInstructionsCond->size();});
if (!rel_84_IfInstructions->empty()&&!rel_75_VarPointsToNull->empty()&&!rel_35_IfNull->empty()&&!rel_38_OperatorAt->empty()) [&](){
auto part = rel_84_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_84_IfInstructions_op_ctxt,rel_84_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_35_IfNull_op_ctxt,rel_35_IfNull->createContext());
CREATE_OP_CONTEXT(rel_38_OperatorAt_op_ctxt,rel_38_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_38_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_38_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,0}});
auto range = rel_35_IfNull->equalRange_1(key,READ_OP_CONTEXT(rel_35_IfNull_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_75_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_85_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt));
freqs[183]++;
}
freqs[184]++;
}
freqs[185]++;
}
freqs[186]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructionsCond;../path_sensitivity/rules.dl [2:7-2:94];savetime;)_",iter, [&](){return rel_85_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_85_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_38_OperatorAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_35_IfNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_84_IfInstructions->purge();
}();
/* END STRATUM 74 */
/* BEGIN STRATUM 75 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueIfInstructions;../path_sensitivity/rules.dl [3:7-3:52];)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"!="),
   ord(left) != ord(right).
in file ../path_sensitivity/rules.dl [43:1-45:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [43:1-45:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(24)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) != (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[187]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"=="),
   ord(left) = ord(right).
in file ../path_sensitivity/rules.dl [47:1-49:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [47:1-49:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(25)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) == (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[188]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<="),
   ord(left) <= ord(right).
in file ../path_sensitivity/rules.dl [51:1-53:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [51:1-53:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(26)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) <= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[189]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">="),
   ord(left) >= ord(right).
in file ../path_sensitivity/rules.dl [55:1-57:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [55:1-57:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(27)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) >= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[190]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<"),
   ord(left) < ord(right).
in file ../path_sensitivity/rules.dl [59:1-61:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [59:1-61:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(28)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) < (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[191]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">"),
   ord(left) > ord(right).
in file ../path_sensitivity/rules.dl [63:1-65:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;../path_sensitivity/rules.dl [63:1-65:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (!rel_85_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(29)}});
auto range = rel_85_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_85_IfInstructionsCond_op_ctxt,rel_85_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) > (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_86_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt));
}
freqs[192]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueIfInstructions;../path_sensitivity/rules.dl [3:7-3:52];savetime;)_",iter, [&](){return rel_86_TrueIfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_86_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_85_IfInstructionsCond->purge();
}();
/* END STRATUM 75 */
/* BEGIN STRATUM 76 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DominatesUnreachable;../path_sensitivity/rules.dl [5:7-5:72];)_",iter, [&](){return rel_87_DominatesUnreachable->size();});
SignalHandler::instance()->setMsg(R"_(DominatesUnreachable(ifIns,ins) :- 
   TrueIfInstructions(ifIns),
   Instruction_Next(ifIns,ins).
in file ../path_sensitivity/rules.dl [67:1-69:30])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DominatesUnreachable;../path_sensitivity/rules.dl [67:1-69:30];DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;)_",iter, [&](){return rel_87_DominatesUnreachable->size();});
if (!rel_30_Instruction_Next->empty()&&!rel_86_TrueIfInstructions->empty()) [&](){
auto part = rel_86_TrueIfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_87_DominatesUnreachable_op_ctxt,rel_87_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt,rel_30_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_86_TrueIfInstructions_op_ctxt,rel_86_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_30_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_30_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_87_DominatesUnreachable->insert(tuple,READ_OP_CONTEXT(rel_87_DominatesUnreachable_op_ctxt));
freqs[193]++;
}
freqs[194]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DominatesUnreachable;../path_sensitivity/rules.dl [5:7-5:72];savetime;)_",iter, [&](){return rel_87_DominatesUnreachable->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_30_Instruction_Next->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_86_TrueIfInstructions->purge();
}();
/* END STRATUM 76 */
/* BEGIN STRATUM 77 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;UnreachablePathNPEIns;../path_sensitivity/rules.dl [4:7-4:55];)_",iter, [&](){return rel_88_UnreachablePathNPEIns->size();});
SignalHandler::instance()->setMsg(R"_(UnreachablePathNPEIns(ins) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   DominatesUnreachable(_,parent),
   BasicBlockHead(ins,headIns),
   Dominates(parent,headIns).
in file ../path_sensitivity/rules.dl [75:1-79:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;UnreachablePathNPEIns;../path_sensitivity/rules.dl [75:1-79:28];UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;)_",iter, [&](){return rel_88_UnreachablePathNPEIns->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_52_Dominates->empty()&&!rel_87_DominatesUnreachable->empty()&&!rel_83_ReachableNullAtLine->empty()) [&](){
auto part = rel_83_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_52_Dominates_op_ctxt,rel_52_Dominates->createContext());
CREATE_OP_CONTEXT(rel_87_DominatesUnreachable_op_ctxt,rel_87_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt,rel_83_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_88_UnreachablePathNPEIns_op_ctxt,rel_88_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_87_DominatesUnreachable) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{env1[1],env2[1]}});
auto range = rel_52_Dominates->equalRange_3(key,READ_OP_CONTEXT(rel_52_Dominates_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[6])}});
rel_88_UnreachablePathNPEIns->insert(tuple,READ_OP_CONTEXT(rel_88_UnreachablePathNPEIns_op_ctxt));
freqs[195]++;
}
freqs[196]++;
}
freqs[197]++;
}
freqs[198]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;UnreachablePathNPEIns;../path_sensitivity/rules.dl [4:7-4:55];savetime;)_",iter, [&](){return rel_88_UnreachablePathNPEIns->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_88_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_29_BasicBlockHead->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_52_Dominates->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_87_DominatesUnreachable->purge();
}();
/* END STRATUM 77 */
/* BEGIN STRATUM 78 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PathSensitiveNullAtLine;../declarations.dl [173:7-173:124];)_",iter, [&](){return rel_89_PathSensitiveNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- 
   ReachableNullAtLine(meth,index,file,line,type,var,ins),
   !UnreachablePathNPEIns(ins).
in file ../path_sensitivity/rules.dl [81:1-83:29])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PathSensitiveNullAtLine;../path_sensitivity/rules.dl [81:1-83:29];PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;)_",iter, [&](){return rel_89_PathSensitiveNullAtLine->size();});
if (!rel_83_ReachableNullAtLine->empty()) [&](){
auto part = rel_83_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_89_PathSensitiveNullAtLine_op_ctxt,rel_89_PathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_83_ReachableNullAtLine_op_ctxt,rel_83_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_88_UnreachablePathNPEIns_op_ctxt,rel_88_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[13]++,!rel_88_UnreachablePathNPEIns->contains(Tuple<RamDomain,1>({{env0[6]}}),READ_OP_CONTEXT(rel_88_UnreachablePathNPEIns_op_ctxt)))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_89_PathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_89_PathSensitiveNullAtLine_op_ctxt));
}
freqs[199]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;PathSensitiveNullAtLine;../declarations.dl [173:7-173:124];savetime;)_",iter, [&](){return rel_89_PathSensitiveNullAtLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_83_ReachableNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_88_UnreachablePathNPEIns->purge();
}();
/* END STRATUM 78 */
/* BEGIN STRATUM 79 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MinPathSensitiveNullAtLine;../may-null/rules.dl [24:7-24:120];)_",iter, [&](){return rel_90_MinPathSensitiveNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- 
   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).
in file ../may-null/rules.dl [339:1-341:70])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MinPathSensitiveNullAtLine;../may-null/rules.dl [339:1-341:70];MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;)_",iter, [&](){return rel_90_MinPathSensitiveNullAtLine->size();});
if (!rel_89_PathSensitiveNullAtLine->empty()) [&](){
auto part = rel_89_PathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_90_MinPathSensitiveNullAtLine_op_ctxt,rel_90_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_89_PathSensitiveNullAtLine_op_ctxt,rel_89_PathSensitiveNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,7> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,7> key({{env0[0],0,env0[2],0,0,env0[5],0}});
auto range = rel_89_PathSensitiveNullAtLine->equalRange_37(key,READ_OP_CONTEXT(rel_89_PathSensitiveNullAtLine_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_90_MinPathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_90_MinPathSensitiveNullAtLine_op_ctxt));
}
}
}
freqs[200]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_89_PathSensitiveNullAtLine->purge();
}();
/* END STRATUM 79 */
/* BEGIN STRATUM 80 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MethodDerefArg;../may-null/rules.dl [31:7-31:59];)_",iter, [&](){return rel_91_MethodDerefArg->size();});
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   NullAt(method,_,_,var,insn),
   MayNullPtr(insn,var,method,_),
   isParam(index,var,method).
in file ../may-null/rules.dl [372:1-375:29])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MethodDerefArg;../may-null/rules.dl [372:1-375:29];MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;)_",iter, [&](){return rel_91_MethodDerefArg->size();});
if (!rel_68_MayNullPtr->empty()&&!rel_82_NullAt->empty()&&!rel_59_isParam->empty()) [&](){
auto part = rel_82_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_91_MethodDerefArg_op_ctxt,rel_91_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_82_NullAt_op_ctxt,rel_82_NullAt->createContext());
CREATE_OP_CONTEXT(rel_59_isParam_op_ctxt,rel_59_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[4],env0[3],env0[0],0}});
auto range = rel_68_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,3> key({{0,env0[3],env0[0]}});
auto range = rel_59_isParam->equalRange_6(key,READ_OP_CONTEXT(rel_59_isParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env2[0]),static_cast<RamDomain>(env0[0])}});
rel_91_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_91_MethodDerefArg_op_ctxt));
freqs[201]++;
}
freqs[202]++;
}
freqs[203]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_92_delta_MethodDerefArg->insertAll(*rel_91_MethodDerefArg);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MethodDerefArg;../may-null/rules.dl [31:7-31:59];)_",iter, [&](){return rel_93_new_MethodDerefArg->size();});
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   MethodDerefArg(invokedIndex,invokedMethod),
   CallGraphEdge(_,from_insn,_,invokedMethod),
   _ActualParam(invokedIndex,from_insn,from),
   isParam(index,from,method).
in file ../may-null/rules.dl [377:1-381:30])_");
{
	Logger logger(R"_(@t-recursive-rule;MethodDerefArg;0;../may-null/rules.dl [377:1-381:30];MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;)_",iter, [&](){return rel_93_new_MethodDerefArg->size();});
if (!rel_92_delta_MethodDerefArg->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_19_ActualParam->empty()&&!rel_59_isParam->empty()) [&](){
auto part = rel_92_delta_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_92_delta_MethodDerefArg_op_ctxt,rel_92_delta_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_93_new_MethodDerefArg_op_ctxt,rel_93_new_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_91_MethodDerefArg_op_ctxt,rel_91_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_59_isParam_op_ctxt,rel_59_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,3> key({{0,env2[2],0}});
auto range = rel_59_isParam->equalRange_2(key,READ_OP_CONTEXT(rel_59_isParam_op_ctxt));
for(const auto& env3 : range) {
if( (reads[14]++,!rel_91_MethodDerefArg->contains(Tuple<RamDomain,2>({{env3[0],env3[2]}}),READ_OP_CONTEXT(rel_91_MethodDerefArg_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2])}});
rel_93_new_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_93_new_MethodDerefArg_op_ctxt));
}
freqs[204]++;
}
freqs[205]++;
}
freqs[206]++;
}
freqs[207]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_93_new_MethodDerefArg->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MethodDerefArg;../may-null/rules.dl [31:7-31:59];)_",iter, [&](){return rel_93_new_MethodDerefArg->size();});
rel_91_MethodDerefArg->insertAll(*rel_93_new_MethodDerefArg);
std::swap(rel_92_delta_MethodDerefArg, rel_93_new_MethodDerefArg);
rel_93_new_MethodDerefArg->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_92_delta_MethodDerefArg->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_93_new_MethodDerefArg->purge();
{
	Logger logger(R"_(@t-relation-savetime;MethodDerefArg;../may-null/rules.dl [31:7-31:59];savetime;)_",iter, [&](){return rel_91_MethodDerefArg->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_91_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_82_NullAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_59_isParam->purge();
}();
/* END STRATUM 80 */
/* BEGIN STRATUM 81 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;JDKFunctionSummary;../may-null/rules.dl [32:7-32:63];)_",iter, [&](){return rel_94_JDKFunctionSummary->size();});
SignalHandler::instance()->setMsg(R"_(JDKFunctionSummary(index,method) :- 
   MethodDerefArg(index,method),
   !ApplicationMethod(method).
in file ../may-null/rules.dl [383:1-385:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;JDKFunctionSummary;../may-null/rules.dl [383:1-385:28];JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;)_",iter, [&](){return rel_94_JDKFunctionSummary->size();});
if (!rel_91_MethodDerefArg->empty()) [&](){
auto part = rel_91_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_94_JDKFunctionSummary_op_ctxt,rel_94_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_91_MethodDerefArg_op_ctxt,rel_91_MethodDerefArg->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[15]++,!rel_27_ApplicationMethod->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_94_JDKFunctionSummary->insert(tuple,READ_OP_CONTEXT(rel_94_JDKFunctionSummary_op_ctxt));
}
freqs[208]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;JDKFunctionSummary;../may-null/rules.dl [32:7-32:63];savetime;)_",iter, [&](){return rel_94_JDKFunctionSummary->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_91_MethodDerefArg->purge();
}();
/* END STRATUM 81 */
/* BEGIN STRATUM 82 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NPEWithMayNull;../may-null/rules.dl [22:7-22:115];)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(meth,index,file,line,type,var,insn) :- 
   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   MayNullPtr(insn,var,meth,_).
in file ../may-null/rules.dl [344:1-346:32])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;../may-null/rules.dl [344:1-346:32];NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
if (!rel_68_MayNullPtr->empty()&&!rel_90_MinPathSensitiveNullAtLine->empty()) [&](){
auto part = rel_90_MinPathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_90_MinPathSensitiveNullAtLine_op_ctxt,rel_90_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt,rel_95_NPEWithMayNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[6],env0[5],env0[0],0}});
auto range = rel_68_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_95_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt));
freqs[209]++;
}
freqs[210]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _StaticMethodInvocation(from_insn,index,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [388:1-401:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;../may-null/rules.dl [388:1-401:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_71_InstructionLine->empty()&&!rel_94_JDKFunctionSummary->empty()&&!rel_68_MayNullPtr->empty()&&!rel_75_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_94_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_71_InstructionLine_op_ctxt,rel_71_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_94_JDKFunctionSummary_op_ctxt,rel_94_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt,rel_95_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_75_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_68_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],0,0,env4[2]}});
auto range = rel_3_StaticMethodInvocation->equalRange_9(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_71_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_71_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(42)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_95_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt));
freqs[211]++;
}
freqs[212]++;
}
freqs[213]++;
}
freqs[214]++;
}
freqs[215]++;
}
freqs[216]++;
}
freqs[217]++;
}
freqs[218]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _VirtualMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [388:1-401:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;../may-null/rules.dl [388:1-401:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_71_InstructionLine->empty()&&!rel_94_JDKFunctionSummary->empty()&&!rel_68_MayNullPtr->empty()&&!rel_75_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_94_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_71_InstructionLine_op_ctxt,rel_71_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_94_JDKFunctionSummary_op_ctxt,rel_94_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt,rel_95_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_75_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_68_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_4_VirtualMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_71_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_71_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(42)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_95_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt));
freqs[219]++;
}
freqs[220]++;
}
freqs[221]++;
}
freqs[222]++;
}
freqs[223]++;
}
freqs[224]++;
}
freqs[225]++;
}
freqs[226]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _SpecialMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file ../may-null/rules.dl [388:1-401:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;../may-null/rules.dl [388:1-401:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_43_CallGraphEdge->empty()&&!rel_71_InstructionLine->empty()&&!rel_94_JDKFunctionSummary->empty()&&!rel_68_MayNullPtr->empty()&&!rel_75_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_94_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt,rel_43_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_71_InstructionLine_op_ctxt,rel_71_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_94_JDKFunctionSummary_op_ctxt,rel_94_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt,rel_68_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt,rel_95_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt,rel_75_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_43_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_43_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_75_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_75_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_68_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_68_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_2_SpecialMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_71_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_71_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(42)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_95_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_95_NPEWithMayNull_op_ctxt));
freqs[227]++;
}
freqs[228]++;
}
freqs[229]++;
}
freqs[230]++;
}
freqs[231]++;
}
freqs[232]++;
}
freqs[233]++;
}
freqs[234]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NPEWithMayNull;../may-null/rules.dl [22:7-22:115];savetime;)_",iter, [&](){return rel_95_NPEWithMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_95_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_71_InstructionLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_43_CallGraphEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_27_ApplicationMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_SpecialMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_VirtualMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_StaticMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_19_ActualParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_75_VarPointsToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_68_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_90_MinPathSensitiveNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_94_JDKFunctionSummary->purge();
}();
/* END STRATUM 82 */
/* BEGIN STRATUM 83 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;JumpTarget;../declarations.dl [119:7-119:53];loadtime;)_",iter, [&](){return rel_96_JumpTarget->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_96_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;JumpTarget;../declarations.dl [119:7-119:53];)",rel_96_JumpTarget->size(),iter);}();
/* END STRATUM 83 */
/* BEGIN STRATUM 84 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueBranch;../may-null/rules.dl [16:7-16:63];)_",iter, [&](){return rel_97_TrueBranch->size();});
SignalHandler::instance()->setMsg(R"_(TrueBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   JumpTarget(insn,ifIns).
in file ../may-null/rules.dl [277:1-279:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueBranch;../may-null/rules.dl [277:1-279:25];TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;)_",iter, [&](){return rel_97_TrueBranch->size();});
if (!rel_96_JumpTarget->empty()&&!rel_36_MayNull_IfInstruction->empty()) [&](){
auto part = rel_36_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_96_JumpTarget_op_ctxt,rel_96_JumpTarget->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstruction_op_ctxt,rel_36_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_97_TrueBranch_op_ctxt,rel_97_TrueBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_96_JumpTarget->equalRange_2(key,READ_OP_CONTEXT(rel_96_JumpTarget_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_97_TrueBranch->insert(tuple,READ_OP_CONTEXT(rel_97_TrueBranch_op_ctxt));
freqs[235]++;
}
freqs[236]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueBranch;../may-null/rules.dl [16:7-16:63];savetime;)_",iter, [&](){return rel_97_TrueBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_96_JumpTarget->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_36_MayNull_IfInstruction->purge();
}();
/* END STRATUM 84 */
/* BEGIN STRATUM 85 */
[&]() {
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;ReachableNullAt;../declarations.dl [167:7-167:92];)",rel_98_ReachableNullAt->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;ReachableNullAt;../declarations.dl [167:7-167:92];savetime;)_",iter, [&](){return rel_98_ReachableNullAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_98_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 85 */
/* BEGIN STRATUM 86 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignNumConstant;../declarations.dl [96:7-96:92];loadtime;)_",iter, [&](){return rel_99_AssignNumConstant->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNumConstant.facts"},{"name","_AssignNumConstant"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_99_AssignNumConstant);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignNumConstant;../declarations.dl [96:7-96:92];)",rel_99_AssignNumConstant->size(),iter);}();
/* END STRATUM 86 */
/* BEGIN STRATUM 87 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReturnFalse;../may-null/rules.dl [187:7-187:66];)_",iter, [&](){return rel_100_ReturnFalse->size();});
SignalHandler::instance()->setMsg(R"_(ReturnFalse(retInsn,invokedMethod) :- 
   _Return(retInsn,_,returnVar,invokedMethod),
   _AssignNumConstant(_,_,"0",returnVar,invokedMethod).
in file ../may-null/rules.dl [228:1-230:57])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReturnFalse;../may-null/rules.dl [228:1-230:57];ReturnFalse(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"0\",returnVar,invokedMethod).;)_",iter, [&](){return rel_100_ReturnFalse->size();});
if (!rel_99_AssignNumConstant->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_20_Return->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_100_ReturnFalse_op_ctxt,rel_100_ReturnFalse->createContext());
CREATE_OP_CONTEXT(rel_99_AssignNumConstant_op_ctxt,rel_99_AssignNumConstant->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,RamDomain(38),env0[2],env0[3]}});
auto range = rel_99_AssignNumConstant->equalRange_28(key,READ_OP_CONTEXT(rel_99_AssignNumConstant_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3])}});
rel_100_ReturnFalse->insert(tuple,READ_OP_CONTEXT(rel_100_ReturnFalse_op_ctxt));
freqs[237]++;
}
freqs[238]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;ReturnFalse;../may-null/rules.dl [187:7-187:66];savetime;)_",iter, [&](){return rel_100_ReturnFalse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","returnInsn\tcallee"},{"filename","./ReturnFalse.csv"},{"name","ReturnFalse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_100_ReturnFalse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 87 */
/* BEGIN STRATUM 88 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReturnTrue;../may-null/rules.dl [188:7-188:65];)_",iter, [&](){return rel_101_ReturnTrue->size();});
SignalHandler::instance()->setMsg(R"_(ReturnTrue(retInsn,invokedMethod) :- 
   _Return(retInsn,_,returnVar,invokedMethod),
   _AssignNumConstant(_,_,"1",returnVar,invokedMethod).
in file ../may-null/rules.dl [232:1-234:57])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReturnTrue;../may-null/rules.dl [232:1-234:57];ReturnTrue(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"1\",returnVar,invokedMethod).;)_",iter, [&](){return rel_101_ReturnTrue->size();});
if (!rel_99_AssignNumConstant->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_20_Return->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_101_ReturnTrue_op_ctxt,rel_101_ReturnTrue->createContext());
CREATE_OP_CONTEXT(rel_99_AssignNumConstant_op_ctxt,rel_99_AssignNumConstant->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,RamDomain(39),env0[2],env0[3]}});
auto range = rel_99_AssignNumConstant->equalRange_28(key,READ_OP_CONTEXT(rel_99_AssignNumConstant_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3])}});
rel_101_ReturnTrue->insert(tuple,READ_OP_CONTEXT(rel_101_ReturnTrue_op_ctxt));
freqs[239]++;
}
freqs[240]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;ReturnTrue;../may-null/rules.dl [188:7-188:65];savetime;)_",iter, [&](){return rel_101_ReturnTrue->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","returnInsn\tcallee"},{"filename","./ReturnTrue.csv"},{"name","ReturnTrue"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_101_ReturnTrue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_99_AssignNumConstant->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_20_Return->purge();
}();
/* END STRATUM 88 */
}
ProfileEventSingleton::instance().stopTimer();
dumpFreqs();

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_AssignReturnValue:\n";
rel_1_AssignReturnValue->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_SpecialMethodInvocation:\n";
rel_2_SpecialMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_StaticMethodInvocation:\n";
rel_3_StaticMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_VirtualMethodInvocation:\n";
rel_4_VirtualMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_AssignReturnValue_WithInvoke:\n";
rel_5_AssignReturnValue_WithInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_IterNextInsn:\n";
rel_6_IterNextInsn->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_Primitive:\n";
rel_7_Primitive->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_Var_Type:\n";
rel_8_Var_Type->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_RefTypeVar:\n";
rel_9_RefTypeVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_AssignCast:\n";
rel_10_AssignCast->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_11_AssignCastNull:\n";
rel_11_AssignCastNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_12_AssignHeapAllocation:\n";
rel_12_AssignHeapAllocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_13_AssignLocal:\n";
rel_13_AssignLocal->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_14_DefineVar:\n";
rel_14_DefineVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_15_AssignNull:\n";
rel_15_AssignNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_16_LoadInstanceField:\n";
rel_16_LoadInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_17_LoadStaticField:\n";
rel_17_LoadStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_18_VarDef:\n";
rel_18_VarDef->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_19_ActualParam:\n";
rel_19_ActualParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_20_Return:\n";
rel_20_Return->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_21_StoreArrayIndex:\n";
rel_21_StoreArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_22_StoreInstanceField:\n";
rel_22_StoreInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_23_StoreStaticField:\n";
rel_23_StoreStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_24_AllUse:\n";
rel_24_AllUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_25_FirstUse:\n";
rel_25_FirstUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_26_LastUse:\n";
rel_26_LastUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_27_ApplicationMethod:\n";
rel_27_ApplicationMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_28_AssignMayNull:\n";
rel_28_AssignMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_29_BasicBlockHead:\n";
rel_29_BasicBlockHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_30_Instruction_Next:\n";
rel_30_Instruction_Next->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_31_IfVar:\n";
rel_31_IfVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_32_BoolIf:\n";
rel_32_BoolIf->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_33_BoolIfVarInvoke:\n";
rel_33_BoolIfVarInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_34_hasNextIf:\n";
rel_34_hasNextIf->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_35_IfNull:\n";
rel_35_IfNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_36_MayNull_IfInstruction:\n";
rel_36_MayNull_IfInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_37_FalseBranch:\n";
rel_37_FalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_38_OperatorAt:\n";
rel_38_OperatorAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_39_MayNull_IfInstructionsCond:\n";
rel_39_MayNull_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_40_BoolFalseBranch:\n";
rel_40_BoolFalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_41_BoolTrueBranch:\n";
rel_41_BoolTrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_42_ParamInBoolBranch:\n";
rel_42_ParamInBoolBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_43_CallGraphEdge:\n";
rel_43_CallGraphEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_44_PhiNodeHead:\n";
rel_44_PhiNodeHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_45_PhiNodeHeadVar:\n";
rel_45_PhiNodeHeadVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_46_CanBeNullBranch:\n";
rel_46_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_47_delta_CanBeNullBranch:\n";
rel_47_delta_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_48_new_CanBeNullBranch:\n";
rel_48_new_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_49_CannotBeNullBranch:\n";
rel_49_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_50_delta_CannotBeNullBranch:\n";
rel_50_delta_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_51_new_CannotBeNullBranch:\n";
rel_51_new_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_52_Dominates:\n";
rel_52_Dominates->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_53_NextInsideHasNext:\n";
rel_53_NextInsideHasNext->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_54_FormalParam:\n";
rel_54_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_55_Instruction_FormalParam:\n";
rel_55_Instruction_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_56_UnfilteredIsParam:\n";
rel_56_UnfilteredIsParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_57_delta_UnfilteredIsParam:\n";
rel_57_delta_UnfilteredIsParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_58_new_UnfilteredIsParam:\n";
rel_58_new_UnfilteredIsParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_59_isParam:\n";
rel_59_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_60_delta_isParam:\n";
rel_60_delta_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_61_new_isParam:\n";
rel_61_new_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_62_ThisVar:\n";
rel_62_ThisVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_63_Var_DeclaringMethod:\n";
rel_63_Var_DeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_64_Instruction_VarDeclaringMethod:\n";
rel_64_Instruction_VarDeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_65_Method_FirstInstruction:\n";
rel_65_Method_FirstInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_66_MayPredecessorModuloThrow:\n";
rel_66_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_67_SpecialIfEdge:\n";
rel_67_SpecialIfEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_68_MayNullPtr:\n";
rel_68_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_69_delta_MayNullPtr:\n";
rel_69_delta_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_70_new_MayNullPtr:\n";
rel_70_new_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_71_InstructionLine:\n";
rel_71_InstructionLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_72_VarPointsTo:\n";
rel_72_VarPointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_73_VarMayPointToNull:\n";
rel_73_VarMayPointToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_74_VarCannotBeNull:\n";
rel_74_VarCannotBeNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_75_VarPointsToNull:\n";
rel_75_VarPointsToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_76_AssignBinop:\n";
rel_76_AssignBinop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_77_AssignOperFrom:\n";
rel_77_AssignOperFrom->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_78_AssignUnop:\n";
rel_78_AssignUnop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_79_EnterMonitor:\n";
rel_79_EnterMonitor->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_80_LoadArrayIndex:\n";
rel_80_LoadArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_81_ThrowNull:\n";
rel_81_ThrowNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_82_NullAt:\n";
rel_82_NullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_83_ReachableNullAtLine:\n";
rel_83_ReachableNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_84_IfInstructions:\n";
rel_84_IfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_85_IfInstructionsCond:\n";
rel_85_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_86_TrueIfInstructions:\n";
rel_86_TrueIfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_87_DominatesUnreachable:\n";
rel_87_DominatesUnreachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_88_UnreachablePathNPEIns:\n";
rel_88_UnreachablePathNPEIns->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_89_PathSensitiveNullAtLine:\n";
rel_89_PathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_90_MinPathSensitiveNullAtLine:\n";
rel_90_MinPathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_91_MethodDerefArg:\n";
rel_91_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_92_delta_MethodDerefArg:\n";
rel_92_delta_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_93_new_MethodDerefArg:\n";
rel_93_new_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_94_JDKFunctionSummary:\n";
rel_94_JDKFunctionSummary->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_95_NPEWithMayNull:\n";
rel_95_NPEWithMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_96_JumpTarget:\n";
rel_96_JumpTarget->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_97_TrueBranch:\n";
rel_97_TrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_98_ReachableNullAt:\n";
rel_98_ReachableNullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_99_AssignNumConstant:\n";
rel_99_AssignNumConstant->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_100_ReturnFalse:\n";
rel_100_ReturnFalse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_101_ReturnTrue:\n";
rel_101_ReturnTrue->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction(".", ".", stratumIndex, false); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction(inputDirectory, outputDirectory, stratumIndex, true);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tpos\tvar"},{"filename","./BoolIf.csv"},{"name","BoolIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_32_BoolIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","assignReturn\tifIns\tvar"},{"filename","./BoolIfVarInvoke.csv"},{"name","BoolIfVarInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_33_BoolIfVarInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./BoolFalseBranch.csv"},{"name","BoolFalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_40_BoolFalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./BoolTrueBranch.csv"},{"name","BoolTrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_41_BoolTrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./ParamInBoolBranch.csv"},{"name","ParamInBoolBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_ParamInBoolBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_59_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_64_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_82_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_84_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_85_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_86_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_88_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_91_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_95_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_98_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","returnInsn\tcallee"},{"filename","./ReturnFalse.csv"},{"name","ReturnFalse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_100_ReturnFalse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","returnInsn\tcallee"},{"filename","./ReturnTrue.csv"},{"name","ReturnTrue"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_101_ReturnTrue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
private:
void dumpFreqs() {
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;0;)_", freqs[30],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;VarDef(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;1;)_", freqs[29],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;AssignReturnValue_WithInvoke(insn,index,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;2;)_", freqs[47],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;0;)_", freqs[49],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;1;)_", freqs[48],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;0;)_", freqs[58],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;1;)_", freqs[57],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_SpecialMethodInvocation(insn,index,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;2;)_", freqs[56],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;0;)_", freqs[52],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;1;)_", freqs[51],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_StaticMethodInvocation(insn,index,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;2;)_", freqs[50],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;0;)_", freqs[55],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;1;)_", freqs[54],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_VirtualMethodInvocation(insn,index,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;2;)_", freqs[53],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;0;)_", freqs[34],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;_AssignCast(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;1;)_", freqs[33],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;0;)_", freqs[32],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;_AssignLocal(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;1;)_", freqs[31],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;0;)_", freqs[46],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;_Return(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;1;)_", freqs[45],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;0;)_", freqs[36],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;_SpecialMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;1;)_", freqs[35],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;0;)_", freqs[40],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;_StoreArrayIndex(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;1;)_", freqs[39],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;0;)_", freqs[42],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;_StoreInstanceField(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;1;)_", freqs[41],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;0;)_", freqs[44],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;_StoreStaticField(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;1;)_", freqs[43],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;0;)_", freqs[38],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;_VirtualMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;1;)_", freqs[37],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;0;)_", freqs[5],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_SpecialMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;1;)_", freqs[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;0;)_", freqs[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_StaticMethodInvocation(insn,index,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;1;)_", freqs[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;0;)_", freqs[3],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_VirtualMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;1;)_", freqs[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolFalseBranch;0;BoolFalseBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\").;MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\");BoolFalseBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\").;0;)_", freqs[78],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolFalseBranch;0;BoolFalseBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);BoolFalseBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[76],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolFalseBranch;0;BoolFalseBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\");BoolFalseBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[77],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolIf;0;BoolIf(ifIns,pos,var) :- \n   _IfVar(ifIns,pos,var),\n   _Var_Type(var,\"boolean\").;_IfVar(ifIns,pos,var);BoolIf(ifIns,pos,var) :- \n   _IfVar(ifIns,pos,var),\n   _Var_Type(var,\"boolean\").;0;)_", freqs[62],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolIf;0;BoolIf(ifIns,pos,var) :- \n   _IfVar(ifIns,pos,var),\n   _Var_Type(var,\"boolean\").;_Var_Type(var,\"boolean\");BoolIf(ifIns,pos,var) :- \n   _IfVar(ifIns,pos,var),\n   _Var_Type(var,\"boolean\").;1;)_", freqs[61],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolIfVarInvoke;0;BoolIfVarInvoke(assignReturn,ifIns,var) :- \n   AssignReturnValue_WithInvoke(assignReturn,_,var,_),\n   BoolIf(ifIns,_,var).;AssignReturnValue_WithInvoke(assignReturn,_,var,_);BoolIfVarInvoke(assignReturn,ifIns,var) :- \n   AssignReturnValue_WithInvoke(assignReturn,_,var,_),\n   BoolIf(ifIns,_,var).;0;)_", freqs[64],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolIfVarInvoke;0;BoolIfVarInvoke(assignReturn,ifIns,var) :- \n   AssignReturnValue_WithInvoke(assignReturn,_,var,_),\n   BoolIf(ifIns,_,var).;BoolIf(ifIns,_,var);BoolIfVarInvoke(assignReturn,ifIns,var) :- \n   AssignReturnValue_WithInvoke(assignReturn,_,var,_),\n   BoolIf(ifIns,_,var).;1;)_", freqs[63],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolTrueBranch;0;BoolTrueBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\").;MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\");BoolTrueBranch(ifIns,ifIns) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"!=\").;0;)_", freqs[81],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolTrueBranch;0;BoolTrueBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);BoolTrueBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[79],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;BoolTrueBranch;0;BoolTrueBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\");BoolTrueBranch(ifIns,insn) :- \n   MayNull_IfInstructionsCond(ifIns,_,\"0\",\"==\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[80],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;@delta_CanBeNullBranch(insn,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[95],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[94],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;0;)_", freqs[91],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[92],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[93],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;@delta_CannotBeNullBranch(insn,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[100],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[99],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;0;)_", freqs[96],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[97],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[98],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;AssignReturnValue_WithInvoke(insn,_,to,method);DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;0;)_", freqs[12],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;_AssignCast(insn,_,_,to,_,method);DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;0;)_", freqs[10],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignHeapAllocation(insn,_,_,to,method,_).;_AssignHeapAllocation(insn,_,_,to,method,_);DefineVar(insn,to,method) :- \n   _AssignHeapAllocation(insn,_,_,to,method,_).;0;)_", freqs[11],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;_AssignLocal(insn,_,_,to,method);DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;0;)_", freqs[9],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;Instruction_Next(ifIns,ins);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;1;)_", freqs[193],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;TrueIfInstructions(ifIns);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;0;)_", freqs[194],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;Instruction_Next(ifIns,insn);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;1;)_", freqs[70],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;MayNull_IfInstruction(ifIns);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;0;)_", freqs[71],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FirstUse;0;FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;AllUse(insn,last,var,method);FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;0;)_", freqs[59],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;BasicBlockHead(ins,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;1;)_", freqs[176],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;MayPredecessorModuloThrow(ifIns,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;2;)_", freqs[175],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;0;)_", freqs[177],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;ReachableNullAtLine(_,_,_,_,_,var,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;0;)_", freqs[179],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;_IfVar(ifIns,_,var);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;1;)_", freqs[178],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;0;)_", freqs[186],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;VarPointsToNull(varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;3;)_", freqs[183],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;2;)_", freqs[184],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;1;)_", freqs[185],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;0;)_", freqs[182],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;2;)_", freqs[180],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;1;)_", freqs[181],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_FormalParam;0;Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;_FormalParam(index,method,param);Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;0;)_", freqs[106],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;RefTypeVar(var);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;0;)_", freqs[116],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;_Var_DeclaringMethod(var,method);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;1;)_", freqs[115],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IterNextInsn;0;IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;_AssignReturnValue(insn,returnVar);IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;0;)_", freqs[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IterNextInsn;0;IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;_VirtualMethodInvocation(insn,_,sig,var,_);IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;1;)_", freqs[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;JDKFunctionSummary;0;JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;MethodDerefArg(index,method);JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;0;)_", freqs[208],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;LastUse;0;LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;AllUse(insn,last,var,method);LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;0;)_", freqs[60],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;@delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;0;)_", freqs[141],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;2;)_", freqs[139],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;Instruction_FormalParam(insn,method,to,index);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;3;)_", freqs[138],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;_ActualParam(index,from_insn,from);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;1;)_", freqs[140],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;@delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;0;)_", freqs[137],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;3;)_", freqs[134],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;2;)_", freqs[135],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;_Return(returnInsn, _unnamed_var2,returnVar,invokedMethod);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;1;)_", freqs[136],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method),\n   !MayNullPtr(next,to,method,\"Alias\").;@delta_MayNullPtr(next,from,method, _unnamed_var1);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;0;)_", freqs[129],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method),\n   !MayNullPtr(next,to,method,\"Alias\").;_AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;1;)_", freqs[128],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignLocal(next, _unnamed_var2,from,to,method),\n   !MayNullPtr(next,to,method,\"Alias\").;@delta_MayNullPtr(next,from,method, _unnamed_var1);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;0;)_", freqs[127],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignLocal(next, _unnamed_var2,from,to,method),\n   !MayNullPtr(next,to,method,\"Alias\").;_AssignLocal(next, _unnamed_var2,from,to,method);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;1;)_", freqs[126],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var),\n   !MayNullPtr(next,var,method,\"Transfer\").;@delta_MayNullPtr(insn,var,method, _unnamed_var1);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;0;)_", freqs[131],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var),\n   !MayNullPtr(next,var,method,\"Transfer\").;MayPredecessorModuloThrow(insn,next);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;1;)_", freqs[130],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var),\n   !MayNullPtr(next,var,method,\"Transfer\").;@delta_MayNullPtr(insn,var,method, _unnamed_var1);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;0;)_", freqs[133],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var),\n   !MayNullPtr(next,var,method,\"Transfer\").;SpecialIfEdge(insn,next,var);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !ParamInBoolBranch(next,var).;1;)_", freqs[132],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;_AssignNull(insn,_,var,method);MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;0;)_", freqs[125],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;Instruction_VarDeclaringMethod(insn,method,var);MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;0;)_", freqs[124],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstruction;0;MayNull_IfInstruction(ifIns) :- \n   BoolIf(ifIns,_,_).;BoolIf(ifIns,_,_);MayNull_IfInstruction(ifIns) :- \n   BoolIf(ifIns,_,_).;0;)_", freqs[69],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstruction;0;MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;_IfNull(ifIns,_,_);MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;0;)_", freqs[68],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"0\",opt) :- \n   BoolIf(ifIns,1,left),\n   _OperatorAt(ifIns,opt).;BoolIf(ifIns,1,left);MayNull_IfInstructionsCond(ifIns,left,\"0\",opt) :- \n   BoolIf(ifIns,1,left),\n   _OperatorAt(ifIns,opt).;0;)_", freqs[75],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"0\",opt) :- \n   BoolIf(ifIns,1,left),\n   _OperatorAt(ifIns,opt).;_OperatorAt(ifIns,opt);MayNull_IfInstructionsCond(ifIns,left,\"0\",opt) :- \n   BoolIf(ifIns,1,left),\n   _OperatorAt(ifIns,opt).;1;)_", freqs[74],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_IfNull(ifIns,_,left);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;0;)_", freqs[73],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_OperatorAt(ifIns,opt);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;1;)_", freqs[72],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Instruction_FormalParam(insn,method,_,_);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;0;)_", freqs[120],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Method_FirstInstruction(method,next);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;1;)_", freqs[119],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;FirstUse(next,_,var,method);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;1;)_", freqs[117],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;Instruction_VarDeclaringMethod(insn,method,var);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;0;)_", freqs[118],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;@delta_MethodDerefArg(invokedIndex,invokedMethod);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;0;)_", freqs[207],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;1;)_", freqs[206],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;_ActualParam(invokedIndex,from_insn,from);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;2;)_", freqs[205],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;isParam(index,from,method);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;3;)_", freqs[204],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;MayNullPtr(insn,var,method,_);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;1;)_", freqs[202],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;NullAt(method,_,_,var,insn);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;0;)_", freqs[203],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;isParam(index,var,method);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;2;)_", freqs[201],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MinPathSensitiveNullAtLine;0;MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;PathSensitiveNullAtLine(meth,index,file,line,type,var,insn);MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;0;)_", freqs[200],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;MayNullPtr(insn,var,meth,_);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;1;)_", freqs[209],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;0;)_", freqs[210],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[229],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[233],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[227],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[234],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[230],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[231],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[232],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_SpecialMethodInvocation(from_insn,index,_,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[228],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[213],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[217],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[211],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[218],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[214],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[215],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[216],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;_StaticMethodInvocation(from_insn,index,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[212],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[221],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[225],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[219],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[226],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[222],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[223],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[224],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_VirtualMethodInvocation(from_insn,index,_,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[220],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;BasicBlockHead(nextInsn,nextInsnHead);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;3;)_", freqs[102],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;Dominates(falseBlockInsn,nextInsnHead);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;2;)_", freqs[103],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;Instruction_Next(ifInsn,falseBlockInsn);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;1;)_", freqs[104],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;IterNextInsn(nextInsn,var,invokeVar);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;4;)_", freqs[101],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;hasNextIf(ifInsn,_,invokeVar);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;0;)_", freqs[105],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;VarPointsToNull(var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;0;)_", freqs[168],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;_AssignBinop(insn,index,_,meth);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;1;)_", freqs[167],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;_AssignOperFrom(insn,_,var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;2;)_", freqs[166],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;0;)_", freqs[171],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;_EnterMonitor(insn,index,var,meth);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;1;)_", freqs[170],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;0;)_", freqs[152],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;_LoadArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;1;)_", freqs[151],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[158],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;_LoadInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[157],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[162],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;_SpecialMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[161],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;0;)_", freqs[154],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;_StoreArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;1;)_", freqs[153],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[156],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;_StoreInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[155],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;CallGraphEdge(_,a,_,b);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;0;)_", freqs[150],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;_SpecialMethodInvocation(a,index,b,_,meth);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;1;)_", freqs[149],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;_ThrowNull(insn,index,meth);NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;0;)_", freqs[169],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;VarPointsToNull(var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;0;)_", freqs[165],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;_AssignOperFrom(insn,_,var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;2;)_", freqs[163],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;_AssignUnop(insn,index,_,meth);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,_,var).;1;)_", freqs[164],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[160],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;_VirtualMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[159],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;BoolFalseBranch(ifIns,insn);ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;0;)_", freqs[84],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;BoolIfVarInvoke(assignReturn,ifIns,_);ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;1;)_", freqs[83],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;_ActualParam(_,assignReturn,var);ParamInBoolBranch(insn,var) :- \n   BoolFalseBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;2;)_", freqs[82],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;BoolIfVarInvoke(assignReturn,ifIns,_);ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;1;)_", freqs[86],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;BoolTrueBranch(ifIns,insn);ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;0;)_", freqs[87],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ParamInBoolBranch;0;ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;_ActualParam(_,assignReturn,var);ParamInBoolBranch(insn,var) :- \n   BoolTrueBranch(ifIns,insn),\n   BoolIfVarInvoke(assignReturn,ifIns,_),\n   _ActualParam(_,assignReturn,var).;2;)_", freqs[85],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PathSensitiveNullAtLine;0;PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;ReachableNullAtLine(meth,index,file,line,type,var,ins);PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins),\n   !UnreachablePathNPEIns(ins).;0;)_", freqs[199],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;PhiNodeHead(insn,headInsn);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;0;)_", freqs[90],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(headInsn,_,headVar,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;2;)_", freqs[88],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(insn,_,var,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;1;)_", freqs[89],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;ApplicationMethod(meth);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;1;)_", freqs[173],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;InstructionLine(meth,index,line,file);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;2;)_", freqs[172],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;NullAt(meth,index,type,var,insn);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;0;)_", freqs[174],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;RefTypeVar;0;RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;_Var_Type(var,type);RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;0;)_", freqs[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReturnFalse;0;ReturnFalse(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"0\",returnVar,invokedMethod).;_AssignNumConstant(_,_,\"0\",returnVar,invokedMethod);ReturnFalse(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"0\",returnVar,invokedMethod).;1;)_", freqs[237],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReturnFalse;0;ReturnFalse(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"0\",returnVar,invokedMethod).;_Return(retInsn,_,returnVar,invokedMethod);ReturnFalse(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"0\",returnVar,invokedMethod).;0;)_", freqs[238],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReturnTrue;0;ReturnTrue(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"1\",returnVar,invokedMethod).;_AssignNumConstant(_,_,\"1\",returnVar,invokedMethod);ReturnTrue(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"1\",returnVar,invokedMethod).;1;)_", freqs[239],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReturnTrue;0;ReturnTrue(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"1\",returnVar,invokedMethod).;_Return(retInsn,_,returnVar,invokedMethod);ReturnTrue(retInsn,invokedMethod) :- \n   _Return(retInsn,_,returnVar,invokedMethod),\n   _AssignNumConstant(_,_,\"1\",returnVar,invokedMethod).;0;)_", freqs[240],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;FalseBranch(ifIns,next);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;0;)_", freqs[123],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;MayNull_IfInstructionsCond(ifIns,var,\"null\",_);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;2;)_", freqs[121],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;MayPredecessorModuloThrow(beforeIf,ifIns);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;1;)_", freqs[122],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;JumpTarget(insn,ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;1;)_", freqs[235],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;MayNull_IfInstruction(ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;0;)_", freqs[236],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;IfInstructionsCond(ifIns,left,right,\"!=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;0;)_", freqs[187],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;IfInstructionsCond(ifIns,left,right,\"<=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;0;)_", freqs[189],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;IfInstructionsCond(ifIns,left,right,\"<\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;0;)_", freqs[191],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;IfInstructionsCond(ifIns,left,right,\"==\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;0;)_", freqs[188],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;IfInstructionsCond(ifIns,left,right,\">=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;0;)_", freqs[190],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;IfInstructionsCond(ifIns,left,right,\">\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;0;)_", freqs[192],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnfilteredIsParam;0;@new_UnfilteredIsParam(index,to,method) :- \n   @delta_UnfilteredIsParam(index,from,method),\n   _AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method),\n   !UnfilteredIsParam(index,to,method).;@delta_UnfilteredIsParam(index,from,method);UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;0;)_", freqs[111],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnfilteredIsParam;0;@new_UnfilteredIsParam(index,to,method) :- \n   @delta_UnfilteredIsParam(index,from,method),\n   _AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method),\n   !UnfilteredIsParam(index,to,method).;_AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method);UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;1;)_", freqs[110],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnfilteredIsParam;0;@new_UnfilteredIsParam(index,to,method) :- \n   @delta_UnfilteredIsParam(index,from,method),\n   _AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method),\n   !UnfilteredIsParam(index,to,method).;@delta_UnfilteredIsParam(index,from,method);UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;0;)_", freqs[109],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnfilteredIsParam;0;@new_UnfilteredIsParam(index,to,method) :- \n   @delta_UnfilteredIsParam(index,from,method),\n   _AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method),\n   !UnfilteredIsParam(index,to,method).;_AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method);UnfilteredIsParam(index,to,method) :- \n   UnfilteredIsParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;1;)_", freqs[108],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnfilteredIsParam;0;UnfilteredIsParam(index,var,method) :- \n   _FormalParam(index,method,var).;_FormalParam(index,method,var);UnfilteredIsParam(index,var,method) :- \n   _FormalParam(index,method,var).;0;)_", freqs[107],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;BasicBlockHead(ins,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;2;)_", freqs[196],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;Dominates(parent,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;3;)_", freqs[195],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;DominatesUnreachable(_,parent);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;1;)_", freqs[197],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;0;)_", freqs[198],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarCannotBeNull;0;VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream err>\".;VarMayPointToNull(var);VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream err>\".;0;)_", freqs[146],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarCannotBeNull;0;VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream err>\".;_LoadStaticField(_,_,var,out,_);VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream err>\".;1;)_", freqs[145],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarCannotBeNull;0;VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream out>\".;VarMayPointToNull(var);VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream out>\".;0;)_", freqs[144],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarCannotBeNull;0;VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream out>\".;_LoadStaticField(_,_,var,out,_);VarCannotBeNull(var) :- \n   VarMayPointToNull(var),\n   _LoadStaticField(_,_,var,out,_),\n   out = \"<java.lang.System: java.io.PrintStream out>\".;1;)_", freqs[143],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;AssignReturnValue_WithInvoke(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;1;)_", freqs[27],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;0;)_", freqs[28],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;0;)_", freqs[26],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;_AssignCast(insn,index,_,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;1;)_", freqs[25],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;0;)_", freqs[24],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;_AssignCastNull(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;1;)_", freqs[23],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;0;)_", freqs[18],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;_AssignHeapAllocation(insn,index,_,var,method,_);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;1;)_", freqs[17],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;0;)_", freqs[16],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;_AssignLocal(insn,index,_,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;1;)_", freqs[15],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;0;)_", freqs[14],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;_AssignNull(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;1;)_", freqs[13],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;0;)_", freqs[20],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;_LoadInstanceField(insn,index,var,_,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;1;)_", freqs[19],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;0;)_", freqs[22],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;_LoadStaticField(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;1;)_", freqs[21],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarMayPointToNull;0;VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   \"<<null pseudo heap>>\" contains alloc.;VarPointsTo(_,alloc,_,var);VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   \"<<null pseudo heap>>\" contains alloc.;0;)_", freqs[142],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarPointsToNull;0;VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarCannotBeNull(var).;VarMayPointToNull(var);VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarCannotBeNull(var).;0;)_", freqs[148],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarPointsToNull;0;VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;_AssignCastNull(_,_,var,_,_);VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;0;)_", freqs[147],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_AssignReturnValue(invokeInsn,returnVar);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;1;)_", freqs[66],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_IfVar(ifInsn,_,returnVar);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;0;)_", freqs[67],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_VirtualMethodInvocation(invokeInsn,_,sig,var,_);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;2;)_", freqs[65],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,var,method) :- \n   @delta_isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar),\n   !isParam(index,var,method).;@delta_isParam(index,headVar,method);isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[114],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,var,method) :- \n   @delta_isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar),\n   !isParam(index,var,method).;PhiNodeHeadVar(var,headVar);isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[113],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;isParam(index,var,method) :- \n   UnfilteredIsParam(index,var,method),\n   !_FormalParam(index,method,var).;UnfilteredIsParam(index,var,method);isParam(index,var,method) :- \n   UnfilteredIsParam(index,var,method),\n   !_FormalParam(index,method,var).;0;)_", freqs[112],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;ApplicationMethod)_", reads[15],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CanBeNullBranch)_", reads[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CannotBeNullBranch)_", reads[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;DefineVar)_", reads[9],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;LastUse)_", reads[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MayNullPtr)_", reads[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MethodDerefArg)_", reads[14],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;NextInsideHasNext)_", reads[11],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;ParamInBoolBranch)_", reads[10],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;Primitive)_", reads[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;UnfilteredIsParam)_", reads[3],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;UnreachablePathNPEIns)_", reads[13],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;VarCannotBeNull)_", reads[12],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;_FormalParam)_", reads[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;_ThisVar)_", reads[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;isParam)_", reads[5],0);
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_71_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_76_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_77_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_78_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_79_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_80_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_81_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_96_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNumConstant.facts"},{"name","_AssignNumConstant"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_99_AssignNumConstant);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_AssignReturnValue");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_SpecialMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_StaticMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_VirtualMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_Var_Type");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_10_AssignCast");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_11_AssignCastNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_12_AssignHeapAllocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_13_AssignLocal");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_15_AssignNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_16_LoadInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_17_LoadStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_19_ActualParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_20_Return");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_21_StoreArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_22_StoreInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_23_StoreStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_27_ApplicationMethod");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_29_BasicBlockHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_IfVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_IfVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_35_IfNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_35_IfNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_38_OperatorAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_OperatorAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_43_CallGraphEdge");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_43_CallGraphEdge);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_44_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_52_Dominates");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_52_Dominates);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_54_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_62_ThisVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_62_ThisVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_63_Var_DeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_63_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_65_Method_FirstInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_65_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_66_MayPredecessorModuloThrow");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_66_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_71_InstructionLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_71_InstructionLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_72_VarPointsTo");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_72_VarPointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_76_AssignBinop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_AssignBinop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_77_AssignOperFrom");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_77_AssignOperFrom);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_78_AssignUnop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_AssignUnop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_79_EnterMonitor");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_EnterMonitor);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_80_LoadArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_80_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_81_ThrowNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_81_ThrowNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_96_JumpTarget");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_96_JumpTarget);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_99_AssignNumConstant");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_99_AssignNumConstant);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_5_AssignReturnValue_WithInvoke");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_6_IterNextInsn");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_9_RefTypeVar");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_14_DefineVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_18_VarDef");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_24_AllUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_25_FirstUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_26_LastUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_28_AssignMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_32_BoolIf");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_32_BoolIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_33_BoolIfVarInvoke");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_33_BoolIfVarInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_34_hasNextIf");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_36_MayNull_IfInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_FalseBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_39_MayNull_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_40_BoolFalseBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_40_BoolFalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_41_BoolTrueBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_41_BoolTrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_42_ParamInBoolBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_ParamInBoolBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_44_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_44_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_45_PhiNodeHeadVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_46_CanBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_49_CannotBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_49_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_53_NextInsideHasNext");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_55_Instruction_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_55_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_59_isParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_59_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_64_Instruction_VarDeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_64_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_68_MayNullPtr");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_75_VarPointsToNull");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_82_NullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_82_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_83_ReachableNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_84_IfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_84_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_85_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_85_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_86_TrueIfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_86_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_87_DominatesUnreachable");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_88_UnreachablePathNPEIns");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_88_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_89_PathSensitiveNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_91_MethodDerefArg");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_91_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_94_JDKFunctionSummary");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_94_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_95_NPEWithMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_95_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_97_TrueBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_97_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_98_ReachableNullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_98_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_100_ReturnFalse");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_100_ReturnFalse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_101_ReturnTrue");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_101_ReturnTrue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may(){return new Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may;}
SymbolTable *getST__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may(SouffleProgram *p){return &reinterpret_cast<Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may();
};
public:
factory_Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may() : ProgramFactory("_home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may"){}
};
static factory_Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may __factory_Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(/home/jshe9611/digger-bugchecker/digger/logic/main/main_no_uninit_may.dl)",
R"(.)",
R"(.)",
true,
R"(/home/jshe9611/digger-bugchecker/digger/out/JSPWiki/profile.txt)",
6,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf__home_jshe9611_digger_bugchecker_digger_logic_main_main_no_uninit_may obj(opt.getProfileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("", opt.getSourceFileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("fact-dir", opt.getInputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("jobs", std::to_string(opt.getNumJobs()));
souffle::ProfileEventSingleton::instance().makeConfigRecord("output-dir", opt.getOutputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("version", "1.4.0-737-gc0b3cde6");
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
