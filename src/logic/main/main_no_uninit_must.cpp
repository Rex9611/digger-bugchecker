
#include "souffle/CompiledSouffle.h"

extern "C" {
}

namespace souffle {
using namespace ram;
struct t_btree_2__1_0__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__1_0__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_3_1_2__1__9 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,3,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_3_1_2__1__9& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_9(const t_tuple& t) const {
context h;
return equalRange_9(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,3,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<0,4,1,2,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_17(const t_tuple& t) const {
context h;
return equalRange_17(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,4,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__2_0_1_3__1__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__2_0_1_3__1__4& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0__1 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_1__0 {
using t_tuple = Tuple<RamDomain, 1>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_1__0& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 direct b-tree index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,5,0,1,3,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_36(const t_tuple& t) const {
context h;
return equalRange_36(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,5,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,4,0,1,3>>;
t_ind_1 ind_1;
using t_ind_2 = btree_set<t_tuple, index_utils::comparator<0,2,4,1,3>>;
t_ind_2 ind_2;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
t_ind_2::operation_hints hints_2;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
ind_2.insert(t, h.hints_2);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
ind_2.insertAll(other.ind_2);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_20(const t_tuple& t) const {
context h;
return equalRange_20(t, h);
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_2.lower_bound(low, h.hints_2), ind_2.upper_bound(high, h.hints_2));
}
range<t_ind_2::iterator> equalRange_21(const t_tuple& t) const {
context h;
return equalRange_21(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
ind_2.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,4,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
const auto& stats_2 = ind_2.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,4,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_2.inserts.getHits() << "/" << stats_2.inserts.getMisses() << "/" << stats_2.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_2.contains.getHits() << "/" << stats_2.contains.getMisses() << "/" << stats_2.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_2.lower_bound.getHits() << "/" << stats_2.lower_bound.getMisses() << "/" << stats_2.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_2.upper_bound.getHits() << "/" << stats_2.upper_bound.getMisses() << "/" << stats_2.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_1_3__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_1_3__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4,5>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [2,0,1,3,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2__2_1_0__3__4__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2__2_1_0__3__4__6& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_0_3_1__4__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_3_1__4__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [2,0,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__2_3_0_1__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_3_0_1__12& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_3_1__13 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,3,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_3_1__13& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_13(const t_tuple& t) const {
context h;
return equalRange_13(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,3,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1_0__1__2 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<1,0>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1_0__1__2& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [1,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_0_2_3__3_0_1_2__2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_0_2_3__3_0_1_2__2__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,0,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_2_1__1__5 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_2_1__1__5& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,2,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_2_1_3__2_3_0_1__5__12 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<2,3,0,1>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_2_1_3__2_3_0_1__5__12& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_12(const t_tuple& t) const {
context h;
return equalRange_12(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,2,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,3,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_2__0_1__1__3 {
using t_tuple = Tuple<RamDomain, 2>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_2__0_1__1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_1(const t_tuple& t) const {
context h;
return equalRange_1(t, h);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 direct b-tree index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__6 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__6& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_3_0_2__10 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,3,0,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_3_0_2__10& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_10(const t_tuple& t) const {
context h;
return equalRange_10(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,3,0,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__1_2_0__2__6__7 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,2,0>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__1_2_0__2__6__7& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_6(const t_tuple& t) const {
context h;
return equalRange_6(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [1,2,0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_0_1_2_4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_1_2_3_4_5_6 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,1,2,3,4,5,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,1,2,3,4,5,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_7__0_2_5_1_3_4_6__37 {
using t_tuple = Tuple<RamDomain, 7>;
Table<t_tuple> dataTable;
Lock insert_lock;
using t_ind_0 = btree_set<const t_tuple*, index_utils::deref_compare<typename index_utils::comparator<0,2,5,1,3,4,6>>>;
t_ind_0 ind_0;
using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
using iterator = iterator_0;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
const t_tuple* masterCopy = nullptr;
{
auto lease = insert_lock.acquire();
if (contains(t, h)) return false;
masterCopy = &dataTable.insert(t);
ind_0.insert(masterCopy, h.hints_0);
}
return true;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[7];
std::copy(ramDomain, ramDomain + 7, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5,RamDomain a6) {
RamDomain data[7] = {a0,a1,a2,a3,a4,a5,a6};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(&t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(&t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<iterator_0> equalRange_37(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[6] = MIN_RAM_DOMAIN;
high[6] = MAX_RAM_DOMAIN;
return range<iterator_0>(ind_0.lower_bound(&low, h.hints_0), ind_0.upper_bound(&high, h.hints_0));
}
range<iterator_0> equalRange_37(const t_tuple& t) const {
context h; return equalRange_37(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.getChunks(400)) {
    res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
dataTable.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 7 indirect b-tree index [0,2,5,1,3,4,6]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__2_0_1__4 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__2_0_1__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [2,0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__3_0_1_2__8 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__3_0_1_2__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [3,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3__7__15 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3__7__15& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_7(const t_tuple& t) const {
context h;
return equalRange_7(t, h);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t, context& h) const {
auto pos = ind_0.find(t, h.hints_0);
auto fin = ind_0.end();
if (pos != fin) {fin = pos; ++fin;}
return make_range(pos, fin);
}
range<t_ind_0::iterator> equalRange_15(const t_tuple& t) const {
context h;
return equalRange_15(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};

class Sf_main_no_uninit_must : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
std::string profiling_fname;
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(boolean)_",
	R"_(short)_",
	R"_(int)_",
	R"_(long)_",
	R"_(float)_",
	R"_(double)_",
	R"_(char)_",
	R"_(byte)_",
	R"_(<<null pseudo heap>>)_",
	R"_(Throw NullPointerException)_",
	R"_(throw NPE)_",
	R"_(java.lang.NullPointerException)_",
	R"_(Load Array Index)_",
	R"_(Store Array Index)_",
	R"_(Store Instance Field)_",
	R"_(Load Instance Field)_",
	R"_(Virtual Method Invocation)_",
	R"_(Special Method Invocation)_",
	R"_(Unary Operator)_",
	R"_(Binary Operator)_",
	R"_(Throw Null)_",
	R"_(throw null)_",
	R"_(Enter Monitor (Synchronized))_",
	R"_(null)_",
	R"_(!=)_",
	R"_(==)_",
	R"_(<=)_",
	R"_(>=)_",
	R"_(<)_",
	R"_(>)_",
	R"_(/var_declaration/)_",
	R"_(/map_param/)_",
	R"_(declaration)_",
	R"_(assignNull)_",
	R"_(Alias)_",
	R"_(Transfer)_",
	R"_(Return)_",
	R"_(Parameter)_",
	R"_(boolean hasNext())_",
	R"_(next())_",
	R"_(JDK Function Summary)_",
};private:
  size_t freqs[1086]{};
  size_t reads[80]{};
// -- Table: _AssignReturnValue
std::unique_ptr<t_btree_2__1_0__2> rel_1_AssignReturnValue = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<0,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_1_AssignReturnValue;
// -- Table: _SpecialMethodInvocation
std::unique_ptr<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17> rel_2_SpecialMethodInvocation = std::make_unique<t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17>();
souffle::RelationWrapper<1,t_btree_5__0_2_1_3_4__3_0_1_2_4__0_4_1_2_3__1__5__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_2_SpecialMethodInvocation;
// -- Table: _StaticMethodInvocation
std::unique_ptr<t_btree_4__0_3_1_2__1__9> rel_3_StaticMethodInvocation = std::make_unique<t_btree_4__0_3_1_2__1__9>();
souffle::RelationWrapper<2,t_btree_4__0_3_1_2__1__9,Tuple<RamDomain,4>,4,true,false> wrapper_rel_3_StaticMethodInvocation;
// -- Table: _VirtualMethodInvocation
std::unique_ptr<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17> rel_4_VirtualMethodInvocation = std::make_unique<t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17>();
souffle::RelationWrapper<3,t_btree_5__3_0_1_2_4__0_4_1_2_3__1__8__17,Tuple<RamDomain,5>,5,true,false> wrapper_rel_4_VirtualMethodInvocation;
// -- Table: AssignReturnValue_WithInvoke
std::unique_ptr<t_btree_4__0_1_2_3__2_0_1_3__1__4> rel_5_AssignReturnValue_WithInvoke = std::make_unique<t_btree_4__0_1_2_3__2_0_1_3__1__4>();
souffle::RelationWrapper<4,t_btree_4__0_1_2_3__2_0_1_3__1__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_5_AssignReturnValue_WithInvoke;
// -- Table: IterNextInsn
std::unique_ptr<t_btree_3__0_2_1__5> rel_6_IterNextInsn = std::make_unique<t_btree_3__0_2_1__5>();
souffle::RelationWrapper<5,t_btree_3__0_2_1__5,Tuple<RamDomain,3>,3,false,true> wrapper_rel_6_IterNextInsn;
// -- Table: Primitive
std::unique_ptr<t_btree_1__0__1> rel_7_Primitive = std::make_unique<t_btree_1__0__1>();
// -- Table: _Var_Type
std::unique_ptr<t_btree_2__0_1> rel_8_Var_Type = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<6,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_8_Var_Type;
// -- Table: RefTypeVar
std::unique_ptr<t_btree_1__0> rel_9_RefTypeVar = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<7,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_9_RefTypeVar;
// -- Table: _AssignCast
std::unique_ptr<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36> rel_10_AssignCast = std::make_unique<t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36>();
souffle::RelationWrapper<8,t_btree_6__3_0_1_2_4_5__2_5_0_1_3_4__4__8__36,Tuple<RamDomain,6>,6,true,false> wrapper_rel_10_AssignCast;
// -- Table: _AssignCastNull
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_11_AssignCastNull = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<9,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_11_AssignCastNull;
// -- Table: _AssignHeapAllocation
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_12_AssignHeapAllocation = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<10,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_12_AssignHeapAllocation;
// -- Table: _AssignLocal
std::unique_ptr<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21> rel_13_AssignLocal = std::make_unique<t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21>();
souffle::RelationWrapper<11,t_btree_5__3_0_1_2_4__2_4_0_1_3__0_2_4_1_3__1__4__8__20__21,Tuple<RamDomain,5>,5,true,false> wrapper_rel_13_AssignLocal;
// -- Table: DefineVar
std::unique_ptr<t_btree_3__0_1_2__7> rel_14_DefineVar = std::make_unique<t_btree_3__0_1_2__7>();
souffle::RelationWrapper<12,t_btree_3__0_1_2__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_14_DefineVar;
// -- Table: _AssignNull
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_15_AssignNull = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<13,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_15_AssignNull;
// -- Table: _LoadInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_16_LoadInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<14,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_16_LoadInstanceField;
// -- Table: _LoadStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_17_LoadStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<15,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_17_LoadStaticField;
// -- Table: VarDef
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_18_VarDef = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<16,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,false,true> wrapper_rel_18_VarDef;
// -- Table: _ActualParam
std::unique_ptr<t_btree_3__0_1_2__2_1_0__3__4__6> rel_19_ActualParam = std::make_unique<t_btree_3__0_1_2__2_1_0__3__4__6>();
souffle::RelationWrapper<17,t_btree_3__0_1_2__2_1_0__3__4__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_19_ActualParam;
// -- Table: _Return
std::unique_ptr<t_btree_4__2_0_3_1__4__13> rel_20_Return = std::make_unique<t_btree_4__2_0_3_1__4__13>();
souffle::RelationWrapper<18,t_btree_4__2_0_3_1__4__13,Tuple<RamDomain,4>,4,true,false> wrapper_rel_20_Return;
// -- Table: _StoreArrayIndex
std::unique_ptr<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8> rel_21_StoreArrayIndex = std::make_unique<t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8>();
souffle::RelationWrapper<19,t_btree_5__2_0_1_3_4__3_0_1_2_4__4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_21_StoreArrayIndex;
// -- Table: _StoreInstanceField
std::unique_ptr<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8> rel_22_StoreInstanceField = std::make_unique<t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8>();
souffle::RelationWrapper<20,t_btree_6__2_0_1_3_4_5__3_0_1_2_4_5__4__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_22_StoreInstanceField;
// -- Table: _StoreStaticField
std::unique_ptr<t_btree_5__2_0_1_3_4__4> rel_23_StoreStaticField = std::make_unique<t_btree_5__2_0_1_3_4__4>();
souffle::RelationWrapper<21,t_btree_5__2_0_1_3_4__4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_23_StoreStaticField;
// -- Table: AllUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_24_AllUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<22,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_24_AllUse;
// -- Table: FirstUse
std::unique_ptr<t_btree_4__2_3_0_1__12> rel_25_FirstUse = std::make_unique<t_btree_4__2_3_0_1__12>();
souffle::RelationWrapper<23,t_btree_4__2_3_0_1__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_25_FirstUse;
// -- Table: LastUse
std::unique_ptr<t_btree_4__0_2_3_1__13> rel_26_LastUse = std::make_unique<t_btree_4__0_2_3_1__13>();
souffle::RelationWrapper<24,t_btree_4__0_2_3_1__13,Tuple<RamDomain,4>,4,false,true> wrapper_rel_26_LastUse;
// -- Table: ApplicationMethod
std::unique_ptr<t_btree_1__0__1> rel_27_ApplicationMethod = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<25,t_btree_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_27_ApplicationMethod;
// -- Table: AssignMayNull
std::unique_ptr<t_btree_3__0_1_2> rel_28_AssignMayNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<26,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_28_AssignMayNull;
// -- Table: BasicBlockHead
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_29_BasicBlockHead = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<27,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_29_BasicBlockHead;
// -- Table: CallGraphEdge
std::unique_ptr<t_btree_4__1_0_2_3__3_0_1_2__2__8> rel_30_CallGraphEdge = std::make_unique<t_btree_4__1_0_2_3__3_0_1_2__2__8>();
souffle::RelationWrapper<28,t_btree_4__1_0_2_3__3_0_1_2__2__8,Tuple<RamDomain,4>,4,true,false> wrapper_rel_30_CallGraphEdge;
// -- Table: Instruction_Next
std::unique_ptr<t_btree_2__0_1__1> rel_31_Instruction_Next = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<29,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_31_Instruction_Next;
// -- Table: _IfNull
std::unique_ptr<t_btree_3__0_2_1__1__5> rel_32_IfNull = std::make_unique<t_btree_3__0_2_1__1__5>();
souffle::RelationWrapper<30,t_btree_3__0_2_1__1__5,Tuple<RamDomain,3>,3,true,false> wrapper_rel_32_IfNull;
// -- Table: MayNull_IfInstruction
std::unique_ptr<t_btree_1__0> rel_33_MayNull_IfInstruction = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<31,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_33_MayNull_IfInstruction;
// -- Table: FalseBranch
std::unique_ptr<t_btree_2__0_1__1> rel_34_FalseBranch = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<32,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_34_FalseBranch;
// -- Table: _OperatorAt
std::unique_ptr<t_btree_2__0_1__1> rel_35_OperatorAt = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<33,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_35_OperatorAt;
// -- Table: MayNull_IfInstructionsCond
std::unique_ptr<t_btree_4__0_2_1_3__2_3_0_1__5__12> rel_36_MayNull_IfInstructionsCond = std::make_unique<t_btree_4__0_2_1_3__2_3_0_1__5__12>();
souffle::RelationWrapper<34,t_btree_4__0_2_1_3__2_3_0_1__5__12,Tuple<RamDomain,4>,4,false,true> wrapper_rel_36_MayNull_IfInstructionsCond;
// -- Table: PhiNodeHead
std::unique_ptr<t_btree_2__0_1> rel_37_PhiNodeHead = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<35,t_btree_2__0_1,Tuple<RamDomain,2>,2,true,true> wrapper_rel_37_PhiNodeHead;
// -- Table: PhiNodeHeadVar
std::unique_ptr<t_btree_2__1_0__2> rel_38_PhiNodeHeadVar = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<36,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,false,true> wrapper_rel_38_PhiNodeHeadVar;
// -- Table: CanBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_39_CanBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<37,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_39_CanBeNullBranch;
// -- Table: @delta_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_40_delta_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CanBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_41_new_CanBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1__3> rel_42_CannotBeNullBranch = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<38,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_42_CannotBeNullBranch;
// -- Table: @delta_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_43_delta_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_CannotBeNullBranch
std::unique_ptr<t_btree_2__0_1> rel_44_new_CannotBeNullBranch = std::make_unique<t_btree_2__0_1>();
// -- Table: Dominates
std::unique_ptr<t_btree_2__0_1__1__3> rel_45_Dominates = std::make_unique<t_btree_2__0_1__1__3>();
souffle::RelationWrapper<39,t_btree_2__0_1__1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_45_Dominates;
// -- Table: _FormalParam
std::unique_ptr<t_btree_3__1_2_0__6> rel_46_FormalParam = std::make_unique<t_btree_3__1_2_0__6>();
souffle::RelationWrapper<40,t_btree_3__1_2_0__6,Tuple<RamDomain,3>,3,true,false> wrapper_rel_46_FormalParam;
// -- Table: Instruction_FormalParam
std::unique_ptr<t_btree_4__1_3_0_2__10> rel_47_Instruction_FormalParam = std::make_unique<t_btree_4__1_3_0_2__10>();
souffle::RelationWrapper<41,t_btree_4__1_3_0_2__10,Tuple<RamDomain,4>,4,false,true> wrapper_rel_47_Instruction_FormalParam;
// -- Table: isParam
std::unique_ptr<t_btree_3__1_2_0__2__6__7> rel_48_isParam = std::make_unique<t_btree_3__1_2_0__2__6__7>();
souffle::RelationWrapper<42,t_btree_3__1_2_0__2__6__7,Tuple<RamDomain,3>,3,false,true> wrapper_rel_48_isParam;
// -- Table: @delta_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_49_delta_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: @new_isParam
std::unique_ptr<t_btree_3__0_1_2> rel_50_new_isParam = std::make_unique<t_btree_3__0_1_2>();
// -- Table: _ThisVar
std::unique_ptr<t_btree_2__0_1__3> rel_51_ThisVar = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<43,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_51_ThisVar;
// -- Table: _Var_DeclaringMethod
std::unique_ptr<t_btree_2__0_1__1> rel_52_Var_DeclaringMethod = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<44,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_52_Var_DeclaringMethod;
// -- Table: Instruction_VarDeclaringMethod
std::unique_ptr<t_btree_3__0_1_2> rel_53_Instruction_VarDeclaringMethod = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<45,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_53_Instruction_VarDeclaringMethod;
// -- Table: Method_FirstInstruction
std::unique_ptr<t_btree_2__0_1__1> rel_54_Method_FirstInstruction = std::make_unique<t_btree_2__0_1__1>();
souffle::RelationWrapper<46,t_btree_2__0_1__1,Tuple<RamDomain,2>,2,true,false> wrapper_rel_54_Method_FirstInstruction;
// -- Table: MayPredecessorModuloThrow
std::unique_ptr<t_btree_2__0_1__1_0__1__2> rel_55_MayPredecessorModuloThrow = std::make_unique<t_btree_2__0_1__1_0__1__2>();
souffle::RelationWrapper<47,t_btree_2__0_1__1_0__1__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_55_MayPredecessorModuloThrow;
// -- Table: SpecialIfEdge
std::unique_ptr<t_btree_3__0_2_1__5> rel_56_SpecialIfEdge = std::make_unique<t_btree_3__0_2_1__5>();
// -- Table: InstructionLine
std::unique_ptr<t_btree_4__0_1_2_3__3> rel_57_InstructionLine = std::make_unique<t_btree_4__0_1_2_3__3>();
souffle::RelationWrapper<48,t_btree_4__0_1_2_3__3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_57_InstructionLine;
// -- Table: VarPointsTo
std::unique_ptr<t_btree_4__1_0_2_3__3_0_1_2__2__8> rel_58_VarPointsTo = std::make_unique<t_btree_4__1_0_2_3__3_0_1_2__2__8>();
souffle::RelationWrapper<49,t_btree_4__1_0_2_3__3_0_1_2__2__8,Tuple<RamDomain,4>,4,true,false> wrapper_rel_58_VarPointsTo;
// -- Table: VarMayPointToNull
std::unique_ptr<t_btree_1__0> rel_59_VarMayPointToNull = std::make_unique<t_btree_1__0>();
// -- Table: VarMayNotPointToNull
std::unique_ptr<t_btree_1__0__1> rel_60_VarMayNotPointToNull = std::make_unique<t_btree_1__0__1>();
// -- Table: VarPointsToNull
std::unique_ptr<t_btree_1__0__1> rel_61_VarPointsToNull = std::make_unique<t_btree_1__0__1>();
souffle::RelationWrapper<50,t_btree_1__0__1,Tuple<RamDomain,1>,1,false,true> wrapper_rel_61_VarPointsToNull;
// -- Table: _AssignBinop
std::unique_ptr<t_btree_4__0_1_2_3> rel_62_AssignBinop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<51,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_62_AssignBinop;
// -- Table: _AssignOperFrom
std::unique_ptr<t_btree_2__0_1__3> rel_63_AssignOperFrom = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<52,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_63_AssignOperFrom;
// -- Table: _AssignUnop
std::unique_ptr<t_btree_4__0_1_2_3> rel_64_AssignUnop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<53,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_64_AssignUnop;
// -- Table: _EnterMonitor
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_65_EnterMonitor = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<54,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_65_EnterMonitor;
// -- Table: _LoadArrayIndex
std::unique_ptr<t_btree_5__3_0_1_2_4__8> rel_66_LoadArrayIndex = std::make_unique<t_btree_5__3_0_1_2_4__8>();
souffle::RelationWrapper<55,t_btree_5__3_0_1_2_4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_66_LoadArrayIndex;
// -- Table: _ThrowNull
std::unique_ptr<t_btree_3__0_1_2> rel_67_ThrowNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<56,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,true,false> wrapper_rel_67_ThrowNull;
// -- Table: NullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_68_NullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<57,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_68_NullAt;
// -- Table: ReachableNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_69_ReachableNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<58,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_69_ReachableNullAtLine;
// -- Table: PathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_2_5_1_3_4_6__37> rel_70_PathSensitiveNullAtLine = std::make_unique<t_btree_7__0_2_5_1_3_4_6__37>();
souffle::RelationWrapper<59,t_btree_7__0_2_5_1_3_4_6__37,Tuple<RamDomain,7>,7,false,true> wrapper_rel_70_PathSensitiveNullAtLine;
// -- Table: MinPathSensitiveNullAtLine
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_71_MinPathSensitiveNullAtLine = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
// -- Table: _IfVar
std::unique_ptr<t_btree_3__2_0_1__4> rel_72_IfVar = std::make_unique<t_btree_3__2_0_1__4>();
souffle::RelationWrapper<60,t_btree_3__2_0_1__4,Tuple<RamDomain,3>,3,true,false> wrapper_rel_72_IfVar;
// -- Table: IfInstructions
std::unique_ptr<t_btree_2__0_1> rel_73_IfInstructions = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<61,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_73_IfInstructions;
// -- Table: IfInstructionsCond
std::unique_ptr<t_btree_4__3_0_1_2__8> rel_74_IfInstructionsCond = std::make_unique<t_btree_4__3_0_1_2__8>();
souffle::RelationWrapper<62,t_btree_4__3_0_1_2__8,Tuple<RamDomain,4>,4,false,true> wrapper_rel_74_IfInstructionsCond;
// -- Table: TrueIfInstructions
std::unique_ptr<t_btree_1__0> rel_75_TrueIfInstructions = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<63,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_75_TrueIfInstructions;
// -- Table: DominatesUnreachable
std::unique_ptr<t_btree_2__0_1> rel_76_DominatesUnreachable = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<64,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_76_DominatesUnreachable;
// -- Table: UnreachablePathNPEIns
std::unique_ptr<t_btree_1__0> rel_77_UnreachablePathNPEIns = std::make_unique<t_btree_1__0>();
souffle::RelationWrapper<65,t_btree_1__0,Tuple<RamDomain,1>,1,false,true> wrapper_rel_77_UnreachablePathNPEIns;
// -- Table: hasNextIf
std::unique_ptr<t_btree_3__0_1_2> rel_78_hasNextIf = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<66,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,false,true> wrapper_rel_78_hasNextIf;
// -- Table: NextInsideHasNext
std::unique_ptr<t_btree_2__0_1__3> rel_79_NextInsideHasNext = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<67,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_79_NextInsideHasNext;
// -- Table: MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3__3__7__15> rel_80_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3__3__7__15>();
souffle::RelationWrapper<68,t_btree_4__0_1_2_3__3__7__15,Tuple<RamDomain,4>,4,false,true> wrapper_rel_80_MayNullPtr;
// -- Table: @delta_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_81_delta_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: @new_MayNullPtr
std::unique_ptr<t_btree_4__0_1_2_3> rel_82_new_MayNullPtr = std::make_unique<t_btree_4__0_1_2_3>();
// -- Table: MethodDerefArg
std::unique_ptr<t_btree_2__0_1__3> rel_83_MethodDerefArg = std::make_unique<t_btree_2__0_1__3>();
souffle::RelationWrapper<69,t_btree_2__0_1__3,Tuple<RamDomain,2>,2,false,true> wrapper_rel_83_MethodDerefArg;
// -- Table: @delta_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_84_delta_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: @new_MethodDerefArg
std::unique_ptr<t_btree_2__0_1> rel_85_new_MethodDerefArg = std::make_unique<t_btree_2__0_1>();
// -- Table: JDKFunctionSummary
std::unique_ptr<t_btree_2__0_1> rel_86_JDKFunctionSummary = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<70,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_86_JDKFunctionSummary;
// -- Table: NPEWithMayNull
std::unique_ptr<t_btree_7__0_1_2_3_4_5_6> rel_87_NPEWithMayNull = std::make_unique<t_btree_7__0_1_2_3_4_5_6>();
souffle::RelationWrapper<71,t_btree_7__0_1_2_3_4_5_6,Tuple<RamDomain,7>,7,false,true> wrapper_rel_87_NPEWithMayNull;
// -- Table: JumpTarget
std::unique_ptr<t_btree_2__1_0__2> rel_88_JumpTarget = std::make_unique<t_btree_2__1_0__2>();
souffle::RelationWrapper<72,t_btree_2__1_0__2,Tuple<RamDomain,2>,2,true,false> wrapper_rel_88_JumpTarget;
// -- Table: TrueBranch
std::unique_ptr<t_btree_2__0_1> rel_89_TrueBranch = std::make_unique<t_btree_2__0_1>();
souffle::RelationWrapper<73,t_btree_2__0_1,Tuple<RamDomain,2>,2,false,true> wrapper_rel_89_TrueBranch;
// -- Table: ReachableNullAt
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_90_ReachableNullAt = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<74,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_90_ReachableNullAt;
public:
Sf_main_no_uninit_must(std::string pf="profile.log") : profiling_fname(pf),

wrapper_rel_1_AssignReturnValue(*rel_1_AssignReturnValue,symTable,"_AssignReturnValue",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"invocation","to"}}),

wrapper_rel_2_SpecialMethodInvocation(*rel_2_SpecialMethodInvocation,symTable,"_SpecialMethodInvocation",std::array<const char *,5>{{"s:symbol","i:Index","s:Method","s:symbol","s:Method"}},std::array<const char *,5>{{"?instruction","i","sig","?base","m"}}),

wrapper_rel_3_StaticMethodInvocation(*rel_3_StaticMethodInvocation,symTable,"_StaticMethodInvocation",std::array<const char *,4>{{"s:Instruction","i:number","s:Method","s:Method"}},std::array<const char *,4>{{"instruction","index","signature","method"}}),

wrapper_rel_4_VirtualMethodInvocation(*rel_4_VirtualMethodInvocation,symTable,"_VirtualMethodInvocation",std::array<const char *,5>{{"s:Instruction","i:Index","s:Method","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","sig","base","m"}}),

wrapper_rel_5_AssignReturnValue_WithInvoke(*rel_5_AssignReturnValue_WithInvoke,symTable,"AssignReturnValue_WithInvoke",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_6_IterNextInsn(*rel_6_IterNextInsn,symTable,"IterNextInsn",std::array<const char *,3>{{"s:Instruction","s:Var","s:Var"}},std::array<const char *,3>{{"insn","returnVar","var"}}),

wrapper_rel_8_Var_Type(*rel_8_Var_Type,symTable,"_Var_Type",std::array<const char *,2>{{"s:Var","s:Type"}},std::array<const char *,2>{{"var","type"}}),

wrapper_rel_9_RefTypeVar(*rel_9_RefTypeVar,symTable,"RefTypeVar",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"var"}}),

wrapper_rel_10_AssignCast(*rel_10_AssignCast,symTable,"_AssignCast",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Type","s:Method"}},std::array<const char *,6>{{"ins","i","from","to","t","m"}}),

wrapper_rel_11_AssignCastNull(*rel_11_AssignCastNull,symTable,"_AssignCastNull",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Type","s:Method"}},std::array<const char *,5>{{"ins","i","to","t","m"}}),

wrapper_rel_12_AssignHeapAllocation(*rel_12_AssignHeapAllocation,symTable,"_AssignHeapAllocation",std::array<const char *,6>{{"s:Instruction","i:number","s:symbol","s:Var","s:Method","i:number"}},std::array<const char *,6>{{"?instruction","?index","?heap","?to","?inmethod","?linenumber"}}),

wrapper_rel_13_AssignLocal(*rel_13_AssignLocal,symTable,"_AssignLocal",std::array<const char *,5>{{"s:Instruction","i:number","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"?instruction","?index","?from","?to","?inmethod"}}),

wrapper_rel_14_DefineVar(*rel_14_DefineVar,symTable,"DefineVar",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_15_AssignNull(*rel_15_AssignNull,symTable,"_AssignNull",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_16_LoadInstanceField(*rel_16_LoadInstanceField,symTable,"_LoadInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","to","base","sig","m"}}),

wrapper_rel_17_LoadStaticField(*rel_17_LoadStaticField,symTable,"_LoadStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","to","sig","m"}}),

wrapper_rel_18_VarDef(*rel_18_VarDef,symTable,"VarDef",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_19_ActualParam(*rel_19_ActualParam,symTable,"_ActualParam",std::array<const char *,3>{{"i:number","s:Instruction","s:Var"}},std::array<const char *,3>{{"index","invocation","var"}}),

wrapper_rel_20_Return(*rel_20_Return,symTable,"_Return",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"instruction","index","var","method"}}),

wrapper_rel_21_StoreArrayIndex(*rel_21_StoreArrayIndex,symTable,"_StoreArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","from","base","m"}}),

wrapper_rel_22_StoreInstanceField(*rel_22_StoreInstanceField,symTable,"_StoreInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method","s:Method"}},std::array<const char *,6>{{"ins","i","from","base","sig","m"}}),

wrapper_rel_23_StoreStaticField(*rel_23_StoreStaticField,symTable,"_StoreStaticField",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Method","s:Method"}},std::array<const char *,5>{{"ins","i","from","sig","m"}}),

wrapper_rel_24_AllUse(*rel_24_AllUse,symTable,"AllUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_25_FirstUse(*rel_25_FirstUse,symTable,"FirstUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_26_LastUse(*rel_26_LastUse,symTable,"LastUse",std::array<const char *,4>{{"s:Instruction","i:number","s:Var","s:Method"}},std::array<const char *,4>{{"insn","index","var","method"}}),

wrapper_rel_27_ApplicationMethod(*rel_27_ApplicationMethod,symTable,"ApplicationMethod",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_28_AssignMayNull(*rel_28_AssignMayNull,symTable,"AssignMayNull",std::array<const char *,3>{{"s:Instruction","s:Var","s:Method"}},std::array<const char *,3>{{"insn","var","method"}}),

wrapper_rel_29_BasicBlockHead(*rel_29_BasicBlockHead,symTable,"BasicBlockHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"insn","headInsn"}}),

wrapper_rel_30_CallGraphEdge(*rel_30_CallGraphEdge,symTable,"CallGraphEdge",std::array<const char *,4>{{"s:Context","s:Instruction","s:Context","s:Method"}},std::array<const char *,4>{{"ctx","ins","hctx","sig"}}),

wrapper_rel_31_Instruction_Next(*rel_31_Instruction_Next,symTable,"Instruction_Next",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?i","?next"}}),

wrapper_rel_32_IfNull(*rel_32_IfNull,symTable,"_IfNull",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_33_MayNull_IfInstruction(*rel_33_MayNull_IfInstruction,symTable,"MayNull_IfInstruction",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_34_FalseBranch(*rel_34_FalseBranch,symTable,"FalseBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_35_OperatorAt(*rel_35_OperatorAt,symTable,"_OperatorAt",std::array<const char *,2>{{"s:Instruction","s:symbol"}},std::array<const char *,2>{{"i","operator"}}),

wrapper_rel_36_MayNull_IfInstructionsCond(*rel_36_MayNull_IfInstructionsCond,symTable,"MayNull_IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:Var","s:Var","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_37_PhiNodeHead(*rel_37_PhiNodeHead,symTable,"PhiNodeHead",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?insn","?headInsn"}}),

wrapper_rel_38_PhiNodeHeadVar(*rel_38_PhiNodeHeadVar,symTable,"PhiNodeHeadVar",std::array<const char *,2>{{"s:Var","s:Var"}},std::array<const char *,2>{{"var","headVar"}}),

wrapper_rel_39_CanBeNullBranch(*rel_39_CanBeNullBranch,symTable,"CanBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_42_CannotBeNullBranch(*rel_42_CannotBeNullBranch,symTable,"CannotBeNullBranch",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_45_Dominates(*rel_45_Dominates,symTable,"Dominates",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?dominator","?insn"}}),

wrapper_rel_46_FormalParam(*rel_46_FormalParam,symTable,"_FormalParam",std::array<const char *,3>{{"i:number","s:Method","s:Var"}},std::array<const char *,3>{{"index","method","var"}}),

wrapper_rel_47_Instruction_FormalParam(*rel_47_Instruction_FormalParam,symTable,"Instruction_FormalParam",std::array<const char *,4>{{"s:Instruction","s:symbol","s:Var","i:number"}},std::array<const char *,4>{{"insn","method","var","index"}}),

wrapper_rel_48_isParam(*rel_48_isParam,symTable,"isParam",std::array<const char *,3>{{"i:number","s:Var","s:Method"}},std::array<const char *,3>{{"index","var","method"}}),

wrapper_rel_51_ThisVar(*rel_51_ThisVar,symTable,"_ThisVar",std::array<const char *,2>{{"s:Method","s:Var"}},std::array<const char *,2>{{"method","var"}}),

wrapper_rel_52_Var_DeclaringMethod(*rel_52_Var_DeclaringMethod,symTable,"_Var_DeclaringMethod",std::array<const char *,2>{{"s:Var","s:Method"}},std::array<const char *,2>{{"?var","?method"}}),

wrapper_rel_53_Instruction_VarDeclaringMethod(*rel_53_Instruction_VarDeclaringMethod,symTable,"Instruction_VarDeclaringMethod",std::array<const char *,3>{{"s:Instruction","s:symbol","s:Var"}},std::array<const char *,3>{{"insn","method","var"}}),

wrapper_rel_54_Method_FirstInstruction(*rel_54_Method_FirstInstruction,symTable,"Method_FirstInstruction",std::array<const char *,2>{{"s:Method","s:Instruction"}},std::array<const char *,2>{{"?method","?i"}}),

wrapper_rel_55_MayPredecessorModuloThrow(*rel_55_MayPredecessorModuloThrow,symTable,"MayPredecessorModuloThrow",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"?prev","?next"}}),

wrapper_rel_57_InstructionLine(*rel_57_InstructionLine,symTable,"InstructionLine",std::array<const char *,4>{{"s:Method","i:Index","i:LineNumber","s:File"}},std::array<const char *,4>{{"m","i","l","f"}}),

wrapper_rel_58_VarPointsTo(*rel_58_VarPointsTo,symTable,"VarPointsTo",std::array<const char *,4>{{"s:HContext","s:Alloc","s:Context","s:Var"}},std::array<const char *,4>{{"hctx","a","ctx","v"}}),

wrapper_rel_61_VarPointsToNull(*rel_61_VarPointsToNull,symTable,"VarPointsToNull",std::array<const char *,1>{{"s:Var"}},std::array<const char *,1>{{"v"}}),

wrapper_rel_62_AssignBinop(*rel_62_AssignBinop,symTable,"_AssignBinop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_63_AssignOperFrom(*rel_63_AssignOperFrom,symTable,"_AssignOperFrom",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"ins","from"}}),

wrapper_rel_64_AssignUnop(*rel_64_AssignUnop,symTable,"_AssignUnop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_65_EnterMonitor(*rel_65_EnterMonitor,symTable,"_EnterMonitor",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_66_LoadArrayIndex(*rel_66_LoadArrayIndex,symTable,"_LoadArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","to","base","m"}}),

wrapper_rel_67_ThrowNull(*rel_67_ThrowNull,symTable,"_ThrowNull",std::array<const char *,3>{{"s:Instruction","i:Index","s:Method"}},std::array<const char *,3>{{"ins","i","m"}}),

wrapper_rel_68_NullAt(*rel_68_NullAt,symTable,"NullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}),

wrapper_rel_69_ReachableNullAtLine(*rel_69_ReachableNullAtLine,symTable,"ReachableNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_70_PathSensitiveNullAtLine(*rel_70_PathSensitiveNullAtLine,symTable,"PathSensitiveNullAtLine",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_72_IfVar(*rel_72_IfVar,symTable,"_IfVar",std::array<const char *,3>{{"s:Instruction","i:number","s:Var"}},std::array<const char *,3>{{"i","pos","var"}}),

wrapper_rel_73_IfInstructions(*rel_73_IfInstructions,symTable,"IfInstructions",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ins","ifIns"}}),

wrapper_rel_74_IfInstructionsCond(*rel_74_IfInstructionsCond,symTable,"IfInstructionsCond",std::array<const char *,4>{{"s:Instruction","s:symbol","s:symbol","s:symbol"}},std::array<const char *,4>{{"ifIns","left","right","opt"}}),

wrapper_rel_75_TrueIfInstructions(*rel_75_TrueIfInstructions,symTable,"TrueIfInstructions",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_76_DominatesUnreachable(*rel_76_DominatesUnreachable,symTable,"DominatesUnreachable",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","ins"}}),

wrapper_rel_77_UnreachablePathNPEIns(*rel_77_UnreachablePathNPEIns,symTable,"UnreachablePathNPEIns",std::array<const char *,1>{{"s:Instruction"}},std::array<const char *,1>{{"ifIns"}}),

wrapper_rel_78_hasNextIf(*rel_78_hasNextIf,symTable,"hasNextIf",std::array<const char *,3>{{"s:Instruction","s:Instruction","s:Var"}},std::array<const char *,3>{{"ifInsn","invokeInsn","var"}}),

wrapper_rel_79_NextInsideHasNext(*rel_79_NextInsideHasNext,symTable,"NextInsideHasNext",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"insn","var"}}),

wrapper_rel_80_MayNullPtr(*rel_80_MayNullPtr,symTable,"MayNullPtr",std::array<const char *,4>{{"s:Instruction","s:Var","s:Method","s:symbol"}},std::array<const char *,4>{{"insn","var","method","reason"}}),

wrapper_rel_83_MethodDerefArg(*rel_83_MethodDerefArg,symTable,"MethodDerefArg",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_86_JDKFunctionSummary(*rel_86_JDKFunctionSummary,symTable,"JDKFunctionSummary",std::array<const char *,2>{{"i:number","s:Method"}},std::array<const char *,2>{{"index","method"}}),

wrapper_rel_87_NPEWithMayNull(*rel_87_NPEWithMayNull,symTable,"NPEWithMayNull",std::array<const char *,7>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,7>{{"m","i","f","l","type","v","insn"}}),

wrapper_rel_88_JumpTarget(*rel_88_JumpTarget,symTable,"JumpTarget",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"to","from"}}),

wrapper_rel_89_TrueBranch(*rel_89_TrueBranch,symTable,"TrueBranch",std::array<const char *,2>{{"s:Instruction","s:Instruction"}},std::array<const char *,2>{{"ifIns","insn"}}),

wrapper_rel_90_ReachableNullAt(*rel_90_ReachableNullAt,symTable,"ReachableNullAt",std::array<const char *,5>{{"s:Method","i:Index","s:NPEType","s:Var","s:Instruction"}},std::array<const char *,5>{{"m","i","type","v","insn"}}){
ProfileEventSingleton::instance().setOutputFile(profiling_fname);
addRelation("_AssignReturnValue",&wrapper_rel_1_AssignReturnValue,1,0);
addRelation("_SpecialMethodInvocation",&wrapper_rel_2_SpecialMethodInvocation,1,0);
addRelation("_StaticMethodInvocation",&wrapper_rel_3_StaticMethodInvocation,1,0);
addRelation("_VirtualMethodInvocation",&wrapper_rel_4_VirtualMethodInvocation,1,0);
addRelation("AssignReturnValue_WithInvoke",&wrapper_rel_5_AssignReturnValue_WithInvoke,0,1);
addRelation("IterNextInsn",&wrapper_rel_6_IterNextInsn,0,1);
addRelation("_Var_Type",&wrapper_rel_8_Var_Type,1,0);
addRelation("RefTypeVar",&wrapper_rel_9_RefTypeVar,0,1);
addRelation("_AssignCast",&wrapper_rel_10_AssignCast,1,0);
addRelation("_AssignCastNull",&wrapper_rel_11_AssignCastNull,1,0);
addRelation("_AssignHeapAllocation",&wrapper_rel_12_AssignHeapAllocation,1,0);
addRelation("_AssignLocal",&wrapper_rel_13_AssignLocal,1,0);
addRelation("DefineVar",&wrapper_rel_14_DefineVar,0,1);
addRelation("_AssignNull",&wrapper_rel_15_AssignNull,1,0);
addRelation("_LoadInstanceField",&wrapper_rel_16_LoadInstanceField,1,0);
addRelation("_LoadStaticField",&wrapper_rel_17_LoadStaticField,1,0);
addRelation("VarDef",&wrapper_rel_18_VarDef,0,1);
addRelation("_ActualParam",&wrapper_rel_19_ActualParam,1,0);
addRelation("_Return",&wrapper_rel_20_Return,1,0);
addRelation("_StoreArrayIndex",&wrapper_rel_21_StoreArrayIndex,1,0);
addRelation("_StoreInstanceField",&wrapper_rel_22_StoreInstanceField,1,0);
addRelation("_StoreStaticField",&wrapper_rel_23_StoreStaticField,1,0);
addRelation("AllUse",&wrapper_rel_24_AllUse,0,1);
addRelation("FirstUse",&wrapper_rel_25_FirstUse,0,1);
addRelation("LastUse",&wrapper_rel_26_LastUse,0,1);
addRelation("ApplicationMethod",&wrapper_rel_27_ApplicationMethod,1,0);
addRelation("AssignMayNull",&wrapper_rel_28_AssignMayNull,0,1);
addRelation("BasicBlockHead",&wrapper_rel_29_BasicBlockHead,1,0);
addRelation("CallGraphEdge",&wrapper_rel_30_CallGraphEdge,1,0);
addRelation("Instruction_Next",&wrapper_rel_31_Instruction_Next,1,1);
addRelation("_IfNull",&wrapper_rel_32_IfNull,1,0);
addRelation("MayNull_IfInstruction",&wrapper_rel_33_MayNull_IfInstruction,0,1);
addRelation("FalseBranch",&wrapper_rel_34_FalseBranch,0,1);
addRelation("_OperatorAt",&wrapper_rel_35_OperatorAt,1,0);
addRelation("MayNull_IfInstructionsCond",&wrapper_rel_36_MayNull_IfInstructionsCond,0,1);
addRelation("PhiNodeHead",&wrapper_rel_37_PhiNodeHead,1,1);
addRelation("PhiNodeHeadVar",&wrapper_rel_38_PhiNodeHeadVar,0,1);
addRelation("CanBeNullBranch",&wrapper_rel_39_CanBeNullBranch,0,1);
addRelation("CannotBeNullBranch",&wrapper_rel_42_CannotBeNullBranch,0,1);
addRelation("Dominates",&wrapper_rel_45_Dominates,1,0);
addRelation("_FormalParam",&wrapper_rel_46_FormalParam,1,0);
addRelation("Instruction_FormalParam",&wrapper_rel_47_Instruction_FormalParam,0,1);
addRelation("isParam",&wrapper_rel_48_isParam,0,1);
addRelation("_ThisVar",&wrapper_rel_51_ThisVar,1,0);
addRelation("_Var_DeclaringMethod",&wrapper_rel_52_Var_DeclaringMethod,1,0);
addRelation("Instruction_VarDeclaringMethod",&wrapper_rel_53_Instruction_VarDeclaringMethod,0,1);
addRelation("Method_FirstInstruction",&wrapper_rel_54_Method_FirstInstruction,1,0);
addRelation("MayPredecessorModuloThrow",&wrapper_rel_55_MayPredecessorModuloThrow,1,0);
addRelation("InstructionLine",&wrapper_rel_57_InstructionLine,1,0);
addRelation("VarPointsTo",&wrapper_rel_58_VarPointsTo,1,0);
addRelation("VarPointsToNull",&wrapper_rel_61_VarPointsToNull,0,1);
addRelation("_AssignBinop",&wrapper_rel_62_AssignBinop,1,0);
addRelation("_AssignOperFrom",&wrapper_rel_63_AssignOperFrom,1,0);
addRelation("_AssignUnop",&wrapper_rel_64_AssignUnop,1,0);
addRelation("_EnterMonitor",&wrapper_rel_65_EnterMonitor,1,0);
addRelation("_LoadArrayIndex",&wrapper_rel_66_LoadArrayIndex,1,0);
addRelation("_ThrowNull",&wrapper_rel_67_ThrowNull,1,0);
addRelation("NullAt",&wrapper_rel_68_NullAt,0,1);
addRelation("ReachableNullAtLine",&wrapper_rel_69_ReachableNullAtLine,0,1);
addRelation("PathSensitiveNullAtLine",&wrapper_rel_70_PathSensitiveNullAtLine,0,1);
addRelation("_IfVar",&wrapper_rel_72_IfVar,1,0);
addRelation("IfInstructions",&wrapper_rel_73_IfInstructions,0,1);
addRelation("IfInstructionsCond",&wrapper_rel_74_IfInstructionsCond,0,1);
addRelation("TrueIfInstructions",&wrapper_rel_75_TrueIfInstructions,0,1);
addRelation("DominatesUnreachable",&wrapper_rel_76_DominatesUnreachable,0,1);
addRelation("UnreachablePathNPEIns",&wrapper_rel_77_UnreachablePathNPEIns,0,1);
addRelation("hasNextIf",&wrapper_rel_78_hasNextIf,0,1);
addRelation("NextInsideHasNext",&wrapper_rel_79_NextInsideHasNext,0,1);
addRelation("MayNullPtr",&wrapper_rel_80_MayNullPtr,0,1);
addRelation("MethodDerefArg",&wrapper_rel_83_MethodDerefArg,0,1);
addRelation("JDKFunctionSummary",&wrapper_rel_86_JDKFunctionSummary,0,1);
addRelation("NPEWithMayNull",&wrapper_rel_87_NPEWithMayNull,0,1);
addRelation("JumpTarget",&wrapper_rel_88_JumpTarget,1,0);
addRelation("TrueBranch",&wrapper_rel_89_TrueBranch,0,1);
addRelation("ReachableNullAt",&wrapper_rel_90_ReachableNullAt,0,1);
}
~Sf_main_no_uninit_must() {
}
private:
void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1, bool performIO = false) {
SignalHandler::instance()->set();
std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(4);
#endif

// -- query evaluation --
ProfileEventSingleton::instance().startTimer();
ProfileEventSingleton::instance().makeTimeEvent("@time;starttime");
{
Logger logger("@runtime;", 0);
ProfileEventSingleton::instance().makeConfigRecord("relationCount", std::to_string(80));[](){
ProfileEventSingleton::instance().makeStratumRecord(0, "relation", "_AssignReturnValue", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(1, "relation", "_SpecialMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(2, "relation", "_StaticMethodInvocation", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(3, "relation", "_VirtualMethodInvocation", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(4, "relation", "AssignReturnValue_WithInvoke", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(5, "relation", "IterNextInsn", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(6, "relation", "Primitive", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(7, "relation", "_Var_Type", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(8, "relation", "RefTypeVar", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(9, "relation", "_AssignCast", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(10, "relation", "_AssignCastNull", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(11, "relation", "_AssignHeapAllocation", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(12, "relation", "_AssignLocal", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(13, "relation", "DefineVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(14, "relation", "_AssignNull", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(15, "relation", "_LoadInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(16, "relation", "_LoadStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(17, "relation", "VarDef", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(18, "relation", "_ActualParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(19, "relation", "_Return", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(20, "relation", "_StoreArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(21, "relation", "_StoreInstanceField", "arity", "6");
ProfileEventSingleton::instance().makeStratumRecord(22, "relation", "_StoreStaticField", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(23, "relation", "AllUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(24, "relation", "FirstUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(25, "relation", "LastUse", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(26, "relation", "ApplicationMethod", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(27, "relation", "AssignMayNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(28, "relation", "BasicBlockHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(29, "relation", "CallGraphEdge", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(30, "relation", "Instruction_Next", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(31, "relation", "_IfNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(32, "relation", "MayNull_IfInstruction", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(33, "relation", "FalseBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(34, "relation", "_OperatorAt", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(35, "relation", "MayNull_IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(36, "relation", "PhiNodeHead", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(37, "relation", "PhiNodeHeadVar", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(38, "relation", "CanBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(39, "relation", "CannotBeNullBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(40, "relation", "Dominates", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(41, "relation", "_FormalParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(42, "relation", "Instruction_FormalParam", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(43, "relation", "isParam", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(44, "relation", "_ThisVar", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(45, "relation", "_Var_DeclaringMethod", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(46, "relation", "Instruction_VarDeclaringMethod", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(47, "relation", "Method_FirstInstruction", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(48, "relation", "MayPredecessorModuloThrow", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(49, "relation", "SpecialIfEdge", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(50, "relation", "InstructionLine", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(51, "relation", "VarPointsTo", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(52, "relation", "VarMayPointToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(53, "relation", "VarMayNotPointToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(54, "relation", "VarPointsToNull", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(55, "relation", "_AssignBinop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(56, "relation", "_AssignOperFrom", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(57, "relation", "_AssignUnop", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(58, "relation", "_EnterMonitor", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(59, "relation", "_LoadArrayIndex", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(60, "relation", "_ThrowNull", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(61, "relation", "NullAt", "arity", "5");
ProfileEventSingleton::instance().makeStratumRecord(62, "relation", "ReachableNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(63, "relation", "PathSensitiveNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(64, "relation", "MinPathSensitiveNullAtLine", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(65, "relation", "_IfVar", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(66, "relation", "IfInstructions", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(67, "relation", "IfInstructionsCond", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(68, "relation", "TrueIfInstructions", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(69, "relation", "DominatesUnreachable", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(70, "relation", "UnreachablePathNPEIns", "arity", "1");
ProfileEventSingleton::instance().makeStratumRecord(71, "relation", "hasNextIf", "arity", "3");
ProfileEventSingleton::instance().makeStratumRecord(72, "relation", "NextInsideHasNext", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(73, "relation", "MayNullPtr", "arity", "4");
ProfileEventSingleton::instance().makeStratumRecord(74, "relation", "MethodDerefArg", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(75, "relation", "JDKFunctionSummary", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(76, "relation", "NPEWithMayNull", "arity", "7");
ProfileEventSingleton::instance().makeStratumRecord(77, "relation", "JumpTarget", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(78, "relation", "TrueBranch", "arity", "2");
ProfileEventSingleton::instance().makeStratumRecord(79, "relation", "ReachableNullAt", "arity", "5");
}();
/* BEGIN STRATUM 0 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignReturnValue;../declarations.dl [137:7-137:59];loadtime;)_",iter, [&](){return rel_1_AssignReturnValue->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignReturnValue;../declarations.dl [137:7-137:59];)",rel_1_AssignReturnValue->size(),iter);}();
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];loadtime;)_",iter, [&](){return rel_2_SpecialMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_SpecialMethodInvocation;../declarations.dl [56:7-56:100];)",rel_2_SpecialMethodInvocation->size(),iter);}();
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StaticMethodInvocation;../declarations.dl [140:7-140:105];loadtime;)_",iter, [&](){return rel_3_StaticMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StaticMethodInvocation;../declarations.dl [140:7-140:105];)",rel_3_StaticMethodInvocation->size(),iter);}();
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];loadtime;)_",iter, [&](){return rel_4_VirtualMethodInvocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_VirtualMethodInvocation;../declarations.dl [79:7-79:94];)",rel_4_VirtualMethodInvocation->size(),iter);}();
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AssignReturnValue_WithInvoke;may-null/rules.dl [14:7-14:102];)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[0]++;
}
freqs[1]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[2]++;
}
freqs[3]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AssignReturnValue_WithInvoke(insn,index,var,method) :- 
   _AssignReturnValue(insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file may-null/rules.dl [16:1-22:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AssignReturnValue_WithInvoke;may-null/rules.dl [16:1-22:3];AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[4])}});
rel_5_AssignReturnValue_WithInvoke->insert(tuple,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
freqs[4]++;
}
freqs[5]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AssignReturnValue_WithInvoke;may-null/rules.dl [14:7-14:102];savetime;)_",iter, [&](){return rel_5_AssignReturnValue_WithInvoke->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IterNextInsn;may-null/rules.dl [236:7-236:71];)_",iter, [&](){return rel_6_IterNextInsn->size();});
SignalHandler::instance()->setMsg(R"_(IterNextInsn(insn,returnVar,var) :- 
   _AssignReturnValue(insn,returnVar),
   _VirtualMethodInvocation(insn,_,sig,var,_),
   "next()" contains sig.
in file may-null/rules.dl [246:1-249:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IterNextInsn;may-null/rules.dl [246:1-249:25];IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;)_",iter, [&](){return rel_6_IterNextInsn->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_1_AssignReturnValue->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
if( (symTable.resolve(env1[2]).find(symTable.resolve(RamDomain(39))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env1[3])}});
rel_6_IterNextInsn->insert(tuple,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
}
freqs[6]++;
}
freqs[7]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IterNextInsn;may-null/rules.dl [236:7-236:71];savetime;)_",iter, [&](){return rel_6_IterNextInsn->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 5 */
/* BEGIN STRATUM 6 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Primitive;../declarations.dl [4:7-4:28];)_",iter, [&](){return rel_7_Primitive->size();});
SignalHandler::instance()->setMsg(R"_(Primitive("boolean").
in file ../declarations.dl [5:1-5:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [5:1-5:22];Primitive(\"boolean\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(0));
}
SignalHandler::instance()->setMsg(R"_(Primitive("short").
in file ../declarations.dl [6:1-6:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [6:1-6:20];Primitive(\"short\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(1));
}
SignalHandler::instance()->setMsg(R"_(Primitive("int").
in file ../declarations.dl [7:1-7:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [7:1-7:18];Primitive(\"int\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(2));
}
SignalHandler::instance()->setMsg(R"_(Primitive("long").
in file ../declarations.dl [8:1-8:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [8:1-8:19];Primitive(\"long\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(3));
}
SignalHandler::instance()->setMsg(R"_(Primitive("float").
in file ../declarations.dl [9:1-9:20])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [9:1-9:20];Primitive(\"float\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(4));
}
SignalHandler::instance()->setMsg(R"_(Primitive("double").
in file ../declarations.dl [10:1-10:21])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [10:1-10:21];Primitive(\"double\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(5));
}
SignalHandler::instance()->setMsg(R"_(Primitive("char").
in file ../declarations.dl [11:1-11:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [11:1-11:19];Primitive(\"char\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(6));
}
SignalHandler::instance()->setMsg(R"_(Primitive("byte").
in file ../declarations.dl [12:1-12:19])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Primitive;../declarations.dl [12:1-12:19];Primitive(\"byte\").;)_",iter, [&](){return rel_7_Primitive->size();});
rel_7_Primitive->insert(RamDomain(7));
}
}
}();
/* END STRATUM 6 */
/* BEGIN STRATUM 7 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_Type;../declarations.dl [107:7-107:38];loadtime;)_",iter, [&](){return rel_8_Var_Type->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_Type;../declarations.dl [107:7-107:38];)",rel_8_Var_Type->size(),iter);}();
/* END STRATUM 7 */
/* BEGIN STRATUM 8 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;RefTypeVar;may-null/rules.dl [2:7-2:34];)_",iter, [&](){return rel_9_RefTypeVar->size();});
SignalHandler::instance()->setMsg(R"_(RefTypeVar(var) :- 
   _Var_Type(var,type),
   !Primitive(type).
in file may-null/rules.dl [78:1-80:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;RefTypeVar;may-null/rules.dl [78:1-80:18];RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;)_",iter, [&](){return rel_9_RefTypeVar->size();});
if (!rel_8_Var_Type->empty()) [&](){
auto part = rel_8_Var_Type->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_7_Primitive_op_ctxt,rel_7_Primitive->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_8_Var_Type_op_ctxt,rel_8_Var_Type->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[0]++,!rel_7_Primitive->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_7_Primitive_op_ctxt)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_9_RefTypeVar->insert(tuple,READ_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt));
}
freqs[8]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;RefTypeVar;may-null/rules.dl [2:7-2:34];savetime;)_",iter, [&](){return rel_9_RefTypeVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_Primitive->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_Var_Type->purge();
}();
/* END STRATUM 8 */
/* BEGIN STRATUM 9 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCast;../declarations.dl [86:7-86:85];loadtime;)_",iter, [&](){return rel_10_AssignCast->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCast;../declarations.dl [86:7-86:85];)",rel_10_AssignCast->size(),iter);}();
/* END STRATUM 9 */
/* BEGIN STRATUM 10 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignCastNull;../declarations.dl [90:7-90:79];loadtime;)_",iter, [&](){return rel_11_AssignCastNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignCastNull;../declarations.dl [90:7-90:79];)",rel_11_AssignCastNull->size(),iter);}();
/* END STRATUM 10 */
/* BEGIN STRATUM 11 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignHeapAllocation;../declarations.dl [131:7-131:131];loadtime;)_",iter, [&](){return rel_12_AssignHeapAllocation->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignHeapAllocation;../declarations.dl [131:7-131:131];)",rel_12_AssignHeapAllocation->size(),iter);}();
/* END STRATUM 11 */
/* BEGIN STRATUM 12 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignLocal;../declarations.dl [134:7-134:98];loadtime;)_",iter, [&](){return rel_13_AssignLocal->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignLocal;../declarations.dl [134:7-134:98];)",rel_13_AssignLocal->size(),iter);}();
/* END STRATUM 12 */
/* BEGIN STRATUM 13 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DefineVar;may-null/rules.dl [5:7-5:68];)_",iter, [&](){return rel_14_DefineVar->size();});
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,var,method) :- 
   _AssignHeapAllocation(insn,_,_,var,method,_).
in file may-null/rules.dl [104:1-105:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;may-null/rules.dl [104:1-105:51];DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_12_AssignHeapAllocation->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[9]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignLocal(insn,_,_,to,method).
in file may-null/rules.dl [111:1-115:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;may-null/rules.dl [111:1-115:3];DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_13_AssignLocal->empty()) [&](){
auto part = rel_13_AssignLocal->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[10]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   _AssignCast(insn,_,_,to,_,method).
in file may-null/rules.dl [111:1-115:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;may-null/rules.dl [111:1-115:3];DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_10_AssignCast->empty()) [&](){
auto part = rel_10_AssignCast->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[5])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[11]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(DefineVar(insn,to,method) :- 
   AssignReturnValue_WithInvoke(insn,_,to,method).
in file may-null/rules.dl [118:1-119:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DefineVar;may-null/rules.dl [118:1-119:51];DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;)_",iter, [&](){return rel_14_DefineVar->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()) [&](){
auto part = rel_5_AssignReturnValue_WithInvoke->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_14_DefineVar->insert(tuple,READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt));
freqs[12]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DefineVar;may-null/rules.dl [5:7-5:68];savetime;)_",iter, [&](){return rel_14_DefineVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 13 */
/* BEGIN STRATUM 14 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignNull;../declarations.dl [93:7-93:66];loadtime;)_",iter, [&](){return rel_15_AssignNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignNull;../declarations.dl [93:7-93:66];)",rel_15_AssignNull->size(),iter);}();
/* END STRATUM 14 */
/* BEGIN STRATUM 15 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadInstanceField;../declarations.dl [66:7-66:97];loadtime;)_",iter, [&](){return rel_16_LoadInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadInstanceField;../declarations.dl [66:7-66:97];)",rel_16_LoadInstanceField->size(),iter);}();
/* END STRATUM 15 */
/* BEGIN STRATUM 16 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadStaticField;../declarations.dl [72:7-72:84];loadtime;)_",iter, [&](){return rel_17_LoadStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadStaticField;../declarations.dl [72:7-72:84];)",rel_17_LoadStaticField->size(),iter);}();
/* END STRATUM 16 */
/* BEGIN STRATUM 17 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarDef;may-null/rules.dl [13:7-13:80];)_",iter, [&](){return rel_18_VarDef->size();});
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignNull(insn,index,var,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_15_AssignNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_15_AssignNull->equalRange_4(key,READ_OP_CONTEXT(rel_15_AssignNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[13]++;
}
freqs[14]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,_,var,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_13_AssignLocal->equalRange_8(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[15]++;
}
freqs[16]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignHeapAllocation(insn,index,_,var,method,_).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_12_AssignHeapAllocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt,rel_12_AssignHeapAllocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_12_AssignHeapAllocation->equalRange_8(key,READ_OP_CONTEXT(rel_12_AssignHeapAllocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[17]++;
}
freqs[18]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadInstanceField(insn,index,var,_,_,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_16_LoadInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[19]++;
}
freqs[20]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _LoadStaticField(insn,index,var,_,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_17_LoadStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt,rel_17_LoadStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_17_LoadStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_17_LoadStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[21]++;
}
freqs[22]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCastNull(insn,index,var,_,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_11_AssignCastNull->equalRange_4(key,READ_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[23]++;
}
freqs[24]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,_,var,_,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_10_AssignCast->equalRange_8(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[25]++;
}
freqs[26]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarDef(insn,index,var,method) :- 
   RefTypeVar(var),
   AssignReturnValue_WithInvoke(insn,index,var,method).
in file may-null/rules.dl [24:1-35:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarDef;may-null/rules.dl [24:1-35:3];VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;)_",iter, [&](){return rel_18_VarDef->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_4(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_18_VarDef->insert(tuple,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
freqs[27]++;
}
freqs[28]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarDef;may-null/rules.dl [13:7-13:80];savetime;)_",iter, [&](){return rel_18_VarDef->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_17_LoadStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_12_AssignHeapAllocation->purge();
}();
/* END STRATUM 17 */
/* BEGIN STRATUM 18 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ActualParam;../declarations.dl [149:7-149:69];loadtime;)_",iter, [&](){return rel_19_ActualParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ActualParam;../declarations.dl [149:7-149:69];)",rel_19_ActualParam->size(),iter);}();
/* END STRATUM 18 */
/* BEGIN STRATUM 19 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Return;../declarations.dl [143:7-143:80];loadtime;)_",iter, [&](){return rel_20_Return->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Return;../declarations.dl [143:7-143:80];)",rel_20_Return->size(),iter);}();
/* END STRATUM 19 */
/* BEGIN STRATUM 20 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreArrayIndex;../declarations.dl [62:7-62:84];loadtime;)_",iter, [&](){return rel_21_StoreArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreArrayIndex;../declarations.dl [62:7-62:84];)",rel_21_StoreArrayIndex->size(),iter);}();
/* END STRATUM 20 */
/* BEGIN STRATUM 21 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreInstanceField;../declarations.dl [68:7-68:100];loadtime;)_",iter, [&](){return rel_22_StoreInstanceField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreInstanceField;../declarations.dl [68:7-68:100];)",rel_22_StoreInstanceField->size(),iter);}();
/* END STRATUM 21 */
/* BEGIN STRATUM 22 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_StoreStaticField;../declarations.dl [74:7-74:87];loadtime;)_",iter, [&](){return rel_23_StoreStaticField->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_StoreStaticField;../declarations.dl [74:7-74:87];)",rel_23_StoreStaticField->size(),iter);}();
/* END STRATUM 22 */
/* BEGIN STRATUM 23 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;AllUse;may-null/rules.dl [8:7-8:80];)_",iter, [&](){return rel_24_AllUse->size();});
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   VarDef(insn,index,var,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_18_VarDef->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_18_VarDef_op_ctxt,rel_18_VarDef->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_18_VarDef->equalRange_4(key,READ_OP_CONTEXT(rel_18_VarDef_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[29]++;
}
freqs[30]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignLocal(insn,index,var,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_13_AssignLocal->equalRange_4(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[31]++;
}
freqs[32]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _AssignCast(insn,index,var,_,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_10_AssignCast->equalRange_4(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[33]++;
}
freqs[34]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _SpecialMethodInvocation(insn,index,_,var,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[35]++;
}
freqs[36]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _VirtualMethodInvocation(insn,index,_,var,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[37]++;
}
freqs[38]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreArrayIndex(insn,index,var,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_21_StoreArrayIndex->equalRange_4(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[39]++;
}
freqs[40]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreInstanceField(insn,index,var,_,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[0],0,0,0}});
auto range = rel_22_StoreInstanceField->equalRange_4(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[5])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[41]++;
}
freqs[42]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _StoreStaticField(insn,index,var,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_23_StoreStaticField->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt,rel_23_StoreStaticField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[0],0,0}});
auto range = rel_23_StoreStaticField->equalRange_4(key,READ_OP_CONTEXT(rel_23_StoreStaticField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[43]++;
}
freqs[44]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _Return(insn,index,var,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_20_Return->equalRange_4(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[45]++;
}
freqs[46]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   AssignReturnValue_WithInvoke(insn,index,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[47]++;
}
freqs[48]++;
}
freqs[49]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _StaticMethodInvocation(insn,index,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env1[1],0,0,0}});
auto range = rel_3_StaticMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[3])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[50]++;
}
freqs[51]++;
}
freqs[52]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _VirtualMethodInvocation(insn,index,_,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[53]++;
}
freqs[54]++;
}
freqs[55]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(AllUse(insn,index,var,method) :- 
   RefTypeVar(var),
   _ActualParam(_,insn,var),
   _SpecialMethodInvocation(insn,index,_,_,method).
in file may-null/rules.dl [37:1-53:3])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;AllUse;may-null/rules.dl [37:1-53:3];AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;)_",iter, [&](){return rel_24_AllUse->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[0]}});
auto range = rel_19_ActualParam->equalRange_4(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env2[4])}});
rel_24_AllUse->insert(tuple,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
freqs[56]++;
}
freqs[57]++;
}
freqs[58]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;AllUse;may-null/rules.dl [8:7-8:80];savetime;)_",iter, [&](){return rel_24_AllUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_23_StoreStaticField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_18_VarDef->purge();
}();
/* END STRATUM 23 */
/* BEGIN STRATUM 24 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FirstUse;may-null/rules.dl [10:7-10:82];)_",iter, [&](){return rel_25_FirstUse->size();});
SignalHandler::instance()->setMsg(R"_(FirstUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = min  I0 : AllUse(_, I0,var,method).
in file may-null/rules.dl [60:1-62:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FirstUse;may-null/rules.dl [60:1-62:42];FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;)_",iter, [&](){return rel_25_FirstUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_25_FirstUse->insert(tuple,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
}
}
}
freqs[59]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FirstUse;may-null/rules.dl [10:7-10:82];savetime;)_",iter, [&](){return rel_25_FirstUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 24 */
/* BEGIN STRATUM 25 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;LastUse;may-null/rules.dl [9:7-9:81];)_",iter, [&](){return rel_26_LastUse->size();});
SignalHandler::instance()->setMsg(R"_(LastUse(insn,last,var,method) :- 
   AllUse(insn,last,var,method),
   last = max  I1 : AllUse(_, I1,var,method).
in file may-null/rules.dl [55:1-57:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;LastUse;may-null/rules.dl [55:1-57:42];LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;)_",iter, [&](){return rel_26_LastUse->size();});
if (!rel_24_AllUse->empty()) [&](){
auto part = rel_24_AllUse->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_24_AllUse_op_ctxt,rel_24_AllUse->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,4> env1;
RamDomain res = MIN_RAM_DOMAIN;
const ram::Tuple<RamDomain,4> key({{0,0,env0[2],env0[3]}});
auto range = rel_24_AllUse->equalRange_12(key,READ_OP_CONTEXT(rel_24_AllUse_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::max(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3])}});
rel_26_LastUse->insert(tuple,READ_OP_CONTEXT(rel_26_LastUse_op_ctxt));
}
}
}
freqs[60]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;LastUse;may-null/rules.dl [9:7-9:81];savetime;)_",iter, [&](){return rel_26_LastUse->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_24_AllUse->purge();
}();
/* END STRATUM 25 */
/* BEGIN STRATUM 26 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;ApplicationMethod;../declarations.dl [31:7-31:35];loadtime;)_",iter, [&](){return rel_27_ApplicationMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;ApplicationMethod;../declarations.dl [31:7-31:35];)",rel_27_ApplicationMethod->size(),iter);}();
/* END STRATUM 26 */
/* BEGIN STRATUM 27 */
[&]() {
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;AssignMayNull;may-null/rules.dl [6:7-6:72];)",rel_28_AssignMayNull->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;AssignMayNull;may-null/rules.dl [6:7-6:72];savetime;)_",iter, [&](){return rel_28_AssignMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 27 */
/* BEGIN STRATUM 28 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;BasicBlockHead;../declarations.dl [37:7-37:61];loadtime;)_",iter, [&](){return rel_29_BasicBlockHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;BasicBlockHead;../declarations.dl [37:7-37:61];)",rel_29_BasicBlockHead->size(),iter);}();
/* END STRATUM 28 */
/* BEGIN STRATUM 29 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;CallGraphEdge;../declarations.dl [24:7-24:80];loadtime;)_",iter, [&](){return rel_30_CallGraphEdge->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;CallGraphEdge;../declarations.dl [24:7-24:80];)",rel_30_CallGraphEdge->size(),iter);}();
/* END STRATUM 29 */
/* BEGIN STRATUM 30 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Instruction_Next;../declarations.dl [46:7-46:65];loadtime;)_",iter, [&](){return rel_31_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Instruction_Next;../declarations.dl [46:7-46:65];)",rel_31_Instruction_Next->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;Instruction_Next;../declarations.dl [46:7-46:65];savetime;)_",iter, [&](){return rel_31_Instruction_Next->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 30 */
/* BEGIN STRATUM 31 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfNull;../declarations.dl [128:7-128:52];loadtime;)_",iter, [&](){return rel_32_IfNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfNull;../declarations.dl [128:7-128:52];)",rel_32_IfNull->size(),iter);}();
/* END STRATUM 31 */
/* BEGIN STRATUM 32 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstruction;may-null/rules.dl [183:7-183:55];)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstruction(ifIns) :- 
   _IfNull(ifIns,_,_).
in file may-null/rules.dl [188:1-189:22])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstruction;may-null/rules.dl [188:1-189:22];MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
if (!rel_32_IfNull->empty()) [&](){
auto part = rel_32_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_33_MayNull_IfInstruction->insert(tuple,READ_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt));
freqs[61]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstruction;may-null/rules.dl [183:7-183:55];savetime;)_",iter, [&](){return rel_33_MayNull_IfInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 32 */
/* BEGIN STRATUM 33 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;FalseBranch;may-null/rules.dl [186:7-186:64];)_",iter, [&](){return rel_34_FalseBranch->size();});
SignalHandler::instance()->setMsg(R"_(FalseBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   Instruction_Next(ifIns,insn).
in file may-null/rules.dl [195:1-197:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;FalseBranch;may-null/rules.dl [195:1-197:31];FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;)_",iter, [&](){return rel_34_FalseBranch->size();});
if (!rel_31_Instruction_Next->empty()&&!rel_33_MayNull_IfInstruction->empty()) [&](){
auto part = rel_33_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt,rel_31_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_31_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_34_FalseBranch->insert(tuple,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
freqs[62]++;
}
freqs[63]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;FalseBranch;may-null/rules.dl [186:7-186:64];savetime;)_",iter, [&](){return rel_34_FalseBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 33 */
/* BEGIN STRATUM 34 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_OperatorAt;../declarations.dl [122:7-122:52];loadtime;)_",iter, [&](){return rel_35_OperatorAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_OperatorAt;../declarations.dl [122:7-122:52];)",rel_35_OperatorAt->size(),iter);}();
/* END STRATUM 34 */
/* BEGIN STRATUM 35 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNull_IfInstructionsCond;may-null/rules.dl [184:7-184:96];)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(MayNull_IfInstructionsCond(ifIns,left,"null",opt) :- 
   _IfNull(ifIns,_,left),
   _OperatorAt(ifIns,opt).
in file may-null/rules.dl [191:1-193:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNull_IfInstructionsCond;may-null/rules.dl [191:1-193:25];MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
if (!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_32_IfNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_36_MayNull_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt));
freqs[64]++;
}
freqs[65]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;MayNull_IfInstructionsCond;may-null/rules.dl [184:7-184:96];savetime;)_",iter, [&](){return rel_36_MayNull_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 35 */
/* BEGIN STRATUM 36 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;PhiNodeHead;../declarations.dl [43:7-43:67];loadtime;)_",iter, [&](){return rel_37_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;PhiNodeHead;../declarations.dl [43:7-43:67];)",rel_37_PhiNodeHead->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHead;../declarations.dl [43:7-43:67];savetime;)_",iter, [&](){return rel_37_PhiNodeHead->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 36 */
/* BEGIN STRATUM 37 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PhiNodeHeadVar;may-null/rules.dl [203:7-203:52];)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
SignalHandler::instance()->setMsg(R"_(PhiNodeHeadVar(var,headVar) :- 
   PhiNodeHead(insn,headInsn),
   _AssignLocal(insn,_,var,_,_),
   _AssignLocal(headInsn,_,headVar,_,_).
in file may-null/rules.dl [205:1-208:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PhiNodeHeadVar;may-null/rules.dl [205:1-208:42];PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
if (!rel_37_PhiNodeHead->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_37_PhiNodeHead->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_37_PhiNodeHead_op_ctxt,rel_37_PhiNodeHead->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env0[1],0,0,0,0}});
auto range = rel_13_AssignLocal->equalRange_1(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[2]),static_cast<RamDomain>(env2[2])}});
rel_38_PhiNodeHeadVar->insert(tuple,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
freqs[66]++;
}
freqs[67]++;
}
freqs[68]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;PhiNodeHeadVar;may-null/rules.dl [203:7-203:52];savetime;)_",iter, [&](){return rel_38_PhiNodeHeadVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_37_PhiNodeHead->purge();
}();
/* END STRATUM 37 */
/* BEGIN STRATUM 38 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CanBeNullBranch;may-null/rules.dl [211:7-211:58];)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","==").
in file may-null/rules.dl [224:1-225:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;may-null/rules.dl [224:1-225:54];CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_39_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt));
freqs[69]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!="),
   FalseBranch(ifIns,insn).
in file may-null/rules.dl [227:1-229:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CanBeNullBranch;may-null/rules.dl [227:1-229:26];CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (!rel_34_FalseBranch->empty()&&!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_34_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_39_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt));
freqs[70]++;
}
freqs[71]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_40_delta_CanBeNullBranch->insertAll(*rel_39_CanBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CanBeNullBranch;may-null/rules.dl [211:7-211:58];)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CanBeNullBranch(insn,var) :- 
   CanBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file may-null/rules.dl [231:1-233:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CanBeNullBranch;0;may-null/rules.dl [231:1-233:30];CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
if (!rel_40_delta_CanBeNullBranch->empty()&&!rel_38_PhiNodeHeadVar->empty()) [&](){
auto part = rel_40_delta_CanBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_40_delta_CanBeNullBranch_op_ctxt,rel_40_delta_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_41_new_CanBeNullBranch_op_ctxt,rel_41_new_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt,rel_39_CanBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_38_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[1]++,!rel_39_CanBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_39_CanBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_41_new_CanBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_41_new_CanBeNullBranch_op_ctxt));
}
freqs[72]++;
}
freqs[73]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_41_new_CanBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CanBeNullBranch;may-null/rules.dl [211:7-211:58];)_",iter, [&](){return rel_41_new_CanBeNullBranch->size();});
rel_39_CanBeNullBranch->insertAll(*rel_41_new_CanBeNullBranch);
std::swap(rel_40_delta_CanBeNullBranch, rel_41_new_CanBeNullBranch);
rel_41_new_CanBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_40_delta_CanBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_41_new_CanBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CanBeNullBranch;may-null/rules.dl [211:7-211:58];savetime;)_",iter, [&](){return rel_39_CanBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_39_CanBeNullBranch->purge();
}();
/* END STRATUM 38 */
/* BEGIN STRATUM 39 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;CannotBeNullBranch;may-null/rules.dl [210:7-210:61];)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(ifIns,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","!=").
in file may-null/rules.dl [213:1-214:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;may-null/rules.dl [213:1-214:54];CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(24)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_42_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt));
freqs[74]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   MayNull_IfInstructionsCond(ifIns,var,"null","=="),
   FalseBranch(ifIns,insn).
in file may-null/rules.dl [216:1-218:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;CannotBeNullBranch;may-null/rules.dl [216:1-218:26];CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (!rel_34_FalseBranch->empty()&&!rel_36_MayNull_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,RamDomain(23),RamDomain(25)}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_12(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_34_FalseBranch->equalRange_1(key,READ_OP_CONTEXT(rel_34_FalseBranch_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1])}});
rel_42_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt));
freqs[75]++;
}
freqs[76]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_43_delta_CannotBeNullBranch->insertAll(*rel_42_CannotBeNullBranch);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;CannotBeNullBranch;may-null/rules.dl [210:7-210:61];)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
SignalHandler::instance()->setMsg(R"_(CannotBeNullBranch(insn,var) :- 
   CannotBeNullBranch(insn,headVar),
   PhiNodeHeadVar(var,headVar).
in file may-null/rules.dl [220:1-222:30])_");
{
	Logger logger(R"_(@t-recursive-rule;CannotBeNullBranch;0;may-null/rules.dl [220:1-222:30];CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
if (!rel_43_delta_CannotBeNullBranch->empty()&&!rel_38_PhiNodeHeadVar->empty()) [&](){
auto part = rel_43_delta_CannotBeNullBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_43_delta_CannotBeNullBranch_op_ctxt,rel_43_delta_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_44_new_CannotBeNullBranch_op_ctxt,rel_44_new_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_38_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[2]++,!rel_42_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env0[0],env1[0]}}),READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_44_new_CannotBeNullBranch->insert(tuple,READ_OP_CONTEXT(rel_44_new_CannotBeNullBranch_op_ctxt));
}
freqs[77]++;
}
freqs[78]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_44_new_CannotBeNullBranch->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;CannotBeNullBranch;may-null/rules.dl [210:7-210:61];)_",iter, [&](){return rel_44_new_CannotBeNullBranch->size();});
rel_42_CannotBeNullBranch->insertAll(*rel_44_new_CannotBeNullBranch);
std::swap(rel_43_delta_CannotBeNullBranch, rel_44_new_CannotBeNullBranch);
rel_44_new_CannotBeNullBranch->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_43_delta_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_44_new_CannotBeNullBranch->purge();
{
	Logger logger(R"_(@t-relation-savetime;CannotBeNullBranch;may-null/rules.dl [210:7-210:61];savetime;)_",iter, [&](){return rel_42_CannotBeNullBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 39 */
/* BEGIN STRATUM 40 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Dominates;../declarations.dl [40:7-40:59];loadtime;)_",iter, [&](){return rel_45_Dominates->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Dominates;../declarations.dl [40:7-40:59];)",rel_45_Dominates->size(),iter);}();
/* END STRATUM 40 */
/* BEGIN STRATUM 41 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_FormalParam;../declarations.dl [146:7-146:60];loadtime;)_",iter, [&](){return rel_46_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_FormalParam;../declarations.dl [146:7-146:60];)",rel_46_FormalParam->size(),iter);}();
/* END STRATUM 41 */
/* BEGIN STRATUM 42 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_FormalParam;may-null/rules.dl [4:7-4:97];)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_FormalParam(cat(param,"/map_param/"),method,param,index) :- 
   _FormalParam(index,method,param).
in file may-null/rules.dl [89:1-91:34])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_FormalParam;may-null/rules.dl [89:1-91:34];Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
if (!rel_46_FormalParam->empty()) [&](){
auto part = rel_46_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_46_FormalParam_op_ctxt,rel_46_FormalParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[2]) + symTable.resolve(RamDomain(31)))),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[0])}});
rel_47_Instruction_FormalParam->insert(tuple,READ_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt));
freqs[79]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_FormalParam;may-null/rules.dl [4:7-4:97];savetime;)_",iter, [&](){return rel_47_Instruction_FormalParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 42 */
/* BEGIN STRATUM 43 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;isParam;may-null/rules.dl [272:7-272:62];)_",iter, [&](){return rel_48_isParam->size();});
SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   _FormalParam(index,method,var).
in file may-null/rules.dl [277:1-278:35])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;isParam;may-null/rules.dl [277:1-278:35];isParam(index,var,method) :- \n   _FormalParam(index,method,var).;)_",iter, [&](){return rel_48_isParam->size();});
if (!rel_46_FormalParam->empty()) [&](){
auto part = rel_46_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_46_FormalParam_op_ctxt,rel_46_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1])}});
rel_48_isParam->insert(tuple,READ_OP_CONTEXT(rel_48_isParam_op_ctxt));
freqs[80]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_49_delta_isParam->insertAll(*rel_48_isParam);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;isParam;may-null/rules.dl [272:7-272:62];)_",iter, [&](){return rel_50_new_isParam->size();});
SignalHandler::instance()->setMsg(R"_(isParam(index,to,method) :- 
   isParam(index,from,method),
   _AssignLocal(_,_,from,to,method).
in file may-null/rules.dl [280:1-285:3])_");
{
	Logger logger(R"_(@t-recursive-rule;isParam;0;may-null/rules.dl [280:1-285:3];isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;)_",iter, [&](){return rel_50_new_isParam->size();});
if (!rel_49_delta_isParam->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_49_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_delta_isParam_op_ctxt,rel_49_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_50_new_isParam_op_ctxt,rel_50_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_20(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,!rel_48_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_48_isParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_50_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_50_new_isParam_op_ctxt));
}
freqs[81]++;
}
freqs[82]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(isParam(index,to,method) :- 
   isParam(index,from,method),
   _AssignCast(_,_,from,to,_,method).
in file may-null/rules.dl [280:1-285:3])_");
{
	Logger logger(R"_(@t-recursive-rule;isParam;0;may-null/rules.dl [280:1-285:3];isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;)_",iter, [&](){return rel_50_new_isParam->size();});
if (!rel_49_delta_isParam->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_49_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_delta_isParam_op_ctxt,rel_49_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_50_new_isParam_op_ctxt,rel_50_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,!rel_48_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[3],env0[2]}}),READ_OP_CONTEXT(rel_48_isParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2])}});
rel_50_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_50_new_isParam_op_ctxt));
}
freqs[83]++;
}
freqs[84]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(isParam(index,var,method) :- 
   isParam(index,headVar,method),
   PhiNodeHeadVar(var,headVar).
in file may-null/rules.dl [287:1-289:30])_");
{
	Logger logger(R"_(@t-recursive-rule;isParam;0;may-null/rules.dl [287:1-289:30];isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;)_",iter, [&](){return rel_50_new_isParam->size();});
if (!rel_49_delta_isParam->empty()&&!rel_38_PhiNodeHeadVar->empty()) [&](){
auto part = rel_49_delta_isParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_49_delta_isParam_op_ctxt,rel_49_delta_isParam->createContext());
CREATE_OP_CONTEXT(rel_50_new_isParam_op_ctxt,rel_50_new_isParam->createContext());
CREATE_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt,rel_38_PhiNodeHeadVar->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[1]}});
auto range = rel_38_PhiNodeHeadVar->equalRange_2(key,READ_OP_CONTEXT(rel_38_PhiNodeHeadVar_op_ctxt));
for(const auto& env1 : range) {
if( (reads[3]++,!rel_48_isParam->contains(Tuple<RamDomain,3>({{env0[0],env1[0],env0[2]}}),READ_OP_CONTEXT(rel_48_isParam_op_ctxt)))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[2])}});
rel_50_new_isParam->insert(tuple,READ_OP_CONTEXT(rel_50_new_isParam_op_ctxt));
}
freqs[85]++;
}
freqs[86]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_50_new_isParam->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;isParam;may-null/rules.dl [272:7-272:62];)_",iter, [&](){return rel_50_new_isParam->size();});
rel_48_isParam->insertAll(*rel_50_new_isParam);
std::swap(rel_49_delta_isParam, rel_50_new_isParam);
rel_50_new_isParam->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_49_delta_isParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_50_new_isParam->purge();
{
	Logger logger(R"_(@t-relation-savetime;isParam;may-null/rules.dl [272:7-272:62];savetime;)_",iter, [&](){return rel_48_isParam->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_48_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_38_PhiNodeHeadVar->purge();
}();
/* END STRATUM 43 */
/* BEGIN STRATUM 44 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ThisVar;../declarations.dl [152:7-152:41];loadtime;)_",iter, [&](){return rel_51_ThisVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_51_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ThisVar;../declarations.dl [152:7-152:41];)",rel_51_ThisVar->size(),iter);}();
/* END STRATUM 44 */
/* BEGIN STRATUM 45 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];loadtime;)_",iter, [&](){return rel_52_Var_DeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_Var_DeclaringMethod;../declarations.dl [52:7-52:55];)",rel_52_Var_DeclaringMethod->size(),iter);}();
/* END STRATUM 45 */
/* BEGIN STRATUM 46 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;Instruction_VarDeclaringMethod;may-null/rules.dl [3:7-3:89];)_",iter, [&](){return rel_53_Instruction_VarDeclaringMethod->size();});
SignalHandler::instance()->setMsg(R"_(Instruction_VarDeclaringMethod(cat(var,"/var_declaration/"),method,var) :- 
   RefTypeVar(var),
   _Var_DeclaringMethod(var,method),
   !_FormalParam(_,method,var),
   !_ThisVar(method,var).
in file may-null/rules.dl [82:1-87:38])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;Instruction_VarDeclaringMethod;may-null/rules.dl [82:1-87:38];Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;)_",iter, [&](){return rel_53_Instruction_VarDeclaringMethod->size();});
if (!rel_9_RefTypeVar->empty()&&!rel_52_Var_DeclaringMethod->empty()) [&](){
auto part = rel_9_RefTypeVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_53_Instruction_VarDeclaringMethod_op_ctxt,rel_53_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_9_RefTypeVar_op_ctxt,rel_9_RefTypeVar->createContext());
CREATE_OP_CONTEXT(rel_46_FormalParam_op_ctxt,rel_46_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_51_ThisVar_op_ctxt,rel_51_ThisVar->createContext());
CREATE_OP_CONTEXT(rel_52_Var_DeclaringMethod_op_ctxt,rel_52_Var_DeclaringMethod->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_52_Var_DeclaringMethod->equalRange_1(key,READ_OP_CONTEXT(rel_52_Var_DeclaringMethod_op_ctxt));
for(const auto& env1 : range) {
if( (((reads[4]++,rel_46_FormalParam->equalRange_6(Tuple<RamDomain,3>({{0,env1[1],env0[0]}}),READ_OP_CONTEXT(rel_46_FormalParam_op_ctxt)).empty())) && ((reads[5]++,!rel_51_ThisVar->contains(Tuple<RamDomain,2>({{env1[1],env0[0]}}),READ_OP_CONTEXT(rel_51_ThisVar_op_ctxt)))))) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(symTable.lookup(symTable.resolve(env0[0]) + symTable.resolve(RamDomain(30)))),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[0])}});
rel_53_Instruction_VarDeclaringMethod->insert(tuple,READ_OP_CONTEXT(rel_53_Instruction_VarDeclaringMethod_op_ctxt));
}
freqs[87]++;
}
freqs[88]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;Instruction_VarDeclaringMethod;may-null/rules.dl [3:7-3:89];savetime;)_",iter, [&](){return rel_53_Instruction_VarDeclaringMethod->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_52_Var_DeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_46_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_51_ThisVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_9_RefTypeVar->purge();
}();
/* END STRATUM 46 */
/* BEGIN STRATUM 47 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;Method_FirstInstruction;../declarations.dl [49:7-49:63];loadtime;)_",iter, [&](){return rel_54_Method_FirstInstruction->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;Method_FirstInstruction;../declarations.dl [49:7-49:63];)",rel_54_Method_FirstInstruction->size(),iter);}();
/* END STRATUM 47 */
/* BEGIN STRATUM 48 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];loadtime;)_",iter, [&](){return rel_55_MayPredecessorModuloThrow->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_55_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
{
	Logger logger(R"_(@t-nonrecursive-relation;MayPredecessorModuloThrow;../declarations.dl [34:7-34:70];)_",iter, [&](){return rel_55_MayPredecessorModuloThrow->size();});
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_VarDeclaringMethod(insn,method,var),
   FirstUse(next,_,var,method).
in file may-null/rules.dl [64:1-66:32])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;may-null/rules.dl [64:1-66:32];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;)_",iter, [&](){return rel_55_MayPredecessorModuloThrow->size();});
if (!rel_25_FirstUse->empty()&&!rel_53_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_53_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_25_FirstUse_op_ctxt,rel_25_FirstUse->createContext());
CREATE_OP_CONTEXT(rel_53_Instruction_VarDeclaringMethod_op_ctxt,rel_53_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt,rel_55_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[2],env0[1]}});
auto range = rel_25_FirstUse->equalRange_12(key,READ_OP_CONTEXT(rel_25_FirstUse_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_55_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt));
freqs[89]++;
}
freqs[90]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayPredecessorModuloThrow(insn,next) :- 
   Instruction_FormalParam(insn,method,_,_),
   Method_FirstInstruction(method,next).
in file may-null/rules.dl [93:1-95:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayPredecessorModuloThrow;may-null/rules.dl [93:1-95:39];MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;)_",iter, [&](){return rel_55_MayPredecessorModuloThrow->size();});
if (!rel_47_Instruction_FormalParam->empty()&&!rel_54_Method_FirstInstruction->empty()) [&](){
auto part = rel_47_Instruction_FormalParam->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt,rel_55_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_54_Method_FirstInstruction_op_ctxt,rel_54_Method_FirstInstruction->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_54_Method_FirstInstruction->equalRange_1(key,READ_OP_CONTEXT(rel_54_Method_FirstInstruction_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_55_MayPredecessorModuloThrow->insert(tuple,READ_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt));
freqs[91]++;
}
freqs[92]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_54_Method_FirstInstruction->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_25_FirstUse->purge();
}();
/* END STRATUM 48 */
/* BEGIN STRATUM 49 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;SpecialIfEdge;may-null/rules.dl [68:7-68:72];)_",iter, [&](){return rel_56_SpecialIfEdge->size();});
SignalHandler::instance()->setMsg(R"_(SpecialIfEdge(beforeIf,next,var) :- 
   FalseBranch(ifIns,next),
   MayPredecessorModuloThrow(beforeIf,ifIns),
   MayNull_IfInstructionsCond(ifIns,var,"null",_).
in file may-null/rules.dl [70:1-73:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;SpecialIfEdge;may-null/rules.dl [70:1-73:51];SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;)_",iter, [&](){return rel_56_SpecialIfEdge->size();});
if (!rel_34_FalseBranch->empty()&&!rel_36_MayNull_IfInstructionsCond->empty()&&!rel_55_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_34_FalseBranch->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_34_FalseBranch_op_ctxt,rel_34_FalseBranch->createContext());
CREATE_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt,rel_36_MayNull_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt,rel_55_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_56_SpecialIfEdge_op_ctxt,rel_56_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_55_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{env0[0],0,RamDomain(23),0}});
auto range = rel_36_MayNull_IfInstructionsCond->equalRange_5(key,READ_OP_CONTEXT(rel_36_MayNull_IfInstructionsCond_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[1])}});
rel_56_SpecialIfEdge->insert(tuple,READ_OP_CONTEXT(rel_56_SpecialIfEdge_op_ctxt));
freqs[93]++;
}
freqs[94]++;
}
freqs[95]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_36_MayNull_IfInstructionsCond->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_34_FalseBranch->purge();
}();
/* END STRATUM 49 */
/* BEGIN STRATUM 50 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;InstructionLine;../declarations.dl [16:7-16:67];loadtime;)_",iter, [&](){return rel_57_InstructionLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;InstructionLine;../declarations.dl [16:7-16:67];)",rel_57_InstructionLine->size(),iter);}();
/* END STRATUM 50 */
/* BEGIN STRATUM 51 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;VarPointsTo;../declarations.dl [20:7-20:66];loadtime;)_",iter, [&](){return rel_58_VarPointsTo->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_58_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;VarPointsTo;../declarations.dl [20:7-20:66];)",rel_58_VarPointsTo->size(),iter);}();
/* END STRATUM 51 */
/* BEGIN STRATUM 52 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarMayPointToNull;../declarations.dl [173:7-173:32];)_",iter, [&](){return rel_59_VarMayPointToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarMayPointToNull(var) :- 
   VarPointsTo(_,alloc,_,var),
   alloc = "<<null pseudo heap>>".
in file ../rules.dl [1:1-2:80])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarMayPointToNull;../rules.dl [1:1-2:80];VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   alloc = \"<<null pseudo heap>>\".;)_",iter, [&](){return rel_59_VarMayPointToNull->size();});
if (!rel_58_VarPointsTo->empty()) [&](){
const Tuple<RamDomain,4> key({{0,RamDomain(8),0,0}});
auto range = rel_58_VarPointsTo->equalRange_2(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_59_VarMayPointToNull_op_ctxt,rel_59_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_58_VarPointsTo_op_ctxt,rel_58_VarPointsTo->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[3])}});
rel_59_VarMayPointToNull->insert(tuple,READ_OP_CONTEXT(rel_59_VarMayPointToNull_op_ctxt));
freqs[96]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
}();
/* END STRATUM 52 */
/* BEGIN STRATUM 53 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarMayNotPointToNull;../declarations.dl [174:7-174:35];)_",iter, [&](){return rel_60_VarMayNotPointToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarMayNotPointToNull(var) :- 
   VarMayPointToNull(var),
   VarPointsTo(_,alloc,_,var),
   alloc != "<<null pseudo heap>>".
in file ../must_rules.dl [1:1-3:61])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarMayNotPointToNull;../must_rules.dl [1:1-3:61];VarMayNotPointToNull(var) :- \n   VarMayPointToNull(var),\n   VarPointsTo(_,alloc,_,var),\n   alloc != \"<<null pseudo heap>>\".;)_",iter, [&](){return rel_60_VarMayNotPointToNull->size();});
if (!rel_59_VarMayPointToNull->empty()&&!rel_58_VarPointsTo->empty()) [&](){
auto part = rel_59_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_60_VarMayNotPointToNull_op_ctxt,rel_60_VarMayNotPointToNull->createContext());
CREATE_OP_CONTEXT(rel_59_VarMayPointToNull_op_ctxt,rel_59_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_58_VarPointsTo_op_ctxt,rel_58_VarPointsTo->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[0]}});
auto range = rel_58_VarPointsTo->equalRange_8(key,READ_OP_CONTEXT(rel_58_VarPointsTo_op_ctxt));
for(const auto& env1 : range) {
if( ((env1[1]) != (RamDomain(8)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_60_VarMayNotPointToNull->insert(tuple,READ_OP_CONTEXT(rel_60_VarMayNotPointToNull_op_ctxt));
}
freqs[97]++;
}
freqs[98]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_58_VarPointsTo->purge();
}();
/* END STRATUM 53 */
/* BEGIN STRATUM 54 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;VarPointsToNull;../declarations.dl [158:7-158:37];)_",iter, [&](){return rel_61_VarPointsToNull->size();});
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   VarMayPointToNull(var),
   !VarMayNotPointToNull(var).
in file ../must_rules.dl [5:1-5:76])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarPointsToNull;../must_rules.dl [5:1-5:76];VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarMayNotPointToNull(var).;)_",iter, [&](){return rel_61_VarPointsToNull->size();});
if (!rel_59_VarMayPointToNull->empty()) [&](){
auto part = rel_59_VarMayPointToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_60_VarMayNotPointToNull_op_ctxt,rel_60_VarMayNotPointToNull->createContext());
CREATE_OP_CONTEXT(rel_59_VarMayPointToNull_op_ctxt,rel_59_VarMayPointToNull->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[6]++,!rel_60_VarMayNotPointToNull->contains(Tuple<RamDomain,1>({{env0[0]}}),READ_OP_CONTEXT(rel_60_VarMayNotPointToNull_op_ctxt)))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_61_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
}
freqs[99]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   _AssignCastNull(_,_,var,_,_).
in file ../rules.dl [5:1-5:54])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;VarPointsToNull;../rules.dl [5:1-5:54];VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;)_",iter, [&](){return rel_61_VarPointsToNull->size();});
if (!rel_11_AssignCastNull->empty()) [&](){
auto part = rel_11_AssignCastNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_11_AssignCastNull_op_ctxt,rel_11_AssignCastNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[2])}});
rel_61_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
freqs[100]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;VarPointsToNull;../declarations.dl [158:7-158:37];savetime;)_",iter, [&](){return rel_61_VarPointsToNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_61_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_11_AssignCastNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_59_VarMayPointToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_60_VarMayNotPointToNull->purge();
}();
/* END STRATUM 54 */
/* BEGIN STRATUM 55 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignBinop;../declarations.dl [100:7-100:67];loadtime;)_",iter, [&](){return rel_62_AssignBinop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignBinop;../declarations.dl [100:7-100:67];)",rel_62_AssignBinop->size(),iter);}();
/* END STRATUM 55 */
/* BEGIN STRATUM 56 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignOperFrom;../declarations.dl [103:7-103:51];loadtime;)_",iter, [&](){return rel_63_AssignOperFrom->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignOperFrom;../declarations.dl [103:7-103:51];)",rel_63_AssignOperFrom->size(),iter);}();
/* END STRATUM 56 */
/* BEGIN STRATUM 57 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_AssignUnop;../declarations.dl [98:7-98:66];loadtime;)_",iter, [&](){return rel_64_AssignUnop->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_64_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_AssignUnop;../declarations.dl [98:7-98:66];)",rel_64_AssignUnop->size(),iter);}();
/* END STRATUM 57 */
/* BEGIN STRATUM 58 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_EnterMonitor;../declarations.dl [111:7-111:68];loadtime;)_",iter, [&](){return rel_65_EnterMonitor->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_EnterMonitor;../declarations.dl [111:7-111:68];)",rel_65_EnterMonitor->size(),iter);}();
/* END STRATUM 58 */
/* BEGIN STRATUM 59 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_LoadArrayIndex;../declarations.dl [60:7-60:81];loadtime;)_",iter, [&](){return rel_66_LoadArrayIndex->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_LoadArrayIndex;../declarations.dl [60:7-60:81];)",rel_66_LoadArrayIndex->size(),iter);}();
/* END STRATUM 59 */
/* BEGIN STRATUM 60 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_ThrowNull;../declarations.dl [83:7-83:56];loadtime;)_",iter, [&](){return rel_67_ThrowNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_67_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_ThrowNull;../declarations.dl [83:7-83:56];)",rel_67_ThrowNull->size(),iter);}();
/* END STRATUM 60 */
/* BEGIN STRATUM 61 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NullAt;../declarations.dl [161:7-161:83];)_",iter, [&](){return rel_68_NullAt->size();});
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw NullPointerException","throw NPE",a) :- 
   CallGraphEdge(_,a,_,b),
   _SpecialMethodInvocation(a,index,b,_,meth),
   "java.lang.NullPointerException" contains a.
in file ../rules.dl [8:1-11:48])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [8:1-11:48];NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_30_CallGraphEdge->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_30_CallGraphEdge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(11))) != std::string::npos)) {
const Tuple<RamDomain,5> key({{env0[1],0,env0[3],0,0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_5(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(9)),static_cast<RamDomain>(RamDomain(10)),static_cast<RamDomain>(env0[1])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[101]++;
}
}
freqs[102]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Array Index",var,insn) :- 
   VarPointsToNull(var),
   _LoadArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [16:1-18:44])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [16:1-18:44];NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_66_LoadArrayIndex->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_66_LoadArrayIndex_op_ctxt,rel_66_LoadArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_66_LoadArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_66_LoadArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(12)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[103]++;
}
freqs[104]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Array Index",var,insn) :- 
   VarPointsToNull(var),
   _StoreArrayIndex(insn,index,_,var,meth).
in file ../rules.dl [28:1-30:45])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [28:1-30:45];NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_21_StoreArrayIndex->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt,rel_21_StoreArrayIndex->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_21_StoreArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_21_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(13)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[105]++;
}
freqs[106]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _StoreInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [40:1-42:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [40:1-42:51];NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_22_StoreInstanceField->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt,rel_22_StoreInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_22_StoreInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_22_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(14)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[107]++;
}
freqs[108]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Instance Field",var,insn) :- 
   VarPointsToNull(var),
   _LoadInstanceField(insn,index,_,var,_,meth).
in file ../rules.dl [53:1-55:50])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [53:1-55:50];NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_16_LoadInstanceField->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt,rel_16_LoadInstanceField->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_16_LoadInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_16_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[5]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(15)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[109]++;
}
freqs[110]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Virtual Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _VirtualMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [66:1-68:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [66:1-68:53];NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(16)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[111]++;
}
freqs[112]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Special Method Invocation",var,insn) :- 
   VarPointsToNull(var),
   _SpecialMethodInvocation(insn,index,_,var,meth).
in file ../rules.dl [78:1-80:53])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [78:1-80:53];NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_2_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[4]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(17)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[113]++;
}
freqs[114]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Unary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignUnop(insn,index,_,meth),
   _AssignOperFrom(insn,var).
in file ../rules.dl [90:1-93:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [90:1-93:28];NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_63_AssignOperFrom->empty()&&!rel_64_AssignUnop->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_63_AssignOperFrom_op_ctxt,rel_63_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_64_AssignUnop_op_ctxt,rel_64_AssignUnop->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_64_AssignUnop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_63_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_63_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(18)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[115]++;
}
freqs[116]++;
}
freqs[117]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Binary Operator",var,insn) :- 
   VarPointsToNull(var),
   _AssignBinop(insn,index,_,meth),
   _AssignOperFrom(insn,var).
in file ../rules.dl [106:1-109:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [106:1-109:28];NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_62_AssignBinop->empty()&&!rel_63_AssignOperFrom->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_62_AssignBinop_op_ctxt,rel_62_AssignBinop->createContext());
CREATE_OP_CONTEXT(rel_63_AssignOperFrom_op_ctxt,rel_63_AssignOperFrom->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_62_AssignBinop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_63_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_63_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(19)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[118]++;
}
freqs[119]++;
}
freqs[120]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw Null","throw null",insn) :- 
   _ThrowNull(insn,index,meth).
in file ../rules.dl [121:1-122:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [121:1-122:31];NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_67_ThrowNull->empty()) [&](){
auto part = rel_67_ThrowNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_67_ThrowNull_op_ctxt,rel_67_ThrowNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(20)),static_cast<RamDomain>(RamDomain(21)),static_cast<RamDomain>(env0[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[121]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Enter Monitor (Synchronized)",var,insn) :- 
   VarPointsToNull(var),
   _EnterMonitor(insn,index,var,meth).
in file ../rules.dl [127:1-129:39])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NullAt;../rules.dl [127:1-129:39];NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;)_",iter, [&](){return rel_68_NullAt->size();});
if (!rel_61_VarPointsToNull->empty()&&!rel_65_EnterMonitor->empty()) [&](){
auto part = rel_61_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_65_EnterMonitor_op_ctxt,rel_65_EnterMonitor->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_65_EnterMonitor->equalRange_4(key,READ_OP_CONTEXT(rel_65_EnterMonitor_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,5> tuple({{static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(RamDomain(22)),static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_68_NullAt->insert(tuple,READ_OP_CONTEXT(rel_68_NullAt_op_ctxt));
freqs[122]++;
}
freqs[123]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NullAt;../declarations.dl [161:7-161:83];savetime;)_",iter, [&](){return rel_68_NullAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_66_LoadArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_21_StoreArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_16_LoadInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_22_StoreInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_67_ThrowNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_64_AssignUnop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_62_AssignBinop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_63_AssignOperFrom->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_65_EnterMonitor->purge();
}();
/* END STRATUM 61 */
/* BEGIN STRATUM 62 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;ReachableNullAtLine;../declarations.dl [167:7-167:120];)_",iter, [&](){return rel_69_ReachableNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(ReachableNullAtLine(meth,index,file,line,type,var,insn) :- 
   NullAt(meth,index,type,var,insn),
   ApplicationMethod(meth),
   InstructionLine(meth,index,line,file).
in file ../rules.dl [140:1-143:42])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;ReachableNullAtLine;../rules.dl [140:1-143:42];ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;)_",iter, [&](){return rel_69_ReachableNullAtLine->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_57_InstructionLine->empty()&&!rel_68_NullAt->empty()) [&](){
auto part = rel_68_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_57_InstructionLine_op_ctxt,rel_57_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt,rel_69_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env0[0],env0[1],0,0}});
auto range = rel_57_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_57_InstructionLine_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4])}});
rel_69_ReachableNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt));
freqs[124]++;
}
freqs[125]++;
}
freqs[126]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;ReachableNullAtLine;../declarations.dl [167:7-167:120];savetime;)_",iter, [&](){return rel_69_ReachableNullAtLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./ReachableNullAtLine.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 62 */
/* BEGIN STRATUM 63 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;PathSensitiveNullAtLine;../declarations.dl [170:7-170:124];)_",iter, [&](){return rel_70_PathSensitiveNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- 
   ReachableNullAtLine(meth,index,file,line,type,var,ins).
in file path_sensitivity/rules.dl [84:1-85:62])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;PathSensitiveNullAtLine;path_sensitivity/rules.dl [84:1-85:62];PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins).;)_",iter, [&](){return rel_70_PathSensitiveNullAtLine->size();});
if (!rel_69_ReachableNullAtLine->empty()) [&](){
auto part = rel_69_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_70_PathSensitiveNullAtLine_op_ctxt,rel_70_PathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt,rel_69_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_70_PathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_70_PathSensitiveNullAtLine_op_ctxt));
freqs[127]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;PathSensitiveNullAtLine;../declarations.dl [170:7-170:124];savetime;)_",iter, [&](){return rel_70_PathSensitiveNullAtLine->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./PathSensitiveNullAtLine.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_70_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 63 */
/* BEGIN STRATUM 64 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MinPathSensitiveNullAtLine;may-null/rules.dl [262:7-262:120];)_",iter, [&](){return rel_71_MinPathSensitiveNullAtLine->size();});
SignalHandler::instance()->setMsg(R"_(MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- 
   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).
in file may-null/rules.dl [264:1-266:70])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MinPathSensitiveNullAtLine;may-null/rules.dl [264:1-266:70];MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;)_",iter, [&](){return rel_71_MinPathSensitiveNullAtLine->size();});
if (!rel_70_PathSensitiveNullAtLine->empty()) [&](){
auto part = rel_70_PathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_71_MinPathSensitiveNullAtLine_op_ctxt,rel_71_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_70_PathSensitiveNullAtLine_op_ctxt,rel_70_PathSensitiveNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
ram::Tuple<RamDomain,7> env1;
RamDomain res = MAX_RAM_DOMAIN;
const ram::Tuple<RamDomain,7> key({{env0[0],0,env0[2],0,0,env0[5],0}});
auto range = rel_70_PathSensitiveNullAtLine->equalRange_37(key,READ_OP_CONTEXT(rel_70_PathSensitiveNullAtLine_op_ctxt));
if(!range.empty()) {
for(const auto& cur : range) {
env1 = cur;
res = std::min(res,env1[1]);
}
env1[0] = res;
{
if( ((env0[1]) == (env1[0]))) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_71_MinPathSensitiveNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_71_MinPathSensitiveNullAtLine_op_ctxt));
}
}
}
freqs[128]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_70_PathSensitiveNullAtLine->purge();
}();
/* END STRATUM 64 */
/* BEGIN STRATUM 65 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;_IfVar;../declarations.dl [125:7-125:51];loadtime;)_",iter, [&](){return rel_72_IfVar->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;_IfVar;../declarations.dl [125:7-125:51];)",rel_72_IfVar->size(),iter);}();
/* END STRATUM 65 */
/* BEGIN STRATUM 66 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructions;path_sensitivity/rules.dl [1:7-1:66];)_",iter, [&](){return rel_73_IfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   BasicBlockHead(ins,headIns),
   MayPredecessorModuloThrow(ifIns,headIns).
in file path_sensitivity/rules.dl [8:1-11:43])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;path_sensitivity/rules.dl [8:1-11:43];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;)_",iter, [&](){return rel_73_IfInstructions->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_55_MayPredecessorModuloThrow->empty()&&!rel_69_ReachableNullAtLine->empty()) [&](){
auto part = rel_69_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_73_IfInstructions_op_ctxt,rel_73_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt,rel_55_MayPredecessorModuloThrow->createContext());
CREATE_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt,rel_69_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{0,env1[1]}});
auto range = rel_55_MayPredecessorModuloThrow->equalRange_2(key,READ_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env2[0])}});
rel_73_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_73_IfInstructions_op_ctxt));
freqs[129]++;
}
freqs[130]++;
}
freqs[131]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructions(ins,ifIns) :- 
   ReachableNullAtLine(_,_,_,_,_,var,ins),
   _IfVar(ifIns,_,var).
in file path_sensitivity/rules.dl [14:1-16:23])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructions;path_sensitivity/rules.dl [14:1-16:23];IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;)_",iter, [&](){return rel_73_IfInstructions->size();});
if (!rel_69_ReachableNullAtLine->empty()&&!rel_72_IfVar->empty()) [&](){
auto part = rel_69_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_73_IfInstructions_op_ctxt,rel_73_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt,rel_69_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_72_IfVar_op_ctxt,rel_72_IfVar->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,0,env0[5]}});
auto range = rel_72_IfVar->equalRange_4(key,READ_OP_CONTEXT(rel_72_IfVar_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[6]),static_cast<RamDomain>(env1[0])}});
rel_73_IfInstructions->insert(tuple,READ_OP_CONTEXT(rel_73_IfInstructions_op_ctxt));
freqs[132]++;
}
freqs[133]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructions;path_sensitivity/rules.dl [1:7-1:66];savetime;)_",iter, [&](){return rel_73_IfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 66 */
/* BEGIN STRATUM 67 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;IfInstructionsCond;path_sensitivity/rules.dl [2:7-2:94];)_",iter, [&](){return rel_74_IfInstructionsCond->size();});
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   varLeft = "null".
in file path_sensitivity/rules.dl [34:1-38:18])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;path_sensitivity/rules.dl [34:1-38:18];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;)_",iter, [&](){return rel_74_IfInstructionsCond->size();});
if (!rel_73_IfInstructions->empty()&&!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_73_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_73_IfInstructions_op_ctxt,rel_73_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,RamDomain(23)}});
auto range = rel_32_IfNull->equalRange_5(key,READ_OP_CONTEXT(rel_32_IfNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_74_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt));
freqs[134]++;
}
freqs[135]++;
}
freqs[136]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(IfInstructionsCond(ifIns,"null","null",opt) :- 
   IfInstructions(_,ifIns),
   _OperatorAt(ifIns,opt),
   _IfNull(ifIns,_,varLeft),
   VarPointsToNull(varLeft).
in file path_sensitivity/rules.dl [40:1-44:26])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;IfInstructionsCond;path_sensitivity/rules.dl [40:1-44:26];IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;)_",iter, [&](){return rel_74_IfInstructionsCond->size();});
if (!rel_73_IfInstructions->empty()&&!rel_61_VarPointsToNull->empty()&&!rel_32_IfNull->empty()&&!rel_35_OperatorAt->empty()) [&](){
auto part = rel_73_IfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_73_IfInstructions_op_ctxt,rel_73_IfInstructions->createContext());
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_32_IfNull_op_ctxt,rel_32_IfNull->createContext());
CREATE_OP_CONTEXT(rel_35_OperatorAt_op_ctxt,rel_35_OperatorAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_35_OperatorAt->equalRange_1(key,READ_OP_CONTEXT(rel_35_OperatorAt_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[1],0,0}});
auto range = rel_32_IfNull->equalRange_1(key,READ_OP_CONTEXT(rel_32_IfNull_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_61_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(RamDomain(23)),static_cast<RamDomain>(env1[1])}});
rel_74_IfInstructionsCond->insert(tuple,READ_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt));
freqs[137]++;
}
freqs[138]++;
}
freqs[139]++;
}
freqs[140]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;IfInstructionsCond;path_sensitivity/rules.dl [2:7-2:94];savetime;)_",iter, [&](){return rel_74_IfInstructionsCond->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_74_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_35_OperatorAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_32_IfNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_73_IfInstructions->purge();
}();
/* END STRATUM 67 */
/* BEGIN STRATUM 68 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueIfInstructions;path_sensitivity/rules.dl [3:7-3:52];)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"!="),
   ord(left) != ord(right).
in file path_sensitivity/rules.dl [46:1-48:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [46:1-48:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(24)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) != (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[141]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"=="),
   ord(left) = ord(right).
in file path_sensitivity/rules.dl [50:1-52:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [50:1-52:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(25)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) == (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[142]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<="),
   ord(left) <= ord(right).
in file path_sensitivity/rules.dl [54:1-56:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [54:1-56:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(26)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) <= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[143]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">="),
   ord(left) >= ord(right).
in file path_sensitivity/rules.dl [58:1-60:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [58:1-60:25];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(27)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) >= (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[144]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,"<"),
   ord(left) < ord(right).
in file path_sensitivity/rules.dl [62:1-64:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [62:1-64:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(28)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) < (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[145]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(TrueIfInstructions(ifIns) :- 
   IfInstructionsCond(ifIns,left,right,">"),
   ord(left) > ord(right).
in file path_sensitivity/rules.dl [66:1-68:24])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueIfInstructions;path_sensitivity/rules.dl [66:1-68:24];TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (!rel_74_IfInstructionsCond->empty()) [&](){
const Tuple<RamDomain,4> key({{0,0,0,RamDomain(29)}});
auto range = rel_74_IfInstructionsCond->equalRange_8(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_74_IfInstructionsCond_op_ctxt,rel_74_IfInstructionsCond->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
if( ((env0[1]) > (env0[2]))) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[0])}});
rel_75_TrueIfInstructions->insert(tuple,READ_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt));
}
freqs[146]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueIfInstructions;path_sensitivity/rules.dl [3:7-3:52];savetime;)_",iter, [&](){return rel_75_TrueIfInstructions->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_74_IfInstructionsCond->purge();
}();
/* END STRATUM 68 */
/* BEGIN STRATUM 69 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;DominatesUnreachable;path_sensitivity/rules.dl [5:7-5:72];)_",iter, [&](){return rel_76_DominatesUnreachable->size();});
SignalHandler::instance()->setMsg(R"_(DominatesUnreachable(ifIns,ins) :- 
   TrueIfInstructions(ifIns),
   Instruction_Next(ifIns,ins).
in file path_sensitivity/rules.dl [70:1-72:30])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;DominatesUnreachable;path_sensitivity/rules.dl [70:1-72:30];DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;)_",iter, [&](){return rel_76_DominatesUnreachable->size();});
if (!rel_31_Instruction_Next->empty()&&!rel_75_TrueIfInstructions->empty()) [&](){
auto part = rel_75_TrueIfInstructions->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_76_DominatesUnreachable_op_ctxt,rel_76_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt,rel_31_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_75_TrueIfInstructions_op_ctxt,rel_75_TrueIfInstructions->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_31_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[1])}});
rel_76_DominatesUnreachable->insert(tuple,READ_OP_CONTEXT(rel_76_DominatesUnreachable_op_ctxt));
freqs[147]++;
}
freqs[148]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;DominatesUnreachable;path_sensitivity/rules.dl [5:7-5:72];savetime;)_",iter, [&](){return rel_76_DominatesUnreachable->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_75_TrueIfInstructions->purge();
}();
/* END STRATUM 69 */
/* BEGIN STRATUM 70 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;UnreachablePathNPEIns;path_sensitivity/rules.dl [4:7-4:55];)_",iter, [&](){return rel_77_UnreachablePathNPEIns->size();});
SignalHandler::instance()->setMsg(R"_(UnreachablePathNPEIns(ins) :- 
   ReachableNullAtLine(_,_,_,_,_,_,ins),
   DominatesUnreachable(_,parent),
   BasicBlockHead(ins,headIns),
   Dominates(parent,headIns).
in file path_sensitivity/rules.dl [78:1-82:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;UnreachablePathNPEIns;path_sensitivity/rules.dl [78:1-82:28];UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;)_",iter, [&](){return rel_77_UnreachablePathNPEIns->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_45_Dominates->empty()&&!rel_76_DominatesUnreachable->empty()&&!rel_69_ReachableNullAtLine->empty()) [&](){
auto part = rel_69_ReachableNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_45_Dominates_op_ctxt,rel_45_Dominates->createContext());
CREATE_OP_CONTEXT(rel_76_DominatesUnreachable_op_ctxt,rel_76_DominatesUnreachable->createContext());
CREATE_OP_CONTEXT(rel_69_ReachableNullAtLine_op_ctxt,rel_69_ReachableNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_77_UnreachablePathNPEIns_op_ctxt,rel_77_UnreachablePathNPEIns->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_76_DominatesUnreachable) {
const Tuple<RamDomain,2> key({{env0[6],0}});
auto range = rel_29_BasicBlockHead->equalRange_1(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{env1[1],env2[1]}});
auto range = rel_45_Dominates->equalRange_3(key,READ_OP_CONTEXT(rel_45_Dominates_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,1> tuple({{static_cast<RamDomain>(env0[6])}});
rel_77_UnreachablePathNPEIns->insert(tuple,READ_OP_CONTEXT(rel_77_UnreachablePathNPEIns_op_ctxt));
freqs[149]++;
}
freqs[150]++;
}
freqs[151]++;
}
freqs[152]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;UnreachablePathNPEIns;path_sensitivity/rules.dl [4:7-4:55];savetime;)_",iter, [&](){return rel_77_UnreachablePathNPEIns->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_77_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_69_ReachableNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_76_DominatesUnreachable->purge();
}();
/* END STRATUM 70 */
/* BEGIN STRATUM 71 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;hasNextIf;may-null/rules.dl [235:7-235:79];)_",iter, [&](){return rel_78_hasNextIf->size();});
SignalHandler::instance()->setMsg(R"_(hasNextIf(ifInsn,invokeInsn,var) :- 
   _IfVar(ifInsn,_,returnVar),
   _AssignReturnValue(invokeInsn,returnVar),
   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),
   "boolean hasNext()" contains sig.
in file may-null/rules.dl [240:1-244:36])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;hasNextIf;may-null/rules.dl [240:1-244:36];hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;)_",iter, [&](){return rel_78_hasNextIf->size();});
if (!rel_1_AssignReturnValue->empty()&&!rel_72_IfVar->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_72_IfVar->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt,rel_1_AssignReturnValue->createContext());
CREATE_OP_CONTEXT(rel_72_IfVar_op_ctxt,rel_72_IfVar->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
CREATE_OP_CONTEXT(rel_78_hasNextIf_op_ctxt,rel_78_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[2]}});
auto range = rel_1_AssignReturnValue->equalRange_2(key,READ_OP_CONTEXT(rel_1_AssignReturnValue_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,5> key({{env1[0],0,0,0,0}});
auto range = rel_4_VirtualMethodInvocation->equalRange_1(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env2 : range) {
if( (symTable.resolve(env2[2]).find(symTable.resolve(RamDomain(38))) != std::string::npos)) {
Tuple<RamDomain,3> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0]),static_cast<RamDomain>(env2[3])}});
rel_78_hasNextIf->insert(tuple,READ_OP_CONTEXT(rel_78_hasNextIf_op_ctxt));
}
freqs[153]++;
}
freqs[154]++;
}
freqs[155]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;hasNextIf;may-null/rules.dl [235:7-235:79];savetime;)_",iter, [&](){return rel_78_hasNextIf->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_72_IfVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_AssignReturnValue->purge();
}();
/* END STRATUM 71 */
/* BEGIN STRATUM 72 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NextInsideHasNext;may-null/rules.dl [237:7-237:60];)_",iter, [&](){return rel_79_NextInsideHasNext->size();});
SignalHandler::instance()->setMsg(R"_(NextInsideHasNext(nextInsn,var) :- 
   hasNextIf(ifInsn,_,invokeVar),
   Instruction_Next(ifInsn,falseBlockInsn),
   Dominates(falseBlockInsn,nextInsnHead),
   BasicBlockHead(nextInsn,nextInsnHead),
   IterNextInsn(nextInsn,var,invokeVar).
in file may-null/rules.dl [251:1-256:40])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NextInsideHasNext;may-null/rules.dl [251:1-256:40];NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;)_",iter, [&](){return rel_79_NextInsideHasNext->size();});
if (!rel_29_BasicBlockHead->empty()&&!rel_45_Dominates->empty()&&!rel_31_Instruction_Next->empty()&&!rel_6_IterNextInsn->empty()&&!rel_78_hasNextIf->empty()) [&](){
auto part = rel_78_hasNextIf->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt,rel_29_BasicBlockHead->createContext());
CREATE_OP_CONTEXT(rel_45_Dominates_op_ctxt,rel_45_Dominates->createContext());
CREATE_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt,rel_31_Instruction_Next->createContext());
CREATE_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt,rel_6_IterNextInsn->createContext());
CREATE_OP_CONTEXT(rel_79_NextInsideHasNext_op_ctxt,rel_79_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_78_hasNextIf_op_ctxt,rel_78_hasNextIf->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_31_Instruction_Next->equalRange_1(key,READ_OP_CONTEXT(rel_31_Instruction_Next_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,2> key({{env1[1],0}});
auto range = rel_45_Dominates->equalRange_1(key,READ_OP_CONTEXT(rel_45_Dominates_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,2> key({{0,env2[1]}});
auto range = rel_29_BasicBlockHead->equalRange_2(key,READ_OP_CONTEXT(rel_29_BasicBlockHead_op_ctxt));
for(const auto& env3 : range) {
const Tuple<RamDomain,3> key({{env3[0],0,env0[2]}});
auto range = rel_6_IterNextInsn->equalRange_5(key,READ_OP_CONTEXT(rel_6_IterNextInsn_op_ctxt));
for(const auto& env4 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env4[1])}});
rel_79_NextInsideHasNext->insert(tuple,READ_OP_CONTEXT(rel_79_NextInsideHasNext_op_ctxt));
freqs[156]++;
}
freqs[157]++;
}
freqs[158]++;
}
freqs[159]++;
}
freqs[160]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NextInsideHasNext;may-null/rules.dl [237:7-237:60];savetime;)_",iter, [&](){return rel_79_NextInsideHasNext->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_29_BasicBlockHead->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_45_Dominates->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_31_Instruction_Next->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_78_hasNextIf->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_6_IterNextInsn->purge();
}();
/* END STRATUM 72 */
/* BEGIN STRATUM 73 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MayNullPtr;may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_80_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"declaration") :- 
   Instruction_VarDeclaringMethod(insn,method,var).
in file may-null/rules.dl [98:1-99:51])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;may-null/rules.dl [98:1-99:51];MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;)_",iter, [&](){return rel_80_MayNullPtr->size();});
if (!rel_53_Instruction_VarDeclaringMethod->empty()) [&](){
auto part = rel_53_Instruction_VarDeclaringMethod->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_53_Instruction_VarDeclaringMethod_op_ctxt,rel_53_Instruction_VarDeclaringMethod->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(RamDomain(32))}});
rel_80_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
freqs[161]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,var,method,"assignNull") :- 
   _AssignNull(insn,_,var,method).
in file may-null/rules.dl [101:1-102:35])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MayNullPtr;may-null/rules.dl [101:1-102:35];MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;)_",iter, [&](){return rel_80_MayNullPtr->size();});
if (!rel_15_AssignNull->empty()) [&](){
auto part = rel_15_AssignNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_15_AssignNull_op_ctxt,rel_15_AssignNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(RamDomain(33))}});
rel_80_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
freqs[162]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_81_delta_MayNullPtr->insertAll(*rel_80_MayNullPtr);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MayNullPtr;may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignLocal(next,_,from,to,method).
in file may-null/rules.dl [132:1-137:3])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [132:1-137:3];MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_13_AssignLocal->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_13_AssignLocal_op_ctxt,rel_13_AssignLocal->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{env0[0],0,env0[1],0,env0[2]}});
auto range = rel_13_AssignLocal->equalRange_21(key,READ_OP_CONTEXT(rel_13_AssignLocal_op_ctxt));
for(const auto& env1 : range) {
if( (reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[163]++;
}
freqs[164]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,to,method,"Alias") :- 
   MayNullPtr(next,from,method,_),
   _AssignCast(_,_,from,to,_,method).
in file may-null/rules.dl [132:1-137:3])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [132:1-137:3];MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_10_AssignCast->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_10_AssignCast_op_ctxt,rel_10_AssignCast->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,env0[1],0,0,env0[2]}});
auto range = rel_10_AssignCast->equalRange_36(key,READ_OP_CONTEXT(rel_10_AssignCast_op_ctxt));
for(const auto& env1 : range) {
if( (reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env0[0],env1[3],env0[2],RamDomain(34)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[3]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(34))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[165]++;
}
freqs[166]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   MayPredecessorModuloThrow(insn,next),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method).
in file may-null/rules.dl [147:1-155:31])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [147:1-155:31];MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_55_MayPredecessorModuloThrow->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt,rel_55_MayPredecessorModuloThrow->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[8]++,rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty())) {
const Tuple<RamDomain,2> key({{env0[0],0}});
auto range = rel_55_MayPredecessorModuloThrow->equalRange_1(key,READ_OP_CONTEXT(rel_55_MayPredecessorModuloThrow_op_ctxt));
for(const auto& env1 : range) {
if( (((((reads[2]++,!rel_42_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt)))) && ((reads[9]++,!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt)))))) && ((reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[167]++;
}
}
freqs[168]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(next,var,method,"Transfer") :- 
   MayNullPtr(insn,var,method,_),
   SpecialIfEdge(insn,next,var),
   !LastUse(insn,_,var,method),
   !CannotBeNullBranch(next,var),
   !DefineVar(next,var,method).
in file may-null/rules.dl [147:1-155:31])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [147:1-155:31];MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_56_SpecialIfEdge->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt,rel_42_CannotBeNullBranch->createContext());
CREATE_OP_CONTEXT(rel_14_DefineVar_op_ctxt,rel_14_DefineVar->createContext());
CREATE_OP_CONTEXT(rel_26_LastUse_op_ctxt,rel_26_LastUse->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_56_SpecialIfEdge_op_ctxt,rel_56_SpecialIfEdge->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[8]++,rel_26_LastUse->equalRange_13(Tuple<RamDomain,4>({{env0[0],0,env0[1],env0[2]}}),READ_OP_CONTEXT(rel_26_LastUse_op_ctxt)).empty())) {
const Tuple<RamDomain,3> key({{env0[0],0,env0[1]}});
auto range = rel_56_SpecialIfEdge->equalRange_5(key,READ_OP_CONTEXT(rel_56_SpecialIfEdge_op_ctxt));
for(const auto& env1 : range) {
if( (((((reads[2]++,!rel_42_CannotBeNullBranch->contains(Tuple<RamDomain,2>({{env1[1],env0[1]}}),READ_OP_CONTEXT(rel_42_CannotBeNullBranch_op_ctxt)))) && ((reads[9]++,!rel_14_DefineVar->contains(Tuple<RamDomain,3>({{env1[1],env0[1],env0[2]}}),READ_OP_CONTEXT(rel_14_DefineVar_op_ctxt)))))) && ((reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env1[1],env0[1],env0[2],RamDomain(35)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env1[1]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(RamDomain(35))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[169]++;
}
}
freqs[170]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Return") :- 
   MayNullPtr(returnInsn,returnVar,invokedMethod,_),
   _Return(returnInsn,_,returnVar,invokedMethod),
   CallGraphEdge(_,insn,_,invokedMethod),
   AssignReturnValue_WithInvoke(insn,_,to,method),
   !NextInsideHasNext(insn,to).
in file may-null/rules.dl [157:1-162:30])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [157:1-162:30];MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_5_AssignReturnValue_WithInvoke->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_20_Return->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt,rel_5_AssignReturnValue_WithInvoke->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_79_NextInsideHasNext_op_ctxt,rel_79_NextInsideHasNext->createContext());
CREATE_OP_CONTEXT(rel_20_Return_op_ctxt,rel_20_Return->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],0,env0[1],env0[2]}});
auto range = rel_20_Return->equalRange_13(key,READ_OP_CONTEXT(rel_20_Return_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{0,0,0,env0[2]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{env2[1],0,0,0}});
auto range = rel_5_AssignReturnValue_WithInvoke->equalRange_1(key,READ_OP_CONTEXT(rel_5_AssignReturnValue_WithInvoke_op_ctxt));
for(const auto& env3 : range) {
if( (((reads[10]++,!rel_79_NextInsideHasNext->contains(Tuple<RamDomain,2>({{env2[1],env3[2]}}),READ_OP_CONTEXT(rel_79_NextInsideHasNext_op_ctxt)))) && ((reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env2[1],env3[2],env3[3],RamDomain(36)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env2[1]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env3[3]),static_cast<RamDomain>(RamDomain(36))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[171]++;
}
freqs[172]++;
}
freqs[173]++;
}
freqs[174]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(MayNullPtr(insn,to,method,"Parameter") :- 
   MayNullPtr(from_insn,from,_,_),
   _ActualParam(index,from_insn,from),
   CallGraphEdge(_,from_insn,_,method),
   Instruction_FormalParam(insn,method,to,index).
in file may-null/rules.dl [165:1-169:50])_");
{
	Logger logger(R"_(@t-recursive-rule;MayNullPtr;0;may-null/rules.dl [165:1-169:50];MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
if (!rel_81_delta_MayNullPtr->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_47_Instruction_FormalParam->empty()&&!rel_19_ActualParam->empty()) [&](){
auto part = rel_81_delta_MayNullPtr->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_81_delta_MayNullPtr_op_ctxt,rel_81_delta_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt,rel_82_new_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt,rel_47_Instruction_FormalParam->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,3> key({{0,env0[0],env0[1]}});
auto range = rel_19_ActualParam->equalRange_6(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,4> key({{0,env0[0],0,0}});
auto range = rel_30_CallGraphEdge->equalRange_2(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,4> key({{0,env2[3],0,env1[0]}});
auto range = rel_47_Instruction_FormalParam->equalRange_10(key,READ_OP_CONTEXT(rel_47_Instruction_FormalParam_op_ctxt));
for(const auto& env3 : range) {
if( (reads[7]++,!rel_80_MayNullPtr->contains(Tuple<RamDomain,4>({{env3[0],env3[2],env2[3],RamDomain(37)}}),READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt)))) {
Tuple<RamDomain,4> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2]),static_cast<RamDomain>(env2[3]),static_cast<RamDomain>(RamDomain(37))}});
rel_82_new_MayNullPtr->insert(tuple,READ_OP_CONTEXT(rel_82_new_MayNullPtr_op_ctxt));
}
freqs[175]++;
}
freqs[176]++;
}
freqs[177]++;
}
freqs[178]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_82_new_MayNullPtr->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MayNullPtr;may-null/rules.dl [1:7-1:85];)_",iter, [&](){return rel_82_new_MayNullPtr->size();});
rel_80_MayNullPtr->insertAll(*rel_82_new_MayNullPtr);
std::swap(rel_81_delta_MayNullPtr, rel_82_new_MayNullPtr);
rel_82_new_MayNullPtr->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_81_delta_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_82_new_MayNullPtr->purge();
{
	Logger logger(R"_(@t-relation-savetime;MayNullPtr;may-null/rules.dl [1:7-1:85];savetime;)_",iter, [&](){return rel_80_MayNullPtr->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_80_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_55_MayPredecessorModuloThrow->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_10_AssignCast->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_15_AssignNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_13_AssignLocal->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_20_Return->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_53_Instruction_VarDeclaringMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_47_Instruction_FormalParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_14_DefineVar->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_26_LastUse->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_5_AssignReturnValue_WithInvoke->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_56_SpecialIfEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_42_CannotBeNullBranch->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_79_NextInsideHasNext->purge();
}();
/* END STRATUM 73 */
/* BEGIN STRATUM 74 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;MethodDerefArg;may-null/rules.dl [273:7-273:59];)_",iter, [&](){return rel_83_MethodDerefArg->size();});
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   NullAt(method,_,_,var,insn),
   MayNullPtr(insn,var,method,_),
   isParam(index,var,method).
in file may-null/rules.dl [291:1-294:29])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;MethodDerefArg;may-null/rules.dl [291:1-294:29];MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;)_",iter, [&](){return rel_83_MethodDerefArg->size();});
if (!rel_80_MayNullPtr->empty()&&!rel_68_NullAt->empty()&&!rel_48_isParam->empty()) [&](){
auto part = rel_68_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_83_MethodDerefArg_op_ctxt,rel_83_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_68_NullAt_op_ctxt,rel_68_NullAt->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[4],env0[3],env0[0],0}});
auto range = rel_80_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,3> key({{0,env0[3],env0[0]}});
auto range = rel_48_isParam->equalRange_6(key,READ_OP_CONTEXT(rel_48_isParam_op_ctxt));
for(const auto& env2 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env2[0]),static_cast<RamDomain>(env0[0])}});
rel_83_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_83_MethodDerefArg_op_ctxt));
freqs[179]++;
}
freqs[180]++;
}
freqs[181]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
rel_84_delta_MethodDerefArg->insertAll(*rel_83_MethodDerefArg);
iter = 0;
for(;;) {
{
	Logger logger(R"_(@t-recursive-relation;MethodDerefArg;may-null/rules.dl [273:7-273:59];)_",iter, [&](){return rel_85_new_MethodDerefArg->size();});
SignalHandler::instance()->setMsg(R"_(MethodDerefArg(index,method) :- 
   MethodDerefArg(invokedIndex,invokedMethod),
   CallGraphEdge(_,from_insn,_,invokedMethod),
   _ActualParam(invokedIndex,from_insn,from),
   isParam(index,from,method).
in file may-null/rules.dl [296:1-300:30])_");
{
	Logger logger(R"_(@t-recursive-rule;MethodDerefArg;0;may-null/rules.dl [296:1-300:30];MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;)_",iter, [&](){return rel_85_new_MethodDerefArg->size();});
if (!rel_84_delta_MethodDerefArg->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_19_ActualParam->empty()&&!rel_48_isParam->empty()) [&](){
auto part = rel_84_delta_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_84_delta_MethodDerefArg_op_ctxt,rel_84_delta_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_85_new_MethodDerefArg_op_ctxt,rel_85_new_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_83_MethodDerefArg_op_ctxt,rel_83_MethodDerefArg->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_48_isParam_op_ctxt,rel_48_isParam->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,3> key({{0,env2[2],0}});
auto range = rel_48_isParam->equalRange_2(key,READ_OP_CONTEXT(rel_48_isParam_op_ctxt));
for(const auto& env3 : range) {
if( (reads[11]++,!rel_83_MethodDerefArg->contains(Tuple<RamDomain,2>({{env3[0],env3[2]}}),READ_OP_CONTEXT(rel_83_MethodDerefArg_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env3[0]),static_cast<RamDomain>(env3[2])}});
rel_85_new_MethodDerefArg->insert(tuple,READ_OP_CONTEXT(rel_85_new_MethodDerefArg_op_ctxt));
}
freqs[182]++;
}
freqs[183]++;
}
freqs[184]++;
}
freqs[185]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
if(rel_85_new_MethodDerefArg->empty()) break;
{
	Logger logger(R"_(@c-recursive-relation;MethodDerefArg;may-null/rules.dl [273:7-273:59];)_",iter, [&](){return rel_85_new_MethodDerefArg->size();});
rel_83_MethodDerefArg->insertAll(*rel_85_new_MethodDerefArg);
std::swap(rel_84_delta_MethodDerefArg, rel_85_new_MethodDerefArg);
rel_85_new_MethodDerefArg->purge();
}
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_84_delta_MethodDerefArg->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_85_new_MethodDerefArg->purge();
{
	Logger logger(R"_(@t-relation-savetime;MethodDerefArg;may-null/rules.dl [273:7-273:59];savetime;)_",iter, [&](){return rel_83_MethodDerefArg->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_68_NullAt->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_48_isParam->purge();
}();
/* END STRATUM 74 */
/* BEGIN STRATUM 75 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;JDKFunctionSummary;may-null/rules.dl [302:7-302:63];)_",iter, [&](){return rel_86_JDKFunctionSummary->size();});
SignalHandler::instance()->setMsg(R"_(JDKFunctionSummary(index,method) :- 
   MethodDerefArg(index,method),
   !ApplicationMethod(method).
in file may-null/rules.dl [304:1-306:28])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;JDKFunctionSummary;may-null/rules.dl [304:1-306:28];JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;)_",iter, [&](){return rel_86_JDKFunctionSummary->size();});
if (!rel_83_MethodDerefArg->empty()) [&](){
auto part = rel_83_MethodDerefArg->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_86_JDKFunctionSummary_op_ctxt,rel_86_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_83_MethodDerefArg_op_ctxt,rel_83_MethodDerefArg->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (reads[12]++,!rel_27_ApplicationMethod->contains(Tuple<RamDomain,1>({{env0[1]}}),READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt)))) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1])}});
rel_86_JDKFunctionSummary->insert(tuple,READ_OP_CONTEXT(rel_86_JDKFunctionSummary_op_ctxt));
}
freqs[186]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;JDKFunctionSummary;may-null/rules.dl [302:7-302:63];savetime;)_",iter, [&](){return rel_86_JDKFunctionSummary->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_86_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_83_MethodDerefArg->purge();
}();
/* END STRATUM 75 */
/* BEGIN STRATUM 76 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;NPEWithMayNull;may-null/rules.dl [259:7-259:115];)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(meth,index,file,line,type,var,insn) :- 
   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),
   MayNullPtr(insn,var,meth,_).
in file may-null/rules.dl [268:1-270:32])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;may-null/rules.dl [268:1-270:32];NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
if (!rel_80_MayNullPtr->empty()&&!rel_71_MinPathSensitiveNullAtLine->empty()) [&](){
auto part = rel_71_MinPathSensitiveNullAtLine->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_71_MinPathSensitiveNullAtLine_op_ctxt,rel_71_MinPathSensitiveNullAtLine->createContext());
CREATE_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt,rel_87_NPEWithMayNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[6],env0[5],env0[0],0}});
auto range = rel_80_MayNullPtr->equalRange_7(key,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env0[1]),static_cast<RamDomain>(env0[2]),static_cast<RamDomain>(env0[3]),static_cast<RamDomain>(env0[4]),static_cast<RamDomain>(env0[5]),static_cast<RamDomain>(env0[6])}});
rel_87_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt));
freqs[187]++;
}
freqs[188]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _StaticMethodInvocation(from_insn,index,_,method),
   InstructionLine(method,index,line,file).
in file may-null/rules.dl [308:1-321:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;may-null/rules.dl [308:1-321:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_57_InstructionLine->empty()&&!rel_86_JDKFunctionSummary->empty()&&!rel_80_MayNullPtr->empty()&&!rel_61_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_3_StaticMethodInvocation->empty()) [&](){
auto part = rel_86_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_57_InstructionLine_op_ctxt,rel_57_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_86_JDKFunctionSummary_op_ctxt,rel_86_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt,rel_87_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt,rel_3_StaticMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_61_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_80_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],0,0,env4[2]}});
auto range = rel_3_StaticMethodInvocation->equalRange_9(key,READ_OP_CONTEXT(rel_3_StaticMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_57_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_57_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(40)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_87_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt));
freqs[189]++;
}
freqs[190]++;
}
freqs[191]++;
}
freqs[192]++;
}
freqs[193]++;
}
freqs[194]++;
}
freqs[195]++;
}
freqs[196]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _VirtualMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file may-null/rules.dl [308:1-321:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;may-null/rules.dl [308:1-321:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_57_InstructionLine->empty()&&!rel_86_JDKFunctionSummary->empty()&&!rel_80_MayNullPtr->empty()&&!rel_61_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_4_VirtualMethodInvocation->empty()) [&](){
auto part = rel_86_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_57_InstructionLine_op_ctxt,rel_57_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_86_JDKFunctionSummary_op_ctxt,rel_86_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt,rel_87_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt,rel_4_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_61_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_80_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_4_VirtualMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_4_VirtualMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_57_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_57_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(40)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_87_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt));
freqs[197]++;
}
freqs[198]++;
}
freqs[199]++;
}
freqs[200]++;
}
freqs[201]++;
}
freqs[202]++;
}
freqs[203]++;
}
freqs[204]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
SignalHandler::instance()->setMsg(R"_(NPEWithMayNull(method,index,file,line,"JDK Function Summary",var,from_insn) :- 
   JDKFunctionSummary(paramIndex,jdkmethod),
   CallGraphEdge(_,from_insn,_,jdkmethod),
   _ActualParam(paramIndex,from_insn,var),
   VarPointsToNull(var),
   MayNullPtr(from_insn,var,method,_),
   ApplicationMethod(method),
   _SpecialMethodInvocation(from_insn,index,_,_,method),
   InstructionLine(method,index,line,file).
in file may-null/rules.dl [308:1-321:31])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;NPEWithMayNull;may-null/rules.dl [308:1-321:31];NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
if (!rel_27_ApplicationMethod->empty()&&!rel_30_CallGraphEdge->empty()&&!rel_57_InstructionLine->empty()&&!rel_86_JDKFunctionSummary->empty()&&!rel_80_MayNullPtr->empty()&&!rel_61_VarPointsToNull->empty()&&!rel_19_ActualParam->empty()&&!rel_2_SpecialMethodInvocation->empty()) [&](){
auto part = rel_86_JDKFunctionSummary->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt,rel_27_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt,rel_30_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_57_InstructionLine_op_ctxt,rel_57_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_86_JDKFunctionSummary_op_ctxt,rel_86_JDKFunctionSummary->createContext());
CREATE_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt,rel_80_MayNullPtr->createContext());
CREATE_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt,rel_87_NPEWithMayNull->createContext());
CREATE_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt,rel_61_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_19_ActualParam_op_ctxt,rel_19_ActualParam->createContext());
CREATE_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt,rel_2_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,0,env0[1]}});
auto range = rel_30_CallGraphEdge->equalRange_8(key,READ_OP_CONTEXT(rel_30_CallGraphEdge_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env0[0],env1[1],0}});
auto range = rel_19_ActualParam->equalRange_3(key,READ_OP_CONTEXT(rel_19_ActualParam_op_ctxt));
for(const auto& env2 : range) {
const Tuple<RamDomain,1> key({{env2[2]}});
auto range = rel_61_VarPointsToNull->equalRange_1(key,READ_OP_CONTEXT(rel_61_VarPointsToNull_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,4> key({{env1[1],env2[2],0,0}});
auto range = rel_80_MayNullPtr->equalRange_3(key,READ_OP_CONTEXT(rel_80_MayNullPtr_op_ctxt));
for(const auto& env4 : range) {
const Tuple<RamDomain,1> key({{env4[2]}});
auto range = rel_27_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_27_ApplicationMethod_op_ctxt));
if(!range.empty()) {
const Tuple<RamDomain,5> key({{env1[1],0,0,0,env4[2]}});
auto range = rel_2_SpecialMethodInvocation->equalRange_17(key,READ_OP_CONTEXT(rel_2_SpecialMethodInvocation_op_ctxt));
for(const auto& env6 : range) {
const Tuple<RamDomain,4> key({{env4[2],env6[1],0,0}});
auto range = rel_57_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_57_InstructionLine_op_ctxt));
for(const auto& env7 : range) {
Tuple<RamDomain,7> tuple({{static_cast<RamDomain>(env4[2]),static_cast<RamDomain>(env6[1]),static_cast<RamDomain>(env7[3]),static_cast<RamDomain>(env7[2]),static_cast<RamDomain>(RamDomain(40)),static_cast<RamDomain>(env2[2]),static_cast<RamDomain>(env1[1])}});
rel_87_NPEWithMayNull->insert(tuple,READ_OP_CONTEXT(rel_87_NPEWithMayNull_op_ctxt));
freqs[205]++;
}
freqs[206]++;
}
freqs[207]++;
}
freqs[208]++;
}
freqs[209]++;
}
freqs[210]++;
}
freqs[211]++;
}
freqs[212]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;NPEWithMayNull;may-null/rules.dl [259:7-259:115];savetime;)_",iter, [&](){return rel_87_NPEWithMayNull->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_57_InstructionLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_30_CallGraphEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_27_ApplicationMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_SpecialMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_VirtualMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_StaticMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_19_ActualParam->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_61_VarPointsToNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_80_MayNullPtr->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_71_MinPathSensitiveNullAtLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_86_JDKFunctionSummary->purge();
}();
/* END STRATUM 76 */
/* BEGIN STRATUM 77 */
[&]() {
{
	Logger logger(R"_(@t-relation-loadtime;JumpTarget;../declarations.dl [116:7-116:53];loadtime;)_",iter, [&](){return rel_88_JumpTarget->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_88_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;JumpTarget;../declarations.dl [116:7-116:53];)",rel_88_JumpTarget->size(),iter);}();
/* END STRATUM 77 */
/* BEGIN STRATUM 78 */
[&]() {
{
	Logger logger(R"_(@t-nonrecursive-relation;TrueBranch;may-null/rules.dl [185:7-185:63];)_",iter, [&](){return rel_89_TrueBranch->size();});
SignalHandler::instance()->setMsg(R"_(TrueBranch(ifIns,insn) :- 
   MayNull_IfInstruction(ifIns),
   JumpTarget(insn,ifIns).
in file may-null/rules.dl [199:1-201:25])_");
{
	Logger logger(R"_(@t-nonrecursive-rule;TrueBranch;may-null/rules.dl [199:1-201:25];TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;)_",iter, [&](){return rel_89_TrueBranch->size();});
if (!rel_88_JumpTarget->empty()&&!rel_33_MayNull_IfInstruction->empty()) [&](){
auto part = rel_33_MayNull_IfInstruction->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_88_JumpTarget_op_ctxt,rel_88_JumpTarget->createContext());
CREATE_OP_CONTEXT(rel_33_MayNull_IfInstruction_op_ctxt,rel_33_MayNull_IfInstruction->createContext());
CREATE_OP_CONTEXT(rel_89_TrueBranch_op_ctxt,rel_89_TrueBranch->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{0,env0[0]}});
auto range = rel_88_JumpTarget->equalRange_2(key,READ_OP_CONTEXT(rel_88_JumpTarget_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,2> tuple({{static_cast<RamDomain>(env0[0]),static_cast<RamDomain>(env1[0])}});
rel_89_TrueBranch->insert(tuple,READ_OP_CONTEXT(rel_89_TrueBranch_op_ctxt));
freqs[213]++;
}
freqs[214]++;
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();}
}
{
	Logger logger(R"_(@t-relation-savetime;TrueBranch;may-null/rules.dl [185:7-185:63];savetime;)_",iter, [&](){return rel_89_TrueBranch->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_88_JumpTarget->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_33_MayNull_IfInstruction->purge();
}();
/* END STRATUM 78 */
/* BEGIN STRATUM 79 */
[&]() {
ProfileEventSingleton::instance().makeQuantityEvent( R"(@n-nonrecursive-relation;ReachableNullAt;../declarations.dl [164:7-164:92];)",rel_90_ReachableNullAt->size(),iter);{
	Logger logger(R"_(@t-relation-savetime;ReachableNullAt;../declarations.dl [164:7-164:92];savetime;)_",iter, [&](){return rel_90_ReachableNullAt->size();});
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
}();
/* END STRATUM 79 */
}
ProfileEventSingleton::instance().stopTimer();
dumpFreqs();

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_AssignReturnValue:\n";
rel_1_AssignReturnValue->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_SpecialMethodInvocation:\n";
rel_2_SpecialMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_StaticMethodInvocation:\n";
rel_3_StaticMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_VirtualMethodInvocation:\n";
rel_4_VirtualMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_AssignReturnValue_WithInvoke:\n";
rel_5_AssignReturnValue_WithInvoke->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_IterNextInsn:\n";
rel_6_IterNextInsn->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_Primitive:\n";
rel_7_Primitive->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_Var_Type:\n";
rel_8_Var_Type->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_RefTypeVar:\n";
rel_9_RefTypeVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_AssignCast:\n";
rel_10_AssignCast->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_11_AssignCastNull:\n";
rel_11_AssignCastNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_12_AssignHeapAllocation:\n";
rel_12_AssignHeapAllocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_13_AssignLocal:\n";
rel_13_AssignLocal->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_14_DefineVar:\n";
rel_14_DefineVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_15_AssignNull:\n";
rel_15_AssignNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_16_LoadInstanceField:\n";
rel_16_LoadInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_17_LoadStaticField:\n";
rel_17_LoadStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_18_VarDef:\n";
rel_18_VarDef->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_19_ActualParam:\n";
rel_19_ActualParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_20_Return:\n";
rel_20_Return->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_21_StoreArrayIndex:\n";
rel_21_StoreArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_22_StoreInstanceField:\n";
rel_22_StoreInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_23_StoreStaticField:\n";
rel_23_StoreStaticField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_24_AllUse:\n";
rel_24_AllUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_25_FirstUse:\n";
rel_25_FirstUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_26_LastUse:\n";
rel_26_LastUse->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_27_ApplicationMethod:\n";
rel_27_ApplicationMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_28_AssignMayNull:\n";
rel_28_AssignMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_29_BasicBlockHead:\n";
rel_29_BasicBlockHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_30_CallGraphEdge:\n";
rel_30_CallGraphEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_31_Instruction_Next:\n";
rel_31_Instruction_Next->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_32_IfNull:\n";
rel_32_IfNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_33_MayNull_IfInstruction:\n";
rel_33_MayNull_IfInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_34_FalseBranch:\n";
rel_34_FalseBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_35_OperatorAt:\n";
rel_35_OperatorAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_36_MayNull_IfInstructionsCond:\n";
rel_36_MayNull_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_37_PhiNodeHead:\n";
rel_37_PhiNodeHead->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_38_PhiNodeHeadVar:\n";
rel_38_PhiNodeHeadVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_39_CanBeNullBranch:\n";
rel_39_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_40_delta_CanBeNullBranch:\n";
rel_40_delta_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_41_new_CanBeNullBranch:\n";
rel_41_new_CanBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_42_CannotBeNullBranch:\n";
rel_42_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_43_delta_CannotBeNullBranch:\n";
rel_43_delta_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_44_new_CannotBeNullBranch:\n";
rel_44_new_CannotBeNullBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_45_Dominates:\n";
rel_45_Dominates->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_46_FormalParam:\n";
rel_46_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_47_Instruction_FormalParam:\n";
rel_47_Instruction_FormalParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_48_isParam:\n";
rel_48_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_49_delta_isParam:\n";
rel_49_delta_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_50_new_isParam:\n";
rel_50_new_isParam->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_51_ThisVar:\n";
rel_51_ThisVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_52_Var_DeclaringMethod:\n";
rel_52_Var_DeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_53_Instruction_VarDeclaringMethod:\n";
rel_53_Instruction_VarDeclaringMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_54_Method_FirstInstruction:\n";
rel_54_Method_FirstInstruction->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_55_MayPredecessorModuloThrow:\n";
rel_55_MayPredecessorModuloThrow->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_56_SpecialIfEdge:\n";
rel_56_SpecialIfEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_57_InstructionLine:\n";
rel_57_InstructionLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_58_VarPointsTo:\n";
rel_58_VarPointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_59_VarMayPointToNull:\n";
rel_59_VarMayPointToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_60_VarMayNotPointToNull:\n";
rel_60_VarMayNotPointToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_61_VarPointsToNull:\n";
rel_61_VarPointsToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_62_AssignBinop:\n";
rel_62_AssignBinop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_63_AssignOperFrom:\n";
rel_63_AssignOperFrom->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_64_AssignUnop:\n";
rel_64_AssignUnop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_65_EnterMonitor:\n";
rel_65_EnterMonitor->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_66_LoadArrayIndex:\n";
rel_66_LoadArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_67_ThrowNull:\n";
rel_67_ThrowNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_68_NullAt:\n";
rel_68_NullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_69_ReachableNullAtLine:\n";
rel_69_ReachableNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_70_PathSensitiveNullAtLine:\n";
rel_70_PathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_71_MinPathSensitiveNullAtLine:\n";
rel_71_MinPathSensitiveNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_72_IfVar:\n";
rel_72_IfVar->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_73_IfInstructions:\n";
rel_73_IfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_74_IfInstructionsCond:\n";
rel_74_IfInstructionsCond->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_75_TrueIfInstructions:\n";
rel_75_TrueIfInstructions->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_76_DominatesUnreachable:\n";
rel_76_DominatesUnreachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_77_UnreachablePathNPEIns:\n";
rel_77_UnreachablePathNPEIns->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_78_hasNextIf:\n";
rel_78_hasNextIf->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_79_NextInsideHasNext:\n";
rel_79_NextInsideHasNext->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_80_MayNullPtr:\n";
rel_80_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_81_delta_MayNullPtr:\n";
rel_81_delta_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_82_new_MayNullPtr:\n";
rel_82_new_MayNullPtr->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_83_MethodDerefArg:\n";
rel_83_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_84_delta_MethodDerefArg:\n";
rel_84_delta_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_85_new_MethodDerefArg:\n";
rel_85_new_MethodDerefArg->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_86_JDKFunctionSummary:\n";
rel_86_JDKFunctionSummary->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_87_NPEWithMayNull:\n";
rel_87_NPEWithMayNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_88_JumpTarget:\n";
rel_88_JumpTarget->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_89_TrueBranch:\n";
rel_89_TrueBranch->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_90_ReachableNullAt:\n";
rel_90_ReachableNullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction(".", ".", stratumIndex, false); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction(inputDirectory, outputDirectory, stratumIndex, true);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AssignReturnValue_WithInvoke.csv"},{"name","AssignReturnValue_WithInvoke"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\treturnVar\tvar"},{"filename","./IterNextInsn.csv"},{"name","IterNextInsn"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var"},{"filename","./RefTypeVar.csv"},{"name","RefTypeVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./DefineVar.csv"},{"name","DefineVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./VarDef.csv"},{"name","VarDef"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./AllUse.csv"},{"name","AllUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./FirstUse.csv"},{"name","FirstUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tindex\tvar\tmethod"},{"filename","./LastUse.csv"},{"name","LastUse"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod"},{"filename","./AssignMayNull.csv"},{"name","AssignMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?i\t?next"},{"filename","./Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./MayNull_IfInstruction.csv"},{"name","MayNull_IfInstruction"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./FalseBranch.csv"},{"name","FalseBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./MayNull_IfInstructionsCond.csv"},{"name","MayNull_IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","?insn\t?headInsn"},{"filename","./PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","var\theadVar"},{"filename","./PhiNodeHeadVar.csv"},{"name","PhiNodeHeadVar"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CanBeNullBranch.csv"},{"name","CanBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./CannotBeNullBranch.csv"},{"name","CannotBeNullBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar\tindex"},{"filename","./Instruction_FormalParam.csv"},{"name","Instruction_FormalParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tvar\tmethod"},{"filename","./isParam.csv"},{"name","isParam"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_48_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tmethod\tvar"},{"filename","./Instruction_VarDeclaringMethod.csv"},{"name","Instruction_VarDeclaringMethod"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","v"},{"filename","./VarPointsToNull.csv"},{"name","VarPointsToNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_61_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./NullAt.csv"},{"name","NullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./ReachableNullAtLine.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"filename","./PathSensitiveNullAtLine.csv"},{"name","PathSensitiveNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_70_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ins\tifIns"},{"filename","./IfInstructions.csv"},{"name","IfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tleft\tright\topt"},{"filename","./IfInstructionsCond.csv"},{"name","IfInstructionsCond"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_74_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./TrueIfInstructions.csv"},{"name","TrueIfInstructions"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tins"},{"filename","./DominatesUnreachable.csv"},{"name","DominatesUnreachable"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns"},{"filename","./UnreachablePathNPEIns.csv"},{"name","UnreachablePathNPEIns"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_77_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifInsn\tinvokeInsn\tvar"},{"filename","./hasNextIf.csv"},{"name","hasNextIf"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar"},{"filename","./NextInsideHasNext.csv"},{"name","NextInsideHasNext"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","insn\tvar\tmethod\treason"},{"filename","./MayNullPtr.csv"},{"name","MayNullPtr"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_80_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./MethodDerefArg.csv"},{"name","MethodDerefArg"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","index\tmethod"},{"filename","./JDKFunctionSummary.csv"},{"name","JDKFunctionSummary"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_86_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype\tv\tinsn"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","NPEWithMayNull"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","ifIns\tinsn"},{"filename","./TrueBranch.csv"},{"name","TrueBranch"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\ttype\tv\tinsn"},{"filename","./ReachableNullAt.csv"},{"name","ReachableNullAt"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
private:
void dumpFreqs() {
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;0;)_", freqs[30],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;VarDef(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   VarDef(insn,index,var,method).;1;)_", freqs[29],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;AssignReturnValue_WithInvoke(insn,index,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;2;)_", freqs[47],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;0;)_", freqs[49],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   AssignReturnValue_WithInvoke(insn,index,_,method).;1;)_", freqs[48],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;0;)_", freqs[58],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;1;)_", freqs[57],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_SpecialMethodInvocation(insn,index,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;2;)_", freqs[56],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;0;)_", freqs[52],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;1;)_", freqs[51],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_StaticMethodInvocation(insn,index,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;2;)_", freqs[50],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;0;)_", freqs[55],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_ActualParam(_,insn,var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;1;)_", freqs[54],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_VirtualMethodInvocation(insn,index,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _ActualParam(_,insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;2;)_", freqs[53],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;0;)_", freqs[34],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;_AssignCast(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,var,_,_,method).;1;)_", freqs[33],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;0;)_", freqs[32],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;_AssignLocal(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,var,_,method).;1;)_", freqs[31],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;0;)_", freqs[46],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;_Return(insn,index,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _Return(insn,index,var,method).;1;)_", freqs[45],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;0;)_", freqs[36],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;_SpecialMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _SpecialMethodInvocation(insn,index,_,var,method).;1;)_", freqs[35],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;0;)_", freqs[40],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;_StoreArrayIndex(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreArrayIndex(insn,index,var,_,method).;1;)_", freqs[39],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;0;)_", freqs[42],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;_StoreInstanceField(insn,index,var,_,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreInstanceField(insn,index,var,_,_,method).;1;)_", freqs[41],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;0;)_", freqs[44],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;_StoreStaticField(insn,index,var,_,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _StoreStaticField(insn,index,var,_,method).;1;)_", freqs[43],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;RefTypeVar(var);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;0;)_", freqs[38],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AllUse;0;AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;_VirtualMethodInvocation(insn,index,_,var,method);AllUse(insn,index,var,method) :- \n   RefTypeVar(var),\n   _VirtualMethodInvocation(insn,index,_,var,method).;1;)_", freqs[37],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;0;)_", freqs[5],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;_SpecialMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _SpecialMethodInvocation(insn,index,_,_,method).;1;)_", freqs[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;0;)_", freqs[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;_StaticMethodInvocation(insn,index,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _StaticMethodInvocation(insn,index,_,method).;1;)_", freqs[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_AssignReturnValue(insn,var);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;0;)_", freqs[3],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;AssignReturnValue_WithInvoke;0;AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;_VirtualMethodInvocation(insn,index,_,_,method);AssignReturnValue_WithInvoke(insn,index,var,method) :- \n   _AssignReturnValue(insn,var),\n   _VirtualMethodInvocation(insn,index,_,_,method).;1;)_", freqs[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;@delta_CanBeNullBranch(insn,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[73],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;@new_CanBeNullBranch(insn,var) :- \n   @delta_CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CanBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CanBeNullBranch(insn,var) :- \n   CanBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[72],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CanBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\").;0;)_", freqs[69],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[70],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CanBeNullBranch;0;CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CanBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[71],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;@delta_CannotBeNullBranch(insn,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[78],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;@new_CannotBeNullBranch(insn,var) :- \n   @delta_CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar),\n   !CannotBeNullBranch(insn,var).;PhiNodeHeadVar(var,headVar);CannotBeNullBranch(insn,var) :- \n   CannotBeNullBranch(insn,headVar),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[77],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\");CannotBeNullBranch(ifIns,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"!=\").;0;)_", freqs[74],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;FalseBranch(ifIns,insn);CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;1;)_", freqs[75],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;CannotBeNullBranch;0;CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\");CannotBeNullBranch(insn,var) :- \n   MayNull_IfInstructionsCond(ifIns,var,\"null\",\"==\"),\n   FalseBranch(ifIns,insn).;0;)_", freqs[76],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;AssignReturnValue_WithInvoke(insn,_,to,method);DefineVar(insn,to,method) :- \n   AssignReturnValue_WithInvoke(insn,_,to,method).;0;)_", freqs[12],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;_AssignCast(insn,_,_,to,_,method);DefineVar(insn,to,method) :- \n   _AssignCast(insn,_,_,to,_,method).;0;)_", freqs[11],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;_AssignLocal(insn,_,_,to,method);DefineVar(insn,to,method) :- \n   _AssignLocal(insn,_,_,to,method).;0;)_", freqs[10],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DefineVar;0;DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;_AssignHeapAllocation(insn,_,_,var,method,_);DefineVar(insn,var,method) :- \n   _AssignHeapAllocation(insn,_,_,var,method,_).;0;)_", freqs[9],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;Instruction_Next(ifIns,ins);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;1;)_", freqs[147],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;DominatesUnreachable;0;DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;TrueIfInstructions(ifIns);DominatesUnreachable(ifIns,ins) :- \n   TrueIfInstructions(ifIns),\n   Instruction_Next(ifIns,ins).;0;)_", freqs[148],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;Instruction_Next(ifIns,insn);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;1;)_", freqs[62],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FalseBranch;0;FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;MayNull_IfInstruction(ifIns);FalseBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   Instruction_Next(ifIns,insn).;0;)_", freqs[63],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;FirstUse;0;FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;AllUse(insn,last,var,method);FirstUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = min  I0 : AllUse(_, I0,var,method).;0;)_", freqs[59],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;BasicBlockHead(ins,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;1;)_", freqs[130],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;MayPredecessorModuloThrow(ifIns,headIns);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;2;)_", freqs[129],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   BasicBlockHead(ins,headIns),\n   MayPredecessorModuloThrow(ifIns,headIns).;0;)_", freqs[131],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;ReachableNullAtLine(_,_,_,_,_,var,ins);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;0;)_", freqs[133],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructions;0;IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;_IfVar(ifIns,_,var);IfInstructions(ins,ifIns) :- \n   ReachableNullAtLine(_,_,_,_,_,var,ins),\n   _IfVar(ifIns,_,var).;1;)_", freqs[132],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;0;)_", freqs[140],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;VarPointsToNull(varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;3;)_", freqs[137],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;2;)_", freqs[138],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   VarPointsToNull(varLeft).;1;)_", freqs[139],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;IfInstructions(_,ifIns);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;0;)_", freqs[136],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_IfNull(ifIns,_,varLeft);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;2;)_", freqs[134],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IfInstructionsCond;0;IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;_OperatorAt(ifIns,opt);IfInstructionsCond(ifIns,\"null\",\"null\",opt) :- \n   IfInstructions(_,ifIns),\n   _OperatorAt(ifIns,opt),\n   _IfNull(ifIns,_,varLeft),\n   varLeft = \"null\".;1;)_", freqs[135],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_FormalParam;0;Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;_FormalParam(index,method,param);Instruction_FormalParam(cat(param,\"/map_param/\"),method,param,index) :- \n   _FormalParam(index,method,param).;0;)_", freqs[79],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;RefTypeVar(var);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;0;)_", freqs[88],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;Instruction_VarDeclaringMethod;0;Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;_Var_DeclaringMethod(var,method);Instruction_VarDeclaringMethod(cat(var,\"/var_declaration/\"),method,var) :- \n   RefTypeVar(var),\n   _Var_DeclaringMethod(var,method),\n   !_FormalParam(_,method,var),\n   !_ThisVar(method,var).;1;)_", freqs[87],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IterNextInsn;0;IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;_AssignReturnValue(insn,returnVar);IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;0;)_", freqs[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;IterNextInsn;0;IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;_VirtualMethodInvocation(insn,_,sig,var,_);IterNextInsn(insn,returnVar,var) :- \n   _AssignReturnValue(insn,returnVar),\n   _VirtualMethodInvocation(insn,_,sig,var,_),\n   \"next()\" contains sig.;1;)_", freqs[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;JDKFunctionSummary;0;JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;MethodDerefArg(index,method);JDKFunctionSummary(index,method) :- \n   MethodDerefArg(index,method),\n   !ApplicationMethod(method).;0;)_", freqs[186],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;LastUse;0;LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;AllUse(insn,last,var,method);LastUse(insn,last,var,method) :- \n   AllUse(insn,last,var,method),\n   last = max  I1 : AllUse(_, I1,var,method).;0;)_", freqs[60],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;@delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;0;)_", freqs[178],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;2;)_", freqs[176],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;Instruction_FormalParam(insn,method,to,index);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;3;)_", freqs[175],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Parameter\") :- \n   @delta_MayNullPtr(from_insn,from, _unnamed_var1, _unnamed_var2),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge( _unnamed_var3,from_insn, _unnamed_var4,method),\n   Instruction_FormalParam(insn,method,to,index),\n   !MayNullPtr(insn,to,method,\"Parameter\").;_ActualParam(index,from_insn,from);MayNullPtr(insn,to,method,\"Parameter\") :- \n   MayNullPtr(from_insn,from,_,_),\n   _ActualParam(index,from_insn,from),\n   CallGraphEdge(_,from_insn,_,method),\n   Instruction_FormalParam(insn,method,to,index).;1;)_", freqs[177],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;@delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;0;)_", freqs[174],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;3;)_", freqs[171],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;2;)_", freqs[172],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(insn,to,method,\"Return\") :- \n   @delta_MayNullPtr(returnInsn,returnVar,invokedMethod, _unnamed_var1),\n   _Return(returnInsn, _unnamed_var2,returnVar,invokedMethod),\n   CallGraphEdge( _unnamed_var3,insn, _unnamed_var4,invokedMethod),\n   AssignReturnValue_WithInvoke(insn, _unnamed_var5,to,method),\n   !NextInsideHasNext(insn,to),\n   !MayNullPtr(insn,to,method,\"Return\").;_Return(returnInsn, _unnamed_var2,returnVar,invokedMethod);MayNullPtr(insn,to,method,\"Return\") :- \n   MayNullPtr(returnInsn,returnVar,invokedMethod,_),\n   _Return(returnInsn,_,returnVar,invokedMethod),\n   CallGraphEdge(_,insn,_,invokedMethod),\n   AssignReturnValue_WithInvoke(insn,_,to,method),\n   !NextInsideHasNext(insn,to).;1;)_", freqs[173],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method),\n   !MayNullPtr(next,to,method,\"Alias\").;@delta_MayNullPtr(next,from,method, _unnamed_var1);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;0;)_", freqs[166],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method),\n   !MayNullPtr(next,to,method,\"Alias\").;_AssignCast( _unnamed_var2, _unnamed_var3,from,to, _unnamed_var4,method);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignCast(_,_,from,to,_,method).;1;)_", freqs[165],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignLocal(next, _unnamed_var2,from,to,method),\n   !MayNullPtr(next,to,method,\"Alias\").;@delta_MayNullPtr(next,from,method, _unnamed_var1);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;0;)_", freqs[164],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,to,method,\"Alias\") :- \n   @delta_MayNullPtr(next,from,method, _unnamed_var1),\n   _AssignLocal(next, _unnamed_var2,from,to,method),\n   !MayNullPtr(next,to,method,\"Alias\").;_AssignLocal(next, _unnamed_var2,from,to,method);MayNullPtr(next,to,method,\"Alias\") :- \n   MayNullPtr(next,from,method,_),\n   _AssignLocal(next,_,from,to,method).;1;)_", freqs[163],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method,\"Transfer\").;@delta_MayNullPtr(insn,var,method, _unnamed_var1);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;0;)_", freqs[168],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method,\"Transfer\").;MayPredecessorModuloThrow(insn,next);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   MayPredecessorModuloThrow(insn,next),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;1;)_", freqs[167],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method,\"Transfer\").;@delta_MayNullPtr(insn,var,method, _unnamed_var1);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;0;)_", freqs[170],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;@new_MayNullPtr(next,var,method,\"Transfer\") :- \n   @delta_MayNullPtr(insn,var,method, _unnamed_var1),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method),\n   !MayNullPtr(next,var,method,\"Transfer\").;SpecialIfEdge(insn,next,var);MayNullPtr(next,var,method,\"Transfer\") :- \n   MayNullPtr(insn,var,method,_),\n   SpecialIfEdge(insn,next,var),\n   !LastUse(insn,_,var,method),\n   !CannotBeNullBranch(next,var),\n   !DefineVar(next,var,method).;1;)_", freqs[169],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;_AssignNull(insn,_,var,method);MayNullPtr(insn,var,method,\"assignNull\") :- \n   _AssignNull(insn,_,var,method).;0;)_", freqs[162],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNullPtr;0;MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;Instruction_VarDeclaringMethod(insn,method,var);MayNullPtr(insn,var,method,\"declaration\") :- \n   Instruction_VarDeclaringMethod(insn,method,var).;0;)_", freqs[161],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstruction;0;MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;_IfNull(ifIns,_,_);MayNull_IfInstruction(ifIns) :- \n   _IfNull(ifIns,_,_).;0;)_", freqs[61],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_IfNull(ifIns,_,left);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;0;)_", freqs[65],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayNull_IfInstructionsCond;0;MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;_OperatorAt(ifIns,opt);MayNull_IfInstructionsCond(ifIns,left,\"null\",opt) :- \n   _IfNull(ifIns,_,left),\n   _OperatorAt(ifIns,opt).;1;)_", freqs[64],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Instruction_FormalParam(insn,method,_,_);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;0;)_", freqs[92],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;Method_FirstInstruction(method,next);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_FormalParam(insn,method,_,_),\n   Method_FirstInstruction(method,next).;1;)_", freqs[91],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;FirstUse(next,_,var,method);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;1;)_", freqs[89],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MayPredecessorModuloThrow;0;MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;Instruction_VarDeclaringMethod(insn,method,var);MayPredecessorModuloThrow(insn,next) :- \n   Instruction_VarDeclaringMethod(insn,method,var),\n   FirstUse(next,_,var,method).;0;)_", freqs[90],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;@delta_MethodDerefArg(invokedIndex,invokedMethod);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;0;)_", freqs[185],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;1;)_", freqs[184],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;_ActualParam(invokedIndex,from_insn,from);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;2;)_", freqs[183],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;@new_MethodDerefArg(index,method) :- \n   @delta_MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge( _unnamed_var1,from_insn, _unnamed_var2,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method),\n   !MethodDerefArg(index,method).;isParam(index,from,method);MethodDerefArg(index,method) :- \n   MethodDerefArg(invokedIndex,invokedMethod),\n   CallGraphEdge(_,from_insn,_,invokedMethod),\n   _ActualParam(invokedIndex,from_insn,from),\n   isParam(index,from,method).;3;)_", freqs[182],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;MayNullPtr(insn,var,method,_);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;1;)_", freqs[180],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;NullAt(method,_,_,var,insn);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;0;)_", freqs[181],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MethodDerefArg;0;MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;isParam(index,var,method);MethodDerefArg(index,method) :- \n   NullAt(method,_,_,var,insn),\n   MayNullPtr(insn,var,method,_),\n   isParam(index,var,method).;2;)_", freqs[179],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;MinPathSensitiveNullAtLine;0;MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;PathSensitiveNullAtLine(meth,index,file,line,type,var,insn);MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn) :- \n   PathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   index = min  I2 : PathSensitiveNullAtLine(meth, I2,file,_,_,var,_).;0;)_", freqs[128],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;MayNullPtr(insn,var,meth,_);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;1;)_", freqs[187],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn);NPEWithMayNull(meth,index,file,line,type,var,insn) :- \n   MinPathSensitiveNullAtLine(meth,index,file,line,type,var,insn),\n   MayNullPtr(insn,var,meth,_).;0;)_", freqs[188],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[207],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[211],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[205],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[212],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[208],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[209],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[210],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_SpecialMethodInvocation(from_insn,index,_,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _SpecialMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[206],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[191],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[195],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[189],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[196],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[192],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[193],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[194],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;_StaticMethodInvocation(from_insn,index,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _StaticMethodInvocation(from_insn,index,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[190],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;ApplicationMethod(method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;5;)_", freqs[199],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;CallGraphEdge(_,from_insn,_,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;1;)_", freqs[203],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;InstructionLine(method,index,line,file);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;7;)_", freqs[197],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;JDKFunctionSummary(paramIndex,jdkmethod);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;0;)_", freqs[204],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;MayNullPtr(from_insn,var,method,_);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;4;)_", freqs[200],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;VarPointsToNull(var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;3;)_", freqs[201],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_ActualParam(paramIndex,from_insn,var);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;2;)_", freqs[202],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NPEWithMayNull;0;NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;_VirtualMethodInvocation(from_insn,index,_,_,method);NPEWithMayNull(method,index,file,line,\"JDK Function Summary\",var,from_insn) :- \n   JDKFunctionSummary(paramIndex,jdkmethod),\n   CallGraphEdge(_,from_insn,_,jdkmethod),\n   _ActualParam(paramIndex,from_insn,var),\n   VarPointsToNull(var),\n   MayNullPtr(from_insn,var,method,_),\n   ApplicationMethod(method),\n   _VirtualMethodInvocation(from_insn,index,_,_,method),\n   InstructionLine(method,index,line,file).;6;)_", freqs[198],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;BasicBlockHead(nextInsn,nextInsnHead);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;3;)_", freqs[157],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;Dominates(falseBlockInsn,nextInsnHead);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;2;)_", freqs[158],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;Instruction_Next(ifInsn,falseBlockInsn);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;1;)_", freqs[159],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;IterNextInsn(nextInsn,var,invokeVar);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;4;)_", freqs[156],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NextInsideHasNext;0;NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;hasNextIf(ifInsn,_,invokeVar);NextInsideHasNext(nextInsn,var) :- \n   hasNextIf(ifInsn,_,invokeVar),\n   Instruction_Next(ifInsn,falseBlockInsn),\n   Dominates(falseBlockInsn,nextInsnHead),\n   BasicBlockHead(nextInsn,nextInsnHead),\n   IterNextInsn(nextInsn,var,invokeVar).;0;)_", freqs[160],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;VarPointsToNull(var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;0;)_", freqs[120],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignBinop(insn,index,_,meth);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;1;)_", freqs[119],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignOperFrom(insn,var);NullAt(meth,index,\"Binary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignBinop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;2;)_", freqs[118],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;0;)_", freqs[123],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;_EnterMonitor(insn,index,var,meth);NullAt(meth,index,\"Enter Monitor (Synchronized)\",var,insn) :- \n   VarPointsToNull(var),\n   _EnterMonitor(insn,index,var,meth).;1;)_", freqs[122],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;0;)_", freqs[104],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;_LoadArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Load Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadArrayIndex(insn,index,_,var,meth).;1;)_", freqs[103],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[110],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;_LoadInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Load Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _LoadInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[109],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[114],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;_SpecialMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Special Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _SpecialMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[113],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;0;)_", freqs[106],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;_StoreArrayIndex(insn,index,_,var,meth);NullAt(meth,index,\"Store Array Index\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreArrayIndex(insn,index,_,var,meth).;1;)_", freqs[105],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;VarPointsToNull(var);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;0;)_", freqs[108],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;_StoreInstanceField(insn,index,_,var,_,meth);NullAt(meth,index,\"Store Instance Field\",var,insn) :- \n   VarPointsToNull(var),\n   _StoreInstanceField(insn,index,_,var,_,meth).;1;)_", freqs[107],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;CallGraphEdge(_,a,_,b);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;0;)_", freqs[102],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;_SpecialMethodInvocation(a,index,b,_,meth);NullAt(meth,index,\"Throw NullPointerException\",\"throw NPE\",a) :- \n   CallGraphEdge(_,a,_,b),\n   _SpecialMethodInvocation(a,index,b,_,meth),\n   \"java.lang.NullPointerException\" contains a.;1;)_", freqs[101],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;_ThrowNull(insn,index,meth);NullAt(meth,index,\"Throw Null\",\"throw null\",insn) :- \n   _ThrowNull(insn,index,meth).;0;)_", freqs[121],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;VarPointsToNull(var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;0;)_", freqs[117],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignOperFrom(insn,var);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;2;)_", freqs[115],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;_AssignUnop(insn,index,_,meth);NullAt(meth,index,\"Unary Operator\",var,insn) :- \n   VarPointsToNull(var),\n   _AssignUnop(insn,index,_,meth),\n   _AssignOperFrom(insn,var).;1;)_", freqs[116],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;VarPointsToNull(var);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;0;)_", freqs[112],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;NullAt;0;NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;_VirtualMethodInvocation(insn,index,_,var,meth);NullAt(meth,index,\"Virtual Method Invocation\",var,insn) :- \n   VarPointsToNull(var),\n   _VirtualMethodInvocation(insn,index,_,var,meth).;1;)_", freqs[111],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PathSensitiveNullAtLine;0;PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins).;ReachableNullAtLine(meth,index,file,line,type,var,ins);PathSensitiveNullAtLine(meth,index,file,line,type,var,ins) :- \n   ReachableNullAtLine(meth,index,file,line,type,var,ins).;0;)_", freqs[127],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;PhiNodeHead(insn,headInsn);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;0;)_", freqs[68],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(headInsn,_,headVar,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;2;)_", freqs[66],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;PhiNodeHeadVar;0;PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;_AssignLocal(insn,_,var,_,_);PhiNodeHeadVar(var,headVar) :- \n   PhiNodeHead(insn,headInsn),\n   _AssignLocal(insn,_,var,_,_),\n   _AssignLocal(headInsn,_,headVar,_,_).;1;)_", freqs[67],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;ApplicationMethod(meth);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;1;)_", freqs[125],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;InstructionLine(meth,index,line,file);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;2;)_", freqs[124],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;ReachableNullAtLine;0;ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;NullAt(meth,index,type,var,insn);ReachableNullAtLine(meth,index,file,line,type,var,insn) :- \n   NullAt(meth,index,type,var,insn),\n   ApplicationMethod(meth),\n   InstructionLine(meth,index,line,file).;0;)_", freqs[126],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;RefTypeVar;0;RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;_Var_Type(var,type);RefTypeVar(var) :- \n   _Var_Type(var,type),\n   !Primitive(type).;0;)_", freqs[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;FalseBranch(ifIns,next);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;0;)_", freqs[95],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;MayNull_IfInstructionsCond(ifIns,var,\"null\",_);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;2;)_", freqs[93],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;SpecialIfEdge;0;SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;MayPredecessorModuloThrow(beforeIf,ifIns);SpecialIfEdge(beforeIf,next,var) :- \n   FalseBranch(ifIns,next),\n   MayPredecessorModuloThrow(beforeIf,ifIns),\n   MayNull_IfInstructionsCond(ifIns,var,\"null\",_).;1;)_", freqs[94],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;JumpTarget(insn,ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;1;)_", freqs[213],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueBranch;0;TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;MayNull_IfInstruction(ifIns);TrueBranch(ifIns,insn) :- \n   MayNull_IfInstruction(ifIns),\n   JumpTarget(insn,ifIns).;0;)_", freqs[214],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;IfInstructionsCond(ifIns,left,right,\"!=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"!=\"),\n   ord(left) != ord(right).;0;)_", freqs[141],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;IfInstructionsCond(ifIns,left,right,\"<=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<=\"),\n   ord(left) <= ord(right).;0;)_", freqs[143],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;IfInstructionsCond(ifIns,left,right,\"<\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"<\"),\n   ord(left) < ord(right).;0;)_", freqs[145],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;IfInstructionsCond(ifIns,left,right,\"==\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\"==\"),\n   ord(left) = ord(right).;0;)_", freqs[142],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;IfInstructionsCond(ifIns,left,right,\">=\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">=\"),\n   ord(left) >= ord(right).;0;)_", freqs[144],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;TrueIfInstructions;0;TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;IfInstructionsCond(ifIns,left,right,\">\");TrueIfInstructions(ifIns) :- \n   IfInstructionsCond(ifIns,left,right,\">\"),\n   ord(left) > ord(right).;0;)_", freqs[146],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;BasicBlockHead(ins,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;2;)_", freqs[150],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;Dominates(parent,headIns);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;3;)_", freqs[149],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;DominatesUnreachable(_,parent);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;1;)_", freqs[151],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;UnreachablePathNPEIns;0;UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;ReachableNullAtLine(_,_,_,_,_,_,ins);UnreachablePathNPEIns(ins) :- \n   ReachableNullAtLine(_,_,_,_,_,_,ins),\n   DominatesUnreachable(_,parent),\n   BasicBlockHead(ins,headIns),\n   Dominates(parent,headIns).;0;)_", freqs[152],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;AssignReturnValue_WithInvoke(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;1;)_", freqs[27],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   AssignReturnValue_WithInvoke(insn,index,var,method).;0;)_", freqs[28],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;0;)_", freqs[26],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;_AssignCast(insn,index,_,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCast(insn,index,_,var,_,method).;1;)_", freqs[25],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;0;)_", freqs[24],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;_AssignCastNull(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignCastNull(insn,index,var,_,method).;1;)_", freqs[23],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;0;)_", freqs[18],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;_AssignHeapAllocation(insn,index,_,var,method,_);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignHeapAllocation(insn,index,_,var,method,_).;1;)_", freqs[17],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;0;)_", freqs[16],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;_AssignLocal(insn,index,_,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignLocal(insn,index,_,var,method).;1;)_", freqs[15],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;0;)_", freqs[14],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;_AssignNull(insn,index,var,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _AssignNull(insn,index,var,method).;1;)_", freqs[13],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;0;)_", freqs[20],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;_LoadInstanceField(insn,index,var,_,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadInstanceField(insn,index,var,_,_,method).;1;)_", freqs[19],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;RefTypeVar(var);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;0;)_", freqs[22],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarDef;0;VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;_LoadStaticField(insn,index,var,_,method);VarDef(insn,index,var,method) :- \n   RefTypeVar(var),\n   _LoadStaticField(insn,index,var,_,method).;1;)_", freqs[21],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarMayNotPointToNull;0;VarMayNotPointToNull(var) :- \n   VarMayPointToNull(var),\n   VarPointsTo(_,alloc,_,var),\n   alloc != \"<<null pseudo heap>>\".;VarMayPointToNull(var);VarMayNotPointToNull(var) :- \n   VarMayPointToNull(var),\n   VarPointsTo(_,alloc,_,var),\n   alloc != \"<<null pseudo heap>>\".;0;)_", freqs[98],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarMayNotPointToNull;0;VarMayNotPointToNull(var) :- \n   VarMayPointToNull(var),\n   VarPointsTo(_,alloc,_,var),\n   alloc != \"<<null pseudo heap>>\".;VarPointsTo(_,alloc,_,var);VarMayNotPointToNull(var) :- \n   VarMayPointToNull(var),\n   VarPointsTo(_,alloc,_,var),\n   alloc != \"<<null pseudo heap>>\".;1;)_", freqs[97],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarMayPointToNull;0;VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   alloc = \"<<null pseudo heap>>\".;VarPointsTo(_,alloc,_,var);VarMayPointToNull(var) :- \n   VarPointsTo(_,alloc,_,var),\n   alloc = \"<<null pseudo heap>>\".;0;)_", freqs[96],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarPointsToNull;0;VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarMayNotPointToNull(var).;VarMayPointToNull(var);VarPointsToNull(var) :- \n   VarMayPointToNull(var),\n   !VarMayNotPointToNull(var).;0;)_", freqs[99],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;VarPointsToNull;0;VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;_AssignCastNull(_,_,var,_,_);VarPointsToNull(var) :- \n   _AssignCastNull(_,_,var,_,_).;0;)_", freqs[100],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_AssignReturnValue(invokeInsn,returnVar);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;1;)_", freqs[154],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_IfVar(ifInsn,_,returnVar);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;0;)_", freqs[155],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;hasNextIf;0;hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;_VirtualMethodInvocation(invokeInsn,_,sig,var,_);hasNextIf(ifInsn,invokeInsn,var) :- \n   _IfVar(ifInsn,_,returnVar),\n   _AssignReturnValue(invokeInsn,returnVar),\n   _VirtualMethodInvocation(invokeInsn,_,sig,var,_),\n   \"boolean hasNext()\" contains sig.;2;)_", freqs[153],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,to,method) :- \n   @delta_isParam(index,from,method),\n   _AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method),\n   !isParam(index,to,method).;@delta_isParam(index,from,method);isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;0;)_", freqs[84],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,to,method) :- \n   @delta_isParam(index,from,method),\n   _AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method),\n   !isParam(index,to,method).;_AssignCast( _unnamed_var1, _unnamed_var2,from,to, _unnamed_var3,method);isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignCast(_,_,from,to,_,method).;1;)_", freqs[83],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,to,method) :- \n   @delta_isParam(index,from,method),\n   _AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method),\n   !isParam(index,to,method).;@delta_isParam(index,from,method);isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;0;)_", freqs[82],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,to,method) :- \n   @delta_isParam(index,from,method),\n   _AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method),\n   !isParam(index,to,method).;_AssignLocal( _unnamed_var1, _unnamed_var2,from,to,method);isParam(index,to,method) :- \n   isParam(index,from,method),\n   _AssignLocal(_,_,from,to,method).;1;)_", freqs[81],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,var,method) :- \n   @delta_isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar),\n   !isParam(index,var,method).;@delta_isParam(index,headVar,method);isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;0;)_", freqs[86],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;@new_isParam(index,var,method) :- \n   @delta_isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar),\n   !isParam(index,var,method).;PhiNodeHeadVar(var,headVar);isParam(index,var,method) :- \n   isParam(index,headVar,method),\n   PhiNodeHeadVar(var,headVar).;1;)_", freqs[85],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@frequency-atom;isParam;0;isParam(index,var,method) :- \n   _FormalParam(index,method,var).;_FormalParam(index,method,var);isParam(index,var,method) :- \n   _FormalParam(index,method,var).;0;)_", freqs[80],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;ApplicationMethod)_", reads[12],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CanBeNullBranch)_", reads[1],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;CannotBeNullBranch)_", reads[2],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;DefineVar)_", reads[9],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;LastUse)_", reads[8],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MayNullPtr)_", reads[7],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;MethodDerefArg)_", reads[11],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;NextInsideHasNext)_", reads[10],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;Primitive)_", reads[0],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;VarMayNotPointToNull)_", reads[6],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;_FormalParam)_", reads[4],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;_ThisVar)_", reads[5],0);
	ProfileEventSingleton::instance().makeQuantityEvent(R"_(@relation-reads;isParam)_", reads[3],0);
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignReturnValue.facts"},{"name","_AssignReturnValue"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","_SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StaticMethodInvocation.facts"},{"name","_StaticMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","_VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-Type.facts"},{"name","_Var_Type"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCast.facts"},{"name","_AssignCast"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","_AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignHeapAllocation.facts"},{"name","_AssignHeapAllocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->readAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignLocal.facts"},{"name","_AssignLocal"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignNull.facts"},{"name","_AssignNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","_LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadStaticField.facts"},{"name","_LoadStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ActualParam.facts"},{"name","_ActualParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Return.facts"},{"name","_Return"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","_StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","_StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreStaticField.facts"},{"name","_StoreStaticField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/BasicBlockHead.csv"},{"name","BasicBlockHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Instruction_Next.csv"},{"name","Instruction_Next"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfNull.facts"},{"name","_IfNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/OperatorAt.facts"},{"name","_OperatorAt"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/PhiNodeHead.csv"},{"name","PhiNodeHead"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Dominates.csv"},{"name","Dominates"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/FormalParam.facts"},{"name","_FormalParam"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThisVar.facts"},{"name","_ThisVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_51_ThisVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Var-DeclaringMethod.facts"},{"name","_Var_DeclaringMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_52_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/Method_FirstInstruction.csv"},{"name","Method_FirstInstruction"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_54_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/MayPredecessorModuloThrow.csv"},{"name","MayPredecessorModuloThrow"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_55_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_58_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","_AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_62_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","_AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_63_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","_AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_64_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","_EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_65_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","_LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_66_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","_ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_67_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/IfVar.facts"},{"name","_IfVar"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_72_IfVar);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/JumpTarget.csv"},{"name","JumpTarget"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_88_JumpTarget);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_AssignReturnValue");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_1_AssignReturnValue);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_SpecialMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_StaticMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_StaticMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_VirtualMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_Var_Type");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_Var_Type);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_10_AssignCast");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_10_AssignCast);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_11_AssignCastNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_11_AssignCastNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_12_AssignHeapAllocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_12_AssignHeapAllocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_13_AssignLocal");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_13_AssignLocal);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_15_AssignNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_15_AssignNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_16_LoadInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_16_LoadInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_17_LoadStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_17_LoadStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_19_ActualParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_19_ActualParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_20_Return");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_20_Return);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_21_StoreArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_22_StoreInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_22_StoreInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_23_StoreStaticField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_23_StoreStaticField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_27_ApplicationMethod");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_27_ApplicationMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_29_BasicBlockHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_29_BasicBlockHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_30_CallGraphEdge");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_30_CallGraphEdge);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_32_IfNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_32_IfNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_35_OperatorAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_35_OperatorAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_45_Dominates");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_45_Dominates);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_46_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_46_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_51_ThisVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_51_ThisVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_52_Var_DeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_52_Var_DeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_54_Method_FirstInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_54_Method_FirstInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_55_MayPredecessorModuloThrow");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_55_MayPredecessorModuloThrow);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_57_InstructionLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_57_InstructionLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_58_VarPointsTo");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_58_VarPointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_62_AssignBinop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_62_AssignBinop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_63_AssignOperFrom");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_63_AssignOperFrom);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_64_AssignUnop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_64_AssignUnop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_65_EnterMonitor");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_65_EnterMonitor);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_66_LoadArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_66_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_67_ThrowNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_67_ThrowNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_72_IfVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_72_IfVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_88_JumpTarget");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_88_JumpTarget);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_5_AssignReturnValue_WithInvoke");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignReturnValue_WithInvoke);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_6_IterNextInsn");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_IterNextInsn);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_9_RefTypeVar");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_9_RefTypeVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_14_DefineVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_DefineVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_18_VarDef");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_18_VarDef);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_24_AllUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_24_AllUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_25_FirstUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_25_FirstUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_26_LastUse");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_26_LastUse);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_28_AssignMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_28_AssignMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_31_Instruction_Next");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_31_Instruction_Next);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_33_MayNull_IfInstruction");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_33_MayNull_IfInstruction);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_34_FalseBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_34_FalseBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_36_MayNull_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_36_MayNull_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_37_PhiNodeHead");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_37_PhiNodeHead);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_38_PhiNodeHeadVar");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_38_PhiNodeHeadVar);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_39_CanBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_39_CanBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_42_CannotBeNullBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_42_CannotBeNullBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_47_Instruction_FormalParam");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 0}), symTable, ioDirectives, 0)->writeAll(*rel_47_Instruction_FormalParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_48_isParam");
IOSystem::getInstance().getWriter(SymbolMask({0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_48_isParam);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_53_Instruction_VarDeclaringMethod");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_53_Instruction_VarDeclaringMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_61_VarPointsToNull");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_61_VarPointsToNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_68_NullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_68_NullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_69_ReachableNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_69_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_70_PathSensitiveNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_70_PathSensitiveNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_73_IfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_73_IfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_74_IfInstructionsCond");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_74_IfInstructionsCond);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_75_TrueIfInstructions");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_75_TrueIfInstructions);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_76_DominatesUnreachable");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_76_DominatesUnreachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_77_UnreachablePathNPEIns");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_77_UnreachablePathNPEIns);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_78_hasNextIf");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_78_hasNextIf);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_79_NextInsideHasNext");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_79_NextInsideHasNext);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_80_MayNullPtr");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_80_MayNullPtr);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_83_MethodDerefArg");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_83_MethodDerefArg);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_86_JDKFunctionSummary");
IOSystem::getInstance().getWriter(SymbolMask({0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_86_JDKFunctionSummary);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_87_NPEWithMayNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_87_NPEWithMayNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_89_TrueBranch");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_89_TrueBranch);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_90_ReachableNullAt");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_90_ReachableNullAt);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_main_no_uninit_must(){return new Sf_main_no_uninit_must;}
SymbolTable *getST_main_no_uninit_must(SouffleProgram *p){return &reinterpret_cast<Sf_main_no_uninit_must*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_main_no_uninit_must: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_main_no_uninit_must();
};
public:
factory_Sf_main_no_uninit_must() : ProgramFactory("main_no_uninit_must"){}
};
static factory_Sf_main_no_uninit_must __factory_Sf_main_no_uninit_must_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(main_no_uninit_must.dl)",
R"(.)",
R"(.)",
true,
R"(profile.txt)",
4,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf_main_no_uninit_must obj(opt.getProfileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("", opt.getSourceFileName());
souffle::ProfileEventSingleton::instance().makeConfigRecord("fact-dir", opt.getInputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("jobs", std::to_string(opt.getNumJobs()));
souffle::ProfileEventSingleton::instance().makeConfigRecord("output-dir", opt.getOutputFileDir());
souffle::ProfileEventSingleton::instance().makeConfigRecord("version", "1.4.0-737-gc0b3cde6");
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
