import sqlite3, csv
import os
import hashlib
import uuid
import json
from pprint import pprint

#hash a string
def hash(w):
	return hashlib.sha1(w.encode('utf-8')).hexdigest()[:10]

#dict factory - transform tuple to dictionary in fetchall()
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

#print coloured text in terminal
def coloured(s, colour):
	return "\033[" + str(colour) + "m" + s + "\033[0m"

#print a table given sql cursor and results
def print_table(cursor, results):
	if len(results) == 0: return

	widths = []
	columns = []
	tavnit = '|'
	separator = '+'

	for cd in cursor.description:
		max_col_length = max(list(map(lambda x: len(str(x[cd[0]])), results)))
		widths.append(max(max_col_length, len(cd[0])))
		columns.append(cd[0])
	
	for w in widths:
		tavnit += " %-"+"%ss |" % (w,)
		separator += '-'*w + '--+'
	
	print(separator)
	print(tavnit % tuple(columns))
	print(separator)
	for row in results:
		row = tuple([row[c] for c in columns])
		print(tavnit % row)
	print(separator)

# Database command line tool - can be used to annotate/print results
class DatabaseCommandLine:
	def __init__(self, db):
		self.db = db
		self.main()

	def main(self):
		self.help()
		while True:
			inpt = input("> ")
			inpt = inpt.split()
			command = inpt[0]
			args = inpt[1:]

			if command == "print" and len(args) > 0:
				if args[0] == "table":
					self.db.print_exception_table()
				if args[0] == "last":
					self.db.print_last_analysis()
				if args[0] == "history":
					self.db.print_analysis_history()
				if args[0] == "run":
					self.db.print_analysis(args[1])
				if args[0] == "compare":
					self.db.print_comparisions(args[1], args[2])
			elif command == "annotate" and len(args) == 1:
				if args[0] == "all":
					self.db.annotate_all()
				elif args[0] == "nulls":
					self.db.annotate()
				elif args[0] == "help":
					self.annotate_help()
				else:
					self.db.change_annotation(args[0])
			elif command == "help":
				self.help()
			elif command == "exit" or command == "q":
				break;
			else:
				print("invalid command.")

	def help(self):
		print("All Commands")
		print("print table \t\t print all NullPointerExceptions found by the bugchecker overtime, including its annotations")
		print("print history \t\t print all the analysis runs history stored in the database together with the timestamp")
		print("print last \t\t print all NullPointerExceptions found by the bugchecker during the last analysis, including its annotations")
		print("print run [run_id] \t\t print all NullPointerExceptions found by the bugchecker during the given analysis id, including its annotations")
		print("print compare [run_id_1] [run_id_2] \t\t ")
		print("annotate \t\t annotate the NullPointerExceptions found by the bug checker valid options:")
		print("\t\t\t\t all: annotate all exceptions")
		print("\t\t\t\t nulls: annotate all exceptions that are yet to be annotate")
		print("\t\t\t\t [exception_id]: annotate a specific exception")
		print("\t\t\t\t help: print help information on how to annotate")
		print("help \t\t\t print this again")
		print("exit \t\t\t exit the annotation program")

	def annotate_help(self):
		print("Annotation Help")
		print("Each exceptions can be marked as a True Positive (TP) or a False Positive (FP)")
		print("\t TP: when the exception found is expected to cause a NullPointerException upon the execution of the source program")
		print("\t FP: when the exception found is not expected to cause a NullPointerException upon the execution of the source program")

# Object that contains a list of exceptions
class ExceptionList:
	def __init__(self):
		self.exception_list = []

	def get_exceptions(self):
		return self.exception_list

	def clear_exceptions(self):
		self.exception_list = []

	# Read expected results from JSON file and populate it into the list
	def read_from_file(self, file, out_dir):
		with open(file) as f:
			data = json.load(f)
		keyname = os.path.basename(os.path.normpath(out_dir))
		if keyname not in data: return
		data = data[keyname]
		for test, (f, exceptions) in enumerate(data.items()):
			for ex in exceptions:
				eid = hash(f+str(ex['line'])+uuid.uuid4().hex[:10])
				self.exception_list.append( (f, ex['line'], ex['reason']) )

# Database - handles all the communication to database (results.db)
class Database:
	def __init__(self, out_dir, db_file):
		self.out_dir = out_dir
		self.conn = sqlite3.connect(os.path.join(out_dir, db_file))
		self.conn.row_factory = dict_factory

	# Create the database
	def setup_db(self):
		cur = self.conn.cursor()

		# Create all the necessary tables
		cur.execute("""CREATE TABLE NullPointerException (
						method TEXT, 
						inum INTEGER, 
						file TEXT NOT NULL, 
						linenumber INTEGER NOT NULL, 
						type TEXT,
						var TEXT,
						exception_id INT unsigned PRIMARY KEY,
						class CHAR(2)
					)""")

		cur.execute("""CREATE TABLE ExpectedResult (
						file TEXT NOT NULL,
						linenumber INTEGER NOT NULL,
						reason TEXT,
						PRIMARY KEY (file, linenumber, reason)
					)""")

		cur.execute("""CREATE TABLE IF NOT EXISTS Runs (
						run_id INT unsigned,
						timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
					)""")

		cur.execute("""CREATE TABLE Run (
						exception_id INT unsigned,
						timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
						run_id INT unsigned,
						FOREIGN KEY(exception_id) REFERENCES NullPointerException(exception_id),
						FOREIGN KEY(run_id) REFERENCES Runs(run_id)
					)""")

		

		self.conn.commit()

	# Read the NPE results from digger and add them to the database
	def csv_to_sqlite(self):		
		csv_file = os.path.join(self.out_dir, "NullPointerExceptions.csv")
		cur = self.conn.cursor()
		cur.execute("SELECT name FROM sqlite_master WHERE name='NullPointerException'")

		if not cur.fetchone():
			self.setup_db()

		run_id = uuid.uuid4().hex[:10]

		# Read from NullPointerExceptions.csv
		with open(csv_file, 'rt') as fin:
			dr = csv.DictReader(fin, delimiter="\t", fieldnames=('method', 'inum', 'file', 'linenumber', 'type', 'var'))
			
			to_db = []
			eids = []
			run_list = [] # for compare_fps
			
			for i in dr:
				eid = hash(i['method'] + i['inum'].lower())
				to_db.append( (i['method'], int(i['inum']), i['file'], int(i['linenumber']),i['type'], i['var'], eid) )
				eids.append( (eid, run_id) )
				run_list.append(eid)
		
		# Populate the database
		self.run_list = run_list
		cur.executemany("""INSERT OR IGNORE INTO NullPointerException (method, inum, file, linenumber, type, var, exception_id) 
						VALUES (?, ?, ?, ?, ?, ?, ?)""", to_db)

		cur.execute("""INSERT INTO Runs (run_id) VALUES (?)""", (run_id, ))

		cur.executemany("""INSERT INTO Run (exception_id, run_id)
							VALUES (?, ?)""", eids)

		cur.execute("""DELETE FROM ExpectedResult""")

		self.conn.commit()
		
	# Run multiple queries to annotate/find false positive/true positive changes
	def test(self, anno):
		exception_list = ExceptionList()

		exception_list.read_from_file(os.path.abspath(os.path.join(self.out_dir, "..", "exceptions.json")), self.out_dir)
		self.add_expected_results(exception_list)
		self.get_expected()
		
		if anno:
			self.annotate()

		self.compare_fps(self.run_list)
		self.compare_tps(self.run_list)
		self.compare_uds(self.run_list)

	# Get all false positive exceptions
	def get_fp(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT exception_id FROM NullPointerException WHERE class = 'FP'""")
		fps = cur.fetchall()

		return fps

	# Get all true positive exceptions
	def get_tp(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT exception_id FROM NullPointerException WHERE class = 'TP'""")
		tps = cur.fetchall()

		return tps

	# Get all previous false positive exceptions (previously FP but undetected in the last analysis run)
	def get_ud(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT exception_id FROM NullPointerException WHERE class = 'UD'""")
		uds = cur.fetchall()

		return uds
	# Get the expected results
	def get_expected(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT file, linenumber, reason FROM ExpectedResult""")
		expected = cur.fetchall()
		
		return expected

	# Find False Positive improvements
	def compare_fps(self, run_list):
		undetected = []
		fp_list = self.get_fp()

		for fp in fp_list:
			fp = fp['exception_id']
			if fp not in run_list:
				undetected.append(fp)

		for ex in undetected:
			print("{}: {} was not detected during the last run!".format(coloured("False Positive Improvement", 32), ex))
			cur = self.conn.cursor()
			cur.execute("""UPDATE NullPointerException SET class = 'UD' WHERE exception_id = ?""", (ex, ))
			self.conn.commit()

	# Find False Positive Reoccurence
	def compare_uds(self, run_list):
		ud_list = self.get_ud()

		for ud in ud_list:
			ud = ud['exception_id']
			if ud in run_list:
				print("{}: {} was detected during the last run!".format(coloured("False Positive Reoccurence", 31), ud))
				cur = self.conn.cursor()
				cur.execute("""UPDATE NullPointerException SET class = 'FP' WHERE exception_id = ?""", (ud, ))
				self.conn.commit()

	# Match ExpectedResult as True Positive
	def mark_tp(self, linenumber, file, count):
		cur = self.conn.cursor()
		cur.execute(""" SELECT file, linenumber
						FROM NullPointerException
						WHERE file = ? AND linenumber = ?""", (file, linenumber))
		exceptions = cur.fetchall()
		
		if count == len(exceptions):
			cur.execute(""" UPDATE NullPointerException
							SET class = 'TP'
							WHERE file = ? AND linenumber = ? """, (file, linenumber))	
		else:
			print("occurence mismatched, please annotate manually")
			cur.execute(""" UPDATE NullPointerException
							SET class = NULL
							WHERE file = ? AND linenumber = ? """, (file, linenumber))
		self.conn.commit()

	# Find True Positive deterioration
	def compare_tps(self, run_list):
		undetected = []

		tp_list = self.get_tp()

		for tp in tp_list:
			tp = tp['exception_id']
			if tp not in run_list:
				undetected.append(tp)

		for ex in undetected:
			print("{}: {} was not detected during the last run!".format(coloured("True Positive Warning", 31), ex))

	# Compare Actual and expected results
	def compare_expected(self):
		cur = self.conn.cursor()

		cur.execute(""" SELECT run_id FROM Runs ORDER BY timestamp DESC LIMIT 1""")

		last_run_id = cur.fetchone()["run_id"]

		# auto-annotate expected/actual results
		cur.execute(""" SELECT exception_id
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ? """, (last_run_id, ))

		exceptions = cur.fetchall()

		for i in exceptions:
			cur.execute(""" UPDATE NullPointerException
							SET class = 'FP'
							WHERE exception_id = ? """, (i['exception_id'], ))
		self.conn.commit()

		cur.execute(""" SELECT file, linenumber, COUNT(*) AS "count"
						FROM ExpectedResult
						GROUP BY linenumber """)

		mark_expected = cur.fetchall()
		for i in mark_expected:
			self.mark_tp(i['linenumber'], i['file'], i['count'])

		# actual - expected
		cur.execute("""	SELECT file, linenumber, COUNT(*) AS "occur(s)"
						FROM Run JOIN NullPointerException USING (exception_id)
						WHERE run_id = ?
						GROUP BY linenumber
						EXCEPT
						SELECT file, linenumber, COUNT(*) AS "occur(s)"
						FROM ExpectedResult
						GROUP BY linenumber
					""", (last_run_id, ))
		actual_res = cur.fetchall()
		if actual_res != []: 
			print("Actual_res") 
			print_table(cur, actual_res)

		cur = self.conn.cursor()
		
		# expected - actual
		cur.execute("""	
						SELECT file, linenumber, COUNT(*) AS "occur(s)"
						FROM ExpectedResult
						GROUP BY linenumber
						EXCEPT
						SELECT file, linenumber, COUNT(*) AS "occur(s)"
						FROM Run JOIN NullPointerException USING (exception_id)
						WHERE run_id = ?
						GROUP BY linenumber
					""", (last_run_id, ))
		expected_res = cur.fetchall()
		if expected_res != []: 
			print("Expected_res")
			print_table(cur, expected_res)

		return (expected_res, actual_res)

	# Add expected results to database
	def add_expected_results(self, exception_obj):
		exception_list = exception_obj.get_exceptions()
		cur = self.conn.cursor()
		cur.executemany("""INSERT OR IGNORE INTO ExpectedResult (file, linenumber, reason) VALUES (?, ?, ?)""", exception_list)
		self.conn.commit()

	# Add actual exceptions to the database
	def add_exceptions(self, exception_obj):
		exception_list = exception_obj.get_exceptions()
		cur = self.conn.cursor()
		cur.executemany("""INSERT INTO NullPointerException (file, linenumber, exception_id, type, class) VALUES (?,?,?,?,?)""", exception_list)
		self.conn.commit()		

	# Annotate all Exceptions that has yet to be annotated
	def annotate(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT * FROM NullPointerException WHERE class IS NULL""")
		npe = cur.fetchall()
		update_db = []
		for i in npe:
			formatted_npe = "{}: {}\n{}: {}\n{}: {}\n{}: {}\n{}: {}\n".format(
				coloured("File", 0), coloured(i['file'], 31), 
				coloured("Method", 0), coloured(i['method'], 32), 
				coloured("Line Number (Source)", 0), coloured(str(i['linenumber']), 33), 
				coloured("Instruction Number (Jimple)", 0), coloured(str(i['inum']), 35), 
				coloured("Exception Type", 0), coloured(i['type'], 36)
				)
			print(formatted_npe)

			an = None
			skip_all = False
			while an not in ["TP", "FP"]:
				an = input("Annotate (TP/FP/skip all)? ")
				if not an: an = None; break;
				if an == "skip all": an = None; skip_all = True; break;

			if an:
				update_db.append((an, i['exception_id']))

			if skip_all:
				break
		cur.executemany("""UPDATE NullPointerException SET class = ? WHERE exception_id = ?""", update_db)	
		self.conn.commit()

	# Annotate all exceptions
	def annotate_all(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT * FROM NullPointerException""")
		npe = cur.fetchall()
		update_db = []
		for i in npe:
			formatted_npe = "{}: {}\n{}: {}\n{}: {}\n{}: {}\n{}: {}\n".format(
				coloured("File", 0), coloured(i['file'], 31), 
				coloured("Method", 0), coloured(i['method'], 32), 
				coloured("Line Number (Source)", 0), coloured(str(i['linenumber']), 33), 
				coloured("Instruction Number (Jimple)", 0), coloured(str(i['inum']), 35), 
				coloured("Exception Type", 0), coloured(i['type'], 36)
				)
			print(formatted_npe)

			an = None
			skip_all = False
			while an not in ["TP", "FP"]:
				an = input("Annotate (TP/FP/skip all)? ")
				if not an: an = None; break;
				if an == "skip all": an = None; skip_all = True; break;

			if an:
				update_db.append((an, i['exception_id']))

			if skip_all:
				break
		cur.executemany("""UPDATE NullPointerException SET class = ? WHERE exception_id = ?""", update_db)	
		self.conn.commit()


	# Print exception table
	def print_exception_table(self):
		cur = self.conn.cursor()
		cur.execute("""SELECT inum, file, linenumber, type, var, exception_id,class FROM NullPointerException ORDER BY file, linenumber""")
		npes = cur.fetchall()
		print_table(cur, npes)
		print("Total defects: {}".format(len(npes)))


	def print_analysis_history(self):
		cur = self.conn.cursor()
		cur.execute(""" SELECT * FROM Runs """)
		history = cur.fetchall()
		print_table(cur, history)

	
	def print_analysis(self, run_id):
		cur = self.conn.cursor()
		# auto-annotate expected/actual results
		print(run_id)
		cur.execute(""" SELECT inum, file, linenumber, type, exception_id, var, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ? ORDER BY file, linenumber""", (run_id, ))

		npes = cur.fetchall()
		print_table(cur, npes)
		print("Total defects: {}".format(len(npes)))

	def compare_analysis(self, a1, a2):
		cur = self.conn.cursor()
		cur.execute("""	SELECT inum, file, linenumber, type, exception_id, var, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ?
						EXCEPT
						SELECT inum, file, linenumber, type, exception_id, var, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ? 
						ORDER BY file, linenumber
						""", (a1, a2))
		a1_except_a2 = cur.fetchall()

		cur.execute("""	SELECT inum, file, linenumber, type, exception_id, var, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ?
						EXCEPT
						SELECT inum, file, linenumber, type, exception_id, var, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ? ORDER BY file, linenumber
						""", (a2, a1))
		a2_except_a1 = cur.fetchall()

		return (cur, a1_except_a2, a2_except_a1)

	def print_comparisions(self, a1, a2):
		cur, a1_a2, a2_a1 = self.compare_analysis(a1, a2)

		print_table(cur, a1_a2)
		print("Total defects: {}".format(len(a1_a2)))
		print_table(cur, a2_a1)
		print("Total defects: {}".format(len(a2_a1)))


	# Print last analysis results
	def print_last_analysis(self):
		cur = self.conn.cursor()
		cur.execute(""" SELECT run_id FROM Runs ORDER BY timestamp DESC LIMIT 1""")

		last_run_id = cur.fetchone()["run_id"]
		# auto-annotate expected/actual results
		cur.execute(""" SELECT inum, file, linenumber, type, var, exception_id, class
						FROM NullPointerException JOIN Run USING (exception_id)
						WHERE run_id = ? ORDER BY file, linenumber""", (last_run_id, ))

		npes = cur.fetchall()
		print_table(cur, npes)
		print("Total defects: {}".format(len(npes)))

	# Change a single annotation based on given exception id
	def change_annotation(self, eid):
		cur = self.conn.cursor()
		cur.execute("""SELECT * FROM NullPointerException WHERE exception_id = ? """, (eid, ))
		npe = cur.fetchall()
		if len(npe) == 0: print("invalid exception_id."); return;
		update_db = []
		for i in npe:
			formatted_npe = "{}: {}\n{}: {}\n{}: {}\n{}: {}\n{}: {}\n".format(
				coloured("File", 0), coloured(i['file'], 31), 
				coloured("Method", 0), coloured(i['method'], 32), 
				coloured("Line Number (Source)", 0), coloured(str(i['linenumber']), 33), 
				coloured("Instruction Number (Jimple)", 0), coloured(str(i['inum']), 35), 
				coloured("Exception Type", 0), coloured(i['type'], 36)
				)
			print(formatted_npe)

			an = None
			while an not in ["TP", "FP", "MK", "UD"]:
				an = input("Annotate (TP/FP/mark(MK)/skip)? ")
				if not an: an = None; break;

			if an:
				update_db.append((an, i['exception_id']))

		cur.executemany("""UPDATE NullPointerException SET class = ? WHERE exception_id = ?""", update_db)		
		self.conn.commit()	 

	def close(self):
		self.conn.close()

