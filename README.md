## [Changelog](https://bitbucket.org/Rex9611/digger-bugchecker/wiki/ChangeLog) ##

![pigger_small.jpg](https://bitbucket.org/repo/p4GdrBA/images/3311391370-pigger_small.jpg)

# Digger-BugChecker  
Static analysis program finding Null Pointer Exceptions in Java Program using [Soufflé-syntax](https://souffle-lang.github.io) datalog 

# How to get Digger
Use git clone

```$ git clone https://bitbucket.org/Rex9611/digger-bugchecker.git --recursive```

# Directory Structure
```lang-none
.
├── Documentation -- Project Docs
├── bugchecker-benchmarks -- Submodule with benchmark facts
│   └── small_examples
├── digger -- Main program contains script and souffle logic file
│   ├── logic
│   └── out
├── doop -- Modified doop framework submodule
│   ├── bin
│   ├── build
│   ├── cache
│   ├── docs
│   ├── gradle
│   ├── logic
│   ├── out
│   ├── results
│   ├── souffle-logic
│   ├── souffle-scripts
│   └── src
└── test -- Directory for unit tests
    ├── access_null_array
    ├── instance_method_invoke
    ├── load_store_fields
    ├── miscellaneous
    ├── single_test
    └── trickery
```
# Execution Flow
![Pipeline](Documentation/pipeline.png)
# Build Digger
- Dependencies: *pyhton3, [Soufflé](https://souffle-lang.github.io/docs/build/)*
- ```$ cd path/to/this/repository```
- ```$ export DIGGER_HOME=$(pwd)```
- ```$ export PATH=$PATH:$DIGGER_HOME/src``` 

# Run Digger  

```lang-none
usage: diggerMax [-h] [-n] [-d DOOP_ANALYSIS] [-s SINGLE] [-j] [-e]
                 [-a ANALYSIS] [-i] [-f FACT_DIR | -F FACT_DIRS]

optional arguments:
  -h, --help            show this help message and exit
  -n, --annotate        Start the Annotation tool to annotate on the current
                        output folder
  -d DOOP_ANALYSIS, --doop-analysis DOOP_ANALYSIS
                        choose from all analysis options from doop('context-
                        insensitive' by default)
  -s SINGLE, --single SINGLE
                        run analysis on single java file
  -j, --jdk             run points-to analysis with jdk
  -e, --enable-annotate
                        Annotate True Positive / False Positive after the
                        analysis
  -a ANALYSIS, --analysis ANALYSIS
                        Choose which type of analysis should be used for the
                        bug checker. options: must-uninit, may-uninit, may,
                        must
  -i, --interpreter     run souffle with the interpreter. default: false
  -f FACT_DIR, --fact-dir FACT_DIR
                        Running without Doop. Directory contains 'facts'
                        folder
  -F FACT_DIRS, --fact-dirs FACT_DIRS
                        Running without Doop. Directoriy contains a list of
                        folders with 'facts'
  
```
Find a detailed digger tutorial [here](https://bitbucket.org/Rex9611/digger-bugchecker/wiki/Home). 

# Annotation
Start the annotating your results by 

- ```$ cd [path/to/this/repository]/digger/out/[your analysed program]```
- ```$ diggerMax -n```

## Why Annotation
Annotation is a tool that let us mark exceptions found by Digger as either True Positive or False Positive.
These annotations would then be compared against the actual analysis output and record the any improvements/bugs found.

## Annotation Commands
```lang-none
All Commands
printtable               print all NullPointerExceptions found by the bugchecker, including its annotations
annotate                 annotate the NullPointerExceptions found by the bug checker valid options:
                                 all: annotate all exceptions
                                 nulls: annotate all exceptions that are yet to be annotate
                                 [exception_id]: annotate a specific exception
                                 help: print help information on how to annotate
help                     print this again
exit                     exit the annotation program
```
Find a detailed Annotation tutorial [here](https://bitbucket.org/Rex9611/digger-bugchecker/wiki/Home#markdown-header-annotation).
