public class LoadStore {

    String a;

    public static void main(String[] args) {
      // LoadStore blah = new LoadStore();
      // blah.a = new String();
      // LoadStore blah_2 = blah;
      // blah_2.a.toString();
      LoadStore blah = new LoadStore();
      LoadStore blah2 = blah;
      blah.a = new String();
      blah2.a.toString();
      blah.a = null;
      blah.a.toString();
    }
}