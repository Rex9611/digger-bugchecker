public class NestedIfs {

    public static void createObject(String a, String b) {
      a.toString();
      return;
    }


    public static void main(String[] args) {
      String a = null;
      String b = new String();
      if(b != null){
        if(a != null){
          createObject(b, b);
        }else{
          createObject(a, b);
        }
      }else{
      	createObject(b, b);
      }
    }
}

