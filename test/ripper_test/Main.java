class NPE{
	public String returnNull(String param){
		String res = null;
		if(param != null){
			res = param;
		}
		return res;
	}
}
public class Main {
	private static void NPECall(NPE npe, String str){
		String result = npe.returnNull(str);
		result.toString();
	}

    public static void main(String[] args) {
      String str = new String();
      NPE npe = new NPE();
      NPECall(npe, str);
    }
}
