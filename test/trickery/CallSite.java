
public class CallSite {

  private int awk() { return 1; }

  public static CallSite id(CallSite a) { return a; }

  public static void fun1() {
    CallSite a1 = new CallSite();
    CallSite b1 = id(a1);
    b1.awk();
  }

  public static void fun2() {
    CallSite a2 = null;
    CallSite b2 = id(a2);
    b2.awk();
  }

  public static void main(String[] args) {
    fun1();
    fun2();
  }
}
