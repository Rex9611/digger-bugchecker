
public class PassNullObject {

    public static PassNullObject createObject() {
      return null;
    }

    public static void CoolMethod(PassNullObject pie) {
      System.out.println(pie.toString());
    }

    public static void main(String[] args) {

      PassNullObject t = createObject();
      CoolMethod(t);

    }
}
