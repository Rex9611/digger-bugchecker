// Calling the instance method of a null object
public class BinopNull {
	public static int sum(int a, int b) {
		return a + b;
	}
	public static void main(String args[]) {
		Integer a = new Integer(0);
		Integer b = null;
		sum(a,b);
	}

}
